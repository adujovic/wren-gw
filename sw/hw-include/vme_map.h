#ifndef __CHEBY__VME_MAP__H__
#define __CHEBY__VME_MAP__H__

#include "host_map.h"
#define VME_MAP_SIZE 131072 /* 0x20000 = 128KB */

/* None */
#define VME_MAP_HOST 0x0UL
#define ADDR_MASK_VME_MAP_HOST 0x18000UL
#define VME_MAP_HOST_SIZE 32768 /* 0x8000 = 32KB */

/* None */
#define VME_MAP_VME 0x10000UL
#define VME_MAP_VME_SIZE 65536 /* 0x10000 = 64KB */

/* None */
#define VME_MAP_VME_IRQ 0x10000UL
#define VME_MAP_VME_IRQ_LEVEL_MASK 0x7UL
#define VME_MAP_VME_IRQ_LEVEL_SHIFT 0
#define VME_MAP_VME_IRQ_VECTOR_MASK 0xff00UL
#define VME_MAP_VME_IRQ_VECTOR_SHIFT 8

/* None */
#define VME_MAP_VME_GA 0x10004UL
#define VME_MAP_VME_GA_VME_GA_MASK 0x3fUL
#define VME_MAP_VME_GA_VME_GA_SHIFT 0
#define VME_MAP_VME_GA_NOGA_MASK 0x3f00UL
#define VME_MAP_VME_GA_NOGA_SHIFT 8
#define VME_MAP_VME_GA_GA_MASK 0x3f0000UL
#define VME_MAP_VME_GA_GA_SHIFT 16
#define VME_MAP_VME_GA_CST_MASK 0xff000000UL
#define VME_MAP_VME_GA_CST_SHIFT 24

/* base address of the window area */
#define VME_MAP_VME_BASE_ADDR 0x10020UL
#define VME_MAP_VME_BASE_ADDR_SIZE 4 /* 0x4 */

/* None */
#define VME_MAP_VME_BASE_ADDR_ADDR 0x0UL

/* windowed access to the board memory */
#define VME_MAP_VME_WINDOWS 0x18000UL
#define ADDR_MASK_VME_MAP_VME_WINDOWS 0x8000UL
#define VME_MAP_VME_WINDOWS_SIZE 32768 /* 0x8000 = 32KB */

#ifndef __ASSEMBLER__
struct vme_map {
  /* [0x0]: SUBMAP (no description) */
  struct host_map host;

  /* padding to: 65536 Bytes */
  uint32_t __padding_0[10240];

  /* [0x10000]: BLOCK (no description) */
  struct vme {
    /* [0x0]: REG (rw) (no description) */
    uint32_t irq;

    /* [0x4]: REG (ro) (no description) */
    uint32_t ga;

    /* padding to: 32 Bytes */
    uint32_t __padding_0[6];

    /* [0x20]: MEMORY base address of the window area */
    struct vme_base_addr {
      /* [0x0]: REG (rw) (no description) */
      uint32_t addr;
    } base_addr[8];

    /* padding to: 32768 Bytes */
    uint32_t __padding_1[8176];

    /* [0x8000]: SUBMAP windowed access to the board memory */
    uint32_t windows[8192];
  } vme;
};
#endif /* !__ASSEMBLER__*/

#endif /* __CHEBY__VME_MAP__H__ */
