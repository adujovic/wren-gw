/* Mailbox commands */
#ifndef __MB_CMD__H__
#define __MB_CMD__H__

#include "wren/wren-common.h"

#define CMD_REPLY       0x80000000
#define CMD_ERROR       0x40000000

#define CMD_PING        0x00000000
#define CMD_WRITE       0x00000002
#define CMD_READ        0x00000003
#define CMD_EXEC        0x00000004
#define CMD_SEND        0x00000005

#define CMD_HW_GET_CONFIG 0x0 /* TODO: model, version, nbr inputs, ... */

#define CMD_CLI         0x0000000f

#define CMD_TX_GET_CONFIG  0x00000006
#define CMD_TX_SET_SOURCE  0x00000007
#define CMD_TX_SEND_PACKET 0x00000008
#define CMD_TX_GET_SOURCE  0x00000009

#define CMD_RX_GET_CONFIG  0x00000010
#define CMD_RX_SET_SOURCE  0x00000011
#define CMD_RX_GET_SOURCE  0x00000012 /* TODO: Source + stats */

#define CMD_RX_SET_COND    0x00000013
#define CMD_RX_DEL_COND    0x00000014
#define CMD_RX_GET_COND    0x00000015

#define CMD_RX_SET_ACTION  0x00000016
#define CMD_RX_DEL_ACTION  0x00000017
#define CMD_RX_GET_ACTION  0x00000018
#define CMD_RX_MOD_ACTION  0x00000019
#define CMD_RX_IMM_ACTION  0x0000001a

#define CMD_RX_COMPARATOR_STATUS  0x00000020
#define CMD_RX_GET_COMPARATOR     0x00000021
#define CMD_RX_SET_COMPARATOR     0x00000022
#define CMD_RX_ABORT_COMPARATOR   0x00000023

#define CMD_RX_PULSERS_STATUS     0x00000024
#define CMD_RX_ABORT_PULSERS      0x00000025
#define CMD_RX_GET_PULSER         0x00000026

#define CMD_RX_GET_OUT_CFG        0x00000027
#define CMD_RX_SET_OUT_CFG        0x00000028

#define CMD_RX_GET_LEMO_OE        0x00000029
#define CMD_RX_SET_LEMO_OE        0x0000002a
#define CMD_RX_GET_LEMO_TERM      0x0000002b
#define CMD_RX_SET_LEMO_TERM      0x0000002c
#define CMD_RX_SUBSCRIBE          0x0000002d
#define CMD_RX_UNSUBSCRIBE        0x0000002e
#define CMD_RX_GET_SUBSCRIBED     0x0000002f

#define CMD_RX_LOG_INDEX          0x00000030
#define CMD_RX_LOG_READ           0x00000031

#define CMD_RX_RESET              0x00000032

#define SW_VERSION 0xdd000001

/* Async commands */
#define CMD_ASYNC_CONTEXT  0x01
#define CMD_ASYNC_EVENT    0x02
#define CMD_ASYNC_CONFIG   0x03
#define CMD_ASYNC_PULSE    0x04
#define CMD_ASYNC_UNLOAD   0x05

/* The low-level structure used by read(2)/write(2) */
struct wren_mb_metadata {
    /* The commands, see CMD_* above. */
    uint32_t cmd;

    /* Length in words. */
    uint32_t len;
};

/* For any command with 1 argument */
struct wren_mb_arg1 {
    uint32_t arg1;
};

struct wren_mb_arg2 {
    uint32_t arg1;
    uint32_t arg2;
};

/* For any reply with 1 or 2 arguments */
struct wren_mb_arg_reply {
    uint32_t arg1;
    uint32_t arg2;
};

struct wren_mb_tx_get_config_reply {
    uint32_t sw_version;
    uint32_t max_sources;
    uint32_t nbr_sources;
};

struct wren_mb_tx_set_source {
    uint32_t source_idx;
    struct wren_protocol proto;

    /* Maximum delay before the next packet */
    uint32_t max_delay_us;
};

struct wren_mb_tx_send_frame {
    uint32_t src_id;
    uint32_t data[1];
};

struct wren_mb_rx_get_config_reply {
    uint32_t sw_version;
    uint32_t max_sources;
    uint32_t max_conds;
    uint32_t max_actions;
};

struct wren_mb_pulser_config {
    uint8_t start;
    uint8_t stop;
    uint8_t clock;
    uint8_t pulser_idx;
    uint8_t repeat;
    uint8_t immediat;
    uint32_t width;
    uint32_t period;
    uint32_t npulses;
    uint32_t idelay;

    /* Due time offset */
    int32_t load_off_sec;
    int32_t load_off_nsec;
};

struct wren_mb_comparator {
    /* Absolute timestamp */
    uint32_t sec;
    uint32_t nsec;
    /* Pulser config */
    struct wren_mb_pulser_config conf;
};

struct wren_mb_rx_set_comparator {
    uint32_t comp_idx;
    struct wren_mb_comparator cfg;
};

struct wren_mb_rx_get_pulser_reply {
    uint32_t state;
    uint32_t cur_comp;
    uint32_t loaded_comp;
    uint32_t ts;
    uint32_t pulses;
};

struct wren_mb_out_cfg {
    uint32_t out_idx;
    uint8_t mask;
    uint8_t inv_in;
    uint8_t inv_out;

    uint8_t oe;
    uint8_t term; /* 50Ohm terminaison */
};

struct wren_mb_rx_subscribe {
    uint32_t src_idx;
    uint32_t ev_id;
};

struct wren_mb_get_subscribed_reply {
    uint32_t map[32];
};

struct wren_mb_rx_set_action {
    uint32_t act_idx;
    uint32_t cond_idx;

    struct wren_mb_pulser_config conf;
};

struct wren_mb_rx_mod_action {
    uint32_t act_idx;

    struct wren_mb_pulser_config conf;
};

struct wren_mb_rx_imm_action {
    uint32_t act_idx;
    struct wren_mb_comparator cmp;
};

struct wren_mb_rx_get_action_reply {
    uint32_t cond_idx;

    /* Due time offset */
    int32_t sec_off;
    int32_t nsec_off;

    struct wren_mb_pulser_config trig;
};

struct wren_mb_rx_source {
    /* 0: events.
       xxx: RF frames */
    uint32_t dest;

    /* Discard N/N+1 frames. */
    uint32_t subsample;

    struct wren_protocol proto;
};

struct wren_mb_rx_set_source {
    /* Source index (from 0 to max_sources - 1) */
    uint32_t idx;

    struct wren_mb_rx_source cfg;
};

struct wren_mb_rx_get_log {
    /* Log index (pulser or input), from 0 to 63 */
    uint32_t log_idx;
    /* Last entry, as returned by log_index command */
    uint32_t entry_idx;
    /* Number of entries (up to 128) */
    uint32_t nentries;
};

#define NO_RX_COND_IDX 0xffff
#define INV_RX_COND_IDX 0xfffe

struct wren_mb_rx_get_source_reply {
    struct wren_mb_rx_source cfg;

    /* First condition */
    uint16_t cond_idx;
};

/* Condition operation. */
struct wren_rx_cond_op {
    uint16_t param_id;
    uint8_t op;
    uint8_t param_src;
};

union wren_rx_cond_word {
    struct wren_rx_cond_op op;
    uint32_t vu32;
    int32_t vs32;
};

#define WREN_COND_MAXLEN 8

struct wren_mb_cond {
    /* Source index */
    uint16_t src_idx;

    /* Event id.  */
    uint16_t evt_id;

    /* Number of words in the conditions.  */
    /* TODO: first parameters id, sorted by source and by id,
       then the expression.  */
    uint8_t len;

    union wren_rx_cond_word ops[WREN_COND_MAXLEN];
};

struct wren_mb_rx_set_cond {
    /* Condition index (from 0 to MAX_RX_CONDS - 1) */
    uint16_t cond_idx;

    struct wren_mb_cond cond;
};

#define WREN_PARAM_SRC_CONTEXT 0
#define WREN_PARAM_SRC_EVENT   1

#define WREN_OP_EQ 0
#define WREN_OP_NE 1


#define WREN_OP_UGT 4
#define WREN_OP_UGE 5
#define WREN_OP_ULT 6
#define WREN_OP_ULE 7

#define WREN_OP_SGT 8
#define WREN_OP_SGE 9
#define WREN_OP_SLT 10
#define WREN_OP_SLE 11

#define WREN_OP_FIRST_BINARY 28

#define WREN_OP_AND 28
#define WREN_OP_OR  29
#define WREN_OP_NOT 30


#endif /* __MB_CMD__H__ */
