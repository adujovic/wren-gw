#ifndef __CHEBY__MAILBOX_OUT_REGS_MAP__H__
#define __CHEBY__MAILBOX_OUT_REGS_MAP__H__
#define MAILBOX_OUT_REGS_MAP_SIZE 12 /* 0xc */

/* None */
#define MAILBOX_OUT_REGS_MAP_CSR 0x0UL
#define MAILBOX_OUT_REGS_MAP_CSR_READY 0x1UL

/* None */
#define MAILBOX_OUT_REGS_MAP_CMD 0x4UL

/* None */
#define MAILBOX_OUT_REGS_MAP_LEN 0x8UL

#ifndef __ASSEMBLER__
struct mailbox_out_regs_map {
  /* [0x0]: REG (rw) (no description) */
  uint32_t csr;

  /* [0x4]: REG (rw) (no description) */
  uint32_t cmd;

  /* [0x8]: REG (rw) (no description) */
  uint32_t len;
};
#endif /* !__ASSEMBLER__*/

#endif /* __CHEBY__MAILBOX_OUT_REGS_MAP__H__ */
