#ifndef __CHEBY__BOARD_MAP__H__
#define __CHEBY__BOARD_MAP__H__

#include "pulser_group_map.h"
#include "mailbox_out_regs_map.h"
#include "mailbox_in_regs_map.h"
#define BOARD_MAP_SIZE 131072 /* 0x20000 = 128KB */

/* Identification */
#define BOARD_MAP_IDENT 0x0UL

/* Map version */
#define BOARD_MAP_VERSION 0x4UL

/* WhiteRabbit status */
#define BOARD_MAP_WR_STATE 0x8UL
#define BOARD_MAP_WR_STATE_LINK_UP 0x1UL
#define BOARD_MAP_WR_STATE_TIME_VALID 0x2UL

/* TAI (seconds) lo part */
#define BOARD_MAP_TM_TAI_LO 0xcUL

/* TAI (seconds) hi part */
#define BOARD_MAP_TM_TAI_HI 0x10UL

/* TAI cycles */
#define BOARD_MAP_TM_CYCLES 0x14UL

/* interrupt force */
#define BOARD_MAP_IFR 0x18UL
#define BOARD_MAP_IFR_MSG 0x8UL
#define BOARD_MAP_IFR_ASYNC 0x10UL

/* None */
#define BOARD_MAP_LEMO_TERM 0x1cUL

/* None */
#define BOARD_MAP_LEMO_OE 0x20UL

/* Raw pulsers status (WTC) */
#define BOARD_MAP_PULSERS_RAW 0x24UL

/* Pulsers status (WTC) */
#define BOARD_MAP_PULSERS_INT 0x28UL

/* Mask for pulsers status */
#define BOARD_MAP_PULSERS_MASK 0x2cUL

/* None */
#define BOARD_MAP_NIC_FIFO 0x30UL
#define BOARD_MAP_NIC_FIFO_MAX_COUNT_MASK 0xffffUL
#define BOARD_MAP_NIC_FIFO_MAX_COUNT_SHIFT 0
#define BOARD_MAP_NIC_FIFO_FULL 0x10000UL

/* None */
#define BOARD_MAP_INPUTS 0x400UL
#define BOARD_MAP_INPUTS_SIZE 1024 /* 0x400 = 1KB */

/* Enable saving timestamps on edges for corresponding inputs */
#define BOARD_MAP_INPUTS_EN 0x400UL

/* Set by HW when a rising edge is detected.
Write 1 to clear the bit (as well as the bit in fall_sr) and
re-enable timestamping.
 */
#define BOARD_MAP_INPUTS_RISE_SR 0x404UL

/* Set by HW when the falling edge has been detected */
#define BOARD_MAP_INPUTS_FALL_SR 0x408UL

/* None */
#define BOARD_MAP_INPUTS_TS 0x600UL
#define BOARD_MAP_INPUTS_TS_SIZE 16 /* 0x10 */

/* None */
#define BOARD_MAP_INPUTS_TS_RISE_SEC 0x0UL

/* None */
#define BOARD_MAP_INPUTS_TS_RISE_NS 0x4UL

/* None */
#define BOARD_MAP_INPUTS_TS_FALL_SEC 0x8UL

/* None */
#define BOARD_MAP_INPUTS_TS_FALL_NS 0xcUL

/* None */
#define BOARD_MAP_LOGS 0x800UL
#define BOARD_MAP_LOGS_SIZE 256 /* 0x100 */

/* None */
#define BOARD_MAP_LOGS_ADDR 0x800UL
#define BOARD_MAP_LOGS_ADDR_SIZE 4 /* 0x4 */

/* None */
#define BOARD_MAP_LOGS_ADDR_VAL 0x0UL

/* None */
#define BOARD_MAP_LEDS 0xa00UL
#define BOARD_MAP_LEDS_SIZE 512 /* 0x200 */

/* None */
#define BOARD_MAP_LEDS_FORCE 0xa00UL

/* None */
#define BOARD_MAP_LEDS_COLORS 0xb00UL
#define BOARD_MAP_LEDS_COLORS_SIZE 4 /* 0x4 */

/* None */
#define BOARD_MAP_LEDS_COLORS_RGB 0x0UL

/* None */
#define BOARD_MAP_RF1 0xc00UL
#define BOARD_MAP_RF1_SIZE 512 /* 0x200 */

/* None */
#define BOARD_MAP_RF1_FTW_HI 0xc00UL

/* None */
#define BOARD_MAP_RF1_FTW_LO 0xc04UL

/* None */
#define BOARD_MAP_RF1_FTW_LOAD 0xc08UL
#define BOARD_MAP_RF1_FTW_LOAD_EN 0x1UL
#define BOARD_MAP_RF1_FTW_LOAD_RESET 0x2UL
#define BOARD_MAP_RF1_FTW_LOAD_RFFRAME 0x4UL

/* None */
#define BOARD_MAP_RF1_H1 0xc0cUL

/* None */
#define BOARD_MAP_RF1_GTH_CFG 0xc10UL

/* None */
#define BOARD_MAP_RF1_GTH_STA 0xc14UL

/* None */
#define BOARD_MAP_RF1_RF_FTW 0xc18UL
#define BOARD_MAP_RF1_RF_FTW_HI_MASK 0xffffUL
#define BOARD_MAP_RF1_RF_FTW_HI_SHIFT 0
#define BOARD_MAP_RF1_RF_FTW_RST 0x10000UL

/* None */
#define BOARD_MAP_RF1_RF_FTW_LO 0xc1cUL

/* None */
#define BOARD_MAP_RF1_RF_RX_TAI 0xc20UL

/* None */
#define BOARD_MAP_RF1_RF_RX_CYC 0xc24UL

/* None */
#define BOARD_MAP_RF1_RF_RX_STA 0xc28UL
#define BOARD_MAP_RF1_RF_RX_STA_VALID 0x1UL

/* None */
#define BOARD_MAP_RF1_STREAMER_FIFO 0xc2cUL
#define BOARD_MAP_RF1_STREAMER_FIFO_MAX_COUNT_MASK 0xffffUL
#define BOARD_MAP_RF1_STREAMER_FIFO_MAX_COUNT_SHIFT 0
#define BOARD_MAP_RF1_STREAMER_FIFO_FULL 0x10000UL

/* None */
#define BOARD_MAP_RF1_STREAMER 0xd00UL
#define ADDR_MASK_BOARD_MAP_RF1_STREAMER 0x100UL
#define BOARD_MAP_RF1_STREAMER_SIZE 256 /* 0x100 */

/* None */
#define BOARD_MAP_PULSER_GROUP 0x6000UL
#define BOARD_MAP_PULSER_GROUP_SIZE 2048 /* 0x800 = 2KB */

/* None */
#define BOARD_MAP_PULSER_GROUP_EL 0x0UL
#define ADDR_MASK_BOARD_MAP_PULSER_GROUP_EL 0x1800UL
#define BOARD_MAP_PULSER_GROUP_EL_SIZE 2048 /* 0x800 = 2KB */

/* None */
#define BOARD_MAP_MB_B2H_BOARD 0x8000UL
#define ADDR_MASK_BOARD_MAP_MB_B2H_BOARD 0x1fff0UL
#define BOARD_MAP_MB_B2H_BOARD_SIZE 16 /* 0x10 */

/* None */
#define BOARD_MAP_MB_H2B_BOARD 0x8010UL
#define ADDR_MASK_BOARD_MAP_MB_H2B_BOARD 0x1fff0UL
#define BOARD_MAP_MB_H2B_BOARD_SIZE 16 /* 0x10 */

/* None */
#define BOARD_MAP_MB_ASYNC_BOARD 0x8020UL
#define BOARD_MAP_MB_ASYNC_BOARD_SIZE 8 /* 0x8 */

/* None */
#define BOARD_MAP_MB_ASYNC_BOARD_BOARD_OFFSET 0x8020UL

/* None */
#define BOARD_MAP_MB_ASYNC_BOARD_HOST_OFFSET 0x8024UL

/* None */
#define BOARD_MAP_MEM_B2H_BOARD 0x9000UL
#define BOARD_MAP_MEM_B2H_BOARD_SIZE 4 /* 0x4 */

/* None */
#define BOARD_MAP_MEM_B2H_BOARD_DATA 0x0UL

/* None */
#define BOARD_MAP_MEM_H2B_BOARD 0xa000UL
#define BOARD_MAP_MEM_H2B_BOARD_SIZE 4 /* 0x4 */

/* None */
#define BOARD_MAP_MEM_H2B_BOARD_DATA 0x0UL

/* None */
#define BOARD_MAP_MEM_ASYNC_BOARD 0xc000UL
#define BOARD_MAP_MEM_ASYNC_BOARD_SIZE 4 /* 0x4 */

/* None */
#define BOARD_MAP_MEM_ASYNC_BOARD_DATA 0x0UL

/* None */
#define BOARD_MAP_WRNIC 0x10000UL
#define ADDR_MASK_BOARD_MAP_WRNIC 0x10000UL
#define BOARD_MAP_WRNIC_SIZE 65536 /* 0x10000 = 64KB */

#ifndef __ASSEMBLER__
struct board_map {
  /* [0x0]: REG (ro) Identification */
  uint32_t ident;

  /* [0x4]: REG (ro) Map version */
  uint32_t version;

  /* [0x8]: REG (ro) WhiteRabbit status */
  uint32_t wr_state;

  /* [0xc]: REG (ro) TAI (seconds) lo part */
  uint32_t tm_tai_lo;

  /* [0x10]: REG (ro) TAI (seconds) hi part */
  uint32_t tm_tai_hi;

  /* [0x14]: REG (ro) TAI cycles */
  uint32_t tm_cycles;

  /* [0x18]: REG (wo) interrupt force */
  uint32_t ifr;

  /* [0x1c]: REG (rw) (no description) */
  uint32_t lemo_term;

  /* [0x20]: REG (rw) (no description) */
  uint32_t lemo_oe;

  /* [0x24]: REG (rw) Raw pulsers status (WTC) */
  uint32_t pulsers_raw;

  /* [0x28]: REG (rw) Pulsers status (WTC) */
  uint32_t pulsers_int;

  /* [0x2c]: REG (rw) Mask for pulsers status */
  uint32_t pulsers_mask;

  /* [0x30]: REG (ro) (no description) */
  uint32_t nic_fifo;

  /* padding to: 1024 Bytes */
  uint32_t __padding_0[243];

  /* [0x400]: BLOCK (no description) */
  struct inputs {
    /* [0x0]: REG (rw) Enable saving timestamps on edges for corresponding inputs */
    uint32_t en;

    /* [0x4]: REG (rw) Set by HW when a rising edge is detected.
Write 1 to clear the bit (as well as the bit in fall_sr) and
re-enable timestamping.
 */
    uint32_t rise_sr;

    /* [0x8]: REG (ro) Set by HW when the falling edge has been detected */
    uint32_t fall_sr;

    /* padding to: 512 Bytes */
    uint32_t __padding_0[125];

    /* [0x200]: MEMORY (no description) */
    struct inputs_ts {
      /* [0x0]: REG (ro) (no description) */
      uint32_t rise_sec;

      /* [0x4]: REG (ro) (no description) */
      uint32_t rise_ns;

      /* [0x8]: REG (ro) (no description) */
      uint32_t fall_sec;

      /* [0xc]: REG (ro) (no description) */
      uint32_t fall_ns;
    } ts[32];
  } inputs;

  /* [0x800]: BLOCK (no description) */
  struct logs {
    /* [0x0]: MEMORY (no description) */
    struct logs_addr {
      /* [0x0]: REG (ro) (no description) */
      uint32_t val;
    } addr[64];
  } logs;

  /* padding to: 2560 Bytes */
  uint32_t __padding_1[64];

  /* [0xa00]: BLOCK (no description) */
  struct leds {
    /* [0x0]: REG (rw) (no description) */
    uint32_t force;

    /* padding to: 256 Bytes */
    uint32_t __padding_0[63];

    /* [0x100]: MEMORY (no description) */
    struct leds_colors {
      /* [0x0]: REG (rw) (no description) */
      uint32_t rgb;
    } colors[64];
  } leds;

  /* [0xc00]: BLOCK (no description) */
  struct rf1 {
    /* [0x0]: REG (rw) (no description) */
    uint32_t ftw_hi;

    /* [0x4]: REG (rw) (no description) */
    uint32_t ftw_lo;

    /* [0x8]: REG (wo) (no description) */
    uint32_t ftw_load;

    /* [0xc]: REG (rw) (no description) */
    uint32_t h1;

    /* [0x10]: REG (rw) (no description) */
    uint32_t gth_cfg;

    /* [0x14]: REG (ro) (no description) */
    uint32_t gth_sta;

    /* [0x18]: REG (ro) (no description) */
    uint32_t rf_ftw;

    /* [0x1c]: REG (ro) (no description) */
    uint32_t rf_ftw_lo;

    /* [0x20]: REG (ro) (no description) */
    uint32_t rf_rx_tai;

    /* [0x24]: REG (ro) (no description) */
    uint32_t rf_rx_cyc;

    /* [0x28]: REG (rw) (no description) */
    uint32_t rf_rx_sta;

    /* [0x2c]: REG (ro) (no description) */
    uint32_t streamer_fifo;

    /* padding to: 256 Bytes */
    uint32_t __padding_0[52];

    /* [0x100]: SUBMAP (no description) */
    uint32_t streamer[64];
  } rf1;

  /* padding to: 24576 Bytes */
  uint32_t __padding_2[5248];

  /* [0x6000]: REPEAT (no description) */
  struct pulser_group {
    /* [0x0]: SUBMAP (no description) */
    struct pulser_group_map el;
  } pulser_group[4];

  /* [0x8000]: SUBMAP (no description) */
  struct mailbox_out_regs_map mb_b2h_board;

  /* padding to: 32784 Bytes */
  uint32_t __padding_3[1];

  /* [0x8010]: SUBMAP (no description) */
  struct mailbox_in_regs_map mb_h2b_board;

  /* padding to: 32800 Bytes */
  uint32_t __padding_4[1];

  /* [0x8020]: BLOCK (no description) */
  struct mb_async_board {
    /* [0x0]: REG (rw) (no description) */
    uint32_t board_offset;

    /* [0x4]: REG (ro) (no description) */
    uint32_t host_offset;
  } mb_async_board;

  /* padding to: 36864 Bytes */
  uint32_t __padding_5[1014];

  /* [0x9000]: MEMORY (no description) */
  struct mem_b2h_board {
    /* [0x0]: REG (wo) (no description) */
    uint32_t data;
  } mem_b2h_board[1024];

  /* [0xa000]: MEMORY (no description) */
  struct mem_h2b_board {
    /* [0x0]: REG (ro) (no description) */
    uint32_t data;
  } mem_h2b_board[1024];

  /* padding to: 49152 Bytes */
  uint32_t __padding_6[1024];

  /* [0xc000]: MEMORY (no description) */
  struct mem_async_board {
    /* [0x0]: REG (wo) (no description) */
    uint32_t data;
  } mem_async_board[2048];

  /* padding to: 65536 Bytes */
  uint32_t __padding_7[2048];

  /* [0x10000]: SUBMAP (no description) */
  uint32_t wrnic[16384];
};
#endif /* !__ASSEMBLER__*/

#endif /* __CHEBY__BOARD_MAP__H__ */
