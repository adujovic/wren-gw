#ifndef __CHEBY__HOST_MAP__H__
#define __CHEBY__HOST_MAP__H__

#include "mailbox_in_regs_map.h"
#include "mailbox_out_regs_map.h"
#define HOST_MAP_SIZE 24576 /* 0x6000 = 24KB */

/* Identification */
#define HOST_MAP_IDENT 0x0UL

/* Map version */
#define HOST_MAP_VERSION 0x4UL

/* WhiteRabbit status */
#define HOST_MAP_WR_STATE 0x8UL
#define HOST_MAP_WR_STATE_LINK_UP 0x1UL
#define HOST_MAP_WR_STATE_TIME_VALID 0x2UL

/* TAI (seconds) lo part */
#define HOST_MAP_TM_TAI_LO 0xcUL

/* TAI (seconds) hi part */
#define HOST_MAP_TM_TAI_HI 0x10UL

/* TAI cycles */
#define HOST_MAP_TM_CYCLES 0x14UL

/* timer and interrupt controller */
#define HOST_MAP_INTC 0x20UL
#define HOST_MAP_INTC_SIZE 32 /* 0x20 */

/* TAI cycles + TAI seconds */
#define HOST_MAP_INTC_TM_COMPACT 0x20UL
#define HOST_MAP_INTC_TM_COMPACT_CYCLES_MASK 0xfffffffUL
#define HOST_MAP_INTC_TM_COMPACT_CYCLES_SHIFT 0
#define HOST_MAP_INTC_TM_COMPACT_SEC_MASK 0xf0000000UL
#define HOST_MAP_INTC_TM_COMPACT_SEC_SHIFT 28

/* generate an interrupt */
#define HOST_MAP_INTC_TM_TIMER 0x24UL
#define HOST_MAP_INTC_TM_TIMER_CYCLES_MASK 0xfffffffUL
#define HOST_MAP_INTC_TM_TIMER_CYCLES_SHIFT 0
#define HOST_MAP_INTC_TM_TIMER_SEC_MASK 0xf0000000UL
#define HOST_MAP_INTC_TM_TIMER_SEC_SHIFT 28

/* interrupt status (after mask) */
#define HOST_MAP_INTC_ISR 0x28UL
#define HOST_MAP_INTC_ISR_CLOCK 0x1UL
#define HOST_MAP_INTC_ISR_TIMER 0x2UL
#define HOST_MAP_INTC_ISR_WR_SYNC 0x4UL
#define HOST_MAP_INTC_ISR_MSG 0x8UL
#define HOST_MAP_INTC_ISR_ASYNC 0x10UL

/* raw interrupt status (before mask) */
#define HOST_MAP_INTC_ISR_RAW 0x2cUL
#define HOST_MAP_INTC_ISR_RAW_CLOCK 0x1UL
#define HOST_MAP_INTC_ISR_RAW_TIMER 0x2UL
#define HOST_MAP_INTC_ISR_RAW_WR_SYNC 0x4UL
#define HOST_MAP_INTC_ISR_RAW_MSG 0x8UL
#define HOST_MAP_INTC_ISR_RAW_ASYNC 0x10UL

/* interrupt mask register */
#define HOST_MAP_INTC_IMR 0x30UL
#define HOST_MAP_INTC_IMR_CLOCK 0x1UL
#define HOST_MAP_INTC_IMR_TIMER 0x2UL
#define HOST_MAP_INTC_IMR_WR_SYNC 0x4UL
#define HOST_MAP_INTC_IMR_MSG 0x8UL
#define HOST_MAP_INTC_IMR_ASYNC 0x10UL

/* ack interrupts */
#define HOST_MAP_INTC_IACK 0x34UL
#define HOST_MAP_INTC_IACK_CLOCK 0x1UL
#define HOST_MAP_INTC_IACK_TIMER 0x2UL
#define HOST_MAP_INTC_IACK_WR_SYNC 0x4UL
#define HOST_MAP_INTC_IACK_MSG 0x8UL
#define HOST_MAP_INTC_IACK_ASYNC 0x10UL

/* None */
#define HOST_MAP_MB_B2H_HOST 0x40UL
#define ADDR_MASK_HOST_MAP_MB_B2H_HOST 0x7ff0UL
#define HOST_MAP_MB_B2H_HOST_SIZE 16 /* 0x10 */

/* None */
#define HOST_MAP_MB_H2B_HOST 0x50UL
#define ADDR_MASK_HOST_MAP_MB_H2B_HOST 0x7ff0UL
#define HOST_MAP_MB_H2B_HOST_SIZE 16 /* 0x10 */

/* None */
#define HOST_MAP_MB_ASYNC_HOST 0x60UL
#define HOST_MAP_MB_ASYNC_HOST_SIZE 8 /* 0x8 */

/* None */
#define HOST_MAP_MB_ASYNC_HOST_BOARD_OFFSET 0x60UL

/* None */
#define HOST_MAP_MB_ASYNC_HOST_HOST_OFFSET 0x64UL

/* None */
#define HOST_MAP_WRPC 0x1000UL
#define ADDR_MASK_HOST_MAP_WRPC 0x7000UL
#define HOST_MAP_WRPC_SIZE 4096 /* 0x1000 = 4KB */

/* None */
#define HOST_MAP_MEM_B2H_HOST 0x2000UL
#define HOST_MAP_MEM_B2H_HOST_SIZE 4 /* 0x4 */

/* None */
#define HOST_MAP_MEM_B2H_HOST_DATA 0x0UL

/* None */
#define HOST_MAP_MEM_H2B_HOST 0x3000UL
#define HOST_MAP_MEM_H2B_HOST_SIZE 4 /* 0x4 */

/* None */
#define HOST_MAP_MEM_H2B_HOST_DATA 0x0UL

/* None */
#define HOST_MAP_MEM_ASYNC_HOST 0x4000UL
#define HOST_MAP_MEM_ASYNC_HOST_SIZE 4 /* 0x4 */

/* None */
#define HOST_MAP_MEM_ASYNC_HOST_DATA 0x0UL

#ifndef __ASSEMBLER__
struct host_map {
  /* [0x0]: REG (ro) Identification */
  uint32_t ident;

  /* [0x4]: REG (ro) Map version */
  uint32_t version;

  /* [0x8]: REG (ro) WhiteRabbit status */
  uint32_t wr_state;

  /* [0xc]: REG (ro) TAI (seconds) lo part */
  uint32_t tm_tai_lo;

  /* [0x10]: REG (ro) TAI (seconds) hi part */
  uint32_t tm_tai_hi;

  /* [0x14]: REG (ro) TAI cycles */
  uint32_t tm_cycles;

  /* padding to: 32 Bytes */
  uint32_t __padding_0[2];

  /* [0x20]: BLOCK timer and interrupt controller */
  struct intc {
    /* [0x0]: REG (ro) TAI cycles + TAI seconds */
    uint32_t tm_compact;

    /* [0x4]: REG (rw) generate an interrupt */
    uint32_t tm_timer;

    /* [0x8]: REG (ro) interrupt status (after mask) */
    uint32_t isr;

    /* [0xc]: REG (ro) raw interrupt status (before mask) */
    uint32_t isr_raw;

    /* [0x10]: REG (rw) interrupt mask register */
    uint32_t imr;

    /* [0x14]: REG (wo) ack interrupts */
    uint32_t iack;

    /* padding to: 64 Bytes */
    uint32_t __padding_0[2];
  } intc;

  /* [0x40]: SUBMAP (no description) */
  struct mailbox_in_regs_map mb_b2h_host;

  /* padding to: 80 Bytes */
  uint32_t __padding_1[1];

  /* [0x50]: SUBMAP (no description) */
  struct mailbox_out_regs_map mb_h2b_host;

  /* padding to: 96 Bytes */
  uint32_t __padding_2[1];

  /* [0x60]: BLOCK (no description) */
  struct mb_async_host {
    /* [0x0]: REG (ro) (no description) */
    uint32_t board_offset;

    /* [0x4]: REG (rw) (no description) */
    uint32_t host_offset;
  } mb_async_host;

  /* padding to: 4096 Bytes */
  uint32_t __padding_3[998];

  /* [0x1000]: SUBMAP (no description) */
  uint32_t wrpc[1024];

  /* [0x2000]: MEMORY (no description) */
  struct mem_b2h_host {
    /* [0x0]: REG (ro) (no description) */
    uint32_t data;
  } mem_b2h_host[1024];

  /* [0x3000]: MEMORY (no description) */
  struct mem_h2b_host {
    /* [0x0]: REG (wo) (no description) */
    uint32_t data;
  } mem_h2b_host[1024];

  /* [0x4000]: MEMORY (no description) */
  struct mem_async_host {
    /* [0x0]: REG (ro) (no description) */
    uint32_t data;
  } mem_async_host[2048];
};
#endif /* !__ASSEMBLER__*/

#endif /* __CHEBY__HOST_MAP__H__ */
