struct pcie_dma {
  uint32_t src_q_ptr_lo;
  uint32_t src_q_ptr_hi;
  uint32_t src_q_size;
  uint32_t src_q_limit;

  uint32_t dst_q_ptr_lo;
  uint32_t dst_q_ptr_hi;
  uint32_t dst_q_size;
  uint32_t dst_q_limit;

  uint32_t stas_q_ptr_lo;
  uint32_t stas_q_ptr_hi;
  uint32_t stas_q_size;
  uint32_t stas_q_limit;

  uint32_t stad_q_ptr_lo;
  uint32_t stad_q_ptr_hi;
  uint32_t stad_q_size;
  uint32_t stad_q_limit;

  uint32_t src_q_next;
  uint32_t dst_q_next;
  uint32_t stas_q_next;
  uint32_t stad_q_next;

  uint32_t scratch0;
  uint32_t scratch1;
  uint32_t scratch2;
  uint32_t scratch3;

  uint32_t pcie_interrupt_control;
  uint32_t pcie_interrupt_status;
  uint32_t axi_interrupt_control;
  uint32_t axi_interrupt_status;

  uint32_t pcie_interrupt_assert; /* 0x70 */
  uint32_t axi_interrupt_assert;
  uint32_t dma_control;
  uint32_t dma_status;
};

#define XPCIE_SOFTWARE_INT (1 << 3)
