#ifndef __CHEBY__PULSER_GROUP_MAP__H__
#define __CHEBY__PULSER_GROUP_MAP__H__
#define PULSER_GROUP_MAP_SIZE 2048 /* 0x800 = 2KB */

/* Configuration of comparators */
#define PULSER_GROUP_MAP_COMPARATORS 0x0UL
#define PULSER_GROUP_MAP_COMPARATORS_SIZE 32 /* 0x20 */

/* None */
#define PULSER_GROUP_MAP_COMPARATORS_TIME_SEC 0x0UL

/* None */
#define PULSER_GROUP_MAP_COMPARATORS_TIME_NSEC 0x4UL

/* None */
#define PULSER_GROUP_MAP_COMPARATORS_CONF1 0x8UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_REPEAT 0x80000000UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_START_MASK 0x1f000000UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_START_SHIFT 24
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_STOP_MASK 0x1f0000UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_STOP_SHIFT 16
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_CLOCK_MASK 0x1f00UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_CLOCK_SHIFT 8
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_IMMEDIAT 0x20UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_PULSER_MASK 0x3UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_PULSER_SHIFT 0

/* Number of system clock periods for the high part of the pulse */
#define PULSER_GROUP_MAP_COMPARATORS_HIGH 0xcUL

/* Number of clock periods for the pulse */
#define PULSER_GROUP_MAP_COMPARATORS_PERIOD 0x10UL

/* Number of pulses */
#define PULSER_GROUP_MAP_COMPARATORS_NPULSES 0x14UL

/* Initial delay (after start), in clock periods */
#define PULSER_GROUP_MAP_COMPARATORS_IDELAY 0x18UL

/* Pulser configuration (RO) */
#define PULSER_GROUP_MAP_PULSERS 0x400UL
#define PULSER_GROUP_MAP_PULSERS_SIZE 32 /* 0x20 */

/* Currently loaded comparator (bit mask) */
#define PULSER_GROUP_MAP_PULSERS_CURRENT_COMP 0x0UL

/* Bit mask of loaded comparators.  Write to clear */
#define PULSER_GROUP_MAP_PULSERS_LOADED_COMP 0x4UL

/* Bit mask of comparators which generated a pulse.  WTC. */
#define PULSER_GROUP_MAP_PULSERS_PULSES 0x8UL

/* Timestamp of the last pulse */
#define PULSER_GROUP_MAP_PULSERS_TS 0xcUL

/* None */
#define PULSER_GROUP_MAP_PULSERS_PAD4 0x10UL

/* None */
#define PULSER_GROUP_MAP_PULSERS_PAD5 0x14UL

/* None */
#define PULSER_GROUP_MAP_PULSERS_PAD6 0x18UL

/* Internal state of the pulser */
#define PULSER_GROUP_MAP_PULSERS_STATE 0x1cUL

/* Configuration of the combined outputs */
#define PULSER_GROUP_MAP_OUT_CFG 0x500UL
#define PULSER_GROUP_MAP_OUT_CFG_SIZE 4 /* 0x4 */

/* None */
#define PULSER_GROUP_MAP_OUT_CFG_CONFIG 0x0UL
#define PULSER_GROUP_MAP_OUT_CFG_CONFIG_MASK_MASK 0xffUL
#define PULSER_GROUP_MAP_OUT_CFG_CONFIG_MASK_SHIFT 0
#define PULSER_GROUP_MAP_OUT_CFG_CONFIG_INV_IN 0x100UL
#define PULSER_GROUP_MAP_OUT_CFG_CONFIG_INV_OUT 0x200UL

/* events on pulse generators */
#define PULSER_GROUP_MAP_PULSES_EVNT 0x520UL
#define PULSER_GROUP_MAP_PULSES_EVNT_SIZE 32 /* 0x20 */

/* None */
#define PULSER_GROUP_MAP_PULSES_EVNT_TM_TAI 0x520UL

/* None */
#define PULSER_GROUP_MAP_PULSES_EVNT_TM_CYC 0x524UL

/* Set for events on pulses generators */
#define PULSER_GROUP_MAP_PULSES_EVNT_EVNT 0x528UL

/* control */
#define PULSER_GROUP_MAP_PULSES_EVNT_CTL 0x52cUL
#define PULSER_GROUP_MAP_PULSES_EVNT_CTL_RD 0x1UL

/* status */
#define PULSER_GROUP_MAP_PULSES_EVNT_STS 0x530UL
#define PULSER_GROUP_MAP_PULSES_EVNT_STS_NEMPTY 0x1UL
#define PULSER_GROUP_MAP_PULSES_EVNT_STS_LOST 0x2UL

/* Status of comparators. Set bit to enable a comparator, cleared by
hw once the pulser has been loaded.
 */
#define PULSER_GROUP_MAP_COMP_STATUS 0x540UL

/* Write a 1 to disable a comparator
 */
#define PULSER_GROUP_MAP_COMP_ABORT 0x544UL

/* Interrupts from comparators (write 1 to clear) */
#define PULSER_GROUP_MAP_COMP_INTR 0x548UL

/* Bit is set when corresponding comparator time is in the past.
 */
#define PULSER_GROUP_MAP_COMP_LATE 0x54cUL

/* Abort pulsers (select by bit) */
#define PULSER_GROUP_MAP_PULSERS_ABORT 0x550UL

/* pulsers status (1 if running) */
#define PULSER_GROUP_MAP_PULSERS_RUNNING 0x554UL

/* sticky pulsers status (1 if has started) */
#define PULSER_GROUP_MAP_PULSERS_RUN 0x558UL

/* true if a pulser has been loaded */
#define PULSER_GROUP_MAP_PULSERS_LOADED 0x55cUL

#ifndef __ASSEMBLER__
struct pulser_group_map {
  /* [0x0]: MEMORY Configuration of comparators */
  struct comparators {
    /* [0x0]: REG (rw) (no description) */
    uint32_t time_sec;

    /* [0x4]: REG (rw) (no description) */
    uint32_t time_nsec;

    /* [0x8]: REG (rw) (no description) */
    uint32_t conf1;

    /* [0xc]: REG (rw) Number of system clock periods for the high part of the pulse */
    uint32_t high;

    /* [0x10]: REG (rw) Number of clock periods for the pulse */
    uint32_t period;

    /* [0x14]: REG (rw) Number of pulses */
    uint32_t npulses;

    /* [0x18]: REG (rw) Initial delay (after start), in clock periods */
    uint32_t idelay;

    /* padding to: 32 Bytes */
    uint32_t __padding_0[1];
  } comparators[32];

  /* [0x400]: MEMORY Pulser configuration (RO) */
  struct pulsers {
    /* [0x0]: REG (ro) Currently loaded comparator (bit mask) */
    uint32_t current_comp;

    /* [0x4]: REG (rw) Bit mask of loaded comparators.  Write to clear */
    uint32_t loaded_comp;

    /* [0x8]: REG (rw) Bit mask of comparators which generated a pulse.  WTC. */
    uint32_t pulses;

    /* [0xc]: REG (ro) Timestamp of the last pulse */
    uint32_t ts;

    /* [0x10]: REG (ro) (no description) */
    uint32_t pad4;

    /* [0x14]: REG (ro) (no description) */
    uint32_t pad5;

    /* [0x18]: REG (ro) (no description) */
    uint32_t pad6;

    /* [0x1c]: REG (ro) Internal state of the pulser */
    uint32_t state;
  } pulsers[8];

  /* [0x500]: MEMORY Configuration of the combined outputs */
  struct out_cfg {
    /* [0x0]: REG (rw) (no description) */
    uint32_t config;
  } out_cfg[8];

  /* [0x520]: BLOCK events on pulse generators */
  struct pulses_evnt {
    /* [0x0]: REG (ro) (no description) */
    uint32_t tm_tai;

    /* [0x4]: REG (ro) (no description) */
    uint32_t tm_cyc;

    /* [0x8]: REG (ro) Set for events on pulses generators */
    uint32_t evnt;

    /* [0xc]: REG (wo) control */
    uint32_t ctl;

    /* [0x10]: REG (ro) status */
    uint32_t sts;

    /* padding to: 1344 Bytes */
    uint32_t __padding_0[3];
  } pulses_evnt;

  /* [0x540]: REG (rw) Status of comparators. Set bit to enable a comparator, cleared by
hw once the pulser has been loaded.
 */
  uint32_t comp_status;

  /* [0x544]: REG (wo) Write a 1 to disable a comparator
 */
  uint32_t comp_abort;

  /* [0x548]: REG (rw) Interrupts from comparators (write 1 to clear) */
  uint32_t comp_intr;

  /* [0x54c]: REG (ro) Bit is set when corresponding comparator time is in the past.
 */
  uint32_t comp_late;

  /* [0x550]: REG (wo) Abort pulsers (select by bit) */
  uint8_t pulsers_abort;

  /* padding to: 1364 Bytes */
  uint8_t __padding_0[3];

  /* [0x554]: REG (ro) pulsers status (1 if running) */
  uint8_t pulsers_running;

  /* padding to: 1368 Bytes */
  uint8_t __padding_1[3];

  /* [0x558]: REG (rw) sticky pulsers status (1 if has started) */
  uint8_t pulsers_run;

  /* padding to: 1372 Bytes */
  uint8_t __padding_2[3];

  /* [0x55c]: REG (ro) true if a pulser has been loaded */
  uint8_t pulsers_loaded;

  /* padding to: 2048 Bytes */
  uint8_t __padding_3[675];
};
#endif /* !__ASSEMBLER__*/

#endif /* __CHEBY__PULSER_GROUP_MAP__H__ */
