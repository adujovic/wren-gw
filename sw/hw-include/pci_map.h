#ifndef __CHEBY__PCI_MAP__H__
#define __CHEBY__PCI_MAP__H__

#include "host_map.h"
#include "board_map.h"
#define PCI_MAP_SIZE 1179648 /* 0x120000 = 1152KB */

/* None */
#define PCI_MAP_HOST 0x0UL
#define ADDR_MASK_PCI_MAP_HOST 0x1f8000UL
#define PCI_MAP_HOST_SIZE 32768 /* 0x8000 = 32KB */

/* None */
#define PCI_MAP_BOARD 0x100000UL
#define ADDR_MASK_PCI_MAP_BOARD 0x1e0000UL
#define PCI_MAP_BOARD_SIZE 131072 /* 0x20000 = 128KB */

#ifndef __ASSEMBLER__
struct pci_map {
  /* [0x0]: SUBMAP (no description) */
  struct host_map host;

  /* padding to: 1048576 Bytes */
  uint32_t __padding_0[256000];

  /* [0x100000]: SUBMAP (no description) */
  struct board_map board;
};
#endif /* !__ASSEMBLER__*/

#endif /* __CHEBY__PCI_MAP__H__ */
