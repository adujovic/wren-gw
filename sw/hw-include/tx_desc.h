#ifndef __CHEBY__TX_DESC__H__
#define __CHEBY__TX_DESC__H__
#define TX_DESC_SIZE 16 /* 0x10 */

/* TX Descriptor  flags */
#define TX_DESC_FLAGS 0x0UL
#define TX_DESC_FLAGS_READY 0x1UL
#define TX_DESC_FLAGS_ERROR 0x2UL
#define TX_DESC_FLAGS_TS_E 0x4UL
#define TX_DESC_FLAGS_PAD_E 0x8UL
#define TX_DESC_FLAGS_TS_ID_MASK 0xffff0000UL
#define TX_DESC_FLAGS_TS_ID_SHIFT 16

/* TX Descriptor  buffer address and length */
#define TX_DESC_BUF 0x4UL
#define TX_DESC_BUF_OFFSET_MASK 0xffffUL
#define TX_DESC_BUF_OFFSET_SHIFT 0
#define TX_DESC_BUF_LEN_MASK 0xffff0000UL
#define TX_DESC_BUF_LEN_SHIFT 16

/* TX Descriptor  destination mask */
#define TX_DESC_DEST 0x8UL
#define TX_DESC_DEST_DPM_MASK 0xffffffffUL
#define TX_DESC_DEST_DPM_SHIFT 0

/* Unused */
#define TX_DESC_UNUSED 0xcUL

#ifndef __ASSEMBLER__
struct tx_desc {
  /* [0x0]: REG (rw) TX Descriptor  flags */
  uint32_t flags;

  /* [0x4]: REG (rw) TX Descriptor  buffer address and length */
  uint32_t buf;

  /* [0x8]: REG (rw) TX Descriptor  destination mask */
  uint32_t dest;

  /* [0xc]: REG (rw) Unused */
  uint32_t unused;
};
#endif /* !__ASSEMBLER__*/

#endif /* __CHEBY__TX_DESC__H__ */
