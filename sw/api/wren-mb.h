#include <time.h>
#include "wren-mb-defs.h"
#include "wrenrx-data.h"

/* Send a message to the board and wait for the reply.
   The header is set from MD and data from INP (length is defined in the
   header).
   The header of the reply is written to MD and the data to RES.
   At most MAXSZ bytes are written.
   Return < 0 in case of device error.
   Return the length in words of the reply */
int wren_mb_msg(int wren_fd,
		struct wren_mb_metadata *md,
		const void *inp,
		void *res,
		unsigned maxsz);

/* Open the device, return < 0 in case of error */
int wrenctl_open(const char *pathname);

/* Check driver version, return < 0 in case of error */
int wren_check_version(int wren_fd);

/* Set TS to the current WR time, return < 0 in case of error (not sync) */
int wrenctl_get_wr_time(int wren_fd, struct timespec *ts);

int wrenctl_msg(int wren_fd,
		unsigned cmd, const void *data, unsigned data_len,
		void *res, unsigned maxsz);

int wrenctl_rx_get_source(int wren_fd, unsigned src_idx,
			  struct wren_mb_rx_get_source_reply *dst);
int wrenctl_rx_get_action(int wren_fd, unsigned act_idx,
			  struct wren_rx_action *dst);
int wrenctl_rx_del_action(int wren_fd, unsigned act_idx);
int wrenctl_rx_get_condition(int wren_fd, unsigned cond_idx,
			     struct wren_rx_cond *dst);
int wrenctl_rx_set_source(int wren_fd, struct wren_mb_rx_set_source *cmd);

int wrenctl_rx_set_cond(int wren_fd, struct wren_mb_rx_set_cond *cmd);
int wrenctl_rx_set_action(int wren_fd, struct wren_mb_rx_set_action *cmd);

struct wrenrx_pulser_log_entry {
    uint32_t sec;
    uint32_t usec;
    uint8_t load;
    uint8_t start;
    uint8_t pulse;
    uint8_t idle;
};

int wrenctl_rx_read_pulser_log(int wren_fd, unsigned pulser_idx, unsigned num,
			       struct wrenrx_pulser_log_entry *entries);

int wrenctl_rx_set_comparator (int wren_fd, unsigned comp_idx,
			       const struct wren_mb_comparator *cfg);
int wrenctl_rx_comparator_status (int wren_fd, unsigned grp, uint32_t *res);
int wrenctl_rx_abort_comparator(int wren_fd, unsigned grp_idx, unsigned mask);

int wrenctl_rx_pulsers_status (int wren_fd, unsigned idx,
			       uint32_t *loaded, uint32_t *running);
int wrenctl_rx_abort_pulsers(int wren_fd, unsigned grp_idx, unsigned mask);
