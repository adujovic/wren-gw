#ifndef __WRENTX_PRIVATE__H__
#define __WRENTX_PRIVATE__H__

#include <stdint.h>
#include "wren/wrentx.h"
#include "wren/wren-packet.h"


struct wrentx_handle_base {
};

struct wrentx_frame {
  // struct wrentx_handle *handle;

  /* Current packet length (in words) excluding the header.  */
  unsigned len;

  /* Offset in the packet of the last capsule (in words).
     Used to truncate the packet. */
  unsigned capsule_offset;

  /* Command field to update when a parameter is added (if not NULL).  */
  struct wren_capsule_hdr *hdr;

  /* Packet buffer */
  struct wren_packet pkt;
};

enum wrentx_table_op {
  wrentx_table_op_send,
  wrentx_table_op_wait
};

union wrentx_table_insn {
  enum wrentx_table_op op;
  struct wrentx_table_insn_send {
    enum wrentx_table_op op;
    /* Offset and number of words for capsules */
    unsigned off;
    unsigned len;
  } send;
  struct wrentx_table_insn_wait {
    enum wrentx_table_op op;
    /* Relative delay */
    struct wren_ts delay;
  } wait;
};

enum wrentx_table_cond {
  wrentx_table_cond_none,
  wrentx_table_cond_ev,
  wrentx_table_cond_ev_param_s32,
  wrentx_table_cond_input
};

/* Maximum number of instructions in a table */
#define WRENTX_MAX_INSNS 32

/* Maximum number of data words for frames */
#define WRENTX_MAX_DATA 1024

/* Table, as created by user. */
struct wrentx_table {
  /* Number of instructions */
  unsigned nbr_insns;
  union wrentx_table_insn insns[WRENTX_MAX_INSNS];

  /* Number of words used (for frames) */
  unsigned nbr_data;
  uint32_t data[WRENTX_MAX_DATA];
};

enum wrentx_table_state {
  /* The table is not configured.  Must be loaded before being played */
  wrentx_table_state_none,

  /* The table has been loaded, but waiting to be started. */
  wrentx_table_state_idle,

  /* Transcient state: table has been started or resumed, but the current
     instruction has to be executed. */
  wrentx_table_state_resumed,

  /* The table is being executed, field pc indicates the current
     instruction (probably a wait). */
  wrentx_table_state_exec
};

/* Table, with execution context */
struct wrentx_table_exec {
  enum wrentx_table_state state;
  char name[WRENTX_TABLE_NAME_MAXLEN];

  /* Start condition */
  enum wrentx_table_cond cond;
  wren_event_id start_ev;
  wren_param_id start_param;
  int32_t start_val_s32;
  unsigned start_input;

  unsigned pc;
  /* Execution time */
  struct wren_ts exec_ts;
  /* Base due time for events */
  struct wren_ts due_ts;
  /* Current number of iterations */
  unsigned count;

  /* Total number of iterations (or 0 for infinite).  Defined when loaded */
  unsigned loop_count;

  struct wrentx_table def;
};

#define NBR_TABLES 8

struct wrentx_source {
  /* Next sequence id, to detect loss.  */
  uint16_t seq_id;

  uint8_t source_id;

  /* When applicable */
  unsigned char daddr[6];
  uint16_t ethertype;

  struct wrentx_table_exec tables[NBR_TABLES];
};

int wrentx_frame_fill_header(struct wrentx_handle *handle,
			     struct wrentx_source *src,
			     struct wrentx_frame *frame);
void wrentx_frame_set_length(struct wrentx_frame *frame);

void wrentx_init_handle_base(struct wrentx_handle_base *handle);

void wrentx_clear_frame_base(struct wrentx_handle_base *handle,
			     struct wrentx_frame *frame);

void wrentx_init_source(struct wrentx_source *src);

struct wrentx_source *wrentx_check_source(struct wrentx_source *srcs,
					  unsigned nbr_srcs,
					  unsigned source_idx);
struct wrentx_table_exec *wrentx_check_table(struct wrentx_source *srcs,
					     unsigned nbr_srcs,
					     unsigned source_idx,
					     unsigned table_idx);

#endif /* __WRENTX_PRIVATE__H__ */
