#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "wren/wren-packet.h"
#include "wren-util.h"

void
wren_dump_params(unsigned poff, uint32_t *buf, unsigned len)
{
  unsigned off = 0;

  while (off < len) {
    uint32_t hdr = buf[off];
    unsigned t = WREN_PACKET_PARAM_GET_DT(hdr);
    unsigned l = WREN_PACKET_PARAM_GET_LEN(hdr);
    printf ("(@%02uw)  param: %04x, len: %uw, typ: %u",
	    poff, WREN_PACKET_PARAM_GET_TYP(hdr), l, t);
    switch (t) {
    case PKT_PARAM_U32:
      printf (" u32: %08x", buf[off + 1]);
      break;
    case PKT_PARAM_S32:
      printf (" s32: %08x", buf[off + 1]);
      break;
    default:
      printf (" unknown");
      break;
    }
    printf ("\n");
    off += l;
    poff += l;
  }
}

void
wren_dump_packet(struct wren_packet *pkt, unsigned len)
{
  const unsigned hlen = sizeof (struct wren_packet_hdr) / 4;
  unsigned off;
  unsigned dlen;

  if (len < sizeof (struct wren_packet_hdr)) {
    printf("...truncated (len=%u)\n", len);
    return;
  }

  printf ("(@00w) ver: 0x%02x, source: 0x%02x\n",
          pkt->hdr.version,
          pkt->hdr.source);
  printf ("(@01w) pkt len: %uw, seq_id: 0x%04x, next_us: %u\n",
          pkt->hdr.len,
	  pkt->hdr.seq_id,
          (unsigned)pkt->hdr.next_us);
  printf ("(@03w) snd ts: %u+%09uns\n",
	  pkt->hdr.snd_ts.sec, pkt->hdr.snd_ts.nsec);

  dlen = pkt->hdr.len - hlen;

  for (off = 0; off < dlen; ) {
    struct wren_capsule_hdr *hdr = (struct wren_capsule_hdr *)&pkt->b[off * 4];
    unsigned coff = hlen + off;

    switch(hdr->typ) {
    case PKT_CTXT:
      {
        const unsigned l = sizeof (struct wren_capsule_ctxt_hdr) / 4;
        struct wren_capsule_ctxt_hdr *ctxt =
	  (struct wren_capsule_ctxt_hdr *)&pkt->b[off * 4];

        printf ("(@%02uw) CONTEXT: id: %u, len: %u\n",
		coff, ctxt->ctxt_id, hdr->len);
        printf ("(@%02uw) from ts: %u+%09uns\n",
		coff + 1, ctxt->valid_from.sec, ctxt->valid_from.nsec);
        wren_dump_params
	  (coff + l, (uint32_t *)(pkt->b + (off + l) * 4), hdr->len - l);
        off += hdr->len;
      }
      break;
    case PKT_EVENT:
      {
        const unsigned l = sizeof (struct wren_capsule_event_hdr) / 4;
        struct wren_capsule_event_hdr *ev =
	  (struct wren_capsule_event_hdr *)&pkt->b[off * 4];

        printf ("(@%02uw) EVENT: id: %u, context: %u, len: %u\n",
		coff, ev->ev_id, ev->ctxt_id, hdr->len);
        printf ("(@%02uw) act ts: %u+%09uns\n",
		coff + 2, ev->ts.sec, ev->ts.nsec);
        wren_dump_params
	  (coff + l, (uint32_t *)(pkt->b + (off + l) * 4), hdr->len - l);
        off += hdr->len;
      }
      break;
    default:
      return; // TODO
      break;
    }
  }
}


/* Raw dump. */
void
wren_dump_hex (unsigned char *buf, unsigned wlen)
{
  unsigned off;

  for (off = 0; off < wlen; off += 4) {
    unsigned i;

    printf ("%04x:", off);
    for (i = off; i < wlen && i < off + 4; i++) {
      unsigned char *b = buf + i * 4;
      printf ("  %02x %02x %02x %02x", b[0], b[1], b[2], b[3]);
    }
    printf("\n");
  }
}

void
wren_dump_packet_hex (struct wren_packet *pkt)
{
  wren_dump_hex ((unsigned char *)pkt, pkt->hdr.len);
}


void
wren_add_nsec(struct wren_ts *ts, unsigned nsec)
{
  assert (nsec < 1000000000);

  ts->nsec += nsec;
  if (ts->nsec >= 1000000000) {
    ts->nsec -= 1000000000;
    ts->sec++;
  }
}

void
wren_add_ts(struct wren_ts *ts, const struct wren_ts *v)
{
  wren_add_nsec(ts, v->nsec);
  ts->sec += v->sec;
}

int
wren_ts_lt(const struct wren_ts *l, const struct wren_ts *r)
{
    if (l->sec < r->sec)
	return 1;
    if (l->sec == r->sec && l->nsec < r->nsec)
	return 1;
    return 0;
}

int
wren_ts_eq(const struct wren_ts *l, const struct wren_ts *r)
{
    return l->sec == r->sec && l->nsec == r->nsec;
}

void
wren_read_packet_ts(struct wren_ts *dest, const struct wren_packet_ts *ts)
{
    dest->nsec = ts->nsec;
    dest->sec = ts->sec;
}

void
wren_add_packet_ts(struct wren_packet_ts *ts, const struct wren_ts *add)
{
  ts->nsec += add->nsec;
  if (ts->nsec > 1000000000) {
    ts->nsec -= 1000000000;
    ts->sec++;
  }
  ts->sec += add->sec;
}
