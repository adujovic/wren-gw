#include <sys/ioctl.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "wren-drv.h"
#include "wrentx-drv.h"
#include "wren-mb.h"
#include "wren-wrtime.h"
#include "wren-ioctl.h"

struct wrentx_handle_wrenctl {
    struct wrentx_handle_op base;

    int wren_fd;
};

static int wrentx_wrenctl_set_source (struct wrentx_handle *handle,
				      unsigned source_idx,
				      struct wren_protocol *proto)
{
    struct wrentx_handle_wrenctl *he = (struct wrentx_handle_wrenctl *)handle;
    struct wren_mb_tx_set_source cmd;
    struct wren_mb_metadata wmd;
    uint32_t res;
    int len;

    cmd.source_idx = source_idx;
    cmd.proto = *proto;
    cmd.max_delay_us = 0;  /* TODO */

    if (proto->proto == WREN_PROTO_ETHERNET) {
	memset (cmd.proto.u.eth.mac, 0xff, 6);
    }
    wmd.cmd = CMD_TX_SET_SOURCE;
    wmd.len = sizeof(cmd) / 4;

    len = wren_mb_msg(he->wren_fd, &wmd, &cmd, &res, sizeof(res));
    if (len != 1 || res != 0)
	return -1;
    return 0;
}

static int
wrentx_wrenctl_send_frame(struct wrentx_handle *handle,
			  unsigned source_idx,
			  struct wrentx_frame *frame)
{
    struct wrentx_handle_wrenctl *he = (struct wrentx_handle_wrenctl *)handle;
    uint32_t *data;
    struct wren_mb_metadata wmd;
    int len;
    uint32_t res;

    wrentx_frame_set_length(frame);

    data = (uint32_t *)(frame->pkt.b); // + sizeof (struct wren_packet_hdr));
    data[-1] = source_idx;

    wmd.cmd = CMD_TX_SEND_PACKET;
    wmd.len = frame->len + 1; /* 1 for source_idx */

    len = wren_mb_msg(he->wren_fd, &wmd, data - 1, &res, sizeof(res));
    if (len != 1 || res != 0)
	return -1;
    return 0;
}

static int wrentx_wrenctl_wren_get_time (struct wrentx_handle *handle,
					 struct wren_ts *time)
{
    struct wrentx_handle_wrenctl *he = (struct wrentx_handle_wrenctl *)handle;
    return wren_wren_get_time(he->wren_fd, time);
}

static int wrentx_wrenctl_wren_wait_until(struct wrentx_handle *handle,
					  const struct wren_ts *time)
{
    struct wrentx_handle_wrenctl *he = (struct wrentx_handle_wrenctl *)handle;
    return wren_wren_wait_until(he->wren_fd, time);
}

static void
wrentx_wrenctl_close(struct wrentx_handle *handle)
{
    struct wrentx_handle_wrenctl *he = (struct wrentx_handle_wrenctl *)handle;

    close(he->wren_fd);
    free(he);
}

static const struct wrentx_operations wrentx_drv_wrenctl_wren_ops = {
    wrentx_wrenctl_close,
    wrentx_wrenctl_set_source,
    wrentx_wrenctl_wren_get_time,
    wrentx_wrenctl_send_frame,
    wrentx_wrenctl_wren_wait_until
};

struct wrentx_drv_wrenctl_init_arg {
    char devname[32];
};

void *
wrentx_drv_init_wrenctl(int *argc, char *argv[])
{
    const char *ifname = argv[1];
    struct wrentx_drv_wrenctl_init_arg *res;
    
    res = malloc (sizeof(*res));
    if (res == NULL)
	return NULL;

    snprintf(res->devname, sizeof(res->devname), "/dev/%s", ifname);

    /* Remove interface name */
    wren_drv_remove_args (argc, argv, 1);


    if (*argc > 1) {
	if (strcmp(argv[1], "--")) {
	    fprintf(stderr, "%s: missing -- after %s\n", progname, ifname);
	    return NULL;
	}
	wren_drv_remove_args (argc, argv, 1);
    }

    return res;
}

struct wrentx_handle *
wrentx_open_wrenctl(const char *devname)

{
    struct wrentx_handle_wrenctl *handle;

    handle = malloc(sizeof(*handle));
    if (handle == NULL) {
	fprintf (stderr, "%s: allocate handle\n", progname);
	return NULL;
    }

    handle->base.op = &wrentx_drv_wrenctl_wren_ops;
    handle->wren_fd = wrenctl_open(devname);
    if (handle->wren_fd < 0) {
	fprintf (stderr, "cannot open %s: %m\n", devname);
	goto error;
    }

    if (wren_check_version(handle->wren_fd) < 0)
	goto error;

    return (struct wrentx_handle *)handle;

  error:
    if (handle->wren_fd >= 0)
      close (handle->wren_fd);
    free(handle);
    return NULL;
}

struct wrentx_handle *
wrentx_drv_open_wrenctl(void *init)
{
    struct wrentx_drv_wrenctl_init_arg *arg = init;
    struct wrentx_handle *handle;

    handle = wrentx_open_wrenctl(arg->devname);
    if (handle == NULL)
	return NULL;
    free (arg);
    return (struct wrentx_handle *)handle;
}
