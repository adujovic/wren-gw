#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "wren-drv.h"
#include "wren-drv-udp.h"

struct wren_drv_udp_init_arg *
wren_drv_init_udp(int *argc, char *argv[])
{
  struct wren_drv_udp_init_arg *res;

  res = malloc(sizeof(*res));
  if (res == NULL)
    return NULL;

  res->dst_port = 1099;
  res->dst_addr.s_addr = htonl(INADDR_LOOPBACK);

  /* Skip 'udp' */
  wren_drv_remove_args (argc, argv, 1);

  if (*argc > 1 && strcmp(argv[1], "--")) {
    if (inet_aton(argv[1], &res->dst_addr) == 0) {
      fprintf(stderr, "%s: cannot parse '%s' as ip address\n",
	      progname, argv[1]);
      return NULL;
    }
    wren_drv_remove_args (argc, argv, 1);
  }

  if (*argc > 1 && strcmp(argv[1], "--")) {
      char *e;

      res->dst_port = strtoul(argv[1], &e, 0);
      if (*e != 0) {
	fprintf(stderr, "%s: cannot parse '%s' as a port\n",
		progname, argv[1]);
	return NULL;
      }
      wren_drv_remove_args (argc, argv, 1);
  }

  if (*argc > 1) {
    if (strcmp(argv[1], "--")) {
      fprintf(stderr, "%s: missing --\n", progname);
      return NULL;
    }
    wren_drv_remove_args (argc, argv, 1);
  }

  return res;
}
