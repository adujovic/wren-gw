#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include "wren-drv.h"
#include "wrenrx-drv.h"
#include "wrenrx-cond.h"

static const struct wren_drv drivers[] = {
    {
	"stdin",
	"stdin\n"
	"  Receive the packets from standard input\n",
	wrenrx_drv_init_stdin,
	(wren_drv_open_t)wrenrx_drv_open_stdin,
    },
    {
	"udp",
	"udp [IP [PORT]]\n"
	"  Receive packets from udp\n",
	wrenrx_drv_init_udp,
	(wren_drv_open_t)wrenrx_drv_open_udp,
    },
    {
	"wren0",
	"wren0\n"
	"  Use wren board lun 0\n",
	wrenrx_drv_wren_init,
	(wren_drv_open_t)wrenrx_drv_wren_open,
    },
    { NULL }
};

struct wren_drv_tuple *
wrenrx_drv_init(int *argc, char *argv[])
{
    return wren_drv_init(drivers, argc, argv);
}

struct wrenrx_handle *
wrenrx_drv_open(struct wren_drv_tuple *init)
{
    struct wrenrx_handle *res;
    res = init->drv->open(init->init);
    free (init);
    return res;
}

int
wrenrx_close(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;
    base->op->close(handle);
    return 0;
}

int
wrenrx_drv_read_packet(struct wrenrx_handle *handle, struct wren_packet *pkt)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;
  return base->op->recv_packet (handle, pkt);
}

int
wrenrx_get_time(struct wrenrx_handle *handle, struct wren_ts *time)
{
    struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

    return base->op->get_time(handle, time);
}

int
wrenrx_pulser_define (struct wrenrx_handle *handle,
		      unsigned pulser_num,
		      const struct wrenrx_cond *cond,
		      const struct wrenrx_pulser_configuration *conf,
		      const char *name)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->pulser_define (handle, pulser_num, cond, conf, name);
}


int wrenrx_pulser_reconfigure (struct wrenrx_handle *handle,
			       unsigned config_idx,
			       const struct wrenrx_pulser_configuration *conf)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->pulser_reconfigure(handle, config_idx, conf);
}

int wrenrx_pulser_remove (struct wrenrx_handle *handle,
			  unsigned config_idx)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->pulser_remove(handle, config_idx);
}

int wrenrx_pulser_get_config (struct wrenrx_handle *handle,
			      unsigned config_idx,
			      unsigned *pulse_num,
			      struct wrenrx_pulser_configuration *conf,
			      char *name)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->pulser_get_config(handle, config_idx, pulse_num, conf, name);
}

int wrenrx_pulser_program(struct wrenrx_handle *handle,
			  unsigned pulser_num,
			  const struct wrenrx_pulser_configuration *conf,
			  const struct wren_ts *time,
			  const char *name)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->pulser_program(handle, pulser_num, conf, time, name);
}

struct wrenrx_msg *
wrenrx_wait(struct wrenrx_handle *handle)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  if (base->op->wait == NULL) {
    while (1)
      sleep (1);
  }
  return base->op->wait (handle);
}

int
wrenrx_get_fd(struct wrenrx_handle *handle)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  if (base->op->get_fd)
    return base->op->get_fd (handle);
  errno = ENOSYS;
  return -1;
}

int
wrenrx_add_source(struct wrenrx_handle *handle,
		  struct wren_protocol *proto)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->add_source (handle, proto);
}

int
wrenrx_get_source(struct wrenrx_handle *handle,
		  unsigned source_idx,
		  struct wren_protocol *proto)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->get_source (handle, source_idx, proto);
}

int wrenrx_remove_source(struct wrenrx_handle *handle,
			 unsigned source_idx)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->del_source (handle, source_idx);
}

int wrenrx_subscribe_context(struct wrenrx_handle *handle,
			     unsigned source_idx)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->context_subscribe(handle, source_idx);
}

int wrenrx_unsubscribe_context(struct wrenrx_handle *handle,
			       unsigned source_idx)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->context_unsubscribe(handle, source_idx);
}


int wrenrx_event_subscribe (struct wrenrx_handle *handle,
			    unsigned source_idx,
			    uint16_t ev_id,
			    int offset_us)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->event_subscribe(handle, source_idx, ev_id, offset_us);
}

int wrenrx_event_unsubscribe (struct wrenrx_handle *handle, unsigned evsubs_id)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->event_unsubscribe(handle, evsubs_id);
}

int wrenrx_subscribe_config(struct wrenrx_handle *handle,
			    unsigned config_idx)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->config_subscribe(handle, config_idx);
}

int wrenrx_unsubscribe_config(struct wrenrx_handle *handle,
			      unsigned config_idx)
{
  struct wrenrx_handle_op *base = (struct wrenrx_handle_op *)handle;

  return base->op->config_unsubscribe(handle, config_idx);
}

void
wrenrx_pulser_configuration_to_mb(struct wren_mb_pulser_config *dst,
				  const struct wrenrx_pulser_configuration *src,
				  unsigned pulser_idx)
{
    memset(dst, 0, sizeof(*dst));

    dst->start = src->start;
    dst->stop = src->stop;
    dst->clock = src->clock;

    dst->pulser_idx = pulser_idx;
    dst->repeat = src->repeat_mode;
    dst->immediat = 0;
    dst->width = src->pulse_width;
    dst->period = src->pulse_period;
    dst->npulses = src->npulses;
    dst->idelay = src->initial_delay;

    dst->load_off_sec = src->load_offset_sec;
    dst->load_off_nsec = src->load_offset_nsec;
}

int
wrenrx_cond_to_mb(struct wren_mb_cond *dst,
		  const struct wrenrx_cond *src)
{
    dst->src_idx = src->source_idx;
    dst->evt_id = src->ev_id;

    if (src->nbr_cond == 0)
	dst->len = 0;
    else {
	int len;
	len = wrenrx_cond_expr_to_words (src, src->nbr_cond - 1,
					 dst->ops, WREN_COND_MAXLEN);
	if (len < 0)
	    return WRENRX_ERR_CONDITION_TOO_LONG;
	dst->len = len;
    }

    return 0;
}
