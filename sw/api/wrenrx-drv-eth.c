#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <netinet/ether.h>
#include <net/if.h>
#include <stdlib.h>
#include "wren-drv.h"
#include "wren-util.h"
#include "wrenrx-drv.h"

struct wrenrx_handle_eth {
  struct wrenrx_handle_op base;

  /* File descriptor to send packets.  */
  int sock;

  unsigned char eth_hdr[12];
  struct sockaddr_ll addr;
};

static int
wrenrx_eth_read(struct wrenrx_handle *handle,
		struct wren_packet *packet)
{
  struct wrenrx_handle_eth *he = (struct wrenrx_handle_eth *)handle;
  int len;
  
  len = recv(he->sock, packet, sizeof(*packet), 0);

  if (0)
    wren_dump_hex((unsigned char *)packet, len);

  return len;
}

static void
wrenrx_eth_close(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_eth *he = (struct wrenrx_handle_eth *)handle;
    close(he->sock);
    free(handle);
}

static const struct wrenrx_operations wrenrx_drv_eth_rt_ops = {
    wrenrx_eth_close,
    wrenrx_eth_read
};

static int
wrenrx_open_eth(struct wrenrx_handle_eth *handle,
		const char *ifname,
		const unsigned char *eth_src, unsigned ethtype)
{
  struct ifreq if_idx;

  handle->sock = socket(PF_PACKET, SOCK_DGRAM, htons(ethtype));
  if (handle->sock < 0) {
    perror("raw socket() failed");
    goto error;
  }

  /* Get interface index. */
  memset(&if_idx, 0, sizeof(struct ifreq));
  strncpy(if_idx.ifr_name, ifname, sizeof(if_idx.ifr_name));
  if (ioctl(handle->sock, SIOCGIFINDEX, &if_idx) < 0) {
    fprintf(stderr, "%s: cannot get index of interface '%s'\n",
	    progname, ifname);
    goto error;
  }

  /* Bind to interface + ethtyp. */
  memset(&handle->addr, 0, sizeof(handle->addr));
  handle->addr.sll_family = AF_PACKET;
  handle->addr.sll_protocol = htons(ETH_P_ALL); // ethtype);
  handle->addr.sll_ifindex = if_idx.ifr_ifindex;
  if (bind(handle->sock, (void *)&handle->addr, sizeof(handle->addr)) < 0) {
    fprintf(stderr, "%s: cannot bind AF_PACKET socket (%m)\n",
	    progname);
    goto error;
  }

  // wrenrx_init_handle_base(&handle->base.base);

  return 0;

 error:
  if (handle->sock >= 0)
    close(handle->sock);
  return -1;

}

struct wrenrx_drv_eth_init_arg {
  char eth_name[IFNAMSIZ];

  unsigned char eth_dst[6];
};

void *
wrenrx_drv_init_eth(int *argc, char *argv[])
{
  struct wrenrx_drv_eth_init_arg *init;
  const char *ifname = argv[1];

  init = malloc(sizeof (*init));
  if (init == NULL)
    return NULL;
  
  strncpy(init->eth_name, ifname, IFNAMSIZ - 1);
  memset(init->eth_dst, 0xff, 6);

  /* Remove interface name */
  wren_drv_remove_args (argc, argv, 1);

  if (*argc > 1) {
    if (strcmp(argv[1], "--")) {
      fprintf(stderr, "%s: missing -- after %s\n", progname, ifname);
      return NULL;
    }
    wren_drv_remove_args (argc, argv, 1);
  }

  return init;
}

struct wrenrx_handle *
wrenrx_drv_open_eth(void *init)
{
  struct wrenrx_drv_eth_init_arg *args = init;
  struct wrenrx_handle_eth *handle;

  handle = malloc(sizeof(*handle));
  if (handle == NULL) {
    fprintf (stderr, "%s: allocate handle\n", progname);
    return NULL;
  }

  handle->base.op = &wrenrx_drv_eth_rt_ops;

  if (wrenrx_open_eth(handle, args->eth_name, args->eth_dst,
		      ('W' << 8) | 'e') < 0) {
    free (handle);
    return NULL;
  }
  free (args);
  return (struct wrenrx_handle *)handle;
}
