#ifndef __WRENRX_DRV__H__
#define __WRENRX_DRV__H__

#include "wren/wrenrx.h"
#include "wren/wren-packet.h"
#include "wren-mb-defs.h"

/* Select a wrentx.
   Typically args are:
     DEVICE OPTIONS... -- program options
   or
     DEVICE OPTIONS...

   Options are removed from argc/argc.
   Return NULLin case of error.
*/
struct wren_drv_tuple *wrenrx_drv_init(int *argc, char *argv[]);

/* Open the selected wrenrx.  The argument is the result of wrenrx_drv_init.
   This happens after so that the program can decode the arguments
   even if the board is not present
*/
struct wrenrx_handle *wrenrx_drv_open(struct wren_drv_tuple *init);

/* Read a wren packet from HANDLE.  Possibly remove any lower level headers
   (like ethernet).
   Return the length (in byte). */
int wrenrx_drv_read_packet(struct wrenrx_handle *handle,
			   struct wren_packet *pkt);

/* Private declarations */

/* For stdin driver */
struct wrenrx_handle *wrenrx_drv_open_fd(int fd);
void *wrenrx_drv_init_stdin(int *argc, char *argv[]);
struct wrenrx_handle *wrenrx_drv_open_stdin(void *);

/* For udp driver */
void *wrenrx_drv_init_udp(int *argc, char *argv[]);
struct wrenrx_handle *wrenrx_drv_open_udp(void *);

/* For eth driver */
void *wrenrx_drv_init_eth(int *argc, char *argv[]);
struct wrenrx_handle *wrenrx_drv_open_eth(void *);

/* For wrenctl driver */
void *wrenrx_drv_wren_init(int *argc, char *argv[]);
struct wrenrx_handle *wrenrx_drv_wren_open(void *);

struct wrenrx_operations
{
  void (*close)(struct wrenrx_handle *handle);
  int (*recv_packet)(struct wrenrx_handle *handle, struct wren_packet *pkt);
  int (*get_time)(struct wrenrx_handle *handle,
		  struct wren_ts *time);
  int (*add_source)(struct wrenrx_handle *handle,
		    struct wren_protocol *proto);
  int (*get_source)(struct wrenrx_handle *handle,
		    unsigned source_idx,
		    struct wren_protocol *proto);
  int (*del_source)(struct wrenrx_handle *handle,
		    unsigned source_idx);

  int (*context_subscribe)(struct wrenrx_handle *handle,
			   unsigned source_idx);
  int (*context_unsubscribe)(struct wrenrx_handle *handle, unsigned source_idx);

  /* Note: internally, the pulser_idx is an index starting from 0 */
  int (*pulser_define)(struct wrenrx_handle *handle,
		       unsigned pulser_idx,
		       const struct wrenrx_cond *cond,
		       const struct wrenrx_pulser_configuration *conf,
		       const char *name);
  int (*pulser_reconfigure)(struct wrenrx_handle *handle,
			    unsigned config_idx,
			    const struct wrenrx_pulser_configuration *conf);
  int (*pulser_remove)(struct wrenrx_handle *handle,
		       unsigned config_idx);
  int (*pulser_get_config)(struct wrenrx_handle *handle,
			   unsigned config_idx,
			   unsigned *pulse_num,
			   struct wrenrx_pulser_configuration *conf,
			   char *name);
  int (*pulser_program)(struct wrenrx_handle *handle,
			unsigned pulser_num,
			const struct wrenrx_pulser_configuration *conf,
			const struct wren_ts *time,
			const char *name);
  int (*event_subscribe)(struct wrenrx_handle *handle,
			 unsigned source_idx,
			 uint16_t ev_id,
			 int offset_us);
  int (*event_unsubscribe)(struct wrenrx_handle *handle, unsigned evsubs_id);
  int (*config_subscribe)(struct wrenrx_handle *handle,
			  unsigned config_idx);
  int (*config_unsubscribe)(struct wrenrx_handle *handle,
			    unsigned config_idx);
  int (*get_fd)(struct wrenrx_handle *handle);
  struct wrenrx_msg *(*wait)(struct wrenrx_handle *handle);
};

struct wrenrx_handle_op {
  const struct wrenrx_operations *op;
};

/* Converters */
void
wrenrx_pulser_configuration_to_mb(struct wren_mb_pulser_config *dst,
				  const struct wrenrx_pulser_configuration *src,
				  unsigned pulser_idx);


int wrenrx_cond_to_mb(struct wren_mb_cond *dst,
		      const struct wrenrx_cond *src);

#endif /* __WRENRX_DRV__H__ */
