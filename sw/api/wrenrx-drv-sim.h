#include "wren/wrenrx.h"
#include "wrenrx-data.h"
#include "wrenrx-drv.h"
#include "wrenrx-drv-evt.h"
#include "wrenrx-cond.h"

#define NBR_ACTIONS 32

enum action_kind
  {
   ACT_NONE,
   ACT_INTER,
   ACT_PULSER
  };


struct wrenrx_handle_sim {
  struct wrenrx_handle_op base;

  unsigned nbr_act;

  /* Last packet reveived.  */
  struct wren_packet pkt;
  /* Packet length and offset.  */
  unsigned len, off, idx;

  /* Actions array.  */
  struct action {
    enum action_kind kind;
    uint16_t cond_ev_id;
    uint8_t cond_src;
    unsigned cond_len;
    union wren_rx_cond_word cond_ops[MAX_RX_COND_WORDS];

    /* Specific parameters that depends on kind. */
    union {
      struct interrupt {
        unsigned id;
        int delay_us;
      } inter;
      struct pulser {
	unsigned id;
	struct wrenrx_pulser_configuration cfg;
	int32_t off_sec;
	int32_t off_nsec;
      } pulser;
    } u;
  } act[NBR_ACTIONS];

  struct wrenrx_handle_events ev;
};

/* Global handler (for actions) */
extern struct wrenrx_handle_sim *sim_handle;


void wrenrx_drv_sim_init(struct wrenrx_handle_sim *handle);

int wrenrx_sim_add_source(struct wrenrx_handle *handle,
			  struct wren_protocol *proto);
int wrenrx_sim_pulser_define (struct wrenrx_handle *handle,
			      unsigned pulser_idx,
			      const struct wrenrx_cond *cond,
			      const struct wrenrx_pulser_configuration *conf,
			      const char *name);
int wrenrx_sim_subscribe (struct wrenrx_handle *handle,
			  unsigned source_idx,
			  uint16_t ev_id,
			  int offset_us);

struct wrenrx_msg *wrenrx_sim_wait(struct wrenrx_handle *handle);
