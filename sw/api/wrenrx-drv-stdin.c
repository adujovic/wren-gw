#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <netinet/ether.h>
#include <net/if.h>
#include <stdlib.h>
#include "wren-drv.h"
#include "wren-simrt.h"
#include "wrenrx-drv.h"
#include "wrenrx-drv-sim.h"

struct wrenrx_handle_fd {
  struct wrenrx_handle_sim base;

  /* File descriptor */
  int fd;
};

static int
wrenrx_fd_read(struct wrenrx_handle *handle,
	       struct wren_packet *packet)
{
  struct wrenrx_handle_fd *he = (struct wrenrx_handle_fd *)handle;
  int len;
  int dlen;

  len = read (he->fd, &packet->hdr, sizeof (struct wren_packet_hdr));
  if (len == 0)
    return 0;

  if (len != sizeof (struct wren_packet_hdr))
    return -1;

  dlen = packet->hdr.len * 4 - sizeof (struct wren_packet_hdr);

  len = read (he->fd, packet->b, dlen);
  if (len != dlen)
    return -1;
  return sizeof (struct wren_packet_hdr) + dlen;
}

static int
wrenrx_drv_stdin_get_time (struct wrenrx_handle *handle,
			 struct wren_ts *time)
{
  return wren_simrt_get_time(time);
}

static void
wrenrx_fd_close(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_fd *he = (struct wrenrx_handle_fd *)handle;
    close(he->fd);
    free(handle);
}

static const struct wrenrx_operations wrenrx_drv_fd_rt_ops = {
  .close = wrenrx_fd_close,
  .recv_packet = wrenrx_fd_read,
  .get_time = wrenrx_drv_stdin_get_time,
  .add_source = wrenrx_sim_add_source,
  .pulser_define = wrenrx_sim_pulser_define,
  .wait = wrenrx_sim_wait,
  .event_subscribe = wrenrx_sim_subscribe
};

void *
wrenrx_drv_init_stdin(int *argc, char *argv[])
{
  /* Remove stdin */
  wren_drv_remove_args (argc, argv, 1);

  if (*argc > 1) {
    if (strcmp(argv[1], "--")) {
      fprintf(stderr, "%s: missing -- after driver name\n", progname);
      return NULL;
    }
    wren_drv_remove_args (argc, argv, 1);
  }

  /* Non NULL... */
  return (void *)1;
}

struct wrenrx_handle *
wrenrx_drv_open_fd(int fd)
{
    struct wrenrx_handle_fd *handle;

    handle = malloc(sizeof(*handle));
    if (handle == NULL) {
	fprintf (stderr, "%s: allocate handle\n", progname);
	return NULL;
    }

    handle->base.base.op = &wrenrx_drv_fd_rt_ops;

    wrenrx_drv_sim_init(&handle->base);
    handle->fd = fd;

    return (struct wrenrx_handle *)handle;
}

struct wrenrx_handle *
wrenrx_drv_open_stdin(void *init)
{
    return wrenrx_drv_open_fd(0);
}
