#ifndef __WRENRX_COND__H__
#define __WRENRX_COND__H__
#include "wren/wrenrx.h"

enum wrenrx_kind { WRENRX_COND, WRENRX_BINARY };

struct wrenrx_cond_expr {
  enum wrenrx_kind kind;
  union {
    struct {
      enum wrenrx_cond_comp op;
      uint8_t src;
      uint16_t param;
      uint32_t val;
    } cond;
    struct {
      enum wrenrx_cond_rel op;
      unsigned l, r;
    } binary;
  } u;
};

#define MAX_COND 8

struct wrenrx_cond {
  unsigned source_idx;
  uint16_t ev_id;

  unsigned nbr_cond;
  struct wrenrx_cond_expr conds[MAX_COND];
};

union wren_rx_cond_word;

int wrenrx_cond_expr_to_words (const struct wrenrx_cond *cond,
			       unsigned idx,
			       union wren_rx_cond_word *data,
			       unsigned maxlen);

struct wrenrx_msg_context {
    struct wren_ts ts;
    /* Context parameters */
    unsigned params_len;
    uint32_t params[1024 / 4];
};

struct wrenrx_msg_event {
    unsigned ev_id;
    unsigned ctxt_id;
    struct wren_ts ts;
    /* Event parameters */
    unsigned param_len;
    uint32_t params[1024 / 4];
};

struct wrenrx_msg
{
    enum wrenrx_msg_kind kind;
    union {
	struct {
	    uint16_t config_id;
	    uint8_t source_idx;
	    struct wren_ts ts;
	    struct wrenrx_msg_event evt;
	} pulse;
	struct {
	    unsigned source_idx;
	    struct wren_ts ts;
	    int offset_us;
	    struct wrenrx_msg_event evt;
	    struct wrenrx_msg_context ctxt;
	} evt;
	struct {
	    unsigned source_idx;
	    unsigned ctxt_id;
	    struct wrenrx_msg_context ctxt;
	} ctxt;
    } u;
};
#endif /* __WRENRX_COND__H__ */
