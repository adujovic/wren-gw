/* Handling of subscribed events */
#include <stdint.h>
#include "wrenrx-cond.h"

#define NBR_SRCS 4
#define NBR_CTXT 4
#define NBR_EVENTS 16
#define NBR_MSGS 32

struct wrenrx_handle_events {
  /* Message returned to user.  */
  struct wrenrx_msg smsg;

  /* Last contexts received.  */
  struct ctxt {
    uint16_t ctxt_id;
    struct wren_ts valid_from;
    uint16_t len;
    uint32_t params[1024 / 4];
  } ctxts[NBR_SRCS][NBR_CTXT];

  struct evt {
    /* Number of users for this event, 0 when free. */
    uint16_t count;

    uint16_t src_idx; /* Also used as chain for free_events linked list */
    uint16_t ev_id;
    uint16_t ctxt_id;
    struct wren_ts ts;
    unsigned param_len;
    uint32_t params[1024 / 4];
  } events[NBR_EVENTS];

  /* Simply linked list of events. */
  unsigned free_events;

  struct msg {
    /* See wrenrx_msg_kind */
    uint8_t kind;
    uint16_t idx;
  } msgs[NBR_MSGS];

  /* Messages cur_msg .. nbr_msg (not included) have to be delivered to the
     user */
  unsigned nbr_msg;
  unsigned cur_msg;

  /* TODO: list of subscribed events */
  /* TODO: list of events received */
  /* TODO: scheduled actions */
};

int wrenrx_drv_evt_has_msg(const struct wrenrx_handle_events *h);
struct wrenrx_msg *wrenrx_drv_evt_get_msg(struct wrenrx_handle_events *h);

/* Initialize the structure */
void wrenrx_drv_evt_init(struct wrenrx_handle_events *h);
