#ifndef __WREN__WRENRX__H__
#define __WREN__WRENRX__H__
/* WhiteRabbit Timing Receiver.

   The boards contain:
   * a WR PTP core
   * Pulsers
   * A processor.

   So a receiver can receive data and events from the sender and pass them
   (using filtering) to the driver and applications.
   The driver can also generate pulses at a specific time, and thanks to WR,
   the time is absolute.
   Finally, the embedded processor can also receive specific events and
   directly generate pulses according to a policy defined by the driver
   (like events selection, filter on parameters, delay and pulser
   configuration).

   A trigger is defined by a set of conditions on a cycle and optionnaly
   an associated event, when they are received from the network.

   A trigger can configure a pulser for a time relative to the event,
   or schedule an interrupt to the cpu.

   TODO: logs, stats.
*/
#include "wren/wren-common.h"

#ifdef __cplusplus
extern "C" {
#endif

/* An opaque structure used through the API.  It contains the association of
   the software with the hardware.  */
struct wrenrx_handle;

/* Errors */

/* No sources available (cannot be added) */
#define WRENRX_ERR_NO_SOURCE -3

/* No conditions available (cannot be added) */
#define WRENRX_ERR_NO_CONDITION -4

/* No actions available (cannot be added) */
#define WRENRX_ERR_NO_ACTION -5
#define WRENRX_ERR_NO_CONFIG -5

/* The condition is too complex */
#define WRENRX_ERR_CONDITION_TOO_LONG -6

/* Invalid source id (or source not created) */
#define WRENRX_ERR_BAD_SOURCE -7

/* Incorrect source id (beyond the limit) */
#define WRENRX_ERR_SRCID_TOO_LARGE -8

/* Event id is too large */
#define WRENRX_ERR_EVID_TOO_LARGE -9

/* Config id is too large */
#define WRENRX_ERR_CFGID_TOO_LARGE -10

/* System error (errno is set) */
#define WRENRX_ERR_SYSTEM -11

/* Incorrect protocol */
#define WRENRX_ERR_BAD_PROTOCOL -12

/* Pulser number is invalid */
#define WRENRX_ERR_PULSER_NUM -13

/* Create an association with the hardware.  Returns NULL and set errno in
   case of error.
   NOTE: the arguments will probably change, and it may be possible to have
   multiple functions to open a device.  Will be refined. */
struct wrenrx_handle *wrenrx_open(const char *filename);

/* For simulation: receive on a UDP socket */
struct wrenrx_handle *wrenrx_open_udp_by_port(unsigned port);
struct wrenrx_handle *wrenrx_open_udp_default(void);

/* Close the association.  */
int wrenrx_close(struct wrenrx_handle *handle);

/* Get current time.  Should return 0. */
int wrenrx_get_time(struct wrenrx_handle *handle, struct wren_ts *time);

/* Add a filter for a source.  Once a filter a set, the packets coming
   from this source are considered by the board and are accepted.

   It is recommanded to define the filters once at boot time.

   The number of filters is limited and defined by the hardware.

   Return the source index, or < 0 in case of error.

   Discussion:
   - Do we want to allow filter overwrite ? Probably not
   - Do we want to remove a filter ? Probably useless in operation, but why
     not.
   - Do we want to have symbolic index (so that even if each board handles
     a subset of all sources, the same index designate the same source
     everywhere) ?  For upper software layer.
 */
int wrenrx_add_source(struct wrenrx_handle *handle,
		      struct wren_protocol *proto);

/* Get the configuration of a source. */
int wrenrx_get_source(struct wrenrx_handle *handle,
		      unsigned source_idx,
		      struct wren_protocol *proto);

/* Remove a source.  All associated pulsers are removed.  Loaded pulsers
   are removed, and subscribed events are unsubscribed */
int wrenrx_remove_source(struct wrenrx_handle *handle,
			 unsigned source_idx);

  /* Subscribe and unsubscribe to contexts of a source. */
int wrenrx_subscribe_context(struct wrenrx_handle *handle,
			     unsigned source_idx);
int wrenrx_unsubscribe_context(struct wrenrx_handle *handle,
			       unsigned source_idx);


/* TODO: get state, config, stats...
   configure BST clocks
*/

/* Opaque structure.  */
struct wrenrx_cond;

/* Allocate and initialize a condition structure.  */
struct wrenrx_cond *wrenrx_cond_alloc(struct wrenrx_handle *handle);

/* Free a condition structure, after use.  */
void wrenrx_cond_free(struct wrenrx_cond *cond);

/* Recycle a condition structure (to avoid a free+alloc). */
void wrenrx_cond_reinit (struct wrenrx_handle *handle,
			 struct wrenrx_cond *cond);

enum wrenrx_cond_comp {
  WRENRX_EQ,  /* Equal */
  WRENRX_NE,  /* Not equal */
  WRENRX_SLT, /* Signed less than */
  WRENRX_SLE, /* Signed less or equal */
  WRENRX_SGT, /* Signed greater than */
  WRENRX_SGE  /* Signed greater or equal */
};

/* Add an expression to a condition, for PARAM OP VAL.
   If ctxt_evt is 0, the parameter is looked at from the context,
   if 1, it is looked at from the event. */
struct wrenrx_cond_expr *wrenrx_cond_param_u32 (struct wrenrx_cond *cond,
						enum wrenrx_cond_comp op,
						unsigned ctxt_evt,
						wren_param_id param,
						uint32_t val);

/* Relations between expressions */
enum wrenrx_cond_rel {
  WRENRX_AND,
  WRENRX_OR,
  WRENRX_NOT /* Right argument is null */
};

/* Add an operation between conditions.  */
struct wrenrx_cond_expr *wrenrx_cond_rel (struct wrenrx_cond *cond,
					  enum wrenrx_cond_rel op,
					  struct wrenrx_cond_expr *l,
					  struct wrenrx_cond_expr *r);


/* Define a condition, which then has to be connected to an interrupt or
   a pulser.
   When an event message from SOURCE_ID is received for event EV_ID, the
   expression EXPR is evaluated.  If true, the action is run.

   Condition can be NULL.  */
int wrenrx_cond_define (struct wrenrx_cond *cond,
			unsigned source_idx,
			wren_event_id ev_id,
			struct wrenrx_cond_expr *expr);

/* Subscribe to a condition.  The event will be delivered to the client
   at its due time + OFFSET_US (which can be negative).

   Return < 0 in case of error,
   or the evsubs id (used to identify the event or to unsubscribe) */
int wrenrx_event_subscribe (struct wrenrx_handle *handle,
			    unsigned source_idx,
			    uint16_t ev_id,
			    int offset_us);

int wrenrx_event_unsubscribe (struct wrenrx_handle *handle, unsigned evsubs_id);

/* TODO: absolute interrupt (based on time) ?  */

/* Maximum number of bytes (including the terminating NUL) for a configuration
   name */
#define WRENRX_CONFIGURATION_NAME_MAXLEN 32

struct wrenrx_pulser_configuration
{
    /* Configuration enabled flag, if 0 this configuration is ignored but
       present */
    unsigned char config_en;

    /* Interrupt enabled flag: if 1, an interrupt is generated on when
       a rising edge is generated (even if outputs are disabled) */
    unsigned char interrupt_en;

    /* Output enabled: if 0, the output stays to 0 */
    unsigned char output_en;

    /* If 1, the configuration is kept in the pulser and can be restarted
       with a start pulse.  A stop pulse does not disable the pulser but
       makes it ready again.  The pulser must be either be reloaded or
       disabled by software to exit from the repeat mode. */
    unsigned char repeat_mode;

    /* Start, stop and clock inputs */
    unsigned start;
    unsigned stop;
    unsigned clock;

    /* Initial delay after the start pulse, in clock cycles
       (used only if the start input is used) */
    unsigned initial_delay;

    /* Duration of the high pulse in ns */
    unsigned pulse_width;
    /* Total duration of the pulse (hi + low) in clock cycles,
       or 0 for level.  */
    unsigned pulse_period;

    /* Number of pulses, or 0 if infinite. */
    unsigned npulses;

    /* Load offset: time offset on the event due time. */
    int32_t load_offset_sec;
    int32_t load_offset_nsec;
};

/* Add an action for pulser \p pulser_num (between 1 and 32).

   Return a config_idx handle or error if value < 0.
 */
int wrenrx_pulser_define (struct wrenrx_handle *handle,
			  unsigned pulser_num,
			  const struct wrenrx_cond *cond,
			  const struct wrenrx_pulser_configuration *conf,
			  const char *name);

/* Remove a configuration. */
int wrenrx_pulser_remove (struct wrenrx_handle *handle,
			  unsigned config_idx);

/* Modify the configuration of an action
   TODO: add a bitmask to specify which fields change. */
int wrenrx_pulser_reconfigure (struct wrenrx_handle *handle,
			       unsigned config_idx,
			       const struct wrenrx_pulser_configuration *conf);

/* Read configuration (and name) */
int wrenrx_pulser_get_config (struct wrenrx_handle *handle,
			      unsigned config_idx,
			      unsigned *pulse_num,
			      struct wrenrx_pulser_configuration *conf,
			      char *name);

/* Subscribe and unsubscribe to a configuration.  Will receive pulses for
   the configuration */
int wrenrx_subscribe_config(struct wrenrx_handle *handle,
			    unsigned config_idx);
int wrenrx_unsubscribe_config(struct wrenrx_handle *handle,
			      unsigned config_idx);

/* TODO: get a list of existing config_id for a pulser_num */

/* Program a pulser (independently of events).

   The pulser \p pulser_num will be programmed at time \p time (if set) or
   immediately (if \p time is NULL).

   TODO:
   * define interraction with pulser_configure.
   * clear (unload) a pulser
   * define what happens if \p time is in the past
*/
int wrenrx_pulser_program(struct wrenrx_handle *handle,
			  unsigned pulser_num,
			  const struct wrenrx_pulser_configuration *conf,
			  const struct wren_ts *time,
			  const char *name);

/* Output configuration.  */
struct wrenrx_output_configuration
{
    /* If set, the output of the pulsers are inverted before being
       combined.  Using De Morgan's law, the pulser outputs can be and-ed */
    unsigned invert_pulsers;
    /* Bit mask of outputs being or-ed.
       If bit N is set, the output N+1 is part of the or-gate. */
    uint32_t mask;
    /* If set the result of the OR gate. */
    unsigned invert_result;
};

/* Configure \p output_num (between 1 and 32).  */
int wrenrx_output_configure (struct wrenrx_handle *handle,
			     unsigned output_num,
			     struct wrenrx_output_configuration *cfg);

struct wrenrx_msg;

/* Wait for interrupts or data from the receiver.  */
struct wrenrx_msg *wrenrx_wait(struct wrenrx_handle *handle);

/* Get file descriptor to be used for poll/select.  */
int wrenrx_get_fd(struct wrenrx_handle *handle);

enum wrenrx_msg_kind
  {
   wrenrx_msg_network_error,
   wrenrx_msg_pulse,
   wrenrx_msg_event,
   wrenrx_msg_context
  };

/* Get the kind of a message.  */
enum wrenrx_msg_kind wrenrx_get_msg(struct wrenrx_msg *msg);

/* Get config id of a pulse message.  */
unsigned wrenrx_msg_get_config_id(struct wrenrx_msg *msg);

/* Get time stamp id of a pulse message.  */
int wrenrx_msg_get_pulse_ts(struct wrenrx_msg *msg, struct wren_ts *ts);

/* Get source index of the event, context or pulse).  */
unsigned wrenrx_msg_get_source_idx(struct wrenrx_msg *msg);

/* Get the event id of an event message */
unsigned wrenrx_msg_get_event_id(struct wrenrx_msg *msg);

/* Get the due timestamp of the event */
int wrenrx_msg_get_event_ts(struct wrenrx_msg *msg, struct wren_ts *ts);

/* Get the context id of an event or context message */
unsigned wrenrx_msg_get_context_id(struct wrenrx_msg *msg);

/* Get the valid-from timestamp of a context */
int wrenrx_msg_get_context_ts(struct wrenrx_msg *msg, struct wren_ts *ts);

/* Iterator for parameters.
   Note: the structure is private, user shouldn't modify it */
struct wrenrx_msg_param;

/* Set an iterator for event or context parameters.
   Any number of iterators can be extracted, but its lifetime is limited
   to the lifetime of MSG.
   The user manage the iterator life time. */
int wrenrx_msg_get_event_params(struct wrenrx_msg *msg,
				struct wrenrx_msg_param *it);
int wrenrx_msg_get_context_params(struct wrenrx_msg *msg,
				  struct wrenrx_msg_param *it);

/* True if the iterator designate a valid parameter, false if at the end of
   the parameters */
unsigned wrenrx_msg_param_is_valid(const struct wrenrx_msg_param *params);

void wrenrx_msg_param_next(struct wrenrx_msg_param *params);

/* Get current parameter id and length */
wren_param_id wrenrx_msg_param_get_id(const struct wrenrx_msg_param *params);
unsigned wrenrx_msg_param_get_len(const struct wrenrx_msg_param *params);
unsigned wrenrx_msg_param_get_dt(const struct wrenrx_msg_param *params);

int32_t wrenrx_msg_param_get_s32(const struct wrenrx_msg_param *params);
float   wrenrx_msg_param_get_f32(const struct wrenrx_msg_param *params);
int64_t wrenrx_msg_param_get_s64(const struct wrenrx_msg_param *params);

/* TODO: special interrupt for cycle and event.
   TODO: wait for interrupt
   TODO: read cycle and event message
   TODO: network errors (loss of packet, unsync, ...)
   TODO: interrupts: subscribe, unsubscribe
         get config_id + timestamp with the interrupt
	 get the event message + context message with the first interrupt
*/

struct wrenrx_msg_param {
  const uint32_t *data;
  unsigned len;
  unsigned off;
};

#ifdef __cplusplus
}
#endif

#endif /* __WREN__WRENRX__H__ */
