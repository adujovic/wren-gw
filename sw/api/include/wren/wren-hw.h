#ifndef __WREN__WREN_HW__H__
#define __WREN__WREN_HW__H__

/* Constants defined by the hardware */

/* Pulser start/stop/clock values */

#define WRENRX_INPUT_EXTERN_1 0	/* External inputs (front-panel) */
#define WRENRX_INPUT_EXTERN_8 7

#define WRENRX_INPUT_PULSER_0 8	/* Output of pulsers in the same group */
#define WRENRX_INPUT_PULSER_7 15

#define WRENRX_INPUT_CLOCK_PPS   16	/* PPS clock */
#define WRENRX_INPUT_CLOCK_1KHZ  17
#define WRENRX_INPUT_CLOCK_1MHZ  18
#define WRENRX_INPUT_CLOCK_10MHZ 19
#define WRENRX_INPUT_CLOCK_40MHZ 20
#define WRENRX_INPUT_CLOCK_1GHZ  31

#define WRENRX_INPUT_NOSTART     31 /* No start input */
#define WRENRX_INPUT_NOSTOP      31 /* No stop input */

/* TODO: RF1-2 Frev, RF1-2 Bunch clock */

#define WREN_PULSERS_PER_GROUP 8
#define WREN_COMPARATORS_PER_GROUP 32

#endif
