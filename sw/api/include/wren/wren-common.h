/* WhiteRabbit Timing common definitions.  */

/** \mainpage
  WRT - WhiteRabbit Timing - is a system to transmit timing events over
  a WhiteRabbit network.

  It is composed of two types of devices: the event transmitters and the
  timing receivers.
*/

#ifndef __WREN_COMMON__H__
#define __WREN_COMMON__H__

#ifdef __KERNEL__
#include <linux/types.h>
#else
#include <stdint.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

/** \file wrt/wrt-common.h
    \brief Definitions used by both the event transmitter and the receiver
*/

/** TAI timestamp.
   Because SEC is a 32b unsigned integer, the largest time is about
   year 2090.
   And it allows to easily handle carry if adding/substracting nanoseconds.
*/
struct wren_ts {
  /** \brief Number of seconds since the epoch */
  uint32_t sec;

  /** \brief Number of nanoseconds */
  uint32_t nsec;
};

/** Add nano seconds to a timestamp.

    \todo add substract
    \todo convert between ts and nanosecond

    @param[in] nsec The number of nanoseconds to be added,  must be less
       than 1 sec.
    @param[in,out] ts The timestamp to be modified.
*/
void wren_add_nsec(struct wren_ts *ts, unsigned nsec);

/** \brief Source identifier

    A source id represent at the frame level an event transmitter.
 */
typedef uint8_t wren_source_id;


/** \brief Context identifier

    An event message can be associated to a context, identified by a number.
    A context identifier is valid only for a specific source.
    There is only a limited number of alive context.
*/
typedef uint8_t wren_context_id;

/** \brief Maximum value for a valid context identifier

    A context id must be between 0 and 127
*/
#define WREN_CONTEXT_MAX 0x7f

/** \brief Special context identifier for no context

    An event message with this context id is not associated to any
    context
*/
#define WREN_CONTEXT_NONE 0xff


/** \brief special context identifier for not yet known

    The wrentx will automatically replace this context with the current
    one. 
 */
#define WREN_CONTEXT_CURRENT 0xfe

/** \brief Event identifier

    This is a number that identifies the event
 */
typedef uint16_t wren_event_id;

/** \brief Maximum event identifier

    The number of event id is limited (in order to allow use of maps).
 */
#define WREN_EVENT_ID_MAX 0x1ff

/* An event id which is invalid (or never used).
   Could be used to return error
 */
#define WREN_EVENT_ID_INVALID 0xffff

/** \brief Parameter identifier

    This is a number that identifies parameters
*/
typedef uint16_t wren_param_id;

/** \brief Maximum parameter identifier value
 */
#define WREN_PARAM_ID_MAX 0x1ff

/* A default ethertype for wren */
#define WREN_ETHERTYPE (('W' << 8) | 'e')

/** \brief Protocol used over WRT
 */
struct wren_protocol {
  /** \brief Protocol selection.
      One of WREN_PROTO_ constant
  */
  uint8_t  proto;

  union {
    struct wren_proto_eth {
      /** \brief VLAN use */
      uint8_t  flags;

      /** \brief MAC address */
      uint8_t  mac[6];

      /** \brief VLAN identifier */
      uint16_t vlan;

      /** \brief Ethertype */
      uint16_t ethertype;

      /** \brief Source identifier for the messages.  */
      wren_source_id source_id;
    } eth;
  } u;
};

/** \brief Raw ethernet protocol

    Need to specify mac, vlan and ethertype.
 */
#define WREN_PROTO_NONE     0
#define WREN_PROTO_ETHERNET 1

/* If the flag is set, filter on the mac address. */
#define WREN_PROTO_ETH_FLAGS_MAC  (1 << 0)

/* If the flag is set, filter on vlan. */
#define WREN_PROTO_ETH_FLAGS_VLAN (1 << 1)

#ifdef __cplusplus
}
#endif

#endif /* __WREN_COMMON__H__ */
