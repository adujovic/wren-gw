#include <sys/ioctl.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include "wren-util.h"
#include "wren-drv.h"
#include "wren-wrtime.h"
#include "wren/wren-packet.h"
#include "wren-mb-defs.h"
#include "wrenrx-core.h"
#include "wrenrx-data.h"
#include "wrenrx-cond.h"
#include "wrenrx-drv.h"
#include "wrenrx-drv-evt.h"
#include "wren-common-utils.h"
#include "wren-mb.h"
#include "wren-ioctl.h"

/* Just in case */
#define wren_rx_sources XXX_sources
#define wren_rx_conds XXX_conds
#define wren_rx_actions XXX_actions

#define NBR_SRCS 4
#define NBR_CTXT 4

struct wrenrx_handle_wrenctl {
    struct wrenrx_handle_op base;

    struct wrenrx_msg msg;

    int wren_fd;
};

static int
wrenrx_drv_wrenctl_read(struct wrenrx_handle *handle,
			struct wren_packet *packet)
{
    return 0;
}

#if 0
int
wrenrx_trigger_interrupt (struct wrenrx_handle *handle,
                        unsigned interrupt_id,
                        struct wrenrx_trigger *trig,
                        int delay_us)
{
  unsigned i;

  for (i = 0; i < NBR_ACTIONS; i++) {
    struct action *act = &handle->act[i];
    if (act->kind != ACT_NONE)
      continue;
    act->kind = ACT_INTER;
    act->trig = *trig;
    act->u.inter.id = interrupt_id;
    act->u.inter.delay_us = delay_us;

    return i;
  }
  return -1;
}
#endif

static int
wrenrx_drv_wren_add_source(struct wrenrx_handle *handle,
			   struct wren_protocol *proto)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    int res;

    res = ioctl(h->wren_fd, WREN_IOC_RX_ADD_SOURCE, proto);
    if (res < 0) {
	if (errno == EINVAL)
	    return WRENRX_ERR_BAD_PROTOCOL;
	if (errno == ENOSPC)
	    return WRENRX_ERR_NO_SOURCE;
	return WRENRX_ERR_SYSTEM;
    }
    return res;
}

static int
wrenrx_drv_wren_del_source(struct wrenrx_handle *handle,
			   unsigned source_idx)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    int res;
    uint32_t idx = source_idx;

    res = ioctl(h->wren_fd, WREN_IOC_RX_DEL_SOURCE, &idx);
    if (res < 0) {
	if (errno == EINVAL)
	    return WRENRX_ERR_SRCID_TOO_LARGE;
	return WRENRX_ERR_SYSTEM;
    }
    return 0;
}

static int
wrenrx_drv_wren_get_source(struct wrenrx_handle *handle,
			   unsigned source_idx,
			   struct wren_protocol *proto)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_get_source src;
    int res;

    src.source_idx = source_idx;
    res = ioctl(h->wren_fd, WREN_IOC_RX_GET_SOURCE, &src);
    if (res < 0) {
	if (errno == EINVAL)
	    return WRENRX_ERR_SRCID_TOO_LARGE;
	return WRENRX_ERR_SYSTEM;;
    }
    memcpy(proto, &src.proto, sizeof(*proto));
    return 0;
}

static int
wrenrx_drv_wren_pulser_define (struct wrenrx_handle *handle,
			       unsigned pulser_num,
			       const struct wrenrx_cond *cond,
			       const struct wrenrx_pulser_configuration *conf,
			       const char *name)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_config cmd;
    int res;

    if (pulser_num == 0)
	return WRENRX_ERR_PULSER_NUM;

    wrenrx_pulser_configuration_to_mb(&cmd.config, conf, pulser_num - 1);

    strncpy(cmd.name, name, sizeof(cmd.name));

    res = wrenrx_cond_to_mb(&cmd.cond, cond);
    if (res < 0)
	return res;

    res = ioctl(h->wren_fd, WREN_IOC_RX_ADD_CONFIG, &cmd);
    if (res < 0) {
	if (errno == EINVAL)
	    return WRENRX_ERR_NO_SOURCE;
	if (errno == ENOSPC)
	    return WRENRX_ERR_NO_CONFIG;
	return WRENRX_ERR_SYSTEM;
    }
    return res;
}

static int
wrenrx_drv_wren_pulser_program (struct wrenrx_handle *handle,
				unsigned pulser_num,
				const struct wrenrx_pulser_configuration *conf,
				const struct wren_ts *time,
				const char *name)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_imm_config cmd;
    int res;

    if (pulser_num == 0)
	return WRENRX_ERR_PULSER_NUM;

    wrenrx_pulser_configuration_to_mb(&cmd.config, conf, pulser_num - 1);

    strncpy(cmd.name, name, sizeof(cmd.name));
    if (time)
      cmd.ts = *time;
    else {
      cmd.ts.sec = 0;
      cmd.ts.nsec = 0;
      cmd.config.immediat = 1;
    }

    res = ioctl(h->wren_fd, WREN_IOC_RX_IMM_CONFIG, &cmd);
    if (res < 0) {
	if (errno == EINVAL)
	    return WRENRX_ERR_NO_SOURCE;
	if (errno == ENOSPC)
	    return WRENRX_ERR_NO_CONFIG;
	return WRENRX_ERR_SYSTEM;
    }
    return res;
}

static int
wrenrx_drv_wren_pulser_reconfigure(struct wrenrx_handle *handle,
				   unsigned config_idx,
				   const struct wrenrx_pulser_configuration *conf)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_mod_config cmd;
    int res;

    cmd.config_idx = config_idx;
    wrenrx_pulser_configuration_to_mb(&cmd.config, conf, 0);

    res = ioctl(h->wren_fd, WREN_IOC_RX_MOD_CONFIG, &cmd);
    if (res < 0) {
	if (errno == EINVAL)
	    return WRENRX_ERR_CFGID_TOO_LARGE;
	if (errno == ENOENT)
	    return WRENRX_ERR_NO_CONFIG;
	return WRENRX_ERR_SYSTEM;
    }

    return 0;
}

static int
wrenrx_drv_wren_pulser_remove(struct wrenrx_handle *handle,
			      unsigned config_idx)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    uint32_t idx = config_idx;
    int res;

    res = ioctl(h->wren_fd, WREN_IOC_RX_DEL_CONFIG, &idx);
    if (res < 0) {
	if (errno == EINVAL)
	    return WRENRX_ERR_CFGID_TOO_LARGE;
	if (errno == ENOENT)
	    return WRENRX_ERR_NO_CONFIG;
	return WRENRX_ERR_SYSTEM;
    }
    return 0;
}

static int
wrenrx_drv_wren_pulser_get_config(struct wrenrx_handle *handle,
				  unsigned config_idx,
				  unsigned *pulser_num,
				  struct wrenrx_pulser_configuration *conf,
				  char *name)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_get_config cfg;
    int res;

    cfg.config_idx = config_idx;
    res = ioctl(h->wren_fd, WREN_IOC_RX_GET_CONFIG, &cfg);
    if (res < 0) {
	if (errno == EINVAL)
	    return WRENRX_ERR_CFGID_TOO_LARGE;
	if (errno == ENOENT)
	    return WRENRX_ERR_NO_CONFIG;
	return WRENRX_ERR_SYSTEM;
    }

    memcpy(name, cfg.config.name, sizeof(cfg.config.name));
    *pulser_num = cfg.config.config.pulser_idx + 1;
    conf->config_en = 1;
    conf->interrupt_en = 1;
    conf->output_en = 1;
    conf->repeat_mode = cfg.config.config.repeat;
    conf->start = cfg.config.config.start;
    conf->stop = cfg.config.config.stop;
    conf->clock = cfg.config.config.clock;
    conf->initial_delay = cfg.config.config.idelay;
    conf->pulse_width = cfg.config.config.width;
    conf->pulse_period = cfg.config.config.period;
    conf->npulses = cfg.config.config.npulses;
    conf->load_offset_sec = cfg.config.config.load_off_sec;
    conf->load_offset_nsec = cfg.config.config.load_off_nsec;
    return 0;
}

union wren_usr_union {
    /* The header contains the kind of message (CMD_ASYNC_xxx) and
       the length */
    union wren_capsule_hdr_un hdr;
    uint32_t buf[512];

    struct wren_usr_ctxt {
	union wren_capsule_hdr_un pdu_hdr;
	struct wren_capsule_ctxt_hdr pdu;
    } ctxt;
    struct wren_usr_pulse {
	union wren_capsule_hdr_un pdu_hdr;
	uint32_t config_id;
	struct wren_ts ts;
	struct wren_capsule_event_hdr evt;
    } pulse;
    struct wren_usr_event {
	union wren_capsule_hdr_un pdu_hdr;
	/* TODO: add subscription-id */
	struct wren_capsule_event_hdr pdu;
    } event;
};

static void
wren_drv_read_pulse(struct wrenrx_handle_wrenctl *h, union wren_usr_union *u)
{
    unsigned len = u->hdr.hdr.len;

    /* Fill msg */
    h->msg.kind = wrenrx_msg_pulse;
    h->msg.u.pulse.ts.sec = u->buf[1];
    h->msg.u.pulse.ts.nsec = u->buf[2];
    h->msg.u.pulse.config_id = u->buf[3] >> 16;
    h->msg.u.pulse.source_idx = u->buf[3] & 0xff;
    if (len == 4) {
	h->msg.u.pulse.evt.ev_id = WREN_EVENT_ID_INVALID;
    } else {
	unsigned hlen = sizeof (struct wren_usr_pulse) / 4;
	h->msg.u.pulse.evt.ev_id = u->pulse.evt.ev_id;
	h->msg.u.pulse.evt.ctxt_id = u->pulse.evt.ctxt_id;
	h->msg.u.pulse.evt.ts.sec = u->pulse.evt.ts.sec;
	h->msg.u.pulse.evt.ts.nsec = u->pulse.evt.ts.nsec;
	h->msg.u.pulse.evt.param_len = len - hlen;
	memcpy(h->msg.u.pulse.evt.params, u->buf + hlen, (len - hlen) * 4);
    }
}

static void
wren_drv_read_ctxt(struct wrenrx_handle_wrenctl *h, union wren_usr_union *u)
{
    /* Save context */
    unsigned len = u->hdr.hdr.len;
    unsigned hlen = sizeof(struct wren_usr_ctxt) / 4;

    h->msg.kind = wrenrx_msg_context;
    h->msg.u.ctxt.source_idx = u->hdr.hdr.pad;
    h->msg.u.ctxt.ctxt_id = u->ctxt.pdu.ctxt_id;
    h->msg.u.ctxt.ctxt.ts.sec = u->ctxt.pdu.valid_from.sec;
    h->msg.u.ctxt.ctxt.ts.nsec = u->ctxt.pdu.valid_from.nsec;
    h->msg.u.ctxt.ctxt.params_len = len - hlen;
    memcpy (h->msg.u.ctxt.ctxt.params, u->buf + hlen, (len - hlen) * 4);
}

static void
wren_drv_read_event(struct wrenrx_handle_wrenctl *h, union wren_usr_union *u)
{
    /* Save context */
    unsigned len = u->hdr.hdr.len;
    unsigned hlen = sizeof(struct wren_usr_event) / 4;

    h->msg.kind = wrenrx_msg_event;
    h->msg.u.evt.source_idx = u->hdr.hdr.pad;
    h->msg.u.evt.ts.sec = 0;
    h->msg.u.evt.ts.nsec = 0;
    h->msg.u.evt.offset_us = 0;
    h->msg.u.evt.evt.ev_id = u->event.pdu.ev_id;
    h->msg.u.evt.evt.ctxt_id = u->event.pdu.ctxt_id;
    h->msg.u.evt.evt.ts.sec = u->event.pdu.ts.sec;
    h->msg.u.evt.evt.ts.nsec = u->event.pdu.ts.nsec;
    h->msg.u.evt.evt.param_len = len - hlen;
    memcpy (h->msg.u.evt.evt.params, u->buf + hlen, (len - hlen) * 4);
}

static struct wrenrx_msg *
wren_drv_wait(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;

    while (1) {
	union wren_usr_union u;
	int len;

	len = read(h->wren_fd, u.buf, sizeof(u.buf));

	if (len < 0 && errno == EIO) {
	    fprintf (stderr, "no sync\n");
	    return NULL;
	}

	/* End of file */
	if (len == 0)
	    return NULL;

	switch (u.hdr.hdr.typ) {
	case CMD_ASYNC_PULSE:
	    wren_drv_read_pulse(h, &u);
	    return &h->msg;
	    break;
	case CMD_ASYNC_CONTEXT:
	    wren_drv_read_ctxt(h, &u);
	    return &h->msg;
	    break;
	case CMD_ASYNC_EVENT:
	    wren_drv_read_event(h, &u);
	    return &h->msg;
	    break;
	default:
	    abort();
	}
    }
}

static int
wren_drv_get_fd(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    return h->wren_fd;
}

static void
wren_drv_close(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    close (h->wren_fd);
}

static int
wrenrx_drv_wren_context_subscribe(struct wrenrx_handle *handle,
				  unsigned source_idx)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    uint32_t idx = source_idx;
    int res;

    res = ioctl(h->wren_fd, WREN_IOC_RX_SUBSCRIBE_CONTEXT, &idx);
    if (res == 0)
	return 0;
    switch (errno) {
    case EINVAL:
	return WRENRX_ERR_BAD_SOURCE;
    default:
	return WRENRX_ERR_NO_CONFIG;
    }
}

static int
wrenrx_drv_wren_context_unsubscribe(struct wrenrx_handle *handle,
				  unsigned source_idx)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    uint32_t idx = source_idx;
    int res;

    res = ioctl(h->wren_fd, WREN_IOC_RX_UNSUBSCRIBE_CONTEXT, &idx);
    if (res == 0)
	return 0;
    switch (errno) {
    case EINVAL:
	return WRENRX_ERR_BAD_SOURCE;
    default:
	return WRENRX_ERR_NO_CONFIG;
    }
}

static int
wrenctl_event_subscribe(struct wrenrx_handle *handle,
			unsigned source_idx,
			uint16_t ev_id,
			int offset_us)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    struct wren_ioctl_event_subscribe msg;
    int res;

    msg.source_idx = source_idx;
    msg.event_id = ev_id;
    msg.offset_us = offset_us;

    res = ioctl(h->wren_fd, WREN_IOC_RX_SUBSCRIBE_EVENT, &msg);
    if (res >= 0)
	return res;
    switch (errno) {
    case EINVAL:
	/* FIXME: could be also invalid source, or source not added */
	return WRENRX_ERR_EVID_TOO_LARGE;
    default:
	return WRENRX_ERR_SYSTEM;
    }
}

static int wrenrx_drv_wren_get_time (struct wrenrx_handle *handle,
					struct wren_ts *time)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    return wren_wren_get_time(h->wren_fd, time);
}

static int wrenrx_drv_wren_subscribe_config(struct wrenrx_handle *handle,
					    unsigned config_idx)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    uint32_t idx = config_idx;
    int res;

    res = ioctl(h->wren_fd, WREN_IOC_RX_SUBSCRIBE_CONFIG, &idx);
    if (res == 0)
	return 0;
    switch (errno) {
    case EINVAL:
	return WRENRX_ERR_CFGID_TOO_LARGE;
    case ENOENT:
    default:
	return WRENRX_ERR_NO_CONFIG;
    }
}

static int wrenrx_drv_wren_unsubscribe_config(struct wrenrx_handle *handle,
					      unsigned config_idx)
{
    struct wrenrx_handle_wrenctl *h = (struct wrenrx_handle_wrenctl *)handle;
    uint32_t idx = config_idx;
    int res;

    res = ioctl(h->wren_fd, WREN_IOC_RX_UNSUBSCRIBE_CONFIG, &idx);
    if (res == 0)
	return 0;
    switch (errno) {
    case EINVAL:
	return WRENRX_ERR_CFGID_TOO_LARGE;
    case ENOENT:
    default:
	return WRENRX_ERR_NO_CONFIG;
    }
}

static const struct wrenrx_operations wrenrx_drv_wren_ops = {
    .close = wren_drv_close,
    .recv_packet = wrenrx_drv_wrenctl_read,
    .get_time = wrenrx_drv_wren_get_time,
    .add_source = wrenrx_drv_wren_add_source,
    .get_source = wrenrx_drv_wren_get_source,
    .del_source = wrenrx_drv_wren_del_source,
    .context_subscribe = wrenrx_drv_wren_context_subscribe,
    .context_unsubscribe = wrenrx_drv_wren_context_unsubscribe,
    .pulser_define = wrenrx_drv_wren_pulser_define,
    .pulser_program = wrenrx_drv_wren_pulser_program,
    .pulser_reconfigure = wrenrx_drv_wren_pulser_reconfigure,
    .pulser_remove = wrenrx_drv_wren_pulser_remove,
    .pulser_get_config = wrenrx_drv_wren_pulser_get_config,
    .event_subscribe = wrenctl_event_subscribe,
    .config_subscribe = wrenrx_drv_wren_subscribe_config,
    .config_unsubscribe = wrenrx_drv_wren_unsubscribe_config,
    .get_fd = wren_drv_get_fd,
    .wait = wren_drv_wait,
};

struct wrenrx_drv_wrenctl_init_args {
    unsigned drv_idx;
};

struct wrenrx_handle *
wrenrx_open(const char *filename)
{
    struct wrenrx_handle_wrenctl *handle;

    handle = malloc(sizeof(*handle));
    if (handle == NULL) {
	fprintf (stderr, "%s: allocate handle\n", progname);
	return NULL;
    }

    handle->base.op = &wrenrx_drv_wren_ops;

    handle->wren_fd = open(filename, O_RDWR);
    if (handle->wren_fd < 0) {
	fprintf (stderr, "cannot open %s: %m\n", filename);
	goto error;
    }

    if (wren_check_version(handle->wren_fd) < 0)
	goto error;

    return (struct wrenrx_handle *)handle;

 error:
    if (handle->wren_fd >= 0)
	close (handle->wren_fd);
    free(handle);
    return NULL;
}

struct wrenrx_handle *
wrenrx_drv_wren_open(void *init)
{
    struct wrenrx_drv_wrenctl_init_args *res = init;
    char name[32];

    snprintf(name, sizeof(name), "/dev/wren%u", res->drv_idx);

    return wrenrx_open(name);
}

void *
wrenrx_drv_wren_init(int *argc, char *argv[])
{
    struct wrenrx_drv_wrenctl_init_args *res;

    res = malloc (sizeof(*res));
    if (res == NULL)
	return NULL;

    /* Save driver index (wrenX) */
    res->drv_idx = argv[1][4] - '0';

    /* Remove wrenX */
    wren_drv_remove_args (argc, argv, 1);

    if (*argc > 1) {
	if (strcmp(argv[1], "--")) {
	    fprintf(stderr, "%s: missing -- after driver name\n", progname);
	    return NULL;
	}
	wren_drv_remove_args (argc, argv, 1);
    }

    return res;
}
