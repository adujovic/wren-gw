#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <sys/poll.h>
#include "wren/wrenrx.h"
#include "wren/wren-hw.h"
#include "wrenrx-cond-parser.h"
#include "wrenrx-tools.h"
#include "wren-drv.h"
#include "wrenrx-drv.h"

static int flag_timestamp;

static enum wrenrx_cond_comp
tok_to_cond (enum tok tok)
{
  switch (tok) {
  case TOK_EQ:
    return WRENRX_EQ;
  default:
    abort();
  }
}

static enum wrenrx_cond_rel
tok_to_oper (enum tok tok)
{
  switch (tok) {
  case TOK_AND:
    return WRENRX_AND;
  case TOK_OR:
    return WRENRX_OR;
  default:
    abort();
  }
}

static struct wrenrx_cond_expr *
tree_to_cond (struct wrenrx_cond *trig, struct wrenrx_cond_tree *t)
{
  struct wrenrx_cond_expr *res;

  if (t == NULL)
    return NULL;

  switch (t->kind) {
  case T_PARAM:
    res = wrenrx_cond_param_u32 (trig, tok_to_cond (t->u.param.op),
				 t->u.param.src == TOK_CTXT_PARAM ? 0 : 1,
				 t->u.param.id, t->u.param.val);
    break;
  case T_BINARY:
    {
      struct wrenrx_cond_expr *l, *r;
      l = tree_to_cond (trig, t->u.binary.l);
      r = tree_to_cond (trig, t->u.binary.r);
      if (l == NULL || r == NULL)
        return NULL;
      res = wrenrx_cond_rel(trig, tok_to_oper(t->u.binary.op), l, r);

      break;
    }
  default:
    abort();
  }

  free(t);
  return res;
}

static void
print_parameters (struct wrenrx_msg_param *it)
{
  while (wrenrx_msg_param_is_valid(it)) {
    wren_param_id id;
    unsigned dt;

    id = wrenrx_msg_param_get_id (it);
    dt = wrenrx_msg_param_get_dt (it);
    switch (dt) {
    case PKT_PARAM_S32:
      printf (" %u s32 %d\n", id, wrenrx_msg_param_get_s32(it));
      break;
    default:
      printf (" %u %u ??\n", id, dt);
      break;
    }
    wrenrx_msg_param_next(it);
  }
}

static void
disp_time (unsigned long long sec, unsigned nsec)
{
    struct tm tm;
    time_t t;
    char atime[32];
    unsigned ns, us, ms;

    ns = nsec;
    ms = ns / 1000000;
    ns -= ms * 1000000;

    us = ns / 1000;
    ns -= us * 1000;

    t = sec;
    if (gmtime_r (&t, &tm) == &tm)
	strftime (atime, sizeof(atime), "%D %T", &tm);
    else
	strcpy(atime, "(error)");

    printf("%llu (%s) + %ums %uus %uns",
	   sec, atime, ms, us, ns);
}

static void
print_event(struct wrenrx_msg *msg)
{
  struct wrenrx_msg_param it;
  struct wren_ts ts;

  wrenrx_msg_get_event_ts(msg, &ts);

  printf ("event src-idx: %u, id: %u, ctxt: %u, ts: ",
	  wrenrx_msg_get_source_idx(msg),
	  wrenrx_msg_get_event_id(msg),
	  wrenrx_msg_get_context_id(msg));
  disp_time(ts.sec, ts.nsec);
  printf ("\n");

  wrenrx_msg_get_event_params(msg, &it);
  printf("event parameters:\n");
  print_parameters (&it);
  wrenrx_msg_get_context_params(msg, &it);
  printf("context parameters:\n");
  print_parameters (&it);
}

static void
print_context(struct wrenrx_msg *msg)
{
  struct wrenrx_msg_param it;
  struct wren_ts ts;

  wrenrx_msg_get_context_ts(msg, &ts);

  printf ("> context src-idx: %u, id: %u, ts: %us+%uns\n",
	  wrenrx_msg_get_source_idx(msg),
	  wrenrx_msg_get_context_id(msg),
	  ts.sec, ts.nsec);

  wrenrx_msg_get_context_params(msg, &it);
  printf("context parameters:\n");
  print_parameters (&it);
}

static void
print_pulse(struct wrenrx_msg *msg)
{
  struct wrenrx_msg_param it;
  struct wren_ts ts;
  unsigned ev_id;

  wrenrx_msg_get_pulse_ts(msg, &ts);
  ev_id = wrenrx_msg_get_event_id(msg);

  printf ("> pulse for config %u, ev: %u, ts: %us+%uns\n",
	  wrenrx_msg_get_config_id (msg),
	  ev_id, ts.sec, ts.nsec);
  if (ev_id == WREN_EVENT_ID_INVALID)
    return;
  wrenrx_msg_get_event_params(msg, &it);
  printf("event parameters:\n");
  print_parameters (&it);
}

static int
handle_wren(struct wrenrx_handle *handle)
{
  struct wrenrx_msg *msg;
  enum wrenrx_msg_kind kind;
  struct wren_ts now;

  msg = wrenrx_wait(handle);
  if (msg == NULL)
    return 0;

  if (flag_timestamp) {
    if (wrenrx_get_time(handle, &now) == 0) {
      printf ("At ");
      disp_time (now.sec, now.nsec);
      printf("\n");
    }
  }


  kind = wrenrx_get_msg (msg);
  switch (kind) {
  case wrenrx_msg_network_error:
    printf ("> network_error\n");
    break;
  case wrenrx_msg_pulse:
    print_pulse(msg);
    break;
  case wrenrx_msg_event:
    print_event(msg);
    break;
  case wrenrx_msg_context:
    print_context(msg);
    break;
  }
  return 1;
}

static int
do_disp_one_source(struct wrenrx_handle *handle, unsigned idx)
{
    struct wren_protocol proto;
    int res;

    res = wrenrx_get_source(handle, idx, &proto);
    if (res != 0) {
	if (res != WRENRX_ERR_SRCID_TOO_LARGE)
	    printf ("do_disp_source error: %d\n", res);
	return 0;
    }
    printf ("%u: ", idx);
    disp_protocol (&proto);
    return 1;
}

static int
do_disp_source (struct wrenrx_handle *handle, char *argv[], int argc)
{
    unsigned idx;

    if (argc == 2) {
	char *e;
	idx = strtoul(argv[1], &e, 10);
	if (*e != 0) {
	    fprintf(stderr, "bad value for source idx (%s)\n", argv[1]);
	    return -1;
	}
	do_disp_one_source(handle, idx);
	return 2;
    } else if (argc == 1) {
	for (idx = 0; ; idx++)
	    if (do_disp_one_source(handle, idx) <= 0)
		break;
	return 1;
    } else {
	fprintf(stderr, "usage: disp-source [IDX]\n");
	return -1;
    }
}

static int
do_add_source (struct wrenrx_handle *handle, char *argv[], int argc)
{
    struct wren_protocol proto;
    int res;

    if (argc != 2) {
	fprintf(stderr, "usage: add-source SRC-ID\n");
	return -1;
    }

    if (parse_protocol(argv + 1, argc - 1, &proto) < 0)
	return -1;

    res = wrenrx_add_source(handle, &proto);
    if (res < 0)
	printf("error: %d\n", res);
    else
	printf("source id: %d\n", res);

    return 1;
}

static int
do_del_source (struct wrenrx_handle *handle, char *argv[], int argc)
{
    uint32_t idx;
    char *e;
    int res;

    if (argc != 2) {
	fprintf(stderr, "usage: del-source SRC-IDX\n");
	return -1;
    }

    idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for idx (%s)\n", argv[1]);
	return -1;
    }

    res = wrenrx_remove_source(handle, idx);
    if (res != 0)
	printf("error: %d\n", res);

    return 2;
}

static int
do_disp_one_config(struct wrenrx_handle *handle, unsigned idx)
{
    struct wrenrx_pulser_configuration conf;
    char name[WRENRX_CONFIGURATION_NAME_MAXLEN];
    unsigned pulser_num;
    int res;

    res = wrenrx_pulser_get_config(handle, idx, &pulser_num, &conf, name);
    if (res != 0) {
	if (res == WRENRX_ERR_CFGID_TOO_LARGE)
	    return -2;
	if (res == WRENRX_ERR_NO_CONFIG)
	    return 0;
	printf ("do_disp_one_config error: %d\n", res);
	return -1;
    }
    printf ("%u: %s\n", idx, name);
    printf ("  pulser:%u en:%u int:%u out:%u repeat:%u",
	    pulser_num, conf.config_en, conf.interrupt_en, conf.output_en,
	    conf.repeat_mode);
    printf (" start:%s stop:%s clock:%s\n",
	    get_input_name(conf.start),
	    get_input_name(conf.stop),
	    get_input_name(conf.clock));
    printf ("  init-delay:%u pulse-width:%u pulse-period:%u npulses:%u\n",
	    conf.initial_delay, conf.pulse_width, conf.pulse_period,
	    conf.npulses);
    printf ("  load-offset:%dsec+%dnsec\n",
	    (int)conf.load_offset_sec, (int)conf.load_offset_nsec);
    return 1;
}

static int
do_disp_config (struct wrenrx_handle *handle, char *argv[], int argc)
{
    unsigned idx;

    if (argc == 2) {
	char *e;
	idx = strtoul(argv[1], &e, 10);
	if (*e != 0) {
	    fprintf(stderr, "bad value for config idx (%s)\n", argv[1]);
	    return -1;
	}
	do_disp_one_config(handle, idx);
	return 2;
    } else if (argc == 1) {
	for (idx = 0; ; idx++)
	    if (do_disp_one_config(handle, idx) < 0)
		break;
	return 1;
    } else {
	fprintf(stderr, "usage: disp-config [IDX]\n");
	return -1;
    }
}

static void
init_pulser_config (struct wrenrx_pulser_configuration *conf)
{
    conf->config_en = 1;
    conf->interrupt_en = 1;
    conf->output_en = 1;
    conf->repeat_mode = 0;
    conf->start = WRENRX_INPUT_NOSTART;
    conf->stop  = WRENRX_INPUT_NOSTOP;
    conf->clock = WRENRX_INPUT_CLOCK_1GHZ;
    conf->initial_delay = 0;
    conf->pulse_width = 8;
    conf->pulse_period = 125;
    conf->npulses = 1;
    conf->load_offset_sec = 0;
    conf->load_offset_nsec = 0;
}

static int
parse_pulser_clock(const char *opt, const char *arg, unsigned *cfg)
{
    int sig = parse_input_name(arg);
    if (arg < 0) {
	fprintf (stderr, "incorrect value for option %s (%s)\n", opt, arg);
	return -1;
    }
    *cfg = sig;
    return 0;
}

static int
parse_pulser_config(struct wrenrx_pulser_configuration *conf,
		    const char *opt, const char *arg)
{
    long val;
    char *e;

    if (opt[0] != '-') {
	fprintf(stderr, "incorrect option %s\n", opt);
	return -1;
    }

    if (arg == NULL) {
	fprintf(stderr, "missing value for option %s\n", opt);
	return -1;
    }

    if (!strcmp(opt, "-start"))
	return parse_pulser_clock (opt, arg, &conf->start);
    else if (!strcmp(opt, "-stop"))
	return parse_pulser_clock (opt, arg, &conf->stop);
    else if (!strcmp(opt, "-clock"))
	return parse_pulser_clock (opt, arg, &conf->clock);

    val = strtol(arg, &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value option %s (%s)\n", opt, arg);
	return -1;
    }

    if (!strcmp(opt, "-width"))
	conf->pulse_width = val;
    else if (!strcmp(opt, "-period"))
	conf->pulse_period = val;
    else if (!strcmp(opt, "-idelay"))
	conf->initial_delay = val;
    else if (!strcmp(opt, "-n"))
	conf->npulses = val;
    else if (!strcmp(opt, "-r"))
	conf->repeat_mode = val;
    else if (!strcmp(opt, "-sec"))
	conf->load_offset_sec = val;
    else if (!strcmp(opt, "-ns"))
	conf->load_offset_nsec = val;
    else {
	fprintf(stderr, "unknown option %s\n", opt);
	return -1;
    }

    return 0;
}

static void
help_pulser_config(const struct wrenrx_pulser_configuration *cfg)
{
    fprintf (stderr,
	     " -start VAL   start input [%s]\n", get_input_name(cfg->start));
    fprintf (stderr,
	     " -stop VAL    stop input [%s]\n", get_input_name(cfg->stop));
    fprintf (stderr,
	     " -clock VAL   clock input [%s]\n", get_input_name(cfg->clock));
    fprintf (stderr,
	     " -width VAL   pulse width [%u]\n", cfg->pulse_width);
    fprintf (stderr,
	     " -period VAL  pulse period [%u]\n", cfg->pulse_period);
    fprintf (stderr,
	     " -idelay VAL  initial delay [%u]\n", cfg->initial_delay);
    fprintf (stderr,
	     " -n VAL       number of pulses [%u]\n", cfg->npulses);
    fprintf (stderr,
	     " -r VAL       repeat mode [%u]\n", cfg->repeat_mode);
    fprintf (stderr,
	     " -sec VAL     second part of load time offset\n");
    fprintf (stderr,
	     " -ns VAL      nsecond part of load time offset (def: 0)\n");
}

static int
do_pulser_define (struct wrenrx_handle *handle, char *argv[], int argc)
{
    struct wrenrx_pulser_configuration conf;
    unsigned pulser_num;
    unsigned source_idx;
    wren_event_id ev_id;
    char name[WRENRX_CONFIGURATION_NAME_MAXLEN];
    int res;
    char *e;
    struct wrenrx_cond *cond;
    struct wrenrx_cond_expr *expr;
    unsigned narg;

    init_pulser_config (&conf);

    /* Default: 1 sec */
    conf.load_offset_sec = 1;

    if (argc < 4) {
	fprintf(stderr, "usage: pulser-define PULSER-NUM SRC-IDX EVT-ID "
		"[-name NAME] [CONFIG] [COND]\n");
	help_pulser_config (&conf);
	return -1;
    }

    pulser_num = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for pulser IDX (%s)\n", argv[1]);
	return -1;
    }

    source_idx = strtoul(argv[2], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for SRC-IDX (%s)\n", argv[2]);
	return -1;
    }

    ev_id = strtoul(argv[3], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for EVT-ID (%s)\n", argv[3]);
	return -1;
    }

    narg = 4;

    if (narg + 1 < argc && !strcmp(argv[narg], "-name")) {
	strncpy (name, argv[narg + 1], sizeof(name));
	narg += 2;
    }
    else
	memset(name, 0, sizeof(name));

    while (narg + 1 < argc && argv[narg][0] == '-') {
	const char *opt = argv[narg];
	const char *arg = argv[narg + 1];

	if (parse_pulser_config(&conf, opt, arg) != 0)
	    return -1;

	narg += 2;
    }

    cond = wrenrx_cond_alloc (handle);

    if (narg < argc) {
	struct wrenrx_cond_tree *etree = wrenrx_cond_parse_expr(argv[narg]);
	if (etree == NULL)
	    return -1;

	expr = wrenrx_cond_tree_to_cond_expr(cond, etree);
	narg++;
    }
    else
	expr = NULL;

    res = wrenrx_cond_define(cond, source_idx, ev_id, expr);
    if (res < 0)
	return res;

    res = wrenrx_pulser_define(handle, pulser_num, cond, &conf, name);

    wrenrx_cond_free(cond);

    if (res < 0)
	printf("error: %d\n", res);
    else
	printf("config-idx: %d\n", res);

    return 1;
}

static int
do_pulser_program (struct wrenrx_handle *handle, char *argv[], int argc)
{
    struct wrenrx_pulser_configuration conf;
    unsigned pulser_num;
    char name[WRENRX_CONFIGURATION_NAME_MAXLEN];
    int res;
    char *e;
    unsigned narg;

    init_pulser_config (&conf);

    /* Default: 1 sec */
    conf.load_offset_sec = 1;

    if (argc < 2) {
	fprintf(stderr, "usage: pulser-program PULSER-NUM "
		"[-name NAME] [CONFIG]\n");
	help_pulser_config (&conf);
	return -1;
    }

    pulser_num = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for pulser IDX (%s)\n", argv[1]);
	return -1;
    }

    narg = 2;

    if (narg + 1 < argc && !strcmp(argv[narg], "-name")) {
	strncpy (name, argv[narg + 1], sizeof(name));
	narg += 2;
    }
    else
	memset(name, 0, sizeof(name));

    while (narg + 1 < argc && argv[narg][0] == '-') {
	const char *opt = argv[narg];
	const char *arg = argv[narg + 1];

	if (parse_pulser_config(&conf, opt, arg) != 0)
	    return -1;

	narg += 2;
    }

    res = wrenrx_pulser_program(handle, pulser_num, &conf, NULL, name);

    if (res < 0)
	printf("error: %d\n", res);
    else
	printf("config-idx: %d\n", res);

    return 1;
}

static int
do_pulser_reconfigure (struct wrenrx_handle *handle, char *argv[], int argc)
{
    struct wrenrx_pulser_configuration conf;
    unsigned config_idx;
    int res;
    char *e;
    unsigned narg;

    init_pulser_config (&conf);

    /* Default: 1 sec */
    conf.load_offset_sec = 1;

    if (argc < 2) {
	fprintf(stderr, "usage: pulser-reconfigure CONFIG-IDX [CONFIG]\n");
	help_pulser_config (&conf);
	return -1;
    }

    config_idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for config IDX (%s)\n", argv[1]);
	return -1;
    }

    narg = 2;

    while (narg + 1 < argc && argv[narg][0] == '-') {
	const char *opt = argv[narg];
	const char *arg = argv[narg + 1];

	if (parse_pulser_config(&conf, opt, arg) != 0)
	    return -1;

	narg += 2;
    }

    res = wrenrx_pulser_reconfigure(handle, config_idx, &conf);

    if (res < 0)
	printf("error: %d\n", res);

    return 1;
}

static int
do_pulser_remove (struct wrenrx_handle *handle, char *argv[], int argc)
{
    unsigned config_idx;
    int res;
    char *e;

    if (argc != 2) {
	fprintf(stderr, "usage: pulser-remove CONFIG-IDX\n");
	return -1;
    }

    config_idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for config IDX (%s)\n", argv[1]);
	return -1;
    }

    res = wrenrx_pulser_remove(handle, config_idx);
    if (res < 0)
	printf("error: %d\n", res);
    return 0;
}

static int
do_subscribe_unsubscribe_config (struct wrenrx_handle *handle,
				 unsigned on_off,
				 char *argv[], int argc)
{
    unsigned config_idx;
    int res;
    char *e;

    if (argc != 2) {
	fprintf(stderr, "usage: subscribe-config CONFIG-IDX\n");
	return -1;
    }

    config_idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for config IDX (%s)\n", argv[1]);
	return -1;
    }

    if (on_off)
	res = wrenrx_subscribe_config(handle, config_idx);
    else
	res = wrenrx_unsubscribe_config(handle, config_idx);
    if (res < 0)
	printf("error: %d\n", res);
    return 0;
}

static int
do_subscribe_config (struct wrenrx_handle *handle,
		     char *argv[], int argc)
{
    return do_subscribe_unsubscribe_config(handle, 1, argv, argc);
}

static int
do_unsubscribe_config (struct wrenrx_handle *handle,
		       char *argv[], int argc)
{
    return do_subscribe_unsubscribe_config(handle, 0, argv, argc);
}

static int do_help(struct wrenrx_handle *handle, char *argv[], int argc);

struct command_t {
  const char *name;
  /* Return the number of args consumed, argv[0] is the command name.  */
  int (*func)(struct wrenrx_handle *handle, char *argv[], int argc);
  const char *help;
};

static const struct command_t commands[] = {
  {"help", do_help, "print this help" },

  {"disp-source", do_disp_source, "disp one or all source" },
  {"add-source", do_add_source, "add a new rx source" },
  {"del-source", do_del_source, "delete an rx source" },

  {"disp-config", do_disp_config, "print rx config" },
  {"pulser-define", do_pulser_define, "define a pulser config" },
  {"pulser-program", do_pulser_program, "program a pulser" },
  {"pulser-remove", do_pulser_remove, "remove a config" },
  {"pulser-reconfigure", do_pulser_reconfigure, "modify a config" },
  {"subscribe-config", do_subscribe_config, "subscribe to a config" },
  {"unsubscribe-config", do_unsubscribe_config, "unsubscribe to a config" },
  { NULL, NULL, NULL }
};

static int do_help(struct wrenrx_handle *handle, char *argv[], int argc)
{
  unsigned i;

  for (i = 0; commands[i].name; i++)
    printf ("%-10s  %s\n", commands[i].name, commands[i].help);

  return 0;
}

static int
handle_stdin(struct wrenrx_handle *handle)
{
  char buf[128];
  int len;
  char *argv[16];
  int argc;
  char *saveptr;
  const struct command_t *cmd;

  len = read(0, buf, sizeof(buf) - 1);
  if (len <= 0) {
      printf ("\n");
      return len;
  }
  buf[len] = 0;

  for (argc = 0; ;argc++) {
    argv[argc] = strtok_r(argc == 0 ? buf : NULL, " \t\n\r", &saveptr);
    if (argv[argc] == NULL)
      break;
  }

  if (0) {
      printf ("Read (%d, %d): ", len, argc);
      for (int i = 0; i < argc; i++)
	  printf ("%s|", argv[i]);
      printf("\n");
  }

  if (argc == 0)
    return 1;

  for (cmd = commands; cmd->name; cmd++)
    if (strcmp (argv[0], cmd->name) == 0)
      break;

  if (cmd->name == NULL) {
    fprintf(stderr, "unknown command %s, try help\n", argv[0]);
    return 1;
  }
  else {
    int res = cmd->func (handle, argv, argc);
    if (res < 0) {
      fprintf(stderr, "error in command %s\n", argv[0]);
    }
  }

  return 1;
}

int
main(int argc, char *argv[])
{
  struct wrenrx_cond *cond;
  unsigned nbr_ev;
  int c;
  struct wrenrx_handle *handle;
  struct wrenrx_pulser_configuration pulser_conf;
  struct wren_protocol proto;
  int src;
  struct my_opt { char opt; char *arg; };
  struct my_opt *nargv;
  int nargc;
  void *init;
  unsigned inc_src;
  unsigned flag_i = 0;
  int fd;

  init = wrenrx_drv_init(&argc, argv);
  if (init == NULL)
    return 1;

  nargv = malloc (argc * sizeof (struct my_opt));
  nargc = 0;

  handle = wrenrx_drv_open(init);
  if (handle == NULL)
    return 1;

  pulser_conf.start = WRENRX_INPUT_NOSTART;
  pulser_conf.stop = WRENRX_INPUT_NOSTOP;
  pulser_conf.clock = WRENRX_INPUT_CLOCK_1GHZ;
  pulser_conf.pulse_width = 3;
  pulser_conf.pulse_period = 31;
  pulser_conf.npulses = 10;
  pulser_conf.load_offset_sec = 1;
  pulser_conf.load_offset_nsec = 0;

  nbr_ev = 0;
  inc_src = 0;

  proto.proto = WREN_PROTO_NONE;
  proto.u.eth.flags = 0;
  proto.u.eth.ethertype = WREN_ETHERTYPE;
  proto.u.eth.source_id = 0;

  while ((c = getopt(argc, argv, "e:s:S:c:Iit")) > 0)
    switch (c) {
    case 'e':
    case 's':
    case 'c':
      nargv[nargc].opt = c;
      nargv[nargc].arg = optarg;
      nargc++;
      break;
    case 'S':
      {
	char *e;
	int id = strtoul(optarg, &e, 0);
	if (*e != 0) {
	  fprintf(stderr, "cannot parse source id in '%s'\n", optarg);
	  return 1;
	}
	proto.proto = WREN_PROTO_ETHERNET;
	proto.u.eth.source_id = id;
      }
      break;
    case 'I':
      {
	  struct wren_protocol proto2;

	  proto2.proto = WREN_PROTO_ETHERNET;
	  proto2.u.eth.flags = 0;
	  proto2.u.eth.ethertype = WREN_ETHERTYPE;
	  proto2.u.eth.source_id = 64 + inc_src++;

	  src = wrenrx_add_source (handle, &proto2);
	  if (src < 0) {
	    fprintf(stderr, "cannot add source: %d\n", src);
	    exit(1);
	  }
      }
      break;
    case 'i':
      flag_i++;
      break;
    case 't':
      flag_timestamp++;
      break;
    default:
      printf ("usage: %s RECV -- [-i] [-S ID] {-I} {-e EVENT,COND} {-s EVENT}\n",
	      progname);
      printf ("Options:\n"
	      " -i        interractive (use poll)\n"
	      " -S ID     specify source id\n"
	      " -s EVENT  subscribe to EVENT id\n"
	      " -c CFG    subscribe to CFG id\n"
	      " -e EV,CD  generate a pulse on condition CD for event EV\n"
	      " -I        Increment source index (by adding a source 64+)\n");
      return 1;
    }

  if (proto.proto != WREN_PROTO_NONE) {
    src = wrenrx_add_source (handle, &proto);
    if (src < 0) {
      fprintf(stderr, "cannot add source: %d\n", src);
      exit(1);
    }
  }

  cond = wrenrx_cond_alloc(handle);

  for (unsigned i = 0; i < nargc; i++) {
    char *arg = nargv[i].arg;

    switch (nargv[i].opt) {
    case 'e': {
      struct wrenrx_cond_tree_event *event;
      struct wrenrx_cond_expr *expr;
      int res;

      event = parse_cond(arg);
      if (event == NULL)
	return 1;

      expr = tree_to_cond(cond, event->expr);

      /* Override the domain */
      event->domain = src;

      wrenrx_cond_define (cond, event->domain, event->ev_id, expr);
      res = wrenrx_pulser_define (handle, nbr_ev++, cond, &pulser_conf, NULL);
      if (res < 0) {
	fprintf (stderr, "cannot define a pulser (err=%d)\n", res);
	return 1;
      }

      free(event);
      wrenrx_cond_reinit(handle, cond);
      break;
    }

    case 's': {
      char *e;
      unsigned id;
      int res;

      id = strtoul(arg, &e, 0);
      if (*e != 0) {
	fprintf (stderr, "cannot parse event id '%s'\n", arg);
	return 1;
      }
      res = wrenrx_event_subscribe(handle, src, id, 0);
      if (res < 0) {
	fprintf(stderr, "cannot subscribe to event %u (err=%d)\n", id, res);
	return 1;
      }
      break;
    }

    case 'c': {
      char *e;
      unsigned id;
      int res;

      id = strtoul(arg, &e, 0);
      if (*e != 0) {
	fprintf (stderr, "cannot parse config id '%s'\n", arg);
	return 1;
      }
      res = wrenrx_subscribe_config(handle, id);
      if (res < 0) {
	fprintf(stderr, "cannot subscribe to config %u (err=%d)\n", id, res);
	return 1;
      }
      break;
    }

    default:
      abort();
    }
  }

  if (flag_i) {
    fd = wrenrx_get_fd(handle);
    if (fd < 0) {
      fprintf (stderr, "cannot get file handle: %m\n");
      return 1;
    }
  }

  if (flag_i) {
    struct pollfd pfd[2];
    int res;

    pfd[0].fd = fd;
    pfd[0].events = POLLIN;
    pfd[1].fd = 0;
    pfd[1].events = POLLIN;

    printf ("cmd> ");
    fflush(stdout);
    while (1) {
      res = poll(pfd, 2, -1);
      if (res < 0) {
	fprintf(stderr, "poll error: %m\n");
	return 1;
      }
      if (res == 0)
	printf ("timeout\n");
      else if (pfd[0].revents & POLLIN) {
	if (handle_wren(handle) <= 0)
	  break;
      }
      else if (pfd[1].revents & POLLIN) {
	if (handle_stdin(handle) <= 0)
	  break;
	printf ("cmd> ");
	fflush(stdout);
      }
    }
  }
  else {
    while (handle_wren(handle) > 0)
      ;
  }

  wrenrx_cond_free(cond);
  wrenrx_close(handle);
  return 0;
}
