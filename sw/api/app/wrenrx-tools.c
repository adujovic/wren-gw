#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "wrenrx-tools.h"
#include "wren/wren-hw.h"

void
disp_protocol(const struct wren_protocol *proto)
{
    switch (proto->proto) {
    case WREN_PROTO_NONE:
	printf ("(unused)\n");
	break;
    case WREN_PROTO_ETHERNET: {
	const struct wren_proto_eth *peth = &proto->u.eth;
	printf ("eth, flags: %02x", peth->flags);
	printf (", ethertype: %04x", peth->ethertype);
	printf (", source_id: %02x", peth->source_id);
	if (peth->flags & WREN_PROTO_ETH_FLAGS_MAC)
	    printf (", mac: %02x:%02x:%02x:%02x:%02x:%02x",
		    peth->mac[0], peth->mac[1], peth->mac[2],
		    peth->mac[3], peth->mac[4], peth->mac[5]);
	putchar('\n');
	break;
    }
    default:
	printf("?? (%u)\n", proto->proto);
	break;
    }
}

int
parse_protocol(char *argv[], int argc, struct wren_protocol *proto)
{
    char *e;

    if (argc != 1) {
	fprintf(stderr, "missing SRC-ID for protocol\n");
	return -1;
    }

    proto->proto = WREN_PROTO_ETHERNET;
    proto->u.eth.flags = 0;
    memset(proto->u.eth.mac, 0x0, 6);
    proto->u.eth.vlan = 0;
    proto->u.eth.ethertype = ('W' << 8) | 'e';

    proto->u.eth.source_id = strtoul(argv[0], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for id (%s)\n", argv[0]);
	return -1;
    }

    return 0;
}

static const char *inp_names[] =
{
    "ext1",
    "ext2",
    "ext3",
    "ext4",
    "ext5",
    "ext6",
    "ext7",
    "ext8",

    "out0",
    "out1",
    "out2",
    "out3",
    "out4",
    "out5",
    "out6",
    "out7",

    "pps",
    "1Khz",
    "1Mhz",
    "10Mhz",
    "40Mhz",
    "21",
    "22",
    "23",

    "24",
    "25",
    "26",
    "27",
    "28",
    "29",
    "30",
    "no"
};

int
parse_input_name(const char *arg)
{
    unsigned i;

    for (i = 0; i < 32; i++)
	if (!strcmp(arg, inp_names[i]))
	    return i;

    if (!strcmp(arg, "1GHz"))
	return WRENRX_INPUT_CLOCK_1GHZ;
    return -1;
}

const char *get_input_name(unsigned idx)
{
  if (idx < 32)
    return inp_names[idx];
  return NULL;
}

const char *get_clock_name(unsigned idx)
{
  if (idx < 31)
    return inp_names[idx];
  if (idx == WRENRX_INPUT_CLOCK_1GHZ)
    return "1GHz";
  return NULL;
}

#if 0
unsigned
wrenrx_cond_to_words(struct wrenrx_cond_tree *t,
		     union wren_rx_cond_word *data)
{
    switch(t->kind) {
    case T_BINARY: {
	unsigned len;
	struct wren_rx_cond_op op;

	len = wrenrx_cond_to_words(t->u.binary.l, data);
	len += wrenrx_cond_to_words(t->u.binary.r, data + len);

	switch(t->u.binary.op) {
	case TOK_AND:
	    op.op = WREN_OP_AND;
	    break;
	case TOK_OR:
	    op.op = WREN_OP_OR;
	    break;
	default:
	    abort();
	}
	op.param_id = 0;
	op.param_src = 0;

	data[len++].op = op;
	return len;
    }
    case T_PARAM: {
	struct wren_rx_cond_op op;

	switch(t->u.param.op) {
	case TOK_EQ:
	    op.op = WREN_OP_EQ;
	    break;
	default:
	    abort();
	}
	op.param_id = t->u.param.id;
	op.param_src = t->u.param.src == TOK_EVT_PARAM ? 1 : 0;
	data[0].op = op;
	data[1].vu32 = t->u.param.val;
	return 2;
    }
    default:
	abort();
    }
}
#endif

struct wrenrx_cond_expr *
wrenrx_cond_tree_to_cond_expr(struct wrenrx_cond *cond,
			      struct wrenrx_cond_tree *t)
{
    switch(t->kind) {
    case T_BINARY: {
	struct wrenrx_cond_expr *l, *r;
	enum wrenrx_cond_rel op;

	l = wrenrx_cond_tree_to_cond_expr(cond, t->u.binary.l);
	r = wrenrx_cond_tree_to_cond_expr(cond, t->u.binary.r);

	switch(t->u.binary.op) {
	case TOK_AND:
	    op = WRENRX_AND;
	    break;
	case TOK_OR:
	    op = WRENRX_OR;
	    break;
	default:
	    abort();
	}

	return wrenrx_cond_rel(cond, op, l, r);
    }
    case T_PARAM: {
	enum wrenrx_cond_comp cmp;

	switch(t->u.param.op) {
	case TOK_EQ:
	    cmp = WRENRX_EQ;
	    break;
	default:
	    abort();
	}

	return wrenrx_cond_param_u32(cond, cmp,
				     t->u.param.src == TOK_EVT_PARAM ? 1 : 0,
				     t->u.param.id, t->u.param.val);
    }
    default:
	abort();
    }
}
