#include "wren/wrenrx.h"
#include "wrenrx-cond-parser.h"

void disp_protocol(const struct wren_protocol *proto);

int parse_protocol(char *argv[], int argc, struct wren_protocol *proto);

int parse_input_name(const char *arg);
const char *get_input_name(unsigned idx);
const char *get_clock_name(unsigned idx);

#if 0
unsigned wrenrx_cond_to_words(struct wrenrx_cond_tree *t,
			      union wren_rx_cond_word *data);
#endif

struct wrenrx_cond_expr *
wrenrx_cond_tree_to_cond_expr(struct wrenrx_cond *cond,
			      struct wrenrx_cond_tree *t);
