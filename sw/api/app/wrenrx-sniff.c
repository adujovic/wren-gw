#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include "wren/wren-packet.h"
#include "wren-util.h"
#include "wren-drv.h"
#include "wrenrx-drv.h"

void
disp_params_text(uint32_t *buf, unsigned len)
{
  unsigned off = 0;
  while (off < len) {
    uint32_t hdr = buf[off++];
    unsigned t = WREN_PACKET_PARAM_GET_DT(hdr);
    unsigned l = WREN_PACKET_PARAM_GET_LEN(hdr);
    printf (" param 0x%03x", WREN_PACKET_PARAM_GET_TYP(hdr));
    switch (t) {
    case PKT_PARAM_U32:
      printf (" u32 %u", buf[off]);
      break;
    case PKT_PARAM_S32:
      printf (" s32 %d", (int32_t)buf[off]);
      break;
    default:
      printf (" unknown(%u)", t);
      break;
    }
    printf ("\n");
    off += l - 1;
  }
}

static void
disp_packet_text(const struct wren_packet *pkt)
{
  unsigned off;
  unsigned len;

  if (pkt->hdr.version != PKT_VERSION) {
    printf ("# bad version: %02x\n", pkt->hdr.version);
    return;
  }

  len = pkt->hdr.len - sizeof (struct wren_packet_hdr) / 4;

  for (off = 0; off < len; ) {
    struct wren_capsule_hdr *hdr = (struct wren_capsule_hdr *)&pkt->b[off * 4];

    switch(hdr->typ) {
    case PKT_CTXT:
      {
        const unsigned l = sizeof (struct wren_capsule_ctxt_hdr) / 4;
        struct wren_capsule_ctxt_hdr *ctxt =
	  (struct wren_capsule_ctxt_hdr *)&pkt->b[off * 4];

        printf ("ctxt %u\n", ctxt->ctxt_id);
        disp_params_text ((uint32_t *)(pkt->b + (off + l) * 4), hdr->len - l);
        off += hdr->len;
      }
      break;
    case PKT_EVENT:
      {
        const unsigned l = sizeof (struct wren_capsule_event_hdr) / 4;
        struct wren_capsule_event_hdr *ev =
	  (struct wren_capsule_event_hdr *)&pkt->b[off * 4];
	
        printf ("event 0x%03x %u %u\n",
		ev->ev_id, ev->ctxt_id, ev->ts.nsec / 1000);
        disp_params_text ((uint32_t *)(pkt->b + (off + l) * 4), hdr->len - l);
        off += hdr->len;
      }
      break;
    default:
      return; // TODO
      break;
    }
  }
  printf ("send\n");
}

int
main(int argc, char *argv[])
{
  void *init;
  struct wrenrx_handle *handle;
  struct wren_packet pkt;
  int flag_hex = 0;
  int flag_text = 0;
  int flag_verbose = 0;
  int c;

  init = wrenrx_drv_init(&argc, argv);
  if (init == NULL)
    return 1;

  while ((c = getopt(argc, argv, "xtv")) > 0)
    switch (c) {
    case 'x':
      flag_hex = 1;
      break;
    case 't':
      flag_text = 1;
      break;
    case 'v':
      flag_verbose++;
      break;
    default:
      printf ("usage: %s [-xtv]\n", progname);
      return 1;
    }

  handle = wrenrx_drv_open(init);
  if (handle == NULL)
    return 1;

  while (1) {
    unsigned len, dlen;

    len = wrenrx_drv_read_packet (handle, &pkt);
    if (len == 0)
      break;
    if (len < sizeof (struct wren_packet_hdr)) {
      printf ("## truncated packet (len: %u)\n", len);
      break;
    }

    dlen = pkt.hdr.len * 4;
    if (len < dlen) {
      printf ("## truncated body (len: %u, len in hdr: %u)\n",
	      len, dlen);
      break;
    }

    printf ("## Receive (%u bytes)", len);

    if (flag_verbose) {
	time_t t = pkt.hdr.snd_ts.sec;
	unsigned ns, us, ms;
	char *at = asctime(gmtime(&t));
	unsigned atl = strlen(at);

	if (at[atl-1] == '\n')
	    at[atl-1] = 0;
	printf(" on %s", at);

	ns = pkt.hdr.snd_ts.nsec;;
	ms = ns / 1000000;
	ns -= ms * 1000000;

	us = ns / 1000;
	ns -= us * 1000;

	printf(" + %ums %uus %uns", ms, us, ns);
    }
    printf (":\n");

    if (flag_hex)
      wren_dump_packet_hex (&pkt);

    if (flag_text)
      disp_packet_text (&pkt);
    else
      wren_dump_packet(&pkt, len);
  }

  wrenrx_close(handle);
  return 0;
}
