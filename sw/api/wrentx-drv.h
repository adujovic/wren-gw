#ifndef __WRENTX_DRV__H__
#define __WRENTX_DRV__H__

#include "wrentx-private.h"

/* Select a wrentx.
   Typically args are:
     DEVICE OPTIONS... -- program options
   or
     DEVICE OPTIONS...

   Options are removed from argc/argc.
   Return < 0 in case of error.
*/
struct wren_drv_tuple *wrentx_drv_init(int *argc, char *argv[]);

/* Open the selected wrentx.
   This happens after so that the program can decode the arguments
   even if the board is not present
*/
struct wrentx_handle *wrentx_drv_open(struct wren_drv_tuple *init);

/* Private declarations */

/* Declarations for stdout driver */
void *wrentx_drv_init_stdout(int *argc, char *argv[]);
struct wrentx_handle *wrentx_drv_open_stdout(void *);

/* For dump driver */
void *wrentx_drv_init_dump(int *argc, char *argv[]);
struct wrentx_handle *wrentx_drv_open_dump(void *);

/* For udp driver */
void *wrentx_drv_init_udp(int *argc, char *argv[]);
struct wrentx_handle *wrentx_drv_open_udp(void *);

/* For eth driver */
void *wrentx_drv_init_eth(int *argc, char *argv[]);
struct wrentx_handle *wrentx_drv_open_eth(void *);
struct wrentx_handle *wrentx_drv_open_eth_wrenctl(void *);

/* For wrenctl driver */
void *wrentx_drv_init_wrenctl(int *argc, char *argv[]);
struct wrentx_handle *wrentx_drv_open_wrenctl(void *);

struct wrentx_operations
{
    void (*close)(struct wrentx_handle *handle);

    int (*set_source)(struct wrentx_handle *handle,
		      unsigned src_idx,
		      struct wren_protocol *proto);
    int (*get_time)(struct wrentx_handle *handle,
		    struct wren_ts *time);

    int (*send_frame)(struct wrentx_handle *handle,
		      unsigned source_idx,
		      struct wrentx_frame *frame);

    int (*wait_until)(struct wrentx_handle *handle,
		      const struct wren_ts *time);

    int (*load_table)(struct wrentx_handle *handle,
		      unsigned source_idx,
		      struct wrentx_table *table,
		      const char *name,
		      unsigned table_idx,
		      unsigned count);

    int (*unload_table)(struct wrentx_handle *handle,
			unsigned source_idx,
			unsigned table_idx);

    int (*play_table)(struct wrentx_handle *handle,
		      unsigned source_idx,
		      unsigned table_idx,
		      const struct wren_ts *ts);

    int (*stop_table)(struct wrentx_handle *handle,
		      unsigned source_idx,
		      unsigned table_idx);

    int (*play_table_on_event)(struct wrentx_handle *handle,
			       unsigned source_idx,
			       unsigned table_idx,
			       wren_event_id ev);

    int (*play_table_on_cond_event_s32)(struct wrentx_handle *handle,
					unsigned source_idx,
					unsigned table_idx,
					wren_event_id ev,
					wren_param_id param,
					int32_t val);

    int (*get_table_name)(struct wrentx_handle *handle,
			  unsigned source_idx,
			  unsigned table_idx,
			  char *name);

    /* To add:
       connect table to event, input
       configure input
    */
};

struct wrentx_handle_op {
    const struct wrentx_operations *op;
    struct wrentx_handle_base base;
};


/* Dump a frame.
   TODO: create a debug header file ? */
void wrentx_dump_frame(struct wrentx_frame *frame);

#endif /* __WRENTX_DRV__H__ */
