#include "lib.h"
#include "si5341.h"

#define I2C_MAIN_SI5341_ADDR 0x75
#include "main-regs.h"

#define I2C_HELPER_SI5340_ADDR 0x74
#include "helper-regs.h"

int si5341_init(void)
{

    si5341_init_hw();

    if (!si534x_is_locked(I2C_MAIN_SI5341_ADDR)) {
	uart_printf("Programming Si5341 main PLL\n");
	si5341_program(si5341_main_regs, SI5341_MAIN_NUM_REGS,
		       I2C_MAIN_SI5341_ADDR);
    }
    
    if (!si534x_is_locked(I2C_HELPER_SI5340_ADDR)) {
	uart_printf("Programming Si5340 helper PLL\n");
	si5341_program(si5340_helper_regs, SI5340_HELPER_NUM_REGS,
		       I2C_HELPER_SI5340_ADDR);
    }

    while (1) {
	int main_lk = si534x_is_locked(I2C_MAIN_SI5341_ADDR);
	int helper_lk = si534x_is_locked(I2C_HELPER_SI5340_ADDR);
	if (main_lk && helper_lk)
	    break;
	uart_printf ("PLL: waiting for");
	if (!main_lk)
	    uart_printf (" main");
	if (!helper_lk)
	    uart_printf (" helper");
	uart_printf (" to lock\n");
	usleep(20000); /* 10ms */
    }

    return 0;
}


void
do_si5341(char *args)
{
    do_si534x (args, I2C_MAIN_SI5341_ADDR, 1);
}

void
do_si5340(char *args)
{
    do_si534x (args, I2C_HELPER_SI5340_ADDR, 0);
}
