#include "lib.h"
#include "board.h"
#include "si5341.h"
#include "wren-hw.h"

void board_pll_init(void)
{
  /* Setup PLLs  */
  si5341_init();
}

void board_init(void)
{
  pcie_init (PL_MAP_BASE);
}
