#include "board-common.h"

#define SI5340_USE_I2C

void do_si5341(char *line);
void do_si5340(char *line);

int si5341_init(void);

#define BOARD_MENU		\
  { "si5341", do_si5341 },	\
  { "si5340", do_si5340 },
