#include "lib.h"
#include "board.h"
#include "si5341.h"

/* WR_CLK_RSTN */
#define MASK_DATA_2_LSW 0x00FF0A0010
#define GPIO_DIRM_2     0x00FF0A0284
#define GPIO_OEN_2      0x00FF0A0288
#define MIO_MST_TRI1    0x00ff180208

void board_pll_init(void)
{
    /* Deassert PLL rstn */
    unsigned v;
    
    /* output */
    v = read32(GPIO_DIRM_2);
    write32(GPIO_DIRM_2, v | (1<<0));

    /* enable */
    v = read32(GPIO_OEN_2);
    write32(GPIO_OEN_2, v | (1<<0));

    /* Disable master tri state */
    v = read32(MIO_MST_TRI1);
    v &= ~(1 << (52 - 32));
    write32(MIO_MST_TRI1, v);

    /* Deassert */
    write32(MASK_DATA_2_LSW, (0xfffe << 16) | 0x0001);


    si5340_init();
}

void board_init(void)
{
}
