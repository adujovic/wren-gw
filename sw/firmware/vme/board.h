#include "board-common.h"

#define SI5340_USE_SPI

int si5340_init(void);
void do_main_si5340(char *args);
void do_helper_si5340(char *args);

#define BOARD_MENU			\
  { "pll-main", do_main_si5340 },	\
  { "pll-helper", do_helper_si5340 },

