#include "lib.h"
#include "si5341.h"

#define SPI_MAIN_SI5340_CS 0
#include "vme-main-regs.h"

#define SPI_HELPER_SI5340_CS 1
#include "vme-helper-regs.h"

int si5341_read_reg1(unsigned cs, uint16_t addr);

int si5340_init(void)
{

    si5341_init_hw();

    // Wait for ready
    while (si5341_read_reg1(SPI_MAIN_SI5340_CS, 0xfe) != 0x0f)
      uart_printf("main Si5340 not ready\n");
    while (si5341_read_reg1(SPI_HELPER_SI5340_CS, 0xfe) != 0x0f)
      uart_printf("helper Si5340 not ready\n");

    //   uart_printf ("PN: %02x\n", si5341_read_reg1(SPI_MAIN_SI5340_CS, 2));
    // dump_regs(SPI_MAIN_SI5340_CS, 0, 16);

    if (!si534x_is_locked(SPI_MAIN_SI5340_CS)) {
	uart_printf("Programming Si5340 main PLL\n");
	si5341_program(si5340_main_regs, SI5340_MAIN_NUM_REGS,
		       SPI_MAIN_SI5340_CS);
    }
    
    if (!si534x_is_locked(SPI_HELPER_SI5340_CS)) {
	uart_printf("Programming Si5340 helper PLL\n");
	si5341_program(si5340_helper_regs, SI5340_HELPER_NUM_REGS,
		       SPI_HELPER_SI5340_CS);
    }

    while (1) {
	int main_lk = si534x_is_locked(SPI_MAIN_SI5340_CS);
	int helper_lk = si534x_is_locked(SPI_HELPER_SI5340_CS);
	if (main_lk && helper_lk)
	    break;
	uart_printf ("PLL: waiting for");
	if (!main_lk)
	    uart_printf (" main");
	if (!helper_lk)
	    uart_printf (" helper");
	uart_printf (" to lock\n");
	usleep(20000); /* 10ms */
    }
    return 0;
}


void
do_main_si5340(char *args)
{
    do_si534x (args, SPI_MAIN_SI5340_CS, 0);
}

void
do_helper_si5340(char *args)
{
    do_si534x (args, SPI_HELPER_SI5340_CS, 0);
}
