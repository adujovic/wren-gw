void pcie_init(unsigned cpu_map_base);

/* For debug: disp ingress registers.  */
void do_pcie_ingress(char *args);

/* Send an interrupt to the host */
void pcie_doorbell(void);
