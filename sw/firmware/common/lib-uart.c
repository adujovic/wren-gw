#include "lib.h"

#define UART_BAUDRATE 115200

#define XUARTPS_BASEADDR 0xFF000000

#define XUARTPS_CR_OFFSET  0x00000000
#define XUARTPS_MR_OFFSET  0x00000004
#define XUARTPS_IER_OFFSET 0x00000008
#define XUARTPS_IDR_OFFSET 0x0000000C
#define XUARTPS_IMR_OFFSET 0x00000010
#define XUARTPS_ISR_OFFSET 0x00000014
#define XUARTPS_BAUDGEN_OFFSET 0x00000018
#define XUARTPS_RXTOUT_OFFSET  0x0000001C
#define XUARTPS_RXWM_OFFSET    0x00000020
#define XUARTPS_MODEMCR_OFFSET 0x00000024
#define XUARTPS_MODEMSR_OFFSET 0x00000028
#define XUARTPS_SR_OFFSET      0x0000002C
#define XUARTPS_FIFO_OFFSET    0x00000030
#define Baud_rate_divider_reg0 0x00000034
#define Flow_delay_reg0        0x00000038
#define Tx_FIFO_trigger_level0 0x00000044

#define XUARTPS_SR_TNFUL   (1 << 14)
#define XUARTPS_SR_TXFULL  (1 << 4)
#define XUARTPS_SR_RXEMPTY (1 << 1)

static void
uart_raw_putc(unsigned c)
{
  while (1) {
    unsigned sr = read32(XUARTPS_BASEADDR + XUARTPS_SR_OFFSET);
    if (!(sr & XUARTPS_SR_TNFUL)) {
      write32(XUARTPS_BASEADDR + XUARTPS_FIFO_OFFSET, c);
      return;
    }
  }
}

void
uart_putc(unsigned c)
{
  if (c == '\n')
    uart_raw_putc('\r');
  uart_raw_putc(c);
}

unsigned
uart_can_read(void)
{
  unsigned sr = read32(XUARTPS_BASEADDR + XUARTPS_SR_OFFSET);
  return !(sr & XUARTPS_SR_RXEMPTY);
}

unsigned
uart_raw_getc(void)
{
  return read32(XUARTPS_BASEADDR + XUARTPS_FIFO_OFFSET);
}

unsigned
uart_getc(void)
{
  while (!uart_can_read())
    ;
  return uart_raw_getc();
}

/* Like uart_getc but for a multi byte key.  Wait the next character for
   at most 1 character transmit time.  */
unsigned
uart_getc_next(void)
{
  unsigned i;

  /* Wait for (8 bits, 1 start, 1 stop) * 2.  */
  for (i = 0; i < 2*(8 + 2); i++) {
    if (uart_can_read()) {
      return uart_raw_getc();
    }
    usleep ((1000000 + UART_BAUDRATE) / UART_BAUDRATE);
  }
  return 0;
}

void
uart_puts(const char *s)
{
  while (*s)
    uart_putc(*s++);
}

static void
uart_putpad (unsigned len, unsigned pad, unsigned zero)
{
  while (len < pad)
    {
      uart_putc (zero ? '0' : ' ');
      len++;
    }
}

static void
uart_putnum_uns (unsigned long val, unsigned pad, unsigned zero)
{
  char buf[20];
  char *p;

  buf[19] = 0;
  p = &buf[19];

  do
    {
      unsigned nval = val / 10;
      *--p = '0' + (val - nval * 10);
      val = nval;
    }
  while (val != 0);
  uart_putpad (&buf[19] - p, pad, zero);
  uart_puts (p);
}

static void
uart_putnum_unsll (unsigned long long val, unsigned pad, unsigned zero)
{
  char buf[40];
  char *p;

  buf[39] = 0;
  p = &buf[39];

  do
    {
      unsigned nval = val / 10;
      *--p = '0' + (val - nval * 10);
      val = nval;
    }
  while (val != 0);
  uart_putpad (&buf[39] - p, pad, zero);
  uart_puts (p);
}

static void
uart_putnum_hex (unsigned long val, unsigned pad, unsigned zero)
{
  char buf[17];
  char *p;

  buf[16] = 0;
  p = &buf[16];

  do
    {
      unsigned d = val & 15;
      if (d < 10)
        d += '0';
      else
        d += 'a' - 10;
      *--p = d;
      val >>= 4;
    }
  while (val != 0);
  uart_putpad (&buf[16] - p, pad, zero);
  uart_puts (p);
}

static void
uart_putnum_hexll (unsigned long long val, unsigned pad, unsigned zero)
{
  char buf[33];
  char *p;

  buf[32] = 0;
  p = &buf[32];

  do
    {
      unsigned d = val & 15;
      if (d < 10)
        d += '0';
      else
        d += 'a' - 10;
      *--p = d;
      val >>= 4;
    }
  while (val != 0);
  uart_putpad (&buf[32] - p, pad, zero);
  uart_puts (p);
}

void
uart_vprintf (const char *fmt, va_list args)
{
  for (; *fmt; fmt++)
    if (*fmt != '%')
      uart_putc (*fmt);
    else
      {
        unsigned zero = 0;
        unsigned pad = 0;
        unsigned lg = 0;
        unsigned long val;

        fmt++;
        if (*fmt == '0')
          {
            zero = 1;
            fmt++;
          }
        for (; *fmt >= '0' && * fmt <= '9'; fmt++)
          pad = pad * 10 + *fmt - '0';

        for (; *fmt == 'l'; fmt++)
          lg++;

        switch (*fmt)
          {
          case '%':
            uart_putc ('%');
            break;
	  case 'c':
	    val = va_arg(args, unsigned);
	    uart_putc(val);
	    break;
          case 's':
            uart_puts (va_arg(args, char *));
            break;
          case 'u':
	    if (lg >= 2) {
	      unsigned long long vll = va_arg(args, unsigned long long);
	      uart_putnum_unsll (vll, pad, zero);
	    }
	    else {
	      if (lg)
		val = va_arg(args, unsigned long);
	      else
		val = va_arg(args, unsigned);
	      uart_putnum_uns (val, pad, zero);
	    }
            break;
          case 'x':
	    if (lg >= 2) {
	      unsigned long long vll = va_arg(args, unsigned long long);
	      uart_putnum_hexll (vll, pad, zero);
	    }
	    else {
	      if (lg)
		val = va_arg(args, unsigned long);
	      else
		val = va_arg(args, unsigned);
	      uart_putnum_hex (val, pad, zero);
	    }
            break;
	  case 'p':
	    val = va_arg(args, unsigned);
	    uart_putnum_hex(val, pad, zero);
	    break;
          default:
            uart_putc ('?');
            break;
          }
      }
}

void
uart_printf (const char *fmt, ...)
{
  va_list args;

  va_start (args, fmt);
  uart_vprintf(fmt, args);
  va_end (args);
}
