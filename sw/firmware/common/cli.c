#include <string.h>
#include "platform.h"
#include "lib.h"
#include "board_map.h"
#include "wren-mb-defs.h"
#include "pcie.h"
#include "wr_nic.h"
#include "rx_desc.h"
#include "tx_desc.h"
#include "wren-hw.h"
#include "nic.h"
#include "wren.h"
#include "wrenrx-data.h"
#include "board.h"
#include "armr5.h"

static volatile struct board_map * const regs =
    (volatile struct board_map *)BOARD_MAP_BASE;

static void
do_cmd(char *line)
{
	unsigned step;

	step = 0;
	while (1) {
		static const char patt[] = "\\|/-";
		uint32_t ver;
		uint32_t csr;

		usleep(250000);

		uart_putc(patt[step++ & 3]);
		ver = regs->version;
		csr = regs->mb_h2b_board.csr;
		uart_printf(" version=%08lx, b2h.csr=%x, h2b.csr=%x, mem_b2h[0]=%x\r",
			    ver, (unsigned)regs->mb_b2h_board.csr,
			    (unsigned)csr,
			    (unsigned)regs->mem_h2b_board[0].data);

		if (csr & MAILBOX_IN_REGS_MAP_CSR_READY)
		    exec_host_command();
		if (uart_can_read()) {
			unsigned c = uart_raw_getc();
			uart_printf("Read %c \n", c);
			return;
		}
	}
}

static void
do_mpu (char *line)
{
  unsigned v;
  unsigned nreg;
  unsigned i;

  asm ("MRC p15,0,%0,c0,c0,0" : "=r"(v));
  uart_printf("midr: %08x\n", v);

  asm ("MRC p15,0,%0,c0,c0,4" : "=r"(v));
  nreg = (v >> 8) & 0xff;
  uart_printf("mpuir: %08x, nregions=%u\n", v, nreg);

  asm ("MRC p15,1,%0,c0,c0,0" : "=r"(v));
  uart_printf("ccsidr: %08x\n", v);

  asm ("MRC p15,1,%0,c0,c0,1" : "=r"(v));
  uart_printf("clidr: %08x\n", v);

  asm ("MRC p15,0,%0,c1,c0,0" : "=r"(v));
  uart_printf("sctlr: %08x\n", v);

  for (i = 0; i < nreg; i++) {
    unsigned sz;
    unsigned acc;

    /* Set index.  */
    asm volatile ("MCR p15,0,%0,c6,c2,0" : : "r"(i));

    /* Read size and enable bit. */
    asm ("MRC p15,0,%0,c6,c1,2" : "=r"(sz));
    if ((sz & 1) == 0)
      continue;

    asm ("MRC p15,0,%0,c6,c1,0" : "=r"(v));
    asm ("MRC p15,0,%0,c6,c1,4" : "=r"(acc));

    uart_printf("reg %02u: base: %08x-%08x [sz=%08x, acc=%08x]\n",
		i, v, v + (2 << ((sz >> 1) & 0x1f)) - 1, sz, acc);
  }
}

static void
do_cache (char *line)
{
  unsigned v;

  asm ("MRC p15,0,%0,c1,c0,0" : "=r"(v));
  uart_printf ("sctlr: %08x", v);
  uart_printf (", mpu:%u", (v >> 0) & 1);
  uart_printf (", align:%u", (v >> 1) & 1);
  uart_printf (", D$:%u", (v >> 2) & 1);
  uart_printf (", bpred:%u", (v >> 11) & 1);
  uart_printf (", I$:%u", (v >> 12) & 1);
  uart_printf (", vect:%u", (v >> 13) & 1);
  uart_printf (", mpu-bg:%u", (v >> 17) & 1);
  uart_printf (", div0:%u", (v >> 19) & 1);
  uart_printf (", fiq0:%u", (v >> 21) & 1);
  uart_printf ("\n");

  /* MRC coproc,op1,Rt,CRn,CRm,op2 */

  asm ("MRC p15,0,%0,c0,c0,1" : "=r"(v));
  uart_printf("cache type: %08x (DMinLine: %u, IMinLine: %u)\n",
	      v, (v >> 16) & 0xf, v & 0x0f);

  asm ("MRC p15,1,%0,c0,c0,1" : "=r"(v));
  uart_printf("clidr: %08x (LoU: %u, LoC: %u CL1: D:%u, I:%u)\n",
	      v, (v >> 27) & 3, (v >> 24) & 3, (v >> 1) & 1, (v >> 0) & 1);

  asm volatile ("mcr p15,2,%0,c0,c0,0" : : "r"(0));
  asm ("MRC p15,1,%0,c0,c0,0" : "=r"(v));
  uart_printf("ccsidr D: %08x\n", v);

  asm volatile ("mcr p15,2,%0,c0,c0,0" : : "r"(1));
  asm ("MRC p15,1,%0,c0,c0,0" : "=r"(v));
  uart_printf("ccsidr I: %08x\n", v);
}

static void
do_tcm (char *line)
{
  unsigned v;

  asm ("MRC p15,0,%0,c0,c0,2" : "=r"(v));
  uart_printf("tcmtr: %08x\n", v);

  asm ("MRC p15,0,%0,c9,c1,0" : "=r"(v));
  uart_printf("btcm: %08x en=%u sz=%uKB\n",
	      v, v & 1, 4 << (((v >> 2) & 0x1f) - 3));

  asm ("MRC p15,0,%0,c9,c1,1" : "=r"(v));
  uart_printf("atcm: %08x en=%u sz=%uKB\n",
	      v, v & 1, 4 << (((v >> 2) & 0x1f) - 3));
}

static void
do_csr(char *args)
{
    uint32_t ver;
    uint32_t csr;

    ver = regs->version;
    csr = regs->mb_h2b_board.csr;
    uart_printf("version=%08lx, b2h.csr=%x, h2b.csr=%x, mem_b2h[0]=%x\n",
		ver, (unsigned)regs->mb_b2h_board.csr,
		(unsigned)csr,
		(unsigned)regs->mem_h2b_board[0].data);
}

static unsigned arg_addr = BOARD_MAP_BASE;
static unsigned arg_len = 256;

static int
arg_memory(const char *name, char *args)
{
  if (args != NULL && arg_next_number(&args, &arg_addr) != 0)
    {
      uart_puts ("address expected\n");
      return -1;
    }
  if (args != NULL && arg_next_number(&args, &arg_len) != 0)
    {
      uart_puts ("length expected\n");
      return -1;
    }
  uart_printf ("%s at 0x%x [%u]:\n", name, arg_addr, arg_len);
  return 0;
}

void
dump_w32(void *addr, unsigned len)
{
  unsigned i, j;
  for (i = 0; i < len; i += 16)
    {
      uart_printf("%08x:", (unsigned)addr + i);
      for (j = 0; j < 16 && i + j < len; j += 4)
        {
	    unsigned val = read32((unsigned)addr + i + j);
          uart_printf(" %08x", val);
        }
      uart_putc('\n');
    }
}

static void
do_dump(char *args)
{
  if (arg_memory("dump", args) != 0)
    return;

  dump_w32((void *)arg_addr, arg_len);

  arg_addr += arg_len;
}

static void
do_w32 (char *args)
{
  unsigned addr;
  unsigned val;

  if (args != NULL && arg_next_number(&args, &addr) != 0)
    {
      uart_puts ("address expected\n");
      return;
    }
  if (args != NULL && arg_next_number(&args, &val) != 0)
    {
      uart_puts ("value expected\n");
      return;
    }
  uart_printf("[%08x] <- 0x%08x\n", addr, val);
  write32(addr, val);
}

static void
do_r32 (char *args)
{
  unsigned addr;
  unsigned val;

  if (args != NULL && arg_next_number(&args, &addr) != 0)
    {
      uart_puts ("address expected\n");
      return;
    }
  val = read32(addr);
  uart_printf("[%08x] -> 0x%08x\n", addr, val);
}

static void
do_rio32 (char *args)
{
  unsigned addr;
  unsigned val;

  if (args != NULL && arg_next_number(&args, &addr) != 0)
    {
      uart_puts ("address expected\n");
      return;
    }
  r5_dccimvac(addr);
  cpu_isb();
  val = read32(addr);
  uart_printf("[%08x] -> 0x%08x\n", addr, val);
}

static void
do_led (char *args)
{
  unsigned off;
  unsigned val;

  if (args != NULL && arg_next_number(&args, &off) != 0)
    {
      uart_puts ("offset expected\n");
      return;
    }
  if (args != NULL && arg_next_number(&args, &val) != 0)
    {
      uart_puts ("value expected\n");
      return;
    }
  off &= 63;
  regs->leds.colors[off].rgb = val;
}

static void
do_version (char *args)
{
  uart_printf("Software: 0x00000000 built on " __DATE__" " __TIME__ "\n");
  uart_printf("Map version: 0x%08x\n", (unsigned)regs->version);
}

static void
do_rx (char *args)
{
    volatile struct nic *nic = (volatile struct nic *)regs->wrnic;
    volatile struct rx_desc *rx_desc =(volatile struct rx_desc *)nic->drx;
    unsigned char *RX_BUF = (unsigned char *)nic->mem;
    unsigned i;
    unsigned rxd;

	/* Initialize descriptors.  */
	uart_puts("init...\n");
	nic->CR = NIC_CR_SW_RST;

	/* 2KB per descriptor (so 16KB in total). */
	for (i = 0; i < 8; i++) {
		rx_desc[i].buf = ((1536 + 8) << RX_DESC_BUF_LEN_SHIFT)
			| (i << 11);
		rx_desc[i].flags = RX_DESC_FLAGS_EMPTY;
	}

	/* Enable RX.  */
	nic->CR = NIC_CR_RX_EN;

	uart_printf ("NIC %08x SR: %08x, CR: %08x\n",
		     (unsigned)nic, (unsigned)nic->SR, (unsigned)nic->CR);

	for (i = 0; i < 8; i++) {
	  uart_printf("RX desc %u: flags: %08x, buf: %08x\n",
		      i, (unsigned)rx_desc[i].flags,
		      (unsigned)rx_desc[i].buf);
	}

	uart_puts("wait for packets...\n");
	rxd = 0;
	while (1 || rxd < 8) {
		unsigned sr;

		/* Wait for packet.  */
		do {
			sr = nic->SR;

			if (uart_can_read()) {
			  uart_raw_getc();
			  uart_puts("Stop\n");
			  return;
			}
		} while ((sr & (NIC_SR_BNA | NIC_SR_REC)) == 0);

		/* Clear status bits.  */
		nic->SR = sr & (NIC_SR_BNA | NIC_SR_REC);

		unsigned cur_rx = (nic->SR & NIC_SR_CUR_RX_DESC_MASK)
		  >> NIC_SR_CUR_RX_DESC_SHIFT;

		/* Disp all received packets.  */
		while (cur_rx != rxd) {
		  /* Disp packet.  */
		  unsigned rxd_buf = rx_desc[rxd].buf;
		  unsigned b =
		      ((unsigned)RX_BUF + (rxd_buf & RX_DESC_BUF_OFFSET_MASK));
		  unsigned l =
		    (rxd_buf & RX_DESC_BUF_LEN_MASK) >> RX_DESC_BUF_LEN_SHIFT;
		  uart_printf ("SR: %08x, rx-desc: %u ", sr, rxd);
		  uart_printf("flags: %08x, buf: %08x (adr=%08x, l=%u)\n ",
			      (unsigned)rx_desc[rxd].flags, rxd_buf,
			      b, l);

		  for (unsigned j = 0; j < 72 && j < l + 2; j += 1) {
		    switch (j) {
		    case 0:
		      break;
		    case 2:  // eth daddr
		    case 2 + 6: // eth saddr
		    case 2 + 6 + 6:  // eth type
		    case 2 + 6 + 6 + 2: // eth payload (ip hdr + frag)
		    case 2 + 14 + 12: // ip daddr
		    case 2 + 14 + 16: // ip saddr
		    case 2 + 14 + 20: // ip payload
		      uart_putc('.');
		      break;
		    case 2 + 14 + 8:  // ip protocol
		    case 48:
		      uart_puts("\n ");
		      break;
		    default:
		      uart_putc(' ');
		      break;
		    }
		    uart_printf ("%02x", *(unsigned char *)(b + j));
		  }
		  uart_putc('\n');

		  /* Reset descriptor.  */
		  rx_desc[rxd].buf = ((1536 + 8) << RX_DESC_BUF_LEN_SHIFT)
		    | (rxd << 11);
		  rx_desc[rxd].flags = RX_DESC_FLAGS_EMPTY;

		  rxd = (rxd + 1) & 7;
		}
	}
}

static void
do_tx (char *args)
{
    volatile struct nic *nic = (volatile struct nic *)regs->wrnic;
    volatile struct tx_desc *tx_desc =(volatile struct tx_desc *)nic->dtx;
    unsigned char *nic_mem = (unsigned char *)nic->mem;
    unsigned i;
    unsigned txd;

	/* Initialize descriptors.  */
	uart_puts("init...\n");
	nic->CR = NIC_CR_SW_RST;

	/* 2KB per descriptor (so 16KB in total). */
	for (i = 0; i < 8; i++) {
		tx_desc[i].flags = 0;
		tx_desc[i].dest = 1;
	}

	uart_printf ("NIC %08x SR: %08x, CR: %08x\n",
		     (unsigned)nic, (unsigned)nic->SR, (unsigned)nic->CR);

	txd = 0;
	while (1) {
		unsigned isr;
		unsigned len = 48;
		unsigned off =  (1 << 14) + (txd << 11);
		volatile unsigned char *b = nic_mem + off;
		unsigned i;

		/* Data offset for wr_nic (the first half-word is ignored) */
		b[0] = 0x00;
		b[1] = 0x00;

		/* Dest: broadcast */
		b[2] = 0xf0;
		b[3] = 0xf1;
		b[4] = 0xf2;
		b[5] = 0xf3;
		b[6] = 0xf4;
		b[7] = 0xf5;
		/* Src: xx */
		b[8] = 0xa0;
		b[9] = 0x22;
		b[10] = 0x33;
		b[11] = 0x44;
		b[12] = 0x55;
		b[13] = 0x66;
		/* Proto */
		b[14] = 'W';
		b[15] = 'e';
		/* Content */
		for (i = 16; i < len; i++)
			b[i] = i;

		/* Write len.  */
		tx_desc[txd].buf = (len << TX_DESC_BUF_LEN_SHIFT)
			| (off << TX_DESC_BUF_OFFSET_SHIFT);
		tx_desc[txd].flags = TX_DESC_FLAGS_READY
			| TX_DESC_FLAGS_PAD_E;

		nic->eic.eic_ier = NIC_EIC_EIC_IER_TCOMP
		    | NIC_EIC_EIC_IER_TXERR;

		/* Enable TX.  */
		nic->CR = NIC_CR_TX_EN;

		/* Wait for packet.  */
		do {
		    isr = nic->eic.eic_isr;
		    uart_printf ("SR: %08x, desc: %u@0x%08x, ",
				 (unsigned)nic->SR,
				 txd, (unsigned)&tx_desc[txd]);
		    uart_printf("flags: %08x, buf: %08x, isr: %02x\n",
				(unsigned)tx_desc[txd].flags,
				(unsigned)tx_desc[txd].buf, isr);


#if 1
		    if (uart_can_read()) {
			uart_raw_getc();
			uart_puts("Stop\n");
			return;
		    }
		    usleep(1000000);
#endif
		} while ((isr & (NIC_EIC_EIC_ISR_TCOMP
				 | NIC_EIC_EIC_ISR_TXERR)) == 0);

		/* Clear status bits.  */
		nic->SR |= (NIC_SR_TX_DONE | NIC_SR_TX_ERROR);
		nic->eic.eic_isr |= NIC_EIC_EIC_ISR_TCOMP
		    | NIC_EIC_EIC_ISR_TXERR;

		txd = (txd + 1) & 7;

		uart_puts("Press 's' to send another packet...\n");

		while (!uart_can_read())
		    ;
		if (uart_raw_getc() != 's') {
		    uart_puts("Stop\n");
		    break;
		}

	}
}

static void
do_pcie_int (char *args)
{
  uart_printf("generate pcie interrupt\n");

  pcie_doorbell();
}

static void
do_gen_int (char *args)
{
    uart_printf("generate interrupt\n");

    host_msg_doorbell();
}

static void
do_wr (char *args)
{
    uart_printf("WR state: %x\n", (unsigned) regs->wr_state);
}

static void
do_time (char *args)
{
    volatile struct host_map *host_regs =
	(volatile struct host_map *)HOST_MAP_BASE;
    uart_printf("tai: %02x %08x + %u cyc\n",
		(unsigned)regs->tm_tai_hi, (unsigned)regs->tm_tai_lo,
		(unsigned)regs->tm_cycles);
    uart_printf("tai compact: %08x\n", (unsigned)host_regs->intc.tm_compact);
    uart_printf("timer:       %08x\n", (unsigned)host_regs->intc.tm_timer);

    uart_printf("pmc cntr:    %u\n", get_cpu_cycles());
}

#define FPD_XMPU_CFG 0x00FD5D0000

static void
do_xmpu (char *args)
{
    volatile unsigned *xmpu =
	(volatile unsigned *)FPD_XMPU_CFG;
    volatile unsigned *xmpu_sink =
	(volatile unsigned *)0x00FD4FFF00;
    unsigned int i;
    char *cmd;

    if (args == NULL || arg_next_string(&args, &cmd) != 0) {
	uart_printf ("CTRL: %08x, ISR: %02x, sink: %08x\n",
		     xmpu[0], xmpu[4], *xmpu_sink);
	uart_printf ("ERR AXI_ADDR: %08x, AXI_ID: %03x\n", xmpu[1], xmpu[2]);
	for (i = 0; i < 4; i++) {
	    volatile unsigned *r = &xmpu[0x40 + i * 4];
	    uart_printf ("R%u (@%x): 0x%08x-0x%08x mst: %08x cfg: %02x\n",
			 i, (unsigned)r, r[0], r[1], r[2], r[3]);
	}
	return;
    }

    if (!strcmp(cmd, "dis")) {
	/* Disable pcie main */
	volatile unsigned *r = &xmpu[0x40];
	r[0] = 0xfd0f0000 >> 12;
	r[1] = 0xfd0f0000 >> 12;
	r[2] = 0; /* For all id */
	r[3] = 9; /* NonSecure + EN */
    }
    else if (!strcmp(cmd, "en")) {
	/* Disable pcie main */
	volatile unsigned *r = &xmpu[0x40];
	r[3] = 0;
    }
    else if (!strcmp(cmd, "ack")) {
	unsigned isr = xmpu[4];
	xmpu[4] = isr;
    }
    else
	uart_printf("usage: xmpu [en|dis]\n");
}

static void
do_intc (char *args)
{
    volatile struct host_map *host_regs =
	(volatile struct host_map *)HOST_MAP_BASE;
    char *cmd;

    if (args == NULL || arg_next_string(&args, &cmd) != 0) {
	uart_printf("isr_raw: %x, imr: %x, isr: %x\n",
		    (unsigned)host_regs->intc.isr_raw,
		    (unsigned)host_regs->intc.imr,
		    (unsigned)host_regs->intc.isr);
    }
    else if (!strcmp(cmd, "ack")) {
	host_regs->intc.iack = 0xff;
    }
    else
	uart_printf("usage: intc [ack]\n");
}

static void
do_rx_src (char *args)
{
    for (unsigned i = 0; i < MAX_RX_SOURCES; i++) {
	struct wren_rx_source *src = &wren_rx_sources[i];
	uart_printf ("%02u: ", i);
	if (src->proto.proto == WREN_PROTO_NONE)
	    uart_printf("unused\n");
	else if (src->proto.proto == WREN_PROTO_ETHERNET) {
	    uart_printf("mac: %02x:%02x:%02x:%02x:%02x:%02x",
			src->proto.u.eth.mac[5],
			src->proto.u.eth.mac[4],
			src->proto.u.eth.mac[3],
			src->proto.u.eth.mac[2],
			src->proto.u.eth.mac[1],
			src->proto.u.eth.mac[0]);
	    uart_printf (" ethtype: %02x, src-id: %02x\n",
			 src->proto.u.eth.ethertype,
			 src->proto.u.eth.source_id);
	    uart_printf ("  nbr_frames: %u, subsample: %u, seqid: %04x\n",
			 (unsigned)src->nbr_frames,
			 (unsigned)src->subsample,
			 (unsigned)src->next_seqid);
	}
    }
}

static void
do_rf1 (char *args)
{
    char *cmd;

    if (args == NULL || arg_next_string(&args, &cmd) != 0) {
	unsigned v;
	uint64_t ftw;

	v = (unsigned)regs->rf1.gth_cfg;
	uart_printf("rf1\n");
	uart_printf("cfg: %08x\n", v);

	v = (unsigned)regs->rf1.gth_sta;
	uart_printf("sta: %08x\n", v);
	if (v & (1 << 0))
	    uart_printf (" usr-clk-tx-act");
	if (v & (1 << 1))
	    uart_printf (" usr-clk-rx-act");
	if (v & (1 << 2))
	    uart_printf (" rx-cdr");
	if (v & (1 << 3))
	    uart_printf (" rst-tx-done");
	if (v & (1 << 4))
	    uart_printf (" rst-rx-done");
	if (v & (1 << 5))
	    uart_printf (" gt-pow-good");
	if (v & (1 << 6))
	    uart_printf (" rx-pma-rst-done");
	if (v & (1 << 7))
	    uart_printf (" tx-pma-rst-done");
	uart_printf("\n");

	v = (unsigned)regs->rf1.h1;
	uart_printf("h1: %u\n", v);

	ftw = (((uint64_t)regs->rf1.ftw_hi) << 32) | regs->rf1.ftw_lo;
	uart_printf("ftw: %016llx\n", ftw);
	uart_printf("ftw freq: %llu\n", 250000 * (ftw >> 16) >> 32);

	ftw = (((uint64_t)(regs->rf1.rf_ftw
			   & BOARD_MAP_RF1_RF_FTW_HI_MASK) << 32)
	       | regs->rf1.rf_ftw_lo);
	uart_printf("rf-ftw: %016llx\n", ftw);
	uart_printf("rf-ftw freq: %llu\n", 125000000 * (ftw >> 16) >> 32);

	uart_printf("rf-time: tai: %08x, cyc: %08x\n",
		    (unsigned)regs->rf1.rf_rx_tai,
		    (unsigned)regs->rf1.rf_rx_cyc);
	uart_printf("rf-sta: %x\n",
		    (unsigned)(regs->rf1.rf_rx_sta
			       & BOARD_MAP_RF1_RF_RX_STA_VALID));
	return;
    }

    if (!strcmp(cmd, "clk-rst")) {
	regs->rf1.gth_cfg = 0x03;
	uart_printf("rx+tx clk reset\n");
	regs->rf1.gth_cfg = 0x00;
    }
    else if (!strcmp(cmd, "+rst-tx")) {
	regs->rf1.gth_cfg |= (1 << 4);
    }
    else if (!strcmp(cmd, "-rst-tx")) {
	regs->rf1.gth_cfg &= ~(1 << 4);
    }
    else if (!strcmp(cmd, "rst")) {
	regs->rf1.gth_cfg = 0x04;
	uart_printf("rx+tx all reset\n");
	regs->rf1.gth_cfg = 0x00;
    }
    else if (!strcmp(cmd, "set-khz")) {
	unsigned long long nco, vall;
	unsigned val;
	if (arg_next_number(&args, &val) != 0)
	    uart_printf("missing number\n");
	else {
	    vall = val;
	    nco = (vall << 32) / 250000;
	    nco <<= 16;
	    regs->rf1.ftw_hi = nco >> 32;
	    regs->rf1.ftw_lo = nco;
	    regs->rf1.ftw_load =
		BOARD_MAP_RF1_FTW_LOAD_EN | BOARD_MAP_RF1_FTW_LOAD_RESET;
	}
    }
    else if (!strcmp(cmd, "h1")) {
	unsigned val;
	if (arg_next_number(&args, &val) != 0)
	    uart_printf("missing number\n");
	else
	    regs->rf1.h1 = val;
    }
}

static void do_help(char *args);

static const struct menu_item top_menu[] =
  {
   {"help", do_help },
   {"dump", do_dump },
   {"w32", do_w32},
   {"r32", do_r32},
   {"rio32", do_rio32},
   {"led", do_led},
   {"mpu", do_mpu },
   {"cache", do_cache },
   {"tcm", do_tcm },
   {"csr", do_csr },
   {"cmd", do_cmd },
   {"pcie-in", do_pcie_ingress},
   {"pcie-int", do_pcie_int},
   {"gen-int", do_gen_int},
   {"nic", do_nic},
   {"rx", do_rx},
   {"tx", do_tx},
   {"wr", do_wr},
   {"time", do_time},
   {"xmpu", do_xmpu},
   {"intc", do_intc},
   {"ver", do_version},
   {"rx-src", do_rx_src},
   {"rf1", do_rf1},
#ifdef BOARD_MENU
   BOARD_MENU
#endif
   {NULL, NULL}
  };

static void
do_help(char *args)
{
  const struct menu_item *m;

  uart_puts("commands:\n");
  for (m = top_menu; m->name; m++)
    uart_printf (" %s\n", m->name);
}

void
cmd_cli(char *buf)
{
  menu_execute (buf, top_menu);
}

void
cli_menu(void)
{
    while (1) {
	char buf[32];

	uart_readline("cmd> ", uart_getc, buf, sizeof (buf));
	if (buf[0] == 'q' && buf[1] == 0)
	    break;
	menu_execute (buf, top_menu);
    }
}
