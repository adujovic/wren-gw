/* Must be called early to initialize internal data. */
void wrenrx_init(void);

/* Packet buffer.
   BUF points to the start of the buffer.
   LEN is the length of the buffer.
   FRAME_OFF is the offset to the ethernet header; it must be 2.
   PKT_OFF is the offset to the ethernet payload or packet; it must be
     2+14 or 2+18 (if there is a vlan).
*/
struct wren_pkt_buf {
  unsigned char *buf;
  uint16_t len;
  uint16_t frame_off;
  uint16_t pkt_off;
};

/* Filter an incoming packet.
   Return the source index, or -1 if no filter applies.
   Set pkt_off.  */
int wrenrx_filter_packet(struct wren_pkt_buf *frame);

/* Decode and handle a packet (once it has been filtered).
   Version and source matches.  */
void wrenrx_handle_packet(struct wren_pkt_buf *frame, int src_idx);

extern void wrenrx_log (const char *fmt, ...) __attribute__((format(printf, 1, 2)));

