#ifndef __BOARD_COMMON__H__
#define __BOARD_COMMON__H__

void board_pll_init(void);
void board_init(void);

#endif /* __BOARD_COMMON__H__ */
