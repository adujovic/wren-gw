#include <stddef.h>
#include <string.h>
#include "lib.h"

static char *
arg_skip(char *l)
{
  while (*l == ' ')
    l++;
  return l;
}

/* Extract next work from LINE.  */
static char *
arg_next(char **line)
{
  char *l = *line;
  char *res;

  l = arg_skip (l);
  if (*l == 0)
    {
      /* End of line.  */
      *line = NULL;
      return NULL;
    }
  res = l;

  while (*l != ' ') {
    if (*l == 0) {
      *line = NULL;
      return res;
    }
    l++;
  }

  *line = l + 1;
  *l = 0;
  return res;
}

static int
strtou (char *l, unsigned *val)
{
  unsigned res;
  char c;

  res = 0;
  if (l[0] == '0' && l[1] == 'x')
    {
      for (l += 2; (c = *l) != 0; l++)
        {
          if (c >= '0' && c <= '9')
            c -= '0';
          else if (c >= 'a' && c <= 'f')
            c -= 'a' - 10;
          else if (c >= 'A' && c <= 'F')
            c -= 'A' - 10;
          else
            return -1;
          res = (res << 4) + c;
        }
    }
  else
    {
      for (; (c = *l) != 0; l++)
        {
          if (c >= '0' && c <= '9')
            c -= '0';
          else
            return -1;
          res = res * 10 + c;
        }
    }
  *val = res;
  return 0;
}

int
arg_next_number (char **line, unsigned *val)
{
  char *l = arg_next(line);

  if (l == NULL)
    return -1;

  return strtou (l, val);
}

int
arg_next_string(char **line, char **s)
{
  char *l = arg_next(line);
  *s = l;
  if (l == NULL)
    return -1;
  return 0;
}

void
menu_execute (char *line, const struct menu_item *menu)
{
  char *cmd;

  /* Skip leading blanks.  */
  line = arg_skip(line);

  /* If the line is empty, nothing to do.  */
  if (*line == 0)
    return;

  /* Add to history.  */
  uart_history_add(line);

  cmd = arg_next (&line);

  for (; menu->name; menu++)
    if (strcmp(menu->name, cmd) == 0)
      {
        menu->cmd(line);
        return;
      }
  uart_printf("unknown command '%s'\n", cmd);
}
