#include <stdint.h>
#include "lib.h"
#include "pcie.h"
#include "xpcie.h"

#define AXIPCIE_MAIN_BASE 0x00FD0E0000

struct axipcie_bridge_core_cfg {
  uint32_t pcie_rx0;
  uint32_t pcie_rx1;
  uint32_t axi_master;
  uint32_t pcie_tx;

  uint32_t interrupt;
  uint32_t ram_disable0;
  uint32_t ram_disable1;
  uint32_t pcie_relaxed_order;

  uint32_t pci_rx_msg_filter;
  uint32_t rq_req_order;
  uint32_t pcie_credit;
  uint32_t axi_m_w_tick_count;

  uint32_t axi_m_r_tick_count;
  uint32_t crs_rpl_tick_count;
};

struct axipcie_e {
  uint32_t breg_capabilities;
  uint32_t breg_status;
  uint32_t breg_control;
  uint32_t pad_20c;

  uint32_t breg_base_lo;
  uint32_t breg_base_hi;
  uint32_t pad_218;
  uint32_t pad_21c;

  uint32_t ecam_capabilities;
  uint32_t ecam_status;
  uint32_t ecam_control;
  uint32_t pad_22c;

  uint32_t ecam_base_lo;
  uint32_t ecam_base_hi;
  uint32_t pad_238;
  uint32_t pad_23c;

  uint32_t msxt_capabilities;
  uint32_t msxt_status;
  uint32_t msxt_control;
  uint32_t pad_24c;

  uint32_t msxt_base_lo;
  uint32_t msxt_base_hi;
  uint32_t pad_258;
  uint32_t pad_25c;

  uint32_t msxp_capabilities;
  uint32_t msxp_status;
  uint32_t msxp_control;
  uint32_t pad_26c;

  uint32_t msxp_base_lo;
  uint32_t msxp_base_hi;
  uint32_t pad_278;
  uint32_t pad_27c;

  uint32_t dreg_capabilities;
  uint32_t dreg_status;
  uint32_t dreg_control;
  uint32_t pad_28c;

  uint32_t dreg_base_lo;
  uint32_t dreg_base_hi;
  uint32_t pad_298;
  uint32_t pad_29c;
};

#define AXIPCIE_E_BASE (AXIPCIE_MAIN_BASE + 0x200)

#define AXIPCIE_ECAM_BASE 0xe0000000
#define AXIPCIE_DMA_BASE  0xfd0f0000

struct pcie_controller {
  uint32_t id;
  uint32_t subsys_id;
  uint32_t rev_id;
  uint32_t dsn_0;

  uint32_t dsn_1;
  uint32_t pad_214;
  uint32_t pm_ctrl;
  uint32_t pad_21c;

  uint32_t pad_220;
  uint32_t pad_224;
  uint32_t pad_228;
  uint32_t pad_22c;

  uint32_t ep_ctrl;
  uint32_t rp_ctrl;
  uint32_t pcie_status;
  uint32_t pad_23c;
};

#define PCIE_ATTRIB_BASE 0xfd480000
#define PCIE_ATTRIB_BAR_OFFSET 0x1c /* 2 attr per BAR, 16b regs */
#define PCIE_CONTROLLER_BASE (PCIE_ATTRIB_BASE + 0x200)

struct pcie_ingress {
  uint32_t capabilities;
  uint32_t status;
  uint32_t control;
  uint32_t pad_0c;

  uint32_t src_base_lo;
  uint32_t src_base_hi;
  uint32_t dst_base_lo;
  uint32_t dst_base_hi;
};

#define PCIE_INGRESS_BASE 0xfd0e0800
#define PCIE_INGRESS_SIZE 0x20

#define PCI_COMMAND_REG 0x04
#define PCI_COMMAND_MSE 0x02

#define PCI_BAR0_REG    0x10
#define PCI_BAR1_REG    0x14
#define PCI_BAR2_REG    0x18
#define PCI_BAR3_REG    0x1c
#define PCI_BAR4_REG    0x20
#define PCI_BAR5_REG    0x24

/* The size of the ingress is 4096 << SZ.  */
static void
set_ingress(unsigned n, unsigned addr, unsigned sz)
{
  volatile struct pcie_ingress *pcie_ingress =
    (volatile struct pcie_ingress *)(PCIE_INGRESS_BASE + n * PCIE_INGRESS_SIZE);

  /* Read BAR n */
  uint32_t bar_lo;
  bar_lo = read32(AXIPCIE_ECAM_BASE + PCI_BAR0_REG + 4 * n);

  uart_printf("pcie_init: bar %u: lo=0x%08x\n", n, (unsigned)bar_lo);

  /* Configure ingress */
  pcie_ingress->src_base_lo = bar_lo;
  pcie_ingress->src_base_hi = 0;
  pcie_ingress->dst_base_lo = addr;
  pcie_ingress->dst_base_hi = 0;
  pcie_ingress->control = (sz << 16) | 0x1; /* 1MB, enable */
}

static void
pcie_set_bar32(unsigned n, unsigned mask_nbits)
{
  unsigned addr = PCIE_ATTRIB_BASE + PCIE_ATTRIB_BAR_OFFSET + n * 8;
  unsigned mask = 0xffffffff << mask_nbits;
  write32(addr + 0, mask & 0xffff);
  write32(addr + 4, (mask >> 16) & 0xffff);
}

void
pcie_init(unsigned cpu_map_base)
{
  volatile struct axipcie_e *axipcie =
    (volatile struct axipcie_e *)AXIPCIE_E_BASE;
  volatile struct pcie_controller *pcie_controller =
    (volatile struct pcie_controller *)PCIE_CONTROLLER_BASE;
  volatile struct pcie_dma *dma_regs =
    (volatile struct pcie_dma *)AXIPCIE_DMA_BASE;
  unsigned cnt;

  pcie_set_bar32(0, 16);
  pcie_set_bar32(1, 18);
  pcie_set_bar32(2, 16);
  pcie_set_bar32(3, 21);
  pcie_set_bar32(4, 16);
    
  uart_puts("pcie_init: wait for link\n");

  /* Wait for link.  */
  cnt = 0;
  while ((pcie_controller->pcie_status & 3) != 3) {
    volatile unsigned *rst_fdp_top = (volatile unsigned *)0xfd1a0100;
    volatile unsigned *msgf_misc_status = (volatile unsigned *)0xFD0E0400;
    volatile unsigned *ltssm = (volatile unsigned *)0xfd480228;
    uart_printf("crf_apb.rst_fpd_top=0x%08x\n", *rst_fdp_top);
    uart_printf("pcie_status=%x\n",
		(unsigned)pcie_controller->pcie_status);
    uart_printf("msgf_misc_status=0x%08x, ltssm=0x%08x\n",
		*msgf_misc_status, *ltssm);
    usleep(1000000);
    cnt++;
    
    if (cnt == 5) {
      *rst_fdp_top |= 0x20000;
      uart_printf("pcie_ctrl_reset (rst_fpd_top=0x%08x)\n", *rst_fdp_top);
      *rst_fdp_top &= ~0x20000;
    }

    if (cnt == 10) {
      uart_printf("pcie failed\n");
      return;
    }
  }


  /* Map bridge register aperture.  */
  axipcie->breg_base_lo = AXIPCIE_MAIN_BASE;
  axipcie->breg_base_hi = 0;
  axipcie->breg_control = (1 << 16) | 0x1; /* 4KB + Enable */

  /* Map ECAM space */
  axipcie->ecam_base_lo = AXIPCIE_ECAM_BASE;
  axipcie->ecam_base_hi = 0;
  axipcie->ecam_control = (1 << 16) | 0x1; /* 4KB + Enable */

  /* Map DMA space */
  axipcie->dreg_base_lo = AXIPCIE_DMA_BASE;
  axipcie->dreg_base_hi = 0;
  axipcie->dreg_control = (1 << 16) | 0x1;  /* 4KB + Enable */

  uart_puts("pcie_init: wait for for enumeration\n");

  /* Wait for enumeration. */
  while (1)
    {
      uint32_t val;
      val = read32 (AXIPCIE_ECAM_BASE + PCI_COMMAND_REG);
      
      uart_printf ("pcie_init: PCI-id: %08x, cmd: %08x\n",
		   (unsigned)read32 (AXIPCIE_ECAM_BASE), (unsigned)val);
      if (val & PCI_COMMAND_MSE)
	    break;
      usleep (1000000);
    }

  /* Bar0: reserved for devices by Xilinx... */
  set_ingress (1, cpu_map_base, 5);  /* Regs, 128KB */
  set_ingress (2, 0xfffc0000, 4);    /* OCM, 64KB */
  set_ingress (3, 0, 8);             /* DDR, 1MB */
  set_ingress (4, 0x00FF0F0000, 4);  /* QSPI, 64KB */

  /* Enable interrupt on PCIe (set interrupt_mask). */
  dma_regs->pcie_interrupt_control = (1 << 0);

  uart_puts("pcie_init: done\n");
}

void
do_pcie_ingress(char *args)
{
  for (int i = 0; i < 8; i++) {
    unsigned addr = PCIE_INGRESS_BASE + i * 0x20;
    uart_printf("ingress[%u] @ %08x:\n", i, addr);

    for (int j = 0; j < 0x20; j += 4) {
      uart_printf(" %08x", (unsigned)read32 (addr + j));
      if ((j & 0xf) == 0xc)
	uart_putc ('\n');
    }
  }

  for (int i = 0; i < 6; i++) {
    unsigned lo = read32(PCIE_ATTRIB_BASE + PCIE_ATTRIB_BAR_OFFSET + i * 8);
    unsigned hi = read32(PCIE_ATTRIB_BASE + PCIE_ATTRIB_BAR_OFFSET + i * 8 + 4);
    unsigned val = read32(AXIPCIE_ECAM_BASE + PCI_BAR0_REG + 4 * i);

    uart_printf("bar%u: mask=%04x%04x, val=%08x\n", i, hi, lo, val);
  }
}

void
pcie_doorbell(void)
{
  volatile struct pcie_dma *dma_regs =
    (volatile struct pcie_dma *)AXIPCIE_DMA_BASE;

  dma_regs->pcie_interrupt_assert = (1 << 3);
}
