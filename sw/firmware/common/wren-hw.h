#include "pci_map.h"

#define PL_MAP_BASE 0x80000000
#define BOARD_MAP_BASE (PL_MAP_BASE + PCI_MAP_BOARD)
#define HOST_MAP_BASE  (PL_MAP_BASE + PCI_MAP_HOST)

uint32_t *mb_get_reply(unsigned cmd, unsigned len);
void mb_send_reply(void);
void mb_reply_word(unsigned cmd, unsigned data);

