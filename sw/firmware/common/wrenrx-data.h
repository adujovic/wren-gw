#ifndef __WRENRX_DATA__H__
#define __WRENRX_DATA__H__
#include <stdint.h>
#include "wren/wren-common.h"
#include "wren/wren-packet.h"
#include "wren-mb-defs.h"

/* RX part */

/* Number of sources.  */
#define MAX_RX_SOURCES 8

/* Destinations of the source.  */
#define RX_DEST_WREN 0  /* WREN source */

/* Bitmap for subscribed events */
#define RX_SUBSCRIBE_MAP_LEN 32

struct wren_rx_source {
  struct wren_protocol proto;
  /* TODO: destination (wren messages, rf frames) */
  uint32_t dest;

  /* Sequence id of the next expected frame */
  uint16_t next_seqid;

  /* Subsampling.  */
  uint32_t subsample;
  uint32_t subsample_cnt;

  /* Stats. */
  uint32_t nbr_frames;

  /* Linked list of conditions */
  uint16_t conds;

  uint32_t subscribed_map[RX_SUBSCRIBE_MAP_LEN];

  /* Latency: TODO
     min, max, avg */

  /* TODO: last context */
};

extern struct wren_rx_source wren_rx_sources[MAX_RX_SOURCES];

struct wren_rx_context {
  uint16_t ctxt_id;

  /* Length of parameters */
  uint16_t len;

  struct wren_packet_ts valid_from;
  uint32_t params[WREN_PACKET_DATA_MAX_WORDS];
};

/* Number of contexts per source */
#define MAX_RX_CONTEXTS 4

extern struct wren_rx_context wren_rx_contexts[MAX_RX_SOURCES][MAX_RX_CONTEXTS];

#define NO_RX_ACT_IDX 0xffff

#define MAX_RX_COND_WORDS 8

#define MAX_RX_CONDS 0x100

struct wren_rx_cond {
  /* Next condition for the source. */
  uint16_t next;

  uint16_t act_idx;

  /* Note: evt_id is set to WREN_EVENT_ID_INVALID when the condition is
     not used. */
  struct wren_mb_cond cond;
};

extern struct wren_rx_cond wren_rx_conds[MAX_RX_CONDS];

#define MAX_RX_ACTIONS 0x100

struct wren_rx_action {
  uint16_t next;
  uint16_t cond_idx;  /* Set to NO_RX_COND_IDX when unused */

  /* Triggerer */
  struct wren_mb_pulser_config conf;
};

extern struct wren_rx_action wren_rx_actions[MAX_RX_ACTIONS];

/* Execution */
void wrenrx_run_event(unsigned src_idx, struct wren_capsule_event_hdr *evt);
void wrenrx_run_context(unsigned src_idx, struct wren_rx_context *ctxt);

void wrenrx_run_action (unsigned src_idx, unsigned act_idx, struct wren_capsule_event_hdr *evt);
#endif /* __WRENRX_DATA__H__ */
