/* Initialize the nic device */
void nic_init(void);

/* Get a TX buffer, and send it. */
uint32_t *nic_get_tx_buf(unsigned len);
void nic_send_tx_buf(void);

/* Send a raw packet */
int nic_send(const void *buf, unsigned len);

/* Interrupt handlers */
void nic_rx_handler(void);
void nic_tx_handler(void);

/* Display nic status */
void do_nic (char *args);

#define ETH_HDR_LEN (6+6+2)
