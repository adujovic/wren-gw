#include <stdint.h>
#include "armr5.h"

static uint32_t
read_sctlr(void)
{
  uint32_t res;

  asm volatile ("MRC p15,0,%0,c1,c0,0" : "=r"(res));
  return res;
}

static void
write_sctlr(uint32_t sctlr)
{
  asm volatile ("MCR p15,0,%0,c1,c0,0" : : "r"(sctlr));
}

static void
write_mpu_addr(uint32_t addr)
{
  asm volatile ("MCR p15,0,%0,c6,c1,0" : : "r"(addr));
}

static void
write_mpu_size(uint32_t val)
{
  asm volatile ("MCR p15,0,%0,c6,c1,2" : : "r"(val));
}

static void
write_mpu_acc(uint32_t val)
{
  asm volatile ("MCR p15,0,%0,c6,c1,4" : : "r"(val));
}

static void
write_mpu_num(uint32_t val)
{
  asm volatile ("MCR p15,0,%0,c6,c2,0" : : "r"(val));
  cpu_isb();
}


static void
enable_mpu(void)
{
  uint32_t sctlr = read_sctlr();

  sctlr |= 1;

  cpu_dsb();

  write_sctlr(sctlr);

  cpu_isb();
}

static void
disable_mpu(void)
{
  uint32_t sctlr = read_sctlr();

  sctlr &= ~1;

  cpu_dsb();

  write_sctlr(sctlr);

  cpu_isb();
}

void
Init_MPU(void)
{
  disable_mpu();
  
  /* 0: 0 - 2GB (ddram) */
  write_mpu_num(0);
  write_mpu_addr(0x00000000);
  write_mpu_acc(0x30b);
  write_mpu_size((30 << 1) | 1);

  /* 1: 0x80000000 2GB + 1GB (PL) */
  write_mpu_num(1);
  write_mpu_addr(0x80000000);
  write_mpu_acc(0x300);
  write_mpu_size((29 << 1) | 1);

  /* 2: 0xC0000000 + 512M (qspi) */
  write_mpu_num(2);
  write_mpu_addr(0xC0000000);
  write_mpu_acc(0x310);
  write_mpu_size((28 << 1) | 1);

  /* 3: 0xE0000000 + 256M (PCIe) */
  write_mpu_num(3);
  write_mpu_addr(0xE0000000);
  write_mpu_acc(0x310);
  write_mpu_size((27 << 1) | 1);

  /* 4: 0xF8000000 + 16M (STM CORESIGHT) */
  write_mpu_num(4);
  write_mpu_addr(0xF8000000);
  write_mpu_acc(0x310);
  write_mpu_size((23 << 1) | 1);

  /* 5: 0xF9000000 + 1M (A53 GIC) */
  write_mpu_num(5);
  write_mpu_addr(0xF9000000);
  write_mpu_acc(0x310);
  write_mpu_size((19 << 1) | 1);

  /* 6: 0xFD000000 + 16M (FPS) */
  write_mpu_num(6);
  write_mpu_addr(0xFD000000);
  write_mpu_acc(0x310);
  write_mpu_size((23 << 1) | 1);

  /* 7: 0xFE000000 + 16M (upper LPS) */
  write_mpu_num(7);
  write_mpu_addr(0xFE000000);
  write_mpu_acc(0x310);
  write_mpu_size((23 << 1) | 1);

  /* 8: 0xFF000000 + 16M (lower LPS + TCM) */
  write_mpu_num(8);
  write_mpu_addr(0xFF000000);
  write_mpu_acc(0x310);
  write_mpu_size((23 << 1) | 1);

  /* 9: 0xFFFC0000 + 256K (OCM) */
  write_mpu_num(9);
  write_mpu_addr(0xFFFC0000);
  write_mpu_acc(0x30b);
  write_mpu_size((17 << 1) | 1);

  enable_mpu();
}

void
Print_DDRSize_Warning(void)
{
}

void
Xil_InitializeExistingMPURegConfig(void)
{
}
