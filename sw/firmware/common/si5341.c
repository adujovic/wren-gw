#include <string.h>
#include "lib.h"
#include "si5341.h"

int si5341_read_reg(unsigned cs, uint16_t addr)
{
    uint8_t buffer;
    if (si5341_read_regs(cs, addr, &buffer, 1) != 0)
      return -1;

    return buffer;
}

int si5341_write_reg(unsigned cs, uint16_t reg, uint8_t val)
{
  /* Set page */
  if (si5341_write_raw(cs, 0x01, reg >> 8) != 0)
    return -1;

  /* Write value. */
  if (si5341_write_raw(cs, reg & 0xff, val) != 0)
    return -1;

  return 0;
}

#if 0
void si5341_i2c_write_nvm(XIicPs *iic) {

	uint8_t buffer[4];

	// see p13 of the reference manual Si5341-40-D-RM

	// write 0xC7 to NVM_WRITE register
	buffer[0] = 0x01;
	buffer[1] = 0;
	buffer[2] = 0xE3;
	buffer[3] = 0xC7;

	XIicPs_MasterSendPolled(iic, buffer, 2, I2C_SI5341_ADDR);
	XIicPs_MasterSendPolled(iic, &buffer[2], 2,
	I2C_SI5341_ADDR);

	// wait until DEVICE_READY = 0x0F
	buffer[0] = 0x01;
	buffer[1] = 0;
	buffer[2] = 0xFE;
	XIicPs_MasterSendPolled(iic, buffer, 2, I2C_SI5341_ADDR);
	XIicPs_MasterSendPolled(iic, &buffer[2], 1,
	I2C_SI5341_ADDR);
	do {
		buffer[0] = 0;
		XIicPs_MasterRecvPolled(iic, buffer, 1, I2C_SI5341_ADDR);
	} while (*buffer != 0xF);

	// Set NVM_READ_BANK 0x00E4[0] = 1
	buffer[0] = 0x01;
	buffer[1] = 0;
	buffer[2] = 0xE4;
	buffer[4] = 1;
	XIicPs_MasterSendPolled(iic, buffer, 2, I2C_SI5341_ADDR);
	XIicPs_MasterSendPolled(iic, &buffer[2], 2,
	I2C_SI5341_ADDR);

	// wait until DEVICE_READY = 0x0F
	buffer[0] = 0x01;
	buffer[1] = 0;
	buffer[2] = 0xFE;
	XIicPs_MasterSendPolled(iic, buffer, 2, I2C_SI5341_ADDR);
	XIicPs_MasterSendPolled(iic, &buffer[2], 1,
	I2C_SI5341_ADDR);
	do {
		buffer[0] = 0;
		XIicPs_MasterRecvPolled(iic, buffer, 1, I2C_SI5341_ADDR);
	} while (*buffer != 0xF);
}
#endif

int si5341_program(const si5341_revd_register_t *regs,
		   unsigned num_regs,
		   unsigned cs)
{
    // send preamble
    for (int i = 0; i < 6; i++) {
	si5341_write_reg(cs, regs[i].address, regs[i].value);
    }

    /* Delay 300 msec */
    usleep(300000);

    for (int i = 6; i < num_regs; i++) {
	si5341_write_reg(cs, regs[i].address, regs[i].value);
    }
    
    /* Wait until bus is idle */
    si5341_wait_done();

    uart_printf("Programming complete\n");

    return 0;
}

static void dump_regs(unsigned cs, unsigned addr, unsigned len)
{
  uint8_t regs[0xff];
  unsigned i;

  /* Cannot switch to the next page. */
  if (len > 0x100)
    len = 0x100;

  uart_printf("For 0x%02x:\n", cs);
  if (si5341_read_regs(cs, addr, regs, len) < 0) {
    uart_printf ("Timeout\n");
    return;
  }

  uart_printf ("    ");
  for (i = 0; i < 16; i++)
    uart_printf("  %x", i);
  uart_printf("\n");

  for (i = 0; i < len; i++) {
    if ((i & 15) == 0)
      uart_printf("%03x:", addr + i);
    uart_printf(" %02x", regs[i]);
    if ((i & 15) == 15 || i == len - 1)
      uart_printf("\n");
  }
}

int si534x_is_locked(unsigned cs)
{
    int v;

    v = si5341_read_reg(cs, 0x0c);
    if (v == -1)
	return -1;
    return (v & 0x0f) == 0;
}


static void
disp_si534x(unsigned cs, int is41)
{
    unsigned status;
    unsigned val;

    val = si5341_read_reg1(cs, 0xfe);
    uart_printf("device_ready: %02x (=0x0f)\n", val);
    if (val != 0x0f)
	return;

    uart_printf("device: %02x%02x grade %c revision %c  i2c: %02x\n",
		si5341_read_reg(cs, 0x0003),
		si5341_read_reg(cs, 0x0002),
		'A' + si5341_read_reg(cs, 0x0004),
		'A' + si5341_read_reg(cs, 0x0005),
		si5341_read_reg(cs, 0x000B));

    status = si5341_read_reg(cs, 0x000C);
    uart_printf("status: %02x", status);
    if (status & (1 << 0))
      uart_printf(" SYSINCAL");
    if (status & (1 << 1))
      uart_printf(" LOSXAXB");
    if (status & (1 << 2))
      uart_printf(" LOSREF");
    if (status & (1 << 3))
      uart_printf(" LOL");
    if (status & (1 << 5))
      uart_printf(" SMBUS");
    uart_printf("\n");

    uart_printf("active NVM bank: %02x\n",
		si5341_read_reg(cs, 0x00E2));
    
    static const uint16_t si5341_out[] =
    {
	0x0108, 0x010d, 0x0112, 0x0117, 0x011c,
	0x0121, 0x0126, 0x012b, 0x0130, 0x013a,
	0
    };
    static const uint16_t si5340_out[] =
    {
	0x0112, 0x0117, 0x0126, 0x012b, 0
    };

    const uint16_t *out_regs = is41 ? si5341_out : si5340_out;

    /* Go to page 1 */
    si5341_write_raw(cs, 0x01, 0x01);
    
    for (unsigned i = 0; out_regs[i]; i++) {
	unsigned reg = out_regs[i];
	uint8_t val[4];
	if (si5341_read_regs(cs, reg, val, 4) < 0)
	  continue;
	uart_printf ("out%u (@%04x) cfg:%02x drv:%02x cma:%02x mux:%02x\n",
		     i, reg, val[0], val[1], val[2], val[3]);
    }
		
}

void
do_si534x(char *args, unsigned cs, int is41)
{
    char *cmd;
    if (args == NULL || arg_next_string(&args, &cmd) != 0) {
	disp_si534x(cs, is41);
    }
    else if (!strcmp(cmd, "dump")) {
	dump_regs(cs, 0x000, 0x20);
	dump_regs(cs, 0x100, 0x40);
    }
    else
	uart_printf("usage: si534x [dump]\n");
}
