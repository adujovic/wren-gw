#include <stdio.h>
#include "armr5.h"
#include "lib.h"
#include "board_map.h"
#include "wren-mb-defs.h"
#include "pcie.h"
#include "wr_nic.h"
#include "rx_desc.h"
#include "tx_desc.h"
#include "wren-hw.h"
#include "wren.h"
#include "nic.h"
#include "wrentx.h"
#include "wrenrx-core.h"
#include "wrenrx-cmd.h"
#include "board.h"

static volatile struct board_map * const regs =
    (volatile struct board_map *)BOARD_MAP_BASE;

static volatile struct nic * const nic = (volatile struct nic *)
    (BOARD_MAP_BASE + BOARD_MAP_WRNIC);

uint32_t *
mb_get_reply(unsigned cmd, unsigned len)
{
  regs->mb_b2h_board.cmd = cmd;
  regs->mb_b2h_board.len = len;
  return (uint32_t *)&regs->mem_b2h_board[0].data;
}

void
mb_send_reply(void)
{
  regs->mb_b2h_board.csr = MAILBOX_OUT_REGS_MAP_CSR_READY;
}

void
mb_reply_word(unsigned cmd, unsigned data)
{
  uint32_t *ptr;

  ptr = mb_get_reply(cmd, sizeof(*ptr) / 4);
  *ptr = data;
  mb_send_reply();
}

void host_msg_doorbell(void)
{
	/* Generate a SW interrupt to the host */
	regs->ifr = BOARD_MAP_IFR_MSG;
}

void host_async_doorbell(void)
{
	/* Generate a SW interrupt to the host */
	regs->ifr = BOARD_MAP_IFR_ASYNC;
}

void exec_host_command(void)
{
    uint32_t cmd = regs->mb_h2b_board.cmd;
    uint32_t *data = (uint32_t*)&regs->mem_h2b_board[0].data;
    unsigned len = regs->mb_h2b_board.len;

    uart_printf("cmd: %08lx\n", cmd);
    switch(cmd) {
    case CMD_PING: {
	uart_printf(" ping\n");
	regs->mb_b2h_board.cmd = CMD_REPLY | CMD_PING;
	regs->mb_b2h_board.len = 0;
	mb_send_reply();
	break;
    }
    case CMD_WRITE: {
	uint32_t addr = data[0];
	unsigned off;

	uart_printf(" write @%08lx+%u\n", addr, len);
	len = (len + 3) >> 2;
	for (off = 0; off < len; off++) {
	    volatile unsigned *dest =
		(volatile unsigned *)(addr + off * 4);
	    *dest = data[off + 1];
	}
	cpu_dsb();
	len += addr & 0x1f;
	addr &= ~0x1f;
	for (off = 0; off < len; off += 0x20) {
	    r5_dccmvac(addr + off);
	    cpu_isb();
	    r5_icimvau(addr + off);
	}
	break;
    }
    case CMD_EXEC: {
	uint32_t addr = regs->mb_h2b_board.len;
	void (*fn)(void);

	uart_printf(" exec @%08lx\n", addr);
	fn = (void (*)(void))addr;
	fn();
	break;
    }
    case CMD_SEND: {
	unsigned char *b;

	uart_printf(" send %u bytes\n", len);
	b = (unsigned char *)data;
	if (nic_send(b, len) < 0) {
	    uart_printf("no free descriptor\n");
	    return;
	}
	break;
    }

    case CMD_CLI: {
	union {
	    char buf[64];
	    uint32_t w[64/4];
	} u;
	if (len * 4 >= sizeof(u.buf)) {
	    mb_reply_word(CMD_ERROR, ~0UL);
	}
	else {
	    w32cpy(u.w, data, len);
	    u.w[len] = 0;
	    uart_printf("execute: %s\n", u.buf);
	    cmd_cli(u.buf);
	    mb_reply_word(CMD_REPLY, 0UL);
	}
	break;
    }

    case CMD_TX_GET_CONFIG:
	cmd_tx_get_config(data, len);
	break;
    case CMD_TX_SET_SOURCE:
	cmd_tx_set_source(data, len);
	break;
    case CMD_TX_GET_SOURCE:
	cmd_tx_get_source (data, len);
	break;
    case CMD_TX_SEND_PACKET:
	cmd_tx_send_packet(data, len);
	break;

    case CMD_RX_GET_CONFIG:
	cmd_rx_get_config(data, len);
	break;
    case CMD_RX_SET_SOURCE:
	cmd_rx_set_source(data, len);
	break;
    case CMD_RX_GET_SOURCE:
	cmd_rx_get_source(data, len);
	break;
    case CMD_RX_SET_COND:
	cmd_rx_set_cond(data, len);
	break;
    case CMD_RX_GET_COND:
	cmd_rx_get_cond(data, len);
	break;

    case CMD_RX_SET_COMPARATOR:
	cmd_rx_set_comparator(data, len);
	break;
    case CMD_RX_GET_COMPARATOR:
	cmd_rx_get_comparator(data, len);
	break;
    case CMD_RX_COMPARATOR_STATUS:
	cmd_rx_get_comparator_status(data, len);
	break;
    case CMD_RX_ABORT_COMPARATOR:
	cmd_rx_abort_comparator(data, len);
	break;

    case CMD_RX_SET_ACTION:
	cmd_rx_set_action(data, len);
	break;
    case CMD_RX_GET_ACTION:
	cmd_rx_get_action(data, len);
	break;
    case CMD_RX_DEL_ACTION:
	cmd_rx_del_action(data, len);
	break;
    case CMD_RX_MOD_ACTION:
	cmd_rx_mod_action(data, len);
	break;
    case CMD_RX_IMM_ACTION:
	cmd_rx_imm_action(data, len);
	break;

    case CMD_RX_SET_OUT_CFG:
	cmd_rx_set_out_cfg(data, len);
	break;
    case CMD_RX_GET_OUT_CFG:
	cmd_rx_get_out_cfg(data, len);
	break;

    case CMD_RX_PULSERS_STATUS:
	cmd_rx_get_pulsers_status(data, len);
	break;
    case CMD_RX_GET_PULSER:
	cmd_rx_get_pulser(data, len);
	break;
    case CMD_RX_ABORT_PULSERS:
	cmd_rx_abort_pulsers(data, len);
	break;

    case CMD_RX_SUBSCRIBE:
	cmd_rx_subscribe(data, len);
	break;
    case CMD_RX_UNSUBSCRIBE:
	cmd_rx_unsubscribe(data, len);
	break;
    case CMD_RX_GET_SUBSCRIBED:
	cmd_rx_get_subscribed(data, len);
	break;

    case CMD_RX_LOG_INDEX:
	cmd_rx_log_index(data, len);
	break;
    case CMD_RX_LOG_READ:
	cmd_rx_log_read(data, len);
	break;

    case CMD_RX_RESET:
	cmd_rx_reset(data, len);
	break;

    default:
	uart_printf (" ???\n");
	mb_reply_word(CMD_REPLY | CMD_ERROR, ~0UL);
	break;
    }

    /* Clear ready bit, and send irq to host */
    regs->mb_h2b_board.csr = MAILBOX_IN_REGS_MAP_CSR_READY;
    host_msg_doorbell();
}

static void
main_loop(void)
{
    unsigned isr;
    unsigned csr;
    unsigned pulses;

    regs->pulsers_mask = ~0;

    uart_printf("main-loop, press any key for menu\n");
    while (1) {
	isr = nic->eic.eic_isr;
	if (isr & NIC_EIC_EIC_IER_RCOMP)
	    nic_rx_handler();
	if (isr & (NIC_EIC_EIC_IER_TCOMP | NIC_EIC_EIC_IER_TXERR)) {
	    /* Clear EIC isr */
	    nic->eic.eic_isr = isr & (NIC_EIC_EIC_ISR_TCOMP
				      | NIC_EIC_EIC_ISR_TXERR);
	    nic_tx_handler();
	}

	csr = regs->mb_h2b_board.csr;
	if (csr & MAILBOX_IN_REGS_MAP_CSR_READY)
	    exec_host_command();

	pulses = regs->pulsers_int;
	if (pulses != 0)
	    pulses_handler(pulses);

	if (uart_can_read())
	    cli_menu();
    }
}

/* pl_reset0 is gpio 95 */
#define MASK_DATA_5_MSW 0x00FF0A002C
#define GPIO_DIRM_5     0x00FF0A0344
#define GPIO_OEN_5      0x00FF0A0348

#define GPIO_DATA_5     0x00FF0A0054
#define GPIO_DATA_5_RO  0x00FF0A0074

static void
pl_reset_init(void)
{
    unsigned v;

    /* output */
    v = read32(GPIO_DIRM_5);
    write32(GPIO_DIRM_5, v | (1<<31));

    /* enable */
    v = read32(GPIO_OEN_5);
    write32(GPIO_OEN_5, v | (1<<31));
}

static void
pl_reset_assert(void)
{
    write32(MASK_DATA_5_MSW, (0x7fff << 16));
}

static void
pl_reset_deassert(void)
{
    write32(MASK_DATA_5_MSW, (0x7fff << 16) | 0x8000);
}

int main(void)
{
    uart_printf("Start of main_app\n");

    pl_reset_init();
    pl_reset_assert();

    board_pll_init();

    pl_reset_deassert();

    uart_printf("before reading version\n");

    uart_printf("Map version: 0x%08x\n", (unsigned)regs->version);

    board_init();

    nic_init ();

    /* Clear the mailboxes, just in case */
    while (1) {
	unsigned csr;
    	csr = regs->mb_h2b_board.csr;
	if (!(csr & MAILBOX_IN_REGS_MAP_CSR_READY))
	    break;
	uart_printf("Discard H2B command\n");
    	regs->mb_h2b_board.csr = MAILBOX_IN_REGS_MAP_CSR_READY;
    }

    wrenrx_init();

    /* Empty async memory */
    regs->mb_async_board.board_offset = regs->mb_async_board.host_offset;

    main_loop();

    return 0;
}
