#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "lib.h"
#include "wrenrx-data.h"
#include "wren-hw.h"
#include "wren.h"
#include "wren-mb-defs.h"
#include "wren/wren-packet.h"
#include "board_map.h"
#include "pulser_group_map.h"
#include "wrenrx-core.h"
#include "wren/wren-hw.h"

static volatile struct board_map * const regs =
    (volatile struct board_map *)BOARD_MAP_BASE;

#define NS_PER_SEC 1000000000

static int
alloc_comparator_for_pulser(unsigned pulser_idx)
{
    volatile struct pulser_group_map *grp;
    uint32_t status;
    unsigned cmp_idx;

    grp = &regs->pulser_group[pulser_idx / WREN_COMPARATORS_PER_GROUP].el;

    /* Find a comparator */
    cmp_idx = (pulser_idx & (WREN_COMPARATORS_PER_GROUP - 1)) << 2;
    status = (grp->comp_status >> cmp_idx) & 0x0f;

    /* TODO: be sure the last loaded comparator cannot be overwritten */

    if ((status & 0x3) == 0x03) {
	cmp_idx += 2;
	status >>= 2;
    }
    if ((status & 1) != 0) {
	if (status == 3) {
	    uart_printf("all comparators busy for pulser %u\n", pulser_idx);
	    return -1;
	}
	cmp_idx++;
    }
    return cmp_idx;
}

void
cmd_rx_get_config (uint32_t *data, uint32_t len)
{
  struct wren_mb_rx_get_config_reply *res;

  res = (struct wren_mb_rx_get_config_reply *)mb_get_reply
    (CMD_RX_GET_CONFIG | CMD_REPLY, sizeof(*res) / 4);
  res->sw_version = SW_VERSION;
  res->max_sources = MAX_RX_SOURCES;
  res->max_conds = MAX_RX_CONDS;
  res->max_actions = MAX_RX_ACTIONS;

  mb_send_reply();
}

static void
setup_comparator (volatile struct comparators *cmp,
		  struct wren_mb_pulser_config *cfg)

{
    cmp->conf1 =
	((cfg->start << PULSER_GROUP_MAP_COMPARATORS_CONF1_START_SHIFT)
	 & PULSER_GROUP_MAP_COMPARATORS_CONF1_START_MASK)
	| ((cfg->stop << PULSER_GROUP_MAP_COMPARATORS_CONF1_STOP_SHIFT)
	   & PULSER_GROUP_MAP_COMPARATORS_CONF1_STOP_MASK)
	| ((cfg->clock << PULSER_GROUP_MAP_COMPARATORS_CONF1_CLOCK_SHIFT)
	   & PULSER_GROUP_MAP_COMPARATORS_CONF1_CLOCK_MASK)
	| ((cfg->pulser_idx << PULSER_GROUP_MAP_COMPARATORS_CONF1_PULSER_SHIFT)
	   & PULSER_GROUP_MAP_COMPARATORS_CONF1_PULSER_MASK)
	| (cfg->repeat ? PULSER_GROUP_MAP_COMPARATORS_CONF1_REPEAT : 0)
	| (cfg->immediat ? PULSER_GROUP_MAP_COMPARATORS_CONF1_IMMEDIAT : 0);
    cmp->high = cfg->width;
    cmp->npulses = cfg->npulses;
    if (cfg->period == 0) {
	/* level */
	cmp->npulses = 0;
    }
    else
	cmp->period = cfg->period;
    cmp->idelay = cfg->idelay;
}

void
cmd_rx_get_comparator (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_mb_comparator *res;
    uint32_t idx = cmd->arg1;
    volatile struct comparators *cmp;
    volatile struct pulser_group_map *grp;
    unsigned val;

    /* Check index. */
    if (idx >= 32) {
	mb_reply_word(CMD_RX_GET_COMPARATOR | CMD_ERROR, ~0UL);
	return;
    }
    grp = &regs->pulser_group[idx / WREN_COMPARATORS_PER_GROUP].el;
    idx &= (WREN_COMPARATORS_PER_GROUP - 1);

    cmp = &grp->comparators[idx];

    res = (struct wren_mb_comparator *)mb_get_reply
	(CMD_RX_GET_COMPARATOR | CMD_REPLY, sizeof(*res) / 4);

    res->sec = cmp->time_sec;
    res->nsec = cmp->time_nsec;

    val = cmp->conf1;
    res->conf.start = (val & PULSER_GROUP_MAP_COMPARATORS_CONF1_START_MASK)
	>> PULSER_GROUP_MAP_COMPARATORS_CONF1_START_SHIFT;
    res->conf.stop = (val & PULSER_GROUP_MAP_COMPARATORS_CONF1_STOP_MASK)
	>> PULSER_GROUP_MAP_COMPARATORS_CONF1_STOP_SHIFT;
    res->conf.clock = (val & PULSER_GROUP_MAP_COMPARATORS_CONF1_CLOCK_MASK)
	>> PULSER_GROUP_MAP_COMPARATORS_CONF1_CLOCK_SHIFT;
    res->conf.pulser_idx =
	((val & PULSER_GROUP_MAP_COMPARATORS_CONF1_PULSER_MASK)
	 >> PULSER_GROUP_MAP_COMPARATORS_CONF1_PULSER_SHIFT);
    res->conf.repeat =
	(val & PULSER_GROUP_MAP_COMPARATORS_CONF1_REPEAT) ? 1 : 0;
    res->conf.immediat =
	(val & PULSER_GROUP_MAP_COMPARATORS_CONF1_IMMEDIAT) ? 1 : 0;

    res->conf.width = cmp->high;
    res->conf.period = cmp->period;
    res->conf.npulses = cmp->npulses;
    res->conf.idelay = cmp->idelay;

    mb_send_reply();
}

void
cmd_rx_set_comparator (uint32_t *data, uint32_t len)
{
    struct wren_mb_rx_set_comparator *arg =
	(struct wren_mb_rx_set_comparator *)data;
    uint32_t idx = arg->comp_idx;
    volatile struct comparators *cmp;
    volatile struct pulser_group_map *grp;

    /* Check index. */
    if (idx >= 4*WREN_COMPARATORS_PER_GROUP) {
	mb_reply_word(CMD_RX_SET_COMPARATOR | CMD_ERROR, ~0UL);
	return;
    }

    grp = &regs->pulser_group[idx / WREN_COMPARATORS_PER_GROUP].el;
    idx &= WREN_COMPARATORS_PER_GROUP - 1;

    /* Check busy */
    if ((grp->comp_status >> idx) & 1) {
	mb_reply_word(CMD_RX_SET_COMPARATOR | CMD_ERROR, ~1UL);
	return;
    }

    cmp = &grp->comparators[idx];

    cmp->time_sec = arg->cfg.sec;
    cmp->time_nsec = arg->cfg.nsec;
    setup_comparator(cmp, &arg->cfg.conf);

    /* Enable */
    grp->comp_status = (1 << idx);

    mb_reply_word(CMD_RX_SET_COMPARATOR | CMD_REPLY, 0);
}

void
cmd_rx_get_comparator_status (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_mb_arg_reply *rep;
    unsigned grp_idx = cmd->arg1;
    volatile struct pulser_group_map *grp;

    if (grp_idx > 3) {
	mb_reply_word(CMD_RX_COMPARATOR_STATUS | CMD_REPLY, ~0UL);
	return;
    }

    grp = &regs->pulser_group[grp_idx].el;

    rep = (struct wren_mb_arg_reply *)mb_get_reply
	(CMD_RX_COMPARATOR_STATUS | CMD_REPLY, sizeof(*rep) / 4);
    rep->arg1 = grp->comp_status;
    rep->arg2 = 0;

    mb_send_reply();
}

void
cmd_rx_abort_comparator (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg2 *arg =(struct wren_mb_arg2 *)data;
    uint32_t grp_idx = arg->arg1;
    volatile struct pulser_group_map *grp;

    /* Check index. */
    if (grp_idx >= 4) {
	mb_reply_word(CMD_RX_ABORT_COMPARATOR | CMD_ERROR, ~0UL);
	return;
    }

    grp = &regs->pulser_group[grp_idx].el;

    grp->comp_abort = arg->arg2;

    mb_reply_word(CMD_RX_ABORT_COMPARATOR | CMD_REPLY, 0);
}

void
cmd_rx_get_pulsers_status (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_mb_arg_reply *rep;
    unsigned grp_idx = cmd->arg1;
    volatile struct pulser_group_map *grp;

    if (grp_idx > 3) {
	mb_reply_word(CMD_RX_PULSERS_STATUS | CMD_REPLY, ~0UL);
	return;
    }

    grp = &regs->pulser_group[grp_idx].el;

    rep = (struct wren_mb_arg_reply *)mb_get_reply
	(CMD_RX_PULSERS_STATUS | CMD_REPLY, sizeof(*rep) / 4);
    rep->arg1 = grp->pulsers_running;
    rep->arg2 = grp->pulsers_loaded;

    mb_send_reply();
}

void
cmd_rx_get_pulser (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_mb_rx_get_pulser_reply *rep;
    unsigned idx = cmd->arg1;
    volatile struct pulser_group_map *grp;

    if (idx > 31) {
	mb_reply_word(CMD_RX_GET_PULSER | CMD_REPLY, ~0UL);
	return;
    }
    grp = &regs->pulser_group[idx / WREN_PULSERS_PER_GROUP].el;

    idx &= WREN_PULSERS_PER_GROUP - 1;

    rep = (struct wren_mb_rx_get_pulser_reply *)mb_get_reply
	(CMD_RX_GET_PULSER | CMD_REPLY, sizeof(*rep) / 4);
    rep->state = grp->pulsers[idx].state;
    rep->cur_comp = grp->pulsers[idx].current_comp;
    rep->ts = grp->pulsers[idx].ts;
    rep->loaded_comp = grp->pulsers[idx].loaded_comp;
    rep->pulses = grp->pulsers[idx].pulses;

    mb_send_reply();
}

void
cmd_rx_abort_pulsers (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg2 *arg =(struct wren_mb_arg2 *)data;
    uint32_t grp_idx = arg->arg1;
    volatile struct pulser_group_map *grp;

    /* Check index. */
    if (grp_idx >= 4) {
	mb_reply_word(CMD_RX_ABORT_PULSERS | CMD_ERROR, ~0UL);
	return;
    }

    grp = &regs->pulser_group[grp_idx].el;

    grp->pulsers_abort = arg->arg2;

    mb_reply_word(CMD_RX_ABORT_PULSERS | CMD_REPLY, 0);
}

void
cmd_rx_get_out_cfg (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_mb_out_cfg *rep;
    unsigned out_idx = cmd->arg1;
    unsigned bit_idx;
    unsigned cfg;
    volatile struct pulser_group_map *grp;

    if (out_idx > 32) {
	mb_reply_word(CMD_RX_GET_OUT_CFG | CMD_ERROR, ~0UL);
	return;
    }

    bit_idx = out_idx & (WREN_PULSERS_PER_GROUP - 1);
    grp = &regs->pulser_group[out_idx / WREN_PULSERS_PER_GROUP].el;
    cfg = grp->out_cfg[bit_idx].config;

    rep = (struct wren_mb_out_cfg  *)mb_get_reply
	(CMD_RX_GET_OUT_CFG | CMD_REPLY, sizeof(*rep) / 4);
    rep->out_idx = out_idx;
    rep->mask = (cfg & PULSER_GROUP_MAP_OUT_CFG_CONFIG_MASK_MASK)
	>> PULSER_GROUP_MAP_OUT_CFG_CONFIG_MASK_SHIFT;
    rep->inv_in = (cfg & PULSER_GROUP_MAP_OUT_CFG_CONFIG_INV_IN) ? 1 : 0;
    rep->inv_out = (cfg & PULSER_GROUP_MAP_OUT_CFG_CONFIG_INV_OUT) ? 1 : 0;

    rep->oe = (regs->lemo_oe >> bit_idx) & 1;
    rep->term = (regs->lemo_term >> bit_idx) & 1;

    mb_send_reply();
}

void
cmd_rx_set_out_cfg (uint32_t *data, uint32_t len)
{
    struct wren_mb_out_cfg *cmd = (struct wren_mb_out_cfg *)data;
    unsigned out_idx = cmd->out_idx;
    unsigned cfg;
    uint32_t term, oe, mask;
    volatile struct pulser_group_map *grp;

    if (out_idx > 32) {
	mb_reply_word(CMD_RX_SET_OUT_CFG | CMD_REPLY, ~0UL);
	return;
    }

    grp = &regs->pulser_group[out_idx / WREN_PULSERS_PER_GROUP].el;

    /* Set combinatorial output configuration */
    cfg = ((cmd->mask) << PULSER_GROUP_MAP_OUT_CFG_CONFIG_MASK_SHIFT)
	& PULSER_GROUP_MAP_OUT_CFG_CONFIG_MASK_MASK;
    cfg |= cmd->inv_in ? PULSER_GROUP_MAP_OUT_CFG_CONFIG_INV_IN : 0;
    cfg |= cmd->inv_out ? PULSER_GROUP_MAP_OUT_CFG_CONFIG_INV_OUT : 0;

    grp->out_cfg[out_idx & (WREN_PULSERS_PER_GROUP - 1)].config = cfg;

    /* Control OE + TERM */
    oe = regs->lemo_oe;
    term = regs->lemo_term;

    mask = 1 << out_idx;

    if (cmd->oe)
	oe |= mask;
    else
	oe &= ~mask;

    if (cmd->term)
	term |= mask;
    else
	term &= ~mask;

    regs->lemo_oe = oe;
    regs->lemo_term = term;

    mb_reply_word(CMD_RX_SET_OUT_CFG | CMD_REPLY, 0);
}

void
cmd_rx_set_source (uint32_t *data, uint32_t len)
{
    struct wren_mb_rx_set_source *arg = (struct wren_mb_rx_set_source *)data;
    struct wren_rx_source *src;
    unsigned idx = arg->idx;

    /* Check index. */
    if (len != sizeof(*arg)/4 || idx >= MAX_RX_SOURCES) {
	mb_reply_word(CMD_RX_SET_SOURCE | CMD_ERROR, ~0UL);
	return;
    }

    src = &wren_rx_sources[idx];

    /* Disable this source entry while it is modified.  */
    src->proto.proto = WREN_PROTO_NONE;
    barrier;

    src->proto.u = arg->cfg.proto.u;

    src->dest = arg->cfg.dest;

    src->subsample = arg->cfg.subsample;
    src->subsample_cnt = 0;

    src->nbr_frames = 0;
    src->next_seqid = PKT_SEQ_SYNC;
    memset(&src->subscribed_map, 0, sizeof(src->subscribed_map));

    barrier;
    src->proto.proto = arg->cfg.proto.proto;

    mb_reply_word(CMD_RX_SET_SOURCE | CMD_REPLY, 0);
}

void
cmd_rx_get_source (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_mb_rx_get_source_reply *res;
    struct wren_rx_source *src;
    unsigned idx = cmd->arg1;

    /* Check index. */
    if (idx >= MAX_RX_SOURCES) {
	mb_reply_word(CMD_RX_GET_SOURCE | CMD_REPLY, ~0UL);
	return;
    }

    src = &wren_rx_sources[idx];

    res = (struct wren_mb_rx_get_source_reply *)mb_get_reply
	(CMD_RX_GET_SOURCE | CMD_REPLY, sizeof(*res) / 4);

    res->cfg.dest = src->dest;
    res->cfg.subsample = src->subsample;
    res->cfg.proto = src->proto;
    res->cond_idx = src->conds;

    mb_send_reply();
}

void
cmd_rx_set_cond (uint32_t *data, uint32_t len)
{
    struct wren_mb_rx_set_cond *arg = (struct wren_mb_rx_set_cond *)data;
    unsigned idx = arg->cond_idx;
    unsigned src_idx = arg->cond.src_idx;
    struct wren_rx_cond *cond;

    /* Check index */
    if (idx >= MAX_RX_CONDS || src_idx >= MAX_RX_SOURCES) {
	mb_reply_word(CMD_RX_SET_COND | CMD_ERROR, ~0UL);
	return;
    }

    /* Check condition is not used */
    cond = &wren_rx_conds[idx];
    if (cond->cond.evt_id != WREN_EVENT_ID_INVALID) {
	mb_reply_word(CMD_RX_SET_COND | CMD_ERROR, ~1UL);
	return;
    }

    /* Fill condition */
    cond->cond.evt_id = arg->cond.evt_id;
    cond->cond.src_idx = src_idx;
    cond->act_idx = NO_RX_ACT_IDX;
    cond->cond.len = arg->cond.len;
    for (unsigned i = 0; i < cond->cond.len; i++)
	cond->cond.ops[i] = arg->cond.ops[i];

    /* Chain condition */
    cond->next = wren_rx_sources[src_idx].conds;
    wren_rx_sources[src_idx].conds = idx;

    mb_reply_word(CMD_RX_SET_COND | CMD_REPLY, 0);
}

void
cmd_rx_get_cond (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_rx_cond *res;
    struct wren_rx_cond *cond;
    unsigned idx = cmd->arg1;

    /* Check index. */
    if (idx >= MAX_RX_CONDS) {
	mb_reply_word(CMD_RX_GET_COND | CMD_REPLY, ~0UL);
	return;
    }

    cond = &wren_rx_conds[idx];

    res = (struct wren_rx_cond *)mb_get_reply
	(CMD_RX_GET_COND | CMD_REPLY, sizeof(*res) / 4);

    *res = *cond;

    mb_send_reply();
}

void
cmd_rx_set_action (uint32_t *data, uint32_t len)
{
    struct wren_mb_rx_set_action *arg = (struct wren_mb_rx_set_action *)data;
    struct wren_rx_action *act;
    struct wren_rx_cond *cond;
    unsigned idx = arg->act_idx;
    unsigned cond_idx = arg->cond_idx;

    /* Check index. */
    if (idx >= MAX_RX_ACTIONS || cond_idx >= MAX_RX_CONDS
	|| arg->conf.pulser_idx > 7) {
	mb_reply_word(CMD_RX_SET_ACTION | CMD_ERROR, ~0UL);
	return;
    }

    act = &wren_rx_actions[idx];
    if (act->cond_idx != NO_RX_COND_IDX)  {
	mb_reply_word(CMD_RX_SET_ACTION | CMD_ERROR, ~1UL);
	return;
    }

    cond = &wren_rx_conds[cond_idx];

    act->cond_idx = cond_idx;

    memcpy (&act->conf, &arg->conf, sizeof (struct wren_mb_pulser_config));

    /* Link */
    act->next = cond->act_idx;
    cond->act_idx = idx;

    mb_reply_word(CMD_RX_SET_ACTION | CMD_REPLY, 0);
}

void
cmd_rx_imm_action (uint32_t *data, uint32_t len)
{
    struct wren_mb_rx_imm_action *arg = (struct wren_mb_rx_imm_action *)data;
    struct wren_rx_action *act;
    unsigned idx = arg->act_idx;
    unsigned pulser_idx = arg->cmp.conf.pulser_idx;
    volatile struct pulser_group_map *grp;
    volatile struct comparators *cmp;
    int cmp_idx;

    /* Check index. */
    if (idx >= MAX_RX_ACTIONS || pulser_idx > 7) {
	mb_reply_word(CMD_RX_IMM_ACTION | CMD_ERROR, ~0UL);
	return;
    }

    act = &wren_rx_actions[idx];
    if (act->cond_idx != NO_RX_COND_IDX)  {
	mb_reply_word(CMD_RX_IMM_ACTION | CMD_ERROR, ~1UL);
	return;
    }

    cmp_idx = alloc_comparator_for_pulser(pulser_idx);
    if (cmp_idx < 0) {
	mb_reply_word(CMD_RX_IMM_ACTION | CMD_ERROR, ~2UL);
	return;
    }

    act->cond_idx = INV_RX_COND_IDX;

    memcpy (&act->conf, &arg->cmp.conf, sizeof (struct wren_mb_pulser_config));

    /* Load comparator */
    grp = &regs->pulser_group[pulser_idx / WREN_COMPARATORS_PER_GROUP].el;
    cmp = (volatile struct comparators *)grp->comparators;
    cmp = &cmp[cmp_idx];

    cmp->time_sec = arg->cmp.sec;
    cmp->time_nsec = arg->cmp.nsec;
    setup_comparator(cmp, &arg->cmp.conf);

    /* Enable */
    grp->comp_status = (1 << cmp_idx);

    uart_printf("imm: load comp %u\n", cmp_idx);

    mb_reply_word(CMD_RX_IMM_ACTION | CMD_REPLY, cmp_idx);
}

void
cmd_rx_del_action (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_rx_action *act;
    unsigned idx = cmd->arg1;
    struct wren_rx_cond *cond;

    /* Check index. */
    if (idx >= MAX_RX_ACTIONS) {
	mb_reply_word(CMD_RX_DEL_ACTION | CMD_ERROR, ~0UL);
	return;
    }

    /* Check action is allocated */
    act = &wren_rx_actions[idx];
    if (act->cond_idx == NO_RX_COND_IDX)  {
	mb_reply_word(CMD_RX_DEL_ACTION | CMD_ERROR, ~1UL);
	return;
    }

    /* Unlink action from conditions chain */
    if (act->cond_idx != INV_RX_COND_IDX) {
	cond = &wren_rx_conds[act->cond_idx];
	if (cond->act_idx == idx)
	    cond->act_idx = act->next;
	else {
	    struct wren_rx_action *link = &wren_rx_actions[cond->act_idx];
	    while (link->next != idx)
		link = &wren_rx_actions[link->next];
	    link->next = act->next;
	}
    }
    act->cond_idx = NO_RX_COND_IDX;

    mb_reply_word(CMD_RX_DEL_ACTION | CMD_REPLY, 0);
}

void
cmd_rx_mod_action (uint32_t *data, uint32_t len)
{
    struct wren_mb_rx_mod_action *cmd = (struct wren_mb_rx_mod_action *)data;
    struct wren_rx_action *act;
    unsigned idx = cmd->act_idx;

    /* Check index. */
    if (idx >= MAX_RX_ACTIONS) {
	mb_reply_word(CMD_RX_MOD_ACTION | CMD_ERROR, ~0UL);
	return;
    }

    /* Check action is allocated */
    act = &wren_rx_actions[idx];
    if (act->cond_idx == NO_RX_COND_IDX)  {
	mb_reply_word(CMD_RX_MOD_ACTION | CMD_ERROR, ~1UL);
	return;
    }

    memcpy (&act->conf, &cmd->conf, sizeof (struct wren_mb_pulser_config));

    mb_reply_word(CMD_RX_MOD_ACTION | CMD_REPLY, 0);
}

void
cmd_rx_get_action (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_rx_action *res;
    struct wren_rx_action *act;
    unsigned idx = cmd->arg1;

    /* Check index. */
    if (idx >= MAX_RX_ACTIONS) {
	mb_reply_word(CMD_RX_GET_ACTION | CMD_REPLY, ~0UL);
	return;
    }

    act = &wren_rx_actions[idx];

    if (act->cond_idx == NO_RX_COND_IDX)  {
	mb_reply_word(CMD_RX_GET_ACTION | CMD_ERROR, ~1UL);
	return;
    }

    res = (struct wren_rx_action *)mb_get_reply
	(CMD_RX_GET_ACTION | CMD_REPLY, sizeof(*res) / 4);

    *res = *act;

    mb_send_reply();
}

static void
cmd_rx_change_subscribe (uint32_t *data, uint32_t len, unsigned cmd)
{
    struct wren_mb_rx_subscribe *msg = (struct wren_mb_rx_subscribe *)data;
    unsigned src_idx = msg->src_idx;
    unsigned ev_id = msg->ev_id;
    struct wren_rx_source *src;
    unsigned res;
    unsigned mask;

    /* Check index. */
    if (src_idx >= MAX_RX_SOURCES
	|| ev_id >= 32 * RX_SUBSCRIBE_MAP_LEN) {
	res = ~0UL;
	goto done;
    }

    src = &wren_rx_sources[src_idx];
    if (src->proto.proto == WREN_PROTO_NONE) {
	res = ~1UL;
	goto done;
    }

    mask = 1 << (ev_id & 0x1f);
    if (cmd == CMD_RX_SUBSCRIBE)
	src->subscribed_map[ev_id >> 5] |= mask;
    else
	src->subscribed_map[ev_id >> 5] &= ~mask;
    res = 0;

  done:
    mb_reply_word(cmd | CMD_REPLY, res);
    return;
}

void
cmd_rx_subscribe (uint32_t *data, uint32_t len, int on_off)
{
    cmd_rx_change_subscribe (data, len, CMD_RX_SUBSCRIBE);
}

void
cmd_rx_unsubscribe (uint32_t *data, uint32_t len, int on_off)
{
    cmd_rx_change_subscribe (data, len, CMD_RX_UNSUBSCRIBE);
}

void
cmd_rx_get_subscribed (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_mb_get_subscribed_reply *res;
    struct wren_rx_source *src;
    unsigned src_idx = cmd->arg1;

    /* Check index. */
    if (src_idx >= MAX_RX_SOURCES) {
	mb_reply_word(CMD_RX_GET_SUBSCRIBED | CMD_REPLY, ~0UL);
	return;
    }

    src = &wren_rx_sources[src_idx];

    res = (struct wren_mb_get_subscribed_reply *)mb_get_reply
	(CMD_RX_GET_SUBSCRIBED | CMD_REPLY, sizeof(*res) / 4);

    memcpy (res->map, src->subscribed_map, sizeof(res->map));

    mb_send_reply();
}

void
cmd_rx_log_index (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    struct wren_mb_arg_reply *rep;
    unsigned log_idx = cmd->arg1;

    if (log_idx >= 64) {
	mb_reply_word(CMD_RX_LOG_INDEX | CMD_REPLY, ~0UL);
	return;
    }

    rep = (struct wren_mb_arg_reply *)mb_get_reply
	(CMD_RX_LOG_INDEX | CMD_REPLY, sizeof(*rep) / 4);
    rep->arg1 = regs->logs.addr[log_idx].val;
    rep->arg2 = 0;

    mb_send_reply();
}

#define LOG_BASE 0x40000000 /* Logs start at 1GB */

/* Number of address bits for log entries of an input (or a pulser)
   1GB for the logs / 64 inputs  */
#define LOG_ADDR_BITS (30 - 6)

static void
r5_clean_invalidate_data_cache_line_by_mva (uint32_t addr)
{
    /*          MRC coproc,op1,Rt,CRn,CRm,op2 */
    asm volatile ("mcr p15,0,%0,c7,c14,1" : : "r"(addr & ~0x1f));
}

static void
r5_data_sync_barrier(void)
{
    asm volatile ("dsb");
}

void
cmd_rx_log_read (uint32_t *data, uint32_t len)
{
    struct wren_mb_rx_get_log *cmd = (struct wren_mb_rx_get_log *)data;
    uint32_t *rep;
    uint32_t nentries = cmd->nentries;
    unsigned log_idx = cmd->log_idx;
    uint32_t addr;

    if (log_idx >= 64 || nentries == 0) {
	mb_reply_word(CMD_RX_LOG_READ | CMD_REPLY, ~0UL);
	return;
    }

    rep = (uint32_t *)mb_get_reply
	(CMD_RX_LOG_READ | CMD_REPLY, nentries);

    /* Set the address to the entry before the index */
    addr = LOG_BASE | (log_idx << LOG_ADDR_BITS);
    addr |= (cmd->entry_idx - 4) & ((1 << LOG_ADDR_BITS) - 4);

    /* Clear cache line */
    r5_clean_invalidate_data_cache_line_by_mva (addr);
    r5_data_sync_barrier();

    while (1) {
	/* Copy the entry */
	*rep++ = *(uint32_t *)addr;
	if (--nentries == 0)
	    break;

	/* Previous entry */
	if ((addr & ((1 << LOG_ADDR_BITS) - 1)) == 0)
	    addr |= ((1 << LOG_ADDR_BITS) - 1) & ~3;
	else
	    addr -= 4;

	/* If the entry is on a new cache line, invalidate the line */
	if ((addr & 0x1f) == 0x1c) {
	    r5_clean_invalidate_data_cache_line_by_mva (addr);
	    r5_data_sync_barrier();
	}
    }

    mb_send_reply();
}

void
cmd_rx_reset (uint32_t *data, uint32_t len)
{
    wrenrx_init();
    mb_reply_word(CMD_REPLY, 0);
}


#define ASYNC_WSIZE (sizeof(regs->mem_async_board) / 4)
static int
async_alloc(unsigned len, unsigned *off)
{
    unsigned b_off = regs->mb_async_board.board_offset;
    unsigned h_off = regs->mb_async_board.host_offset;
    int used;
    unsigned avail;

    used = b_off - h_off;
    if (used < 0)
	used = ASYNC_WSIZE + used;
    avail = ASYNC_WSIZE - used;

    if (len > avail) {
	uart_printf("async_alloc failued (len=%u, avail=%u, b_off=%u, h_off=%u)\n",
		    len, avail, b_off, h_off);
	return -1;
    }
    // uart_printf("async_alloc: off=%u, len=%u\n", b_off, len);
    *off = b_off;
    return 0;
}

static unsigned
async_write32(unsigned off, uint32_t val)
{
    // uart_printf("async[%u] = %08x\n", off, (unsigned)val);
    regs->mem_async_board[off].data = val;
    return (off + 1) & (ASYNC_WSIZE - 1);
}

static void
async_update(unsigned off)
{
    // uart_printf("async_update: off=%u\n", off);
    regs->mb_async_board.board_offset = off;
    host_async_doorbell();
}

void
wrenrx_run_event(unsigned src_idx, struct wren_capsule_event_hdr *evt)
{
    unsigned hlen = sizeof(struct wren_capsule_event_hdr) / 4;
    unsigned len = evt->hdr.len;
    unsigned i;
    union wren_capsule_hdr_un hdr;
    uint32_t *data;
    unsigned off;

    if (async_alloc(len, &off) < 0)
	return;

    uart_printf("send event %u\n", evt->ev_id);

    hdr.hdr.typ = CMD_ASYNC_EVENT;
    hdr.hdr.pad = src_idx;
    hdr.hdr.len = len;
    off = async_write32(off, hdr.u32);
    off = async_write32(off, evt->ev_id | (evt->ctxt_id << 16));
    off = async_write32(off, evt->ts.nsec);
    off = async_write32(off, evt->ts.sec);

    data = (uint32_t *)(evt + 1);
    for (i = 0; i < len - hlen; i++)
	off = async_write32(off, data[i]);
    async_update(off);
}

void
wrenrx_run_context(unsigned src_idx, struct wren_rx_context *ctxt)
{
    unsigned len = 1 + 1 + 2 + ctxt->len;
    unsigned i;
    union wren_capsule_hdr_un hdr;
    unsigned off;

    if (async_alloc(len, &off) < 0)
	return;

    uart_printf("send context %u\n", ctxt->ctxt_id);

    hdr.hdr.typ = CMD_ASYNC_CONTEXT;
    hdr.hdr.pad = src_idx;
    hdr.hdr.len = len;
    /* Capsule header */
    off = async_write32(off, hdr.u32);
    off = async_write32(off, ctxt->ctxt_id);
    off = async_write32(off, ctxt->valid_from.nsec);
    off = async_write32(off, ctxt->valid_from.sec);
    /* Parameters */
    for (i = 0; i < ctxt->len; i++)
	off = async_write32(off, ctxt->params[i]);
    async_update(off);
}

void
wrenrx_run_action (unsigned src_idx, unsigned act_idx, struct wren_capsule_event_hdr *evt)
{
    struct wren_rx_action *act = &wren_rx_actions[act_idx];
    volatile struct pulser_group_map *grp;
    unsigned pulser_idx = act->conf.pulser_idx;
    volatile struct comparators *cmp;
    union wren_capsule_hdr_un hdr;
    int cmp_idx;
    uint32_t due_sec;
    int32_t due_nsec;
    unsigned off;

    grp = &regs->pulser_group[pulser_idx / WREN_COMPARATORS_PER_GROUP].el;

    cmp_idx = alloc_comparator_for_pulser(pulser_idx);
    if (cmp_idx < 0)
	return;

    /* Compute due time */
    due_sec = evt->ts.sec + act->conf.load_off_sec;
    due_nsec = evt->ts.nsec + act->conf.load_off_nsec;
    if (due_nsec < 0) {
	due_sec--;
	due_nsec += NS_PER_SEC;
    }
    else if (due_nsec > NS_PER_SEC) {
	due_sec++;
	due_nsec -= NS_PER_SEC;
    }

    /* Load comparator */
    cmp = (volatile struct comparators *)grp->comparators;
    cmp = &cmp[cmp_idx];

    cmp->time_sec = due_sec;
    cmp->time_nsec = due_nsec;
    setup_comparator(cmp, &act->conf);

    /* Enable */
    grp->comp_status = (1 << cmp_idx);

    uart_printf("act: load comp %u\n", cmp_idx);

    /* Send config to the host */
    if (async_alloc(2, &off) < 0)
	return;
    hdr.hdr.typ = CMD_ASYNC_CONFIG;
    hdr.hdr.pad = src_idx;
    hdr.hdr.len = 2;
    off = async_write32(off, hdr.u32);
    off = async_write32(off, act_idx | (cmp_idx << 16));
    async_update(off);
}

void
pulses_handler(unsigned pulses)
{
    unsigned off;
    union wren_capsule_hdr_un hdr;

    /* Ack interrupts */
    regs->pulsers_int = pulses;

    uart_printf("Pulses: %08x\n", pulses);

    while (1) {
	/* Extract a pulser index.  */
	unsigned idx = __builtin_ffs(pulses);
	if (idx == 0)
	    break;
	idx--;

	pulses &= ~(1 << idx);

	unsigned grp_idx = idx / 8;
	volatile struct pulser_group_map *grp = &regs->pulser_group[grp_idx].el;
	idx &= 7;
	unsigned comps = grp->pulsers[idx].pulses;
	unsigned ts = grp->pulsers[idx].ts;
	uint32_t tai = regs->tm_tai_lo;

	/* Expand time-stamp */
	if ((ts >> 30) == ((tai + 1) & 3))
	    tai++;
	else if ((ts >> 30) != (tai & 3))
	    tai = 0;
	ts = ts & 0x3fffffff;

	/* Clear bits */
	grp->pulsers[idx].pulses = comps;
	grp->pulsers[idx].loaded_comp = comps;

	while (comps != 0) {
	    unsigned sub_comp = __builtin_ffs(comps) - 1;
	    unsigned comp_id = grp_idx * 32 + sub_comp;

	    comps &= ~(1 << sub_comp);

	    /* Send comparator idx to the host */
	    if (async_alloc(4, &off) < 0)
		return;
	    hdr.hdr.typ = CMD_ASYNC_PULSE;
	    hdr.hdr.pad = 0;
	    hdr.hdr.len = 4;
	    off = async_write32(off, hdr.u32);
	    off = async_write32(off, comp_id);
	    off = async_write32(off, tai);
	    off = async_write32(off, ts);
	    async_update(off);
	}
    }
}


void
wrenrx_log (const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);
    uart_vprintf(fmt, args);
    va_end(args);
}
