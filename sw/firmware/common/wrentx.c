#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "lib.h"
#include "wren.h"
#include "wrentx-data.h"
#include "wren-hw.h"
#include "wren-mb-defs.h"
#include "nic.h"
#include "wren/wren-packet.h"
#include "board_map.h"
#include "wrentx.h"

struct wren_tx_source wren_tx_sources[MAX_TX_SOURCES];

void
get_now_packet_ts(struct wren_packet_ts *now)
{
  volatile struct board_map *regs =
    (volatile struct board_map *)BOARD_MAP_BASE;
  unsigned lo;

  lo = regs->tm_tai_lo;

  do {
    now->sec = lo;
    now->nsec = regs->tm_cycles << 4;
    lo = regs->tm_tai_lo;
  } while (lo != now->sec);
}

void
cmd_tx_get_config (uint32_t *data, uint32_t len)
{
  struct wren_mb_tx_get_config_reply *res;
  unsigned i;
  unsigned nbr;

  res = (struct wren_mb_tx_get_config_reply *)mb_get_reply
    (CMD_TX_GET_CONFIG | CMD_REPLY, sizeof(*res) / 4);
  res->sw_version = SW_VERSION;
  res->max_sources = MAX_TX_SOURCES;

  nbr = 0;
  for (i = 0; i < MAX_TX_SOURCES; i++)
    if (wren_tx_sources[i].proto.proto != WREN_PROTO_NONE)
      nbr++;
  res->nbr_sources = nbr;

  mb_send_reply();
}

void
cmd_tx_set_source (uint32_t *data, uint32_t len)
{
    struct wren_mb_tx_set_source *arg = (struct wren_mb_tx_set_source *)data;
    uint32_t source_idx = arg->source_idx;
    struct wren_tx_source *src;

    if (source_idx >= MAX_TX_SOURCES) {
	mb_reply_word(CMD_TX_SET_SOURCE | CMD_ERROR, ~0UL);
	return;
    }

    if (arg->proto.proto != WREN_PROTO_ETHERNET) {
	mb_reply_word(CMD_TX_SET_SOURCE | CMD_ERROR, ~1UL);
	return;
    }

    src = &wren_tx_sources[source_idx];
    src->proto = arg->proto;
    src->max_delay_us = arg->max_delay_us;
    src->seq_id = 0;

    mb_reply_word(CMD_TX_SET_SOURCE | CMD_REPLY, 0);
}

void
cmd_tx_get_source (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *arg = (struct wren_mb_arg1 *)data;
    uint32_t source_idx = arg->arg1;
    struct wren_tx_source *src;
    struct wren_protocol *res;

    if (source_idx >= MAX_TX_SOURCES) {
	mb_reply_word(CMD_TX_GET_SOURCE | CMD_ERROR, ~0UL);
	return;
    }

    src = &wren_tx_sources[source_idx];

    res = (struct wren_protocol *)mb_get_reply
      (CMD_TX_GET_SOURCE | CMD_REPLY, sizeof(*res) / 4);

    w32cpy (res, &src->proto, sizeof(src->proto) / 4);
    //    *res = src->proto;

    mb_send_reply();
}

void
w32cpy(void *dst, void *src, unsigned wlen)
{
  volatile uint32_t *d = (volatile uint32_t *)dst;
  uint32_t *s = (uint32_t *)src;

  while (wlen--)
    *d++ = *s++;
}

void
cmd_tx_send_packet (uint32_t *data, uint32_t len)
{
  uint32_t src_idx;
  struct wren_tx_source *src;
  union {
    uint8_t b[16];
    uint32_t w[4];
  } eth_hdr;
  unsigned pkt_len;
  union {
    struct wren_packet_hdr p;
    uint32_t w[sizeof(struct wren_packet_hdr) / 4];
  } phdr;
  uint8_t *buf;
  unsigned ret = ~0U;

  /* Check source */
  if (len < 1)
    goto err;

  src_idx = *data;
  if (src_idx >= MAX_TX_SOURCES)
    goto err;
  src = &wren_tx_sources[src_idx];
  if (src->proto.proto != WREN_PROTO_ETHERNET) {
    uart_printf("send packet: source not defiend\n");
    goto err;
  }

  /* Remove source */
  len--;

  /* Allocate a nic buffer */
  pkt_len = 2 + ETH_HDR_LEN + sizeof(struct wren_packet_hdr) + len * 4;
  buf = (uint8_t*)nic_get_tx_buf (pkt_len);
  if (buf == NULL) {
    uart_printf("send packet: no tx buf\n");
    goto err;
  }

  /* Write protocol header (TODO: vlan + length) */
  memcpy (eth_hdr.b + 2, src->proto.u.eth.mac, 6);
  eth_hdr.b[2 + 6 + 6 + 0] = src->proto.u.eth.ethertype >> 8;
  eth_hdr.b[2 + 6 + 6 + 1] = src->proto.u.eth.ethertype;

  w32cpy(buf, eth_hdr.w, sizeof(eth_hdr) / 4);

  /* Write packet header */
  phdr.p.version = PKT_VERSION;
  phdr.p.source = src->proto.u.eth.source_id;
  phdr.p.pad0 = 0;
  phdr.p.pad1 = 0x7b;
  phdr.p.len = len + sizeof(struct wren_packet_hdr) / 4;
  phdr.p.seq_id = src->seq_id;
  src->seq_id = (src->seq_id + 1) & PKT_SEQ_MASK;
  phdr.p.next_us = src->max_delay_us;

  get_now_packet_ts(&phdr.p.snd_ts);

  w32cpy(buf + 16, phdr.w, sizeof(phdr) / 4);

  /* Copy data */
  w32cpy(buf + 16 + sizeof(struct wren_packet_hdr), data + 1, len);

  if (0) {
    uart_printf("send buf=%x, pkt_len=0x%x\n", (unsigned)buf, pkt_len);
    dump_w32(buf, pkt_len);
  }

  /* Send packet */
  nic_send_tx_buf();
  mb_reply_word(CMD_TX_SEND_PACKET | CMD_REPLY, 0);
  return;

 err:
  mb_reply_word(CMD_TX_SEND_PACKET | CMD_ERROR, ret);
  return;
}
