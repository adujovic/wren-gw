#include <string.h>
#include "lib.h"

#define UART_HIST_MAX 16
#define UART_HIST_LINELEN 32

/* The buffer for the history.  Fully static.  */
static char uart_history[UART_HIST_MAX][UART_HIST_LINELEN];
/* Number of used entries in the history.  Must be <= UART_HIST_MAX.  */
static unsigned uart_hist_len;
/* The next index to add a new entry.  */
static unsigned uart_hist_next;

void
uart_history_add (const char *line)
{
  unsigned len = strlen(line);
  char *dest = uart_history[uart_hist_next];

  if (len >= UART_HIST_LINELEN) {
    /* Truncate.  */
    len = UART_HIST_LINELEN - 1;
  }
  memcpy (dest, line, len);
  dest[len] = 0;

  uart_hist_next = (uart_hist_next + 1) % UART_HIST_MAX;
  if (uart_hist_len < UART_HIST_MAX)
    uart_hist_len++;
}

struct uart_rl_context {
  char *buf;
  unsigned maxlen;
  unsigned pos;
  unsigned hist_pos;
};

static void
uart_rl_backspace(struct uart_rl_context *ctxt)
{
  if (ctxt->pos == 0)
    uart_putc('\a');
  else
    {
      uart_puts("\b \b");
      ctxt->pos--;
    }
}

static void
uart_rl_clear(struct uart_rl_context *ctxt)
{
  for (; ctxt->pos > 0; ctxt->pos--)
    uart_puts("\b \b");
}

/* Internal function: replace current buffer with the new history position.  */
static void
uart_rl_hist_replace(struct uart_rl_context *ctxt)
{
    /* Select entry.  */
    unsigned hidx =
      (uart_hist_next - ctxt->hist_pos + UART_HIST_MAX) % UART_HIST_MAX;
    const char *h = uart_history[hidx];
    unsigned hlen = strlen(h);
    unsigned i;

    if (hlen > ctxt->maxlen)
      hlen = ctxt->maxlen;

    /* Clear current line.  */
    for (i = 0; i < ctxt->pos; i++)
      uart_putc('\b');

    /* Write line.  */
    for (i = 0; i < hlen; i++) {
      uart_putc(h[i]);
      ctxt->buf[i] = h[i];
    }

    /* Clear extra char.  */
    for (i = hlen; i < ctxt->pos; i++)
      uart_putc(' ');
    for (i = hlen; i < ctxt->pos; i++)
      uart_putc('\b');
    ctxt->pos = hlen;
}

static void
uart_rl_prev_hist(struct uart_rl_context *ctxt)
{
  if (ctxt->hist_pos >= uart_hist_len) {
    /* No more entries.  */
    uart_putc('\a');
    return;
  }

  ctxt->hist_pos++;

  uart_rl_hist_replace(ctxt);
}

static void
uart_rl_next_hist(struct uart_rl_context *ctxt)
{
  if (ctxt->hist_pos == 0) {
    /* No more entries.  */
    uart_putc('\a');
    return;
  }

  if (--ctxt->hist_pos == 0)
    uart_rl_clear(ctxt);
  else
    uart_rl_hist_replace(ctxt);
}

static void
uart_rl_add(struct uart_rl_context *ctxt, char c)
{
  if (c >= ' ' && ctxt->pos < ctxt->maxlen - 1)
    {
      ctxt->buf[ctxt->pos++] = c;
      uart_putc (c);
    }
  else
    uart_putc ('\a');
}

int
uart_readline(const char *prompt,
	      unsigned (*getchar)(void),
	      char *buf,
	      unsigned maxlen)
{
  struct uart_rl_context ctxt;

  ctxt.buf = buf;
  ctxt.maxlen = maxlen;
  ctxt.pos = 0;
  ctxt.hist_pos = 0;

  uart_puts (prompt);

  while (1)
    {
      unsigned c = getchar();

      switch(c)
        {
        case '\r':
          uart_putc ('\n');
          /* Remove trailing spaces.  */
          while (ctxt.pos > 0 && ctxt.buf[ctxt.pos - 1] == ' ')
            ctxt.pos--;
          /* Append a NULL.  */
          ctxt.buf[ctxt.pos] = 0;
          return ctxt.pos;

        case '\b':
        case 0x7f:
          /* Backspace or DEL.  */
          uart_rl_backspace(&ctxt);
          break;

        case 'U' - '@':
          /* C-u: clear the whole line.  */
          uart_rl_clear(&ctxt);
          break;

        case 'P' - '@':
          /* C-p: previous command.  */
          uart_rl_prev_hist(&ctxt);
          break;

        case 'N' - '@':
          /* C-n: next command.  */
          uart_rl_next_hist(&ctxt);
          break;

        case '\e':
          /* ESC.  Sequence expected.  */
          {
            unsigned c1 = uart_getc_next();

            if (c1 == '[') {
              unsigned c2 = uart_getc_next();

              if (c2 == 'A')
                uart_rl_prev_hist(&ctxt);
              else if (c2 == 'B')
                uart_rl_next_hist(&ctxt);
              else
                uart_putc('\a');
            }
            else
              uart_putc('\a');
          }
          break;

        default:
          uart_rl_add(&ctxt, c);
          break;
        }
    }
}
