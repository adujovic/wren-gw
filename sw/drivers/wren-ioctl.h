#ifndef __LINUX__WREN_H_
#define __LINUX__WREN_H_

/* To make easy to include in user program, avoid any specific linux
   includes (except linux/ioctl.h) or types. */

/* For _IO helpers */
#include <linux/ioctl.h>

#include "wren-mb-defs.h"

/* Maximum number of bytes (including the terminating NUL) for names */
#define WREN_NAME_MAXLEN 32

struct wren_wr_time {
	uint32_t tai_lo;
	uint32_t tai_hi;
	uint32_t ns;
};

/* For communication with the firmware, using the mailboxes */
struct wren_cmd_msg {
	uint32_t cmd;
	uint32_t len;
	uint32_t data[1024];
};

struct wren_ioctl_get_source {
	/* Input: source index */
	uint32_t source_idx;
	/* Output: protocol */
	struct wren_protocol proto;
};

struct wren_ioctl_config {
	struct wren_mb_cond cond;
	struct wren_mb_pulser_config config;
	char name[WREN_NAME_MAXLEN];
};

struct wren_ioctl_imm_config {
	struct wren_mb_pulser_config config;
	struct wren_ts ts;
	char name[WREN_NAME_MAXLEN];
};

struct wren_ioctl_get_config {
	uint32_t config_idx;
	struct wren_ioctl_config config;
};

struct wren_ioctl_mod_config {
	uint32_t config_idx;
	struct wren_mb_pulser_config config;
};

struct wren_ioctl_event_subscribe {
	uint32_t source_idx;
	uint32_t event_id;
	int32_t  offset_us;
};

#define WREN_CURRENT_VERSION 0x00000006

#define WREN_IOC_GET_VERSION _IOR('W', 0, uint32_t)
#define WREN_IOC_GET_LINK _IOR('W', 1, uint32_t)
#define WREN_IOC_GET_TIME _IOR('W', 2, struct wren_wr_time)
#define WREN_IOC_WAIT_TIME _IOR('W', 3, struct wren_wr_time)
#define WREN_IOC_MSG _IOR('W', 4, struct wren_cmd_msg)

#define WREN_IOC_RX_ADD_SOURCE _IOR('W', 5, struct wren_protocol)
#define WREN_IOC_RX_GET_SOURCE _IOR('W', 6, struct wren_ioctl_get_source)
#define WREN_IOC_RX_DEL_SOURCE _IOR('W', 7, uint32_t)

#define WREN_IOC_RX_ADD_CONFIG _IOR('W', 8, struct wren_ioctl_config)
#define WREN_IOC_RX_GET_CONFIG _IOR('W', 9, struct wren_ioctl_get_config)
#define WREN_IOC_RX_DEL_CONFIG _IOR('W', 10, uint32_t)
#define WREN_IOC_RX_MOD_CONFIG _IOR('W', 11, struct wren_ioctl_mod_config)
#define WREN_IOC_RX_IMM_CONFIG _IOR('W', 12, struct wren_ioctl_imm_config)
#define WREN_IOC_RX_SUBSCRIBE_CONFIG _IOR('W', 13, uint32_t)
#define WREN_IOC_RX_UNSUBSCRIBE_CONFIG _IOR('W', 14, uint32_t)
#define WREN_IOC_RX_SUBSCRIBE_EVENT _IOR('W', 15, struct wren_ioctl_event_subscribe)
#define WREN_IOC_RX_UNSUBSCRIBE_EVENT _IOR('W', 16, uint32_t)
#define WREN_IOC_RX_SUBSCRIBE_CONTEXT _IOR('W', 17, uint32_t)
#define WREN_IOC_RX_UNSUBSCRIBE_CONTEXT _IOR('W', 18, uint32_t)

#endif /* __LINUX__WREN_H_ */
