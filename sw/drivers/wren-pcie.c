// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/*
 * Inspired by SPEC driver by Federico Vaga <fvaga@cern.ch>
 * https://ohwr.org/project/spec
 *
 * Driver for WREN_PCIE (PXI express FMC Carrier) board.
 */

#include <linux/device.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/pci.h>
#include <linux/moduleparam.h>
#include <linux/version.h>
#include <linux/firmware.h>

#include "wren-core.h"
#include "pci_map.h"
#include "xpcie.h"

#define PCI_VENDOR_ID_CERN      (0x10DC)
#define PCI_DEVICE_ID_WREN      (0x0455)

#define WREN_PSPCIE_BAR	0
#define WREN_REGS_BAR	1
#define WREN_OCM_BAR	2
#define WREN_DDR_BAR	3

#define NBR_IRQS 4

#if 0
/**
 * It initializes the debugfs interface
 * @wren_dev: WREN_PCIE device instance
 *
 * Return: 0 on success, otherwise a negative error number
 */
static int wren_pcie_dbg_init(struct wren_pcie_dev *wren_dev)
{
	struct device *dev = &wren_dev->pdev->dev;

	wren_dev->dbg_dir = debugfs_create_dir(dev_name(dev), NULL);
	if (IS_ERR_OR_NULL(wren_dev->dbg_dir)) {
		dev_err(dev, "Cannot create debugfs directory (%ld)\n",
			PTR_ERR(wren_dev->dbg_dir));
		return PTR_ERR(wren_dev->dbg_dir);
	}

	return 0;
}

/**
 * It removes the debugfs interface
 * @spec: SPEC device instance
 */
static void wren_pcie_dbg_exit(struct wren_pcie_dev *wren_dev)
{
	debugfs_remove_recursive(wren_dev->dbg_dir);
}

#endif

static int wren_pcie_set_dma_mask(struct pci_dev *pdev)
{
	int err;

	err = dma_set_mask_and_coherent(&pdev->dev, DMA_BIT_MASK(64));
	if (err) {
		dev_info(&pdev->dev, "Cannot set 64 bit DMA mask\n");
		err = dma_set_mask_and_coherent(&pdev->dev, DMA_BIT_MASK(32));
		if (err) {
			dev_err(&pdev->dev, "DMA mask set error\n");
			return err;
		}
	}

	return 0;
}

static irqreturn_t wren_pcie_irq_handler(int irq, void *arg)
{
	struct wren_dev *wren = (struct wren_dev *)arg;
	struct pcie_dma __iomem *psdma = wren->psdma;
	uint32_t status;
	
	status = ioread32(&psdma->pcie_interrupt_status);

	dev_notice(wren->dev_ctl, "irq: status %08x\n", status);

	if (status & XPCIE_SOFTWARE_INT) {
		iowrite32(XPCIE_SOFTWARE_INT, &psdma->pcie_interrupt_status);
		return wren_irq_handler(wren);
	}

	return IRQ_HANDLED;
}

static int wren_pcie_probe(struct pci_dev *pdev,
			   const struct pci_device_id *id)
{
	struct wren_dev *wren_dev;
	struct pci_map *regs;
	int irq;
	int err = 0;

	wren_dev = vzalloc(sizeof(*wren_dev));
	if (!wren_dev)
		return -ENOMEM;

	pci_set_drvdata(pdev, wren_dev);
	wren_dev->pdev = pdev;
	wren_dev->owner = THIS_MODULE;

	/* Enable pci device (managed version). */
	err = pcim_enable_device(pdev);
	if (err) {
		dev_err(&pdev->dev, "Failed to enable PCI device (%d)\n",
			err);
		goto err_enable;
	}

	if (pci_request_regions(pdev, "wren-pcie"))
		goto err_enable;

	wren_dev->psdma = pci_ioremap_bar(pdev, 0);
	wren_dev->regs = pci_ioremap_bar(pdev, 1);
	if (wren_dev->psdma == NULL
	    || wren_dev->regs == NULL)
		goto err_remap;

	regs = (struct pci_map *)wren_dev->regs;
	wren_dev->hw_ident = ioread32(&regs->host.ident);
	wren_dev->hw_ver = ioread32(&regs->host.version);
	if (wren_dev->hw_ver == 0xffffffff) {
		dev_err(&pdev->dev, "invalid hw version (%08x)\n",
			wren_dev->hw_ver);
		goto err_dma_mask;
	}
	dev_notice(&pdev->dev, "wren hw version: %08x\n", wren_dev->hw_ver);

	err = wren_pcie_set_dma_mask(pdev);
	if (err)
		goto err_dma_mask;

	pci_set_master(pdev);

	/* Allocate interrupts */
	wren_dev->irq_count = pci_alloc_irq_vectors
		(pdev, NBR_IRQS, NBR_IRQS, PCI_IRQ_MSI); // | PCI_IRQ_MSIX);

	if (wren_dev->irq_count < 0) {
		dev_err(&pdev->dev, "cannot alloc irqs (err=%d)\n",
			wren_dev->irq_count);
		goto err_alloc_irq;
	}

	if (wren_dev->irq_count != NBR_IRQS) {
		dev_err(&pdev->dev, "can only alloc %u irqs\n",
			wren_dev->irq_count);
		goto err_alloc_irq2;
	}

#if 0
	/* enable MSI */
	err = pci_enable_msi(pdev);
	if (err) {
		dev_err(&pdev->dev, "Couldn't enable MSI mode: %d\n", err);
		goto err_msi;
	}
#endif

#if 0
	err = wren_pcie_sysfs_create(pdev, wren_dev);
	if (err)
		goto err_sysfs;

	wren_pcie_dbg_init(wren_dev);

	/*
	 * First, initialize PS PCIe controller.
	 * Please, note that one of its responsibility, among others, is to
	 * allocate the proper number of PCI IRQs for the device
	 */
	err = wren_pcie_pcie_init(wren_dev);
	if (err) {
		dev_err(&pdev->dev,
			"wren_pcie pspcie initialization failed (%d)\n", err);
		goto err_pcie_init;
	}

	err = wren_pcie_ps_cmd_init(wren_dev);
	if (err) {
		dev_err(&pdev->dev,
			"wren_pcie cmd service initialization failed (%d)\n",
			err);
		goto err_cmd_init;
	}

	wren_dev->status = 0; /* reset bits */
	err = wren_pcie_pl_init(wren_dev);
	if (err) {
		dev_err(&pdev->dev,
			"wren_pcie PL initialization failed (%d)\n", err);
		goto err_pl_init;
	}
#endif

	if (wren_register (wren_dev, &pdev->dev) != 0)
		goto err_register;

	irq = pci_irq_vector(pdev, 0);
	err = request_threaded_irq(irq, wren_pcie_irq_handler,
				   wren_irq_thread, 0,
				   dev_name(wren_dev->dev_ctl), wren_dev);
	if (err) {
		dev_err(&pdev->dev, "Can't request IRQ %d (%d)\n",
			pdev->irq, err);
		goto err_irq;
	}


	dev_info(&pdev->dev, "wren%u pcie carrier driver probed.\n",
		 wren_dev->index);
	return 0;

err_irq:
	wren_unregister(wren_dev);
err_register:
#if 0
err_pl_init:
	wren_pcie_pl_exit(wren_dev); /* free alocated resources */
	wren_pcie_ps_cmd_exit(wren_dev);
err_cmd_init:
	wren_pcie_pcie_exit(wren_dev);
err_pcie_init:
	wren_pcie_dbg_exit(wren_dev);
	wren_pcie_sysfs_remove(pdev, wren_dev);
	mutex_unlock(&wren_dev->mtx);
err_sysfs:
	pci_disable_device(pdev);
#endif
err_alloc_irq2:
	pci_free_irq_vectors(pdev);
err_alloc_irq:
err_msi:
err_dma_mask:
err_remap:
	if (wren_dev->psdma)
		pci_iounmap(pdev, wren_dev->psdma);
	if (wren_dev->regs)
		pci_iounmap(pdev, wren_dev->regs);
err_enable:
	vfree(wren_dev);
	return err;
}

static void wren_pcie_remove(struct pci_dev *pdev)
{
	struct wren_dev *wren_dev = pci_get_drvdata(pdev);
	int irq;

	irq = pci_irq_vector(pdev, 0);
	free_irq(irq, wren_dev);

	wren_unregister(wren_dev);
#if 0
	wren_pcie_pl_exit(wren_dev);
	wren_pcie_ps_cmd_exit(wren_dev);
	wren_pcie_pcie_exit(wren_dev);
	wren_pcie_dbg_exit(wren_dev);
	wren_pcie_sysfs_remove(pdev, wren_dev);
#endif

	pci_disable_device(pdev);
	pci_iounmap(pdev, wren_dev->psdma);
	pci_iounmap(pdev, wren_dev->regs);

	vfree(wren_dev);
}

static const struct pci_device_id wren_pcie_tbl[] = {
  	{ PCI_DEVICE(PCI_VENDOR_ID_CERN, PCI_DEVICE_ID_WREN)},
	{ PCI_DEVICE(0x10ee, 0xd011) }, // FIXME: temporary ids
	{0,},
};


static struct pci_driver wren_pcie_driver = {
	.driver = {
		.owner = THIS_MODULE,
	},
	.name = "wren-pcie",
	.probe = wren_pcie_probe,
	.remove = wren_pcie_remove,
	.id_table = wren_pcie_tbl,
};

module_pci_driver(wren_pcie_driver);

MODULE_AUTHOR("Tristan Gingold <tristan.gingold@cern.ch>");
MODULE_LICENSE("GPL v2");
MODULE_VERSION(VERSION);
MODULE_DESCRIPTION("Driver for the PCIe WREN");
MODULE_DEVICE_TABLE(pci, wren_pcie_tbl);

MODULE_SOFTDEP("pre: wren-core");

