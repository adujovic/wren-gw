// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/*
 * Driver for WREN_PCIE (PXI express FMC Carrier) board.
 */

#include <linux/device.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/moduleparam.h>
#include <linux/version.h>
#include <linux/sched.h>
#include <linux/poll.h>

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 11, 0)
#include <linux/sched/signal.h>
#endif

#include "wren/wren-packet.h"
#include "wren-common-utils.h"
#include "wren-core.h"
#include "wren-ioctl.h"
#include "host_map.h"

#define WREN_MAX_DEVS 32

/* Cheat..  */
#include "wren-common-utils.c"

/* Major id for wren devices */
static dev_t wren_devt;

static struct class *wren_class;

static struct dentry *dfs_root;

static DEFINE_IDA(wren_map);

struct wren_compact_time {
	/* Number of cycles + low bits of second */
	uint32_t lo;
	/* High bits of second */
	uint32_t hi;
};

/* Use linux/timerqueue.h instead ? */
struct wren_timeout_entry {
	struct list_head entry;
	struct wren_compact_time ctime;
	struct task_struct *me;
};


static int wren_ctl_open(struct inode *inode, struct file *file)
{
	struct wren_dev *wren = container_of(inode->i_cdev,
					     struct wren_dev, cdev_ctl);
	file->private_data = wren;
	kobject_get(&wren->dev_ctl->kobj);
	return 0;
}

static int wren_ctl_release(struct inode *inode, struct file *file)
{
	struct wren_dev *wren = container_of(inode->i_cdev,
					     struct wren_dev, cdev_ctl);
	kobject_put(&wren->dev_ctl->kobj);
	return 0;
}

static int wren_usr_open(struct inode *inode, struct file *file)
{
	struct wren_dev *wren = container_of(inode->i_cdev,
					     struct wren_dev, cdev_usr);
	struct wren_client *client;
	unsigned idx;
	int res;
	unsigned i;

	write_lock(&wren->client_lock);
	idx = find_first_zero_bit(wren->client_used.b, WREN_MAX_CLIENTS);
	if (idx == WREN_MAX_CLIENTS) {
		res = -ENOSPC;
		goto err;
	}

	/* Alloc and init structure */
	client = kmalloc(sizeof(*client), GFP_KERNEL);
	if (client == NULL) {
		res = -ENOMEM;
		goto err;
	}
	client->wren = wren;
	client->idx = idx;
	atomic_set(&client->ring_head, 0);
	atomic_set(&client->ring_tail, 0);
	init_waitqueue_head(&client->wqh);

	for (i = 0; i < WREN_NBR_SOURCES; i++)
		atomic_set(&client->sources[i], 0);

	bitmap_zero(client->evsubs, WREN_NBR_EVENTID);

	wren->clients[idx] = client;

	set_bit(idx, wren->client_used.b);
	file->private_data = client;

	kobject_get(&wren->dev_usr->kobj);

	res = 0;
err:
	write_unlock(&wren->client_lock);
	return res;
}

static int wren_usr_release(struct inode *inode, struct file *file)
{
	struct wren_client *client = file->private_data;
	struct wren_dev *wren = client->wren;
	unsigned idx = client->idx;
	unsigned i;

	/* Clear bits: unsubscribe to all configs and all sources.
	   (clear_bit is atomic) */
	for (i = 0; i < WREN_NBR_ACTIONS; i++)
		clear_bit(idx, wren->client_configs[i].b);
	for (i = 0; i < WREN_NBR_SOURCES; i++)
		clear_bit(idx, wren->client_sources[i].b);

	/* Remove the client */
	write_lock(&wren->client_lock);
	clear_bit(idx, wren->client_used.b);
	wren->clients[idx] = NULL;
	write_unlock(&wren->client_lock);

	kfree(client);

	kobject_put(&wren->dev_usr->kobj);

	return 0;
}

static unsigned wren_get_buffer(struct wren_dev *wren)
{
	while (1) {
		unsigned head = atomic_read(&wren->buf_head);
		unsigned next;

		if (head == WREN_NO_BUFFER) {
			dev_err(wren->dev_ctl, "cannot alloc buffer");
			return WREN_NO_BUFFER;
		}
		next = atomic_read(&wren->bufs[head].len);
		if (atomic_cmpxchg(&wren->buf_head, head, next) == head) {
			atomic_set(&wren->bufs[next].refcnt, 1);
			return next;
		}
	}
}

static void wren_put_buffer(struct wren_dev *wren, unsigned idx)
{
	if (!atomic_dec_and_test(&wren->bufs[idx].refcnt))
		return;

	/* Put it on the free list */
	while (1) {
		unsigned head = atomic_read(&wren->buf_head);

		atomic_set(&wren->bufs[idx].len, head);
		if (atomic_cmpxchg(&wren->buf_head, head, idx) == head)
			return;
	}
}

static int wren_client_empty_msg(struct wren_client *client)
{
	unsigned tail = atomic_read(&client->ring_tail);
	unsigned head = atomic_read(&client->ring_head);

	if (head == tail)
		return 1;
	/* Check the message was filled */
	head &= WREN_MAX_CLIENT_MSG - 1;
	if (atomic_read(&client->ring[head].buf_cmd) == 0)
		return 1;
	return 0;
}

static ssize_t wren_usr_read(struct file *filp, char __user *usrbuf, size_t count, loff_t *f_pos)
{
	struct wren_client *client = filp->private_data;
	ssize_t err;
	unsigned head, next;
	unsigned buf_cmd;
	unsigned buf_idx;
	unsigned src_idx;
	uint32_t cmd;
	uint32_t ids;
	unsigned blen, hlen;
	struct wren_client_msg *msg;
	struct wren_buffer *buf;
	struct wren_ts ts;
	union wren_capsule_hdr_un hdr;

	if (!client->wren->wr_synced)
		return -EIO;

	/* Sanity check (must be aligned). */
	if (count < 4 || (count & 3) != 0 || ((uintptr_t)usrbuf & 3) != 0)
		return -EINVAL;

	spin_lock(&client->wqh.lock);

	/* Check the mailbox is empty. */
	if (filp->f_flags & O_NONBLOCK) {
		if (wren_client_empty_msg(client)) {
			err = -EWOULDBLOCK;
			goto out;
		}
	} else {
		/* TODO: exit on sync loss */
		err = wait_event_interruptible_locked
			(client->wqh, !wren_client_empty_msg(client));
		if (err < 0)
			goto out;
	}

	/* Read message from the queue */
	head = atomic_read(&client->ring_head);
	next = (head + 1) & (WREN_MAX_CLIENT_MSG + WREN_MAX_CLIENT_MSG - 1);
	head &= WREN_MAX_CLIENT_MSG - 1;
	msg = &client->ring[head];
	buf_cmd = atomic_read(&msg->buf_cmd);
	cmd = buf_cmd & 0xff;
	buf_idx = buf_cmd >> 8;

	if (buf_idx != WREN_NO_BUFFER) {
		buf = &client->wren->bufs[buf_idx];
		blen = atomic_read(&buf->len);
	}
	else {
		buf = NULL;
		blen = 0;
	}

	ts.sec = 0;
	ts.nsec = 0;
	ids = 0;
	src_idx = 0;

	switch (cmd) {
	case CMD_ASYNC_PULSE:
		hlen = 4; /* cmd + config_id + ts */
		ts = msg->ts;
		ids = (msg->config_id << 16) | msg->source_idx;
		break;
	case CMD_ASYNC_CONTEXT:
	case CMD_ASYNC_EVENT:
		hlen = 1; /* cmd */
		src_idx = msg->source_idx;
		break;
	default:
		hlen = 0;
		break;
	}

	/* Free slot in the message queue */
	smp_mb__before_atomic();
	atomic_set(&client->ring_head, next);

	spin_unlock(&client->wqh.lock);

	err = -EFAULT;

	/* First word: header. */
	hdr.hdr.typ = cmd;
	hdr.hdr.pad = src_idx;
	hdr.hdr.len = hlen + blen;

	if (put_user(hdr.u32, (u32 __user*)usrbuf))
		goto out1;
	usrbuf += sizeof(uint32_t);

	/* Arguments */
	switch (cmd) {
	case CMD_ASYNC_PULSE:
		if (count >= 8 && put_user(ts.sec, (u32 __user*)usrbuf))
			goto out1;
		if (count >= 12 && put_user(ts.nsec, (u32 __user*)(usrbuf + 4)))
			goto out1;
		if (count >= 16 && put_user(ids, (u32 __user*)(usrbuf + 8)))
			goto out1;
		usrbuf += 12;
		break;
	default:
		break;
	}

	if (count < hlen * 4) {
		/* Buffer was too short for header+args, stop now */
		err = count;
		goto out1;
	}

	/* Packet */
	if (count < (hlen + blen) * 4) {
		/* Truncate packet if user buffer is too small */
		blen = (count / 4) - hlen;
	}
	err = (blen + hlen) * 4;
	if (buf != NULL && copy_to_user(usrbuf, buf->data, blen * 4))
		err = -EFAULT;

out1:
	if (buf_idx != WREN_NO_BUFFER)
		wren_put_buffer(client->wren, buf_idx);
	return err;

out:
	spin_unlock(&client->wqh.lock);
	return err;
}

static unsigned wren_usr_poll(struct file *filp, poll_table *wait)
{
	struct wren_client *client = filp->private_data;

	poll_wait(filp, &client->wqh, wait);
	if (!client->wren->wr_synced)
		return POLLERR;
	if (!wren_client_empty_msg(client))
		return POLLIN | POLLRDNORM;
	return 0;
}

static int wren_b2h_busy(struct wren_dev *wren)
{
	struct host_map *regs = wren->regs;
	uint32_t csr;

	csr = ioread32 (&regs->mb_b2h_host.csr);
	return csr & MAILBOX_OUT_REGS_MAP_CSR_READY;
}

static int wren_async_empty(struct wren_dev *wren)
{
	struct host_map *regs = wren->regs;
	uint32_t b_off, h_off;

	b_off = ioread32 (&regs->mb_async_host.board_offset);
	h_off = ioread32 (&regs->mb_async_host.host_offset);
	return b_off == h_off;
}

static int wren_read_ctime(struct wren_dev *wren,
			   struct wren_compact_time *res)
{
	struct host_map *regs = wren->regs;

	if (!wren->wr_synced) {
		return -1;
	}

	res->lo = ioread32(&regs->intc.tm_compact);
	res->hi = wren->tai_hi[res->lo >> 31];
	return 0;
}

static int wren_read_time(struct wren_dev *wren,
			  struct wren_wr_time *res)
{
	struct wren_compact_time ctime;

	if (wren_read_ctime(wren, &ctime) < 0) {
		return -1;
	}

	/* Cycles is clk_ref, which is 62.5Mhz so 16ns */
	res->ns = (ctime.lo & 0x0fffffff) << 4;
	res->tai_lo = (ctime.hi << 4) | (ctime.lo >> 28);
	res->tai_hi = ctime.hi >> 28;
	return 0;
}

static ssize_t wren_ctl_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
	struct wren_dev *wren = filp->private_data;
	struct host_map *regs = wren->regs;
	uint32_t h_off, b_off;
	int32_t  len;
	unsigned i;
	unsigned nwords;
	int err;

	/* Sanity check. */
	if (count < 16 || (count & 3) != 0)
		return -EINVAL;

	spin_lock_irq(&wren->wqh_a.lock);

	/* Check the mailbox is empty. */
	if (filp->f_flags & O_NONBLOCK) {
		if (wren_async_empty(wren)) {
			err = -EWOULDBLOCK;
			goto out;
		}
	} else {
		err = wait_event_interruptible_locked_irq
			(wren->wqh_a, !wren_async_empty(wren));
		if (err < 0)
			goto out;
	}

	/* Read offsets */
	b_off = ioread32 (&regs->mb_async_host.board_offset);
	h_off = ioread32 (&regs->mb_async_host.host_offset);

	/* Compute length in words */
	len = b_off - h_off;
	if (len < 0)
		len += 2048;

	if (len * 4 < count)
		count = len * 4;

	/* Copy data (TODO: DMA ?) */
	nwords = count / 4;
	for (i = 0; i < nwords; i++) {
		uint32_t data;

		data = ioread32(&regs->mem_async_host[h_off].data);
		if (put_user(data, (u32 __user*)(buf + i * 4))) {
			err = -EFAULT;
			goto out;
		}
		h_off = (h_off + 1) & 2047;
	}

	/* Update host offet */
	iowrite32 (h_off, &regs->mb_async_host.host_offset);

	err = count;
out:
	spin_unlock_irq(&wren->wqh_a.lock);
	return err;
}

static int wren_mb_snd_wait(struct wren_dev *wren,
			    uint32_t cmd, uint32_t len)
{
	int err;
	struct host_map *regs = wren->regs;

	/* Write header */
	iowrite32 (cmd, &regs->mb_h2b_host.cmd);
	iowrite32 (len, &regs->mb_h2b_host.len);

		/* Mark ready (for the board). */
	iowrite32 (MAILBOX_OUT_REGS_MAP_CSR_READY, &regs->mb_h2b_host.csr);

	/* Wait for answer */
	err = wait_event_interruptible
		(wren->wqh_r, wren_b2h_busy(wren));
	if (err < 0) {
		wren->wqh_r_discard = 1;
		return err;
	}
	return 0;
}

/* Send and receive a message.  Return < 0 in case of kernel error,
   or the length of the response.
   TODO: in case of remote error, should return CMD_ERROR.
*/
static int wren_mb_msg(struct wren_dev *wren,
		       uint32_t cmd, void *data, uint32_t len,
		       void *rsp, uint32_t rsp_max)
{
	struct host_map *regs = wren->regs;
	unsigned i;
	int err;

	/* Get exclusive access to the messages */
	err = mutex_lock_interruptible(&wren->msg_mutex);
	if (err)
		return err;

	/* Copy data (TODO: DMA ?) */
	for (i = 0; i < len; i++) {
		iowrite32(((uint32_t *)data)[i], &regs->mem_h2b_host[i].data);
	}

	err = wren_mb_snd_wait(wren, cmd, len);
	if (err < 0)
		goto out;

	/* Read header */
	cmd = ioread32 (&regs->mb_b2h_host.cmd);
	len = ioread32 (&regs->mb_b2h_host.len);

	if (len > rsp_max)
		len = rsp_max;

	/* Copy data (TODO: DMA ?) */
	for (i = 0; i < len; i++) {
		((uint32_t *)rsp)[i] = ioread32(&regs->mem_b2h_host[i].data);
	}

	/* Mark as read */
	iowrite32 (MAILBOX_OUT_REGS_MAP_CSR_READY, &regs->mb_b2h_host.csr);

	if (cmd & CMD_ERROR)
		err = -1;
	else
		err = len;
out:
	mutex_unlock(&wren->msg_mutex);
	return err;
}

static int wren_ioctl_msg_usr(struct wren_dev *wren, struct wren_cmd_msg __user *msg)
{
	struct host_map *regs = wren->regs;
	uint32_t cmd, len, data;
	unsigned i;
	int err;

	/* Read header. */
	if (get_user(cmd, &msg->cmd) || get_user(len, &msg->len))
		return -EFAULT;

	if (len > ARRAY_SIZE(msg->data))
		return -EINVAL;

	/* Get exclusive access to the messages */
	err = mutex_lock_interruptible(&wren->msg_mutex);
	if (err)
		return err;

	/* Copy data (TODO: DMA ?) */
	for (i = 0; i < len; i++) {
		if (get_user(data, &msg->data[i])) {
			/* Failed */
			mutex_unlock(&wren->msg_mutex);
			return -EFAULT;
		}
		iowrite32(data, &regs->mem_h2b_host[i].data);
	}

	err = wren_mb_snd_wait(wren, cmd, len);
	if (err < 0)
		goto out;

	/* Read header */
	cmd = ioread32 (&regs->mb_b2h_host.cmd);
	len = ioread32 (&regs->mb_b2h_host.len);

	/* Write header. */
	if (put_user(cmd, &msg->cmd) || put_user(len, &msg->len)) {
		err = -EFAULT;
		goto out;
	}

	if (len > ARRAY_SIZE(msg->data))
		len = ARRAY_SIZE(msg->data);

	/* Copy data (TODO: DMA ?) */
	for (i = 0; i < len; i++) {
		data = ioread32(&regs->mem_b2h_host[i].data);
		if (put_user(data, &msg->data[i])) {
			err = -EFAULT;
			goto out;
		}
	}

	/* Mark as read */
	iowrite32 (MAILBOX_OUT_REGS_MAP_CSR_READY, &regs->mb_b2h_host.csr);

	err = 0;
out:
	mutex_unlock(&wren->msg_mutex);
	return err;
}

static int wren_ioctl_rx_add_source(struct wren_dev *wren, struct wren_protocol __user *uproto)
{
	struct wren_mb_rx_set_source cmd;
	int slot;
	int res;
	unsigned i;
	uint32_t rep;

	if (copy_from_user(&cmd.cfg.proto, uproto, sizeof(cmd.cfg.proto)))
		return -EFAULT;
	if (cmd.cfg.proto.proto == WREN_PROTO_NONE)
		return -EINVAL;

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	/* Find if the source already exist.  */
	slot = -1;
	for (i = 0; i < WREN_NBR_SOURCES; i++) {
		struct wren_protocol *src = &wren->sources[i].proto;
		if (src->proto != WREN_PROTO_NONE) {
			if (wren_is_same_proto(src, &cmd.cfg.proto)) {
				res = i;
				goto out;
			}
		} else if (slot < 0) {
			slot = i;
		}
	}

	if (slot < 0) {
		res = -ENOSPC;
		goto out;
	}

	/* Fill slot */
	memcpy(&wren->sources[slot].proto, &cmd.cfg.proto, sizeof(cmd.cfg.proto));
	wren->sources[slot].conds = WREN_NO_IDX;

	/* Write proto on board */
	cmd.idx = slot;
	cmd.cfg.dest = 0;
	cmd.cfg.subsample = 0;
	res = wren_mb_msg(wren, CMD_RX_SET_SOURCE,
			  &cmd, sizeof(cmd) / 4, &rep, sizeof(rep));
	if (res < 0) {
		wren->sources[slot].proto.proto = WREN_PROTO_NONE;
		goto out;
	}
	res = slot;
out:
	mutex_unlock(&wren->data_mutex);
	return res;
}

static int wren_ioctl_rx_get_source(struct wren_dev *wren, struct wren_ioctl_get_source __user *uarg)
{
	struct wren_ioctl_get_source arg;
	int res;

	if (copy_from_user(&arg.source_idx, &uarg->source_idx, sizeof(arg.source_idx)))
		return -EFAULT;
	if (arg.source_idx >= WREN_NBR_SOURCES)
		return -EINVAL;

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	if (copy_to_user(&uarg->proto, &wren->sources[arg.source_idx].proto, sizeof(arg.proto)))
		res = -EFAULT;
	mutex_unlock(&wren->data_mutex);

	return res;
}

static int wren_ioctl_rx_del_source(struct wren_dev *wren, uint32_t *uidx)
{
	struct wren_drv_source *src;
	uint32_t idx;
	struct wren_mb_rx_set_source cmd;
	unsigned i;
	int res;
	uint32_t rep;

	if (copy_from_user(&idx, uidx, sizeof(idx)))
		return -EFAULT;

	if (idx >= WREN_NBR_SOURCES)
		return -EINVAL;

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	/* Check the source is not used (no conds, no subscriptions) */
	src = &wren->sources[idx];
	res = -EBUSY;
	if (src->conds != WREN_NO_IDX)
		goto err;
	for (i = 0; i < WREN_MAX_CLIENTS; i++)
		if (atomic_read(&src->evsubs[i]) != 0)
			goto err;

	/* Remove the source on the board */
	memset(&cmd, 0, sizeof(cmd));
	cmd.idx = idx;
	cmd.cfg.dest = 0;
	cmd.cfg.subsample = 0;
	cmd.cfg.proto.proto = WREN_PROTO_NONE;
	res = wren_mb_msg(wren, CMD_RX_SET_SOURCE,
			  &cmd, sizeof(cmd) / 4, &rep, sizeof(rep));

err:
	mutex_unlock(&wren->data_mutex);
	return res;
}

static int wren_ioctl_rx_add_config(struct wren_dev *wren, struct wren_ioctl_config __user *ucfg)
{
	struct wren_ioctl_config cfg;
	int res;
	uint32_t src_idx;
	uint32_t cond_idx;
	uint32_t act_idx;
	struct wren_dev_action *act;
	struct wren_mb_rx_set_action cmd_act;

	if (copy_from_user(&cfg, ucfg, sizeof(cfg)))
		return -EFAULT;
	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	/* Check if the source exist.  */
	src_idx = cfg.cond.src_idx;
	if (src_idx >= WREN_NBR_SOURCES
	    || wren->sources[src_idx].proto.proto == WREN_PROTO_NONE) {
		res = -EINVAL;
		goto out;
	}

	/* Get an action slot */
	act_idx = wren->free_actions;
	if (act_idx == WREN_NO_IDX)
		return -ENOSPC;

	/* Search for existing condition */
	cond_idx = wren->sources[src_idx].conds;
	while (cond_idx != WREN_NO_IDX) {
		struct wren_dev_cond *dcond = &wren->conds[cond_idx];
		if (wren_is_same_cond (&dcond->cond, &cfg.cond))
			break;
		cond_idx = dcond->link;
	}

	/* Maybe create the condition */
	if (cond_idx == WREN_NO_IDX) {
		struct wren_mb_rx_set_cond cmd;
		struct wren_dev_cond *dcond;
		cond_idx = wren->free_conds;
		if (cond_idx == WREN_NO_IDX) {
			res = -ENOSPC;
			goto out;
		}

		/* Program cond on board */
		cmd.cond_idx = cond_idx;
		memcpy(&cmd.cond, &cfg.cond, sizeof(cfg.cond));
		res = wren_mb_msg(wren, CMD_RX_SET_COND, &cmd, sizeof(cmd)/4,
				  NULL, 0);
		if (res < 0) {
			res = -ECOMM;
			goto out;
		}

		dcond = &wren->conds[cond_idx];
		/* Remove from chain */
		wren->free_conds = dcond->link;
		/* Add to source chain */
		dcond->link = wren->sources[src_idx].conds;
		wren->sources[src_idx].conds = cond_idx;

		memcpy(&dcond->cond, &cfg.cond, sizeof(cfg.cond));
	}

	/* Programm on board */
	cmd_act.act_idx = act_idx;
	cmd_act.cond_idx = cond_idx;
	memcpy(&cmd_act.conf, &cfg.config, sizeof(cmd_act.conf));
	res = wren_mb_msg(wren, CMD_RX_SET_ACTION, &cmd_act, sizeof(cmd_act)/4,
			  NULL, 0);
	if (res < 0) {
		/* TODO: maybe free condition ? */
		res = -ECOMM;
		goto out;
	}

	/* Extract action slot */
	act = &wren->actions[act_idx];
	wren->free_actions = act->link;
	act->cond_idx = cond_idx;
	act->link = wren->conds[cond_idx].acts;
	wren->conds[cond_idx].acts = act_idx;

	/* Fill it */
	memcpy(&act->config, &cfg.config, sizeof(act->config));
	memcpy(act->name, cfg.name, sizeof(act->name));

	res = act_idx;
out:
	mutex_unlock(&wren->data_mutex);
	return res;
}

static int wren_ioctl_rx_imm_config(struct wren_dev *wren, struct wren_ioctl_imm_config __user *ucfg)
{
	struct wren_ioctl_imm_config cfg;
	int res;
	uint32_t act_idx;
	struct wren_dev_action *act;
	struct wren_mb_rx_imm_action cmd_act;
	uint32_t cmp_idx;
	struct wren_dev_comparator *cmp;

	if (copy_from_user(&cfg, ucfg, sizeof(cfg)))
		return -EFAULT;
	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	/* Get an action slot */
	act_idx = wren->free_actions;
	if (act_idx == WREN_NO_IDX)
		return -ENOSPC;

	/* Programm on board */
	cmd_act.act_idx = act_idx;
	memcpy(&cmd_act.cmp.conf, &cfg.config, sizeof(cmd_act.cmp.conf));
	cmd_act.cmp.sec = cfg.ts.sec;
	cmd_act.cmp.nsec = cfg.ts.nsec;
	res = wren_mb_msg(wren, CMD_RX_IMM_ACTION, &cmd_act, sizeof(cmd_act)/4,
			  &cmp_idx, 1);
	if (res < 0) {
		/* TODO: maybe free condition ? */
		res = -ECOMM;
		goto out;
	}

	/* Extract action slot */
	act = &wren->actions[act_idx];
	wren->free_actions = act->link;
	act->cond_idx = WREN_INV_IDX;
	act->link = WREN_INV_IDX;

	/* Fill it */
	memcpy(&act->config, &cfg.config, sizeof(act->config));
	memcpy(act->name, cfg.name, sizeof(act->name));

	/* Set action to the comparator */
	cmp = &wren->comp_buf[cmp_idx];
	cmp->buf = WREN_NO_BUFFER;
	cmp->config_id = act_idx;

	res = act_idx;
out:
	mutex_unlock(&wren->data_mutex);
	return res;
}

static int wren_ioctl_rx_get_config(struct wren_dev *wren, struct wren_ioctl_get_config __user *ucfg)
{
	struct wren_ioctl_get_config cfg;
	int res;
	uint32_t act_idx;
	struct wren_dev_action *act;

	if (copy_from_user(&act_idx, &ucfg->config_idx, sizeof(act_idx)))
		return -EFAULT;
	if (act_idx >= WREN_NBR_ACTIONS)
		return -EINVAL;

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	/* Check if the action exist.  */
	act = &wren->actions[act_idx];
	if (act->cond_idx == WREN_NO_IDX) {
		res = -ENOENT;
		goto err;
	}

	/* Copy */
	cfg.config_idx = act_idx;
	memcpy (cfg.config.name, act->name, sizeof(cfg.config.name));
	memcpy (&cfg.config.config, &act->config, sizeof(cfg.config.config));
	if (act->cond_idx == WREN_INV_IDX) {
		cfg.config.cond.src_idx = WREN_NO_IDX;
		/* FIXME: same as INVALID_ID... */
		cfg.config.cond.evt_id = WREN_NO_IDX;
	}
	else {
		memcpy (&cfg.config.cond, &wren->conds[act->cond_idx], sizeof(cfg.config.cond));
	}

	/* Done with the mutex */
	mutex_unlock(&wren->data_mutex);

	if (copy_to_user(ucfg, &cfg, sizeof(cfg)))
		return -EFAULT;
	return 0;

err:
	mutex_unlock(&wren->data_mutex);
	return res;
}

static int wren_ioctl_rx_del_config(struct wren_dev *wren, uint32_t __user *uidx)
{
	int res;
	uint32_t act_idx, cond_idx;
	struct wren_dev_action *act;
	uint32_t prev_idx;

	if (copy_from_user(&act_idx, uidx, sizeof(act_idx)))
		return -EFAULT;
	if (act_idx >= WREN_NBR_ACTIONS)
		return -EINVAL;

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	/* Check if the action exist.  */
	act = &wren->actions[act_idx];
	cond_idx = act->cond_idx;
	if (cond_idx == WREN_NO_IDX) {
		res = -ENOENT;
		goto err;
	}

	/* Check if the action is not subscribed */
	if (!bitmap_empty(wren->client_configs[act_idx].b, WREN_MAX_CLIENTS)) {
		res = -EBUSY;
		goto err;
	}

	/* Remove from board */
	res = wren_mb_msg(wren, CMD_RX_DEL_ACTION, &act_idx, 1, NULL, 0);
	if (res < 0) {
		/* TODO: maybe free condition ? */
		res = -ECOMM;
		goto err;
	}

	/* Remove from condition chain */
	if (act->link != WREN_INV_IDX) {
		prev_idx = wren->conds[cond_idx].acts;
		if (prev_idx == act_idx)
			wren->conds[cond_idx].acts = act->link;
		else {
			while (1) {
				struct wren_dev_action *prev;
				prev = &wren->actions[prev_idx];
				if (prev->link == act_idx) {
					prev->link = act->link;
					break;
				}
				prev_idx = prev->link;
			}
		}
	}

	/* Put to free chain */
	act->link = wren->free_actions;
	wren->free_actions = act_idx;

	/* And mark it as free */
	act->cond_idx = WREN_NO_IDX;

	/* TODO: free condition (if unused) */

	res = 0;
err:
	mutex_unlock(&wren->data_mutex);
	return res;
}

static int wren_ioctl_rx_mod_config(struct wren_dev *wren, struct wren_ioctl_mod_config __user *ucfg)
{
	struct wren_ioctl_mod_config cfg;
	int res;
	uint32_t act_idx;
	struct wren_dev_action *act;
	struct wren_mb_rx_mod_action cmd_act;

	if (copy_from_user(&cfg, ucfg, sizeof(cfg)))
		return -EFAULT;
	act_idx = cfg.config_idx;
	if (act_idx >= WREN_NBR_ACTIONS)
		return -EINVAL;

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	/* Check if the action exist.  */
	act = &wren->actions[act_idx];
	if (act->cond_idx == WREN_NO_IDX) {
		res = -ENOENT;
		goto err;
	}

	/* Cannot change pulser idx (so copy from existing action) */
	cfg.config.pulser_idx = act->config.pulser_idx;

	/* Programm on board */
	cmd_act.act_idx = act_idx;
	memcpy(&cmd_act.conf, &cfg.config, sizeof(cmd_act.conf));
	res = wren_mb_msg(wren, CMD_RX_MOD_ACTION, &cmd_act, sizeof(cmd_act)/4,
			  NULL, 0);
	if (res < 0) {
		/* TODO: what to do ?  */
		res = -ECOMM;
		goto err;
	}

	/* Copy */
	memcpy (&act->config, &cfg.config, sizeof(cfg.config));

	res = 0;

err:
	mutex_unlock(&wren->data_mutex);
	return res;
}

static int wren_ioctl_rx_subscribe_config(struct wren_client *client, uint32_t __user *uidx, int en)
{
	struct wren_dev *wren = client->wren;
	int res;
	uint32_t act_idx, cond_idx, src_idx;

	if (copy_from_user(&act_idx, uidx, sizeof(act_idx)))
		return -EFAULT;
	if (act_idx >= WREN_NBR_ACTIONS)
		return -EINVAL;

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	cond_idx = wren->actions[act_idx].cond_idx;
	if (cond_idx == WREN_NO_IDX) {
		res = -ENOENT;
		goto out;
	}

	src_idx = wren->conds[cond_idx].cond.src_idx;

	if (en) {
		set_bit(client->idx, wren->client_configs[act_idx].b);

		if (src_idx != WREN_INV_IDX
		    && atomic_inc_return(&client->sources[src_idx]) == 1)
			set_bit(client->idx, wren->client_sources[src_idx].b);
	} else {
		clear_bit(client->idx, wren->client_configs[act_idx].b);

		if (src_idx != WREN_INV_IDX
		    && atomic_dec_return(&client->sources[src_idx]) == 0)
			clear_bit(client->idx, wren->client_sources[src_idx].b);
	}

	/* TODO: if status has changed, need to inform the board */

	res = 0;
out:
	mutex_unlock(&wren->data_mutex);
	return res;
}

static int wren_ioctl_rx_subscribe_event(struct wren_client *client, struct wren_ioctl_event_subscribe __user *uesub)
{
	struct wren_dev *wren = client->wren;
	struct wren_ioctl_event_subscribe esub;
	int res;

	if (copy_from_user(&esub, uesub, sizeof(esub)))
		return -EFAULT;
	if (esub.event_id >= WREN_NBR_EVENTID
	    || esub.source_idx >= WREN_NBR_SOURCES)
		return -EINVAL;

	res = mutex_lock_interruptible(&wren->data_mutex);
	if (res)
		return res;

	if (test_and_set_bit(esub.event_id, client->evsubs)) {
		/* Already subscribed */
		res = -EBUSY;
		goto out;
	}

	if (atomic_inc_return(&wren->sources[esub.source_idx].evsubs[esub.event_id]) == 1) {
		/* Need to subscribe to the event */
		struct wren_mb_rx_subscribe cmd;
		uint32_t rep;
		cmd.src_idx = esub.source_idx;
		cmd.ev_id = esub.event_id;
		res = wren_mb_msg(wren, CMD_RX_SUBSCRIBE,
				  &cmd, sizeof(cmd) / 4, &rep, sizeof(rep));
		if (res < 0) {
			atomic_dec(&wren->sources[esub.source_idx].evsubs[esub.event_id]);
			goto out;
		}
	}

	res = esub.event_id;
out:
	mutex_unlock(&wren->data_mutex);
	return res;
}

static int wren_ioctl_rx_subscribe_context(struct wren_client *client, uint32_t __user *usrc, int en)
{
	struct wren_dev *wren = client->wren;
	uint32_t src_idx;

	if (copy_from_user(&src_idx, usrc, sizeof(src_idx)))
		return -EFAULT;
	if (src_idx >= WREN_NBR_SOURCES)
		return -EINVAL;
	
	if (en) {
		if (atomic_inc_return(&client->sources[src_idx]) == 1)
			set_bit(client->idx, wren->client_sources[src_idx].b);
	} else {
		if (atomic_dec_return(&client->sources[src_idx]) == 0)
			clear_bit(client->idx, wren->client_sources[src_idx].b);
	}

	/* TODO: add a call to the board */
	return 0;
}

/* Return true if L > R */
static int wren_ctime_after(const struct wren_compact_time *l,
			    const struct wren_compact_time *r)
{
	if (l->hi > r->hi)
		return 1;
	if (l->hi == r->hi && l->lo > r->lo)
		return 1;
	return 0;
}

static void to_compact_time(struct wren_compact_time *dst,
			    struct wren_wr_time *src)
{
	dst->lo = ((src->ns >> 4) & 0x0fffffff) | (src->tai_lo << 28);
	dst->hi = (src->tai_lo >> 4) | (src->tai_hi << 28);
}

static void timeout_add_entry(struct wren_dev *wren,
			      struct wren_timeout_entry *my_toe)
{
	struct list_head *prev;
	struct wren_timeout_entry *it;

	prev = &wren->to_list;
	list_for_each_entry(it, &wren->to_list, entry) {
		if (!wren_ctime_after(&my_toe->ctime, &it->ctime)) {
			__list_add(&my_toe->entry, prev, &it->entry);
			return;
		}
		prev = &it->entry;
	}
	list_add_tail(&my_toe->entry, &wren->to_list);
}

/* Must be called with to_lock hold; can modify now. */
static void timeout_run(struct wren_dev *wren,
			struct wren_compact_time *now)
{
	struct host_map *regs = wren->regs;

	while (1) {
		struct wren_timeout_entry *it;

		if (list_empty(&wren->to_list)) {
			/* Empty list, can mask timer interrupt */
			if (wren->imr & HOST_MAP_INTC_IMR_TIMER) {
				wren->imr &= ~HOST_MAP_INTC_IMR_TIMER;
				iowrite32(wren->imr, &regs->intc.imr);
			}
			return;
		}

		it = list_first_entry(&wren->to_list,
				      struct wren_timeout_entry, entry);
		dev_notice(wren->dev_ctl,
			   "run_timeout now=%x:%x, ctime=%x:%x\n",
			   now->hi, now->lo,
			   it->ctime.hi, it->ctime.lo);
		if (wren_ctime_after(&it->ctime, now)) {
			/* Reprogram the timer (FIXME: useless if no wakeup) */
			iowrite32(it->ctime.lo, &regs->intc.tm_timer);

			/* Check if time in the past */
			if (wren_read_ctime(wren, now) < 0) {
				/* Should not happen */
				return;
			}

			/* If not, it's ok */
			if (wren_ctime_after(&it->ctime, now))
				return;

			/* Otherwise, continue */
		}
		list_del(&it->entry);
		wake_up_process(it->me);
	}
}

static int wren_wait_time(struct wren_dev *wren, struct wren_compact_time *to)
{
	unsigned long flags;
	struct wren_timeout_entry my_entry;

	/* Init entry */
	my_entry.ctime = *to;
	INIT_LIST_HEAD(&my_entry.entry);
	my_entry.me = current;

	spin_lock_irqsave(&wren->to_lock, flags);
	timeout_add_entry(wren, &my_entry);

	if (list_first_entry(&wren->to_list,
			     struct wren_timeout_entry, entry) == &my_entry) {
		struct host_map *regs = wren->regs;
		struct wren_compact_time now;

		/* First in the list, reprogram the timeout.  */
		iowrite32(my_entry.ctime.lo, &regs->intc.tm_timer);
		if (!(wren->imr & HOST_MAP_INTC_IMR_TIMER)) {
			wren->imr |= HOST_MAP_INTC_IMR_TIMER;
			iowrite32(wren->imr, &regs->intc.imr);
		}

		/* Maybe the timer was programmed in the past... */
		if (wren_read_ctime(wren, &now) < 0) {
			/* TODO: in case of error (not sync) */
			timeout_run(wren, &now);

			spin_unlock_irqrestore(&wren->to_lock, flags);
			return -EIO;
		}

		/* In that case, remove from list and check the list */
		if (!wren_ctime_after(&my_entry.ctime, &now)) {
			list_del(&my_entry.entry);
			timeout_run(wren, &now);

			spin_unlock_irqrestore(&wren->to_lock, flags);
			return 0;
		}
	}

	if (signal_pending(current)) {
		list_del(&my_entry.entry);
		spin_unlock_irqrestore(&wren->to_lock, flags);
		return -ERESTARTSYS;
	}
	else {
		set_current_state(TASK_INTERRUPTIBLE);
		spin_unlock_irqrestore(&wren->to_lock, flags);
		schedule();
		if (signal_pending(current)) {
			/* Remove link if interrupted! */
			spin_lock_irqsave(&wren->to_lock, flags);
			list_del(&my_entry.entry);
			spin_unlock_irqrestore(&wren->to_lock, flags);

			return -ERESTARTSYS;
		}
		return 0;
	}
}

static int wren_ioctl_get_link(struct wren_dev *wren, u32 __user *uarg)
{
	struct host_map *regs = wren->regs;
	u32 val;

	val = (regs->wr_state & HOST_MAP_WR_STATE_LINK_UP) != 0;

	if (copy_to_user(uarg, &val, sizeof(val)))
		return -EFAULT;

	return 0;
}

static int wren_ioctl_get_time(struct wren_dev *wren, struct wren_wr_time __user *uarg)
{
	struct wren_wr_time res;

	memset(&res, 0, sizeof(res));
	if (wren_read_time(wren, &res) < 0)
		return -EIO;

	if (copy_to_user(uarg, &res, sizeof(res)))
		return -EFAULT;

	return 0;
}

static int wren_ioctl_wait_time(struct wren_dev *wren, struct wren_wr_time __user *uarg)
{
	struct wren_wr_time wr_to;
	struct wren_compact_time cto;

	if (!wren->wr_synced) {
		return -EIO;
	}

	if (copy_from_user(&wr_to, uarg, sizeof(wr_to)))
		return -EFAULT;

	/* Convert to compact time */
	to_compact_time(&cto, &wr_to);

	return wren_wait_time(wren, &cto);
}

static int wren_ioctl_version(struct wren_dev *wren, uint32_t __user *uver)
{
	u32 ver = WREN_CURRENT_VERSION;
	if (copy_to_user((u32 __user *)uver, &ver, sizeof(ver)))
		return -EFAULT;
	return 0;
}

static ssize_t wren_ctl_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	struct wren_dev *wren = filp->private_data;

	switch(cmd) {
	case WREN_IOC_GET_VERSION:
		return wren_ioctl_version(wren, (uint32_t __user*)arg);
	case WREN_IOC_GET_LINK:
		return wren_ioctl_get_link(wren, (u32 __user *)arg);
	case WREN_IOC_GET_TIME:
		return wren_ioctl_get_time(wren, (void __user*)arg);
	case WREN_IOC_WAIT_TIME:
		return wren_ioctl_wait_time(wren, (void __user*)arg);
	case WREN_IOC_MSG:
		return wren_ioctl_msg_usr(wren, (void __user *)arg);

	case WREN_IOC_RX_ADD_SOURCE:
		return wren_ioctl_rx_add_source(wren, (void __user*)arg);
	case WREN_IOC_RX_GET_SOURCE:
		return wren_ioctl_rx_get_source(wren, (void __user*)arg);
	case WREN_IOC_RX_DEL_SOURCE:
		return wren_ioctl_rx_del_source(wren, (void __user*)arg);

	case WREN_IOC_RX_ADD_CONFIG:
		return wren_ioctl_rx_add_config(wren, (void __user*)arg);
	case WREN_IOC_RX_GET_CONFIG:
		return wren_ioctl_rx_get_config(wren, (void __user*)arg);
	case WREN_IOC_RX_DEL_CONFIG:
		return wren_ioctl_rx_del_config(wren, (void __user*)arg);
	case WREN_IOC_RX_MOD_CONFIG:
		return wren_ioctl_rx_mod_config(wren, (void __user*)arg);
	case WREN_IOC_RX_IMM_CONFIG:
		return wren_ioctl_rx_imm_config(wren, (void __user*)arg);
	default:
		return -ENOTTY;
	}
}

static ssize_t wren_usr_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	struct wren_client *client = filp->private_data;

	switch(cmd) {
	case WREN_IOC_GET_VERSION:
		return wren_ioctl_version(client->wren, (uint32_t __user*)arg);
	case WREN_IOC_GET_LINK:
		return wren_ioctl_get_link(client->wren, (u32 __user *)arg);
	case WREN_IOC_GET_TIME:
		return wren_ioctl_get_time(client->wren, (void __user*)arg);

	case WREN_IOC_RX_ADD_SOURCE:
		return wren_ioctl_rx_add_source(client->wren, (void __user*)arg);
	case WREN_IOC_RX_GET_SOURCE:
		return wren_ioctl_rx_get_source(client->wren, (void __user*)arg);
	case WREN_IOC_RX_DEL_SOURCE:
		return wren_ioctl_rx_del_source(client->wren, (void __user*)arg);

	case WREN_IOC_RX_ADD_CONFIG:
		return wren_ioctl_rx_add_config(client->wren, (void __user*)arg);
	case WREN_IOC_RX_GET_CONFIG:
		return wren_ioctl_rx_get_config(client->wren, (void __user*)arg);
	case WREN_IOC_RX_DEL_CONFIG:
		return wren_ioctl_rx_del_config(client->wren, (void __user*)arg);
	case WREN_IOC_RX_MOD_CONFIG:
		return wren_ioctl_rx_mod_config(client->wren, (void __user*)arg);
	case WREN_IOC_RX_IMM_CONFIG:
		return wren_ioctl_rx_imm_config(client->wren, (void __user*)arg);

	case WREN_IOC_RX_SUBSCRIBE_CONFIG:
		return wren_ioctl_rx_subscribe_config(client, (void __user*)arg, 1);
	case WREN_IOC_RX_UNSUBSCRIBE_CONFIG:
		return wren_ioctl_rx_subscribe_config(client, (void __user*)arg, 0);
	case WREN_IOC_RX_SUBSCRIBE_EVENT:
		return wren_ioctl_rx_subscribe_event(client, (void __user*)arg);
	case WREN_IOC_RX_SUBSCRIBE_CONTEXT:
		return wren_ioctl_rx_subscribe_context(client, (void __user*)arg, 1);
	case WREN_IOC_RX_UNSUBSCRIBE_CONTEXT:
		return wren_ioctl_rx_subscribe_context(client, (void __user*)arg, 0);
	default:
		return -ENOTTY;
	}
}

/* Assuming WR is sync (and time valid), read time from the board.
   This function does *NOT* set the valid bit of RES. */
static void wren_read_time_from_tm(struct wren_dev *wren,
				   struct wren_wr_time *res)
{
	struct host_map *regs = wren->regs;

	/* Get a coherent set of values.
	   TODO: do it in HW */
	while (1) {
		res->tai_lo = ioread32(&regs->tm_tai_lo);
		res->tai_hi = ioread32(&regs->tm_tai_hi);
		res->ns = ioread32(&regs->tm_cycles) << 3;

		/* Ack a potentially pending interrupt (even if masked) so
		   that the pending interrupt doesn't corrupt tai_hi */
		iowrite32(HOST_MAP_INTC_ISR_CLOCK, &regs->intc.iack);

		if (res->tai_lo == ioread32(&regs->tm_tai_lo)
		    && res->tai_hi == ioread32(&regs->tm_tai_hi))
			break;
	}

	return;
}

static void wren_sync_time(struct wren_dev *wren)
{
	struct wren_wr_time res;
	uint32_t hi;
	unsigned msb;

	wren_read_time_from_tm(wren, &res);

	/* Save high */
	hi = (res.tai_lo >> 4) | (res.tai_hi) << 28;
	msb = (res.tai_lo >> 3) & 1;
	wren->tai_msb = msb;
	wren->tai_hi[msb] = hi;
	wren->tai_hi[msb ^ 1] = hi + (msb ? 1 : 0);
}

static void wren_init_hw(struct wren_dev *wren)
{
	struct host_map *regs = wren->regs;
	uint32_t b_off;
 	uint32_t wr_state;

	/* Clear all pending interrupts. */
	iowrite32(0xffffff, &regs->intc.iack);

	/* Default mask.  */
	wren->imr = HOST_MAP_INTC_IMR_MSG
		| HOST_MAP_INTC_IMR_ASYNC
		| HOST_MAP_INTC_IMR_WR_SYNC;

	/* If already synchronized, read time.  */
	wr_state = ioread32(&regs->wr_state);

	if (wr_state & HOST_MAP_WR_STATE_TIME_VALID) {
		/* Read time */
		wren->wr_synced = 1;
		wren_sync_time(wren);

		/* Allow clock interrupts */
		wren->imr |= HOST_MAP_INTC_IMR_CLOCK;
	}
	else
		wren->wr_synced = 0;

	/* Clear all pending interrupts and enable interrupts */
	iowrite32(0xffffff, &regs->intc.iack);
	iowrite32(wren->imr, &regs->intc.imr);

	/* Empty the async memory */
	b_off = ioread32 (&regs->mb_async_host.board_offset);
	iowrite32 (b_off, &regs->mb_async_host.host_offset);
}

static void wren_init_data(struct wren_dev *wren)
{
	unsigned i, j;

	mutex_init(&wren->msg_mutex);

	/* Clear all the sources.  */
	for (i = 0; i < WREN_NBR_SOURCES; i++) {
		wren->sources[i].proto.proto = WREN_PROTO_NONE;
		wren->sources[i].conds = WREN_NO_IDX;
		bitmap_zero(wren->client_sources[i].b, WREN_MAX_CLIENTS);

		wren->last_evt[i] = WREN_NO_BUFFER;
		for (j = 0; j < WREN_NBR_EVENTID; j++)
			atomic_set(&wren->sources[i].evsubs[j], 0);
	}

	/* All the conds are free. */
	for (i = 0; i < WREN_NBR_CONDS; i++) {
		wren->conds[i].cond.src_idx = WREN_NO_IDX;
		wren->conds[i].link = i + 1;
	}
	wren->conds[WREN_NBR_CONDS - 1].link = WREN_NO_IDX;
	wren->free_conds = 0;

	/* All the actions are free. */
	for (i = 0; i < WREN_NBR_ACTIONS; i++) {
		wren->actions[i].cond_idx = WREN_NO_IDX;
		wren->actions[i].link = i + 1;
		bitmap_zero(wren->client_configs[i].b, WREN_MAX_CLIENTS);
	}
	wren->actions[WREN_NBR_ACTIONS - 1].link = WREN_NO_IDX;
	wren->free_actions = 0;

	/* Clients */
	rwlock_init(&wren->client_lock);
	bitmap_zero(wren->client_used.b, WREN_MAX_CLIENTS);
	memset(wren->clients, 0, sizeof(wren->clients));

	/* Buffers */
	for (i = 0; i < WREN_MAX_BUFFERS - 1; i++)
		atomic_set(&wren->bufs[i].len, i + 1);
	atomic_set(&wren->bufs[i].len, WREN_NO_BUFFER);
	atomic_set(&wren->buf_head, 0);

	/* Comparators */
	for (i = 0; i < WREN_MAX_COMPARATORS; i++)
		wren->comp_buf[i].buf = WREN_NO_BUFFER;

}

static unsigned wren_irq_read_packet(struct wren_dev *wren, union wren_capsule_hdr_un hdr, unsigned h_off)
{
	struct host_map *regs = wren->regs;
#define MEM_ASYNC_WORDS (sizeof(regs->mem_async_host) / 4)
	struct wren_buffer *buf;
	unsigned i;
	unsigned buf_idx = wren_get_buffer(wren);
	if (buf_idx == WREN_NO_BUFFER)
		return WREN_NO_BUFFER;

	buf = &wren->bufs[buf_idx];
	buf->data[0] = hdr.u32;
	atomic_set(&buf->len, hdr.hdr.len);
	for (i = 1; i < hdr.hdr.len; i++) {
		h_off = (h_off + 1) & (MEM_ASYNC_WORDS - 1);
		buf->data[i] =
			ioread32(&regs->mem_async_host[h_off].data);
	}
	return buf_idx;
}

static struct wren_client_msg *wren_get_client_msg(struct wren_dev *wren, unsigned idx)
{
	struct wren_client *client = wren->clients[idx];

	while (1) {
		unsigned res = atomic_read(&client->ring_tail);
		unsigned head = atomic_read(&client->ring_head);
		unsigned next;

		if (res == (head ^ WREN_MAX_CLIENT_MSG))
			break;
		next = (res + 1) & (WREN_MAX_CLIENT_MSG + WREN_MAX_CLIENT_MSG - 1);
		/* It's a little bit over-engineered as we are currently the
		   only writer */
		if (atomic_cmpxchg(&client->ring_tail, res, next) == res)
			return &client->ring[res & (WREN_MAX_CLIENT_MSG - 1)];
	}
	dev_err(wren->dev_ctl, "message queue of client %u is full\n", idx);
	return NULL;
}

static void wren_irq_context(struct wren_dev *wren, unsigned buf_idx, unsigned src_idx)
{
	const unsigned long *bm = wren->client_sources[src_idx].b;
	unsigned n;

	read_lock(&wren->client_lock);

	/* Push to all clients who subscribed to the source */
	n = find_first_bit(bm, WREN_MAX_CLIENTS);
	while (n < WREN_MAX_CLIENTS) {
		struct wren_client_msg *msg = wren_get_client_msg(wren, n);

		if (msg != NULL) {
			atomic_inc(&wren->bufs[buf_idx].refcnt);
			atomic_set(&msg->buf_cmd,
				   CMD_ASYNC_CONTEXT | (buf_idx << 8));
			msg->source_idx = src_idx;
			wake_up(&wren->clients[n]->wqh);
		}

		n = find_next_bit(bm, WREN_MAX_CLIENTS, n + 1);
	}

	read_unlock(&wren->client_lock);

	/* When the buffer was allocated/filled, it's refcount was initialized
	   to 1.  So decrement the refcount as we don't own it anymore. */
	wren_put_buffer(wren, buf_idx);
}

static void wren_irq_event(struct wren_dev *wren, unsigned buf_idx, unsigned src_idx)
{
	struct wren_capsule_event_hdr hdr;
	struct wren_buffer *buf;
	unsigned evid;

	/* Update last_evt, so that the event packet will be used for all
	   the triggered configurations */
	unsigned last = wren->last_evt[src_idx];
	if (last != WREN_NO_BUFFER)
		wren_put_buffer(wren, last);
	wren->last_evt[src_idx] = buf_idx;

	dev_notice(wren->dev_ctl, "wren_irq_event: src=%u, last=%u, buf=%u\n",
		   src_idx, last, buf_idx);

	/* Subscribed event */
	buf = &wren->bufs[buf_idx];
	memcpy(&hdr, buf->data, sizeof (hdr));
	evid = hdr.ev_id;
	if (evid < WREN_NBR_EVENTID) {
		const unsigned long *bm = wren->client_used.b;
		unsigned n;

		read_lock(&wren->client_lock);

		/* Push to all clients who subscribed to the source */
		n = find_first_bit(bm, WREN_MAX_CLIENTS);
		while (n < WREN_MAX_CLIENTS) {
			struct wren_client *client = wren->clients[n];
			if (test_bit(evid, client->evsubs)) {
				struct wren_client_msg *msg = wren_get_client_msg(wren, n);
				if (msg != NULL) {
					atomic_inc(&wren->bufs[buf_idx].refcnt);
					atomic_set(&msg->buf_cmd,
						   CMD_ASYNC_EVENT | (buf_idx << 8));
					msg->source_idx = src_idx;
					wake_up(&wren->clients[n]->wqh);
				}
			}

			n = find_next_bit(bm, WREN_MAX_CLIENTS, n + 1);
		}

		read_unlock(&wren->client_lock);
	}
}

/* Called on a config message which means a configuration has been triggered
   by a received event (a comparator has been loaded) */
static void wren_irq_config(struct wren_dev *wren, unsigned src_idx, unsigned data)
{
	unsigned act_idx = data & 0xffff;
	unsigned cmp_idx = data >> 16;
	struct wren_dev_comparator *cmp = &wren->comp_buf[cmp_idx];
	unsigned last;

	if (cmp_idx >= WREN_MAX_COMPARATORS) {
		dev_err(wren->dev_ctl, "async config: invalid comparator %u\n",
			cmp_idx);
		return;
	}

	last = wren->last_evt[src_idx];
	if (last == WREN_NO_BUFFER) {
		dev_err(wren->dev_ctl, "got a config for source %u without an event", src_idx);
		return;
	}
	dev_notice(wren->dev_ctl, "wren_irq_config src=%u, cmp=%u, act=%u, last=%u\n", src_idx, cmp_idx, act_idx, last);

	if (cmp->buf != WREN_NO_BUFFER)
		wren_put_buffer(wren, cmp->buf);

	atomic_inc(&wren->bufs[last].refcnt);

	/* TODO: should we also save the comparator idx in the config ?
	   So that we can free the comparator if the config is removed */
	cmp->buf = last;
	cmp->config_id = act_idx;
	cmp->src_idx = src_idx;
}

/* Called by the irq thread when a message has been received for a generated
   pulse */
static void wren_irq_pulse(struct wren_dev *wren, unsigned cmp_idx, struct wren_ts *ts)
{
	struct wren_dev_comparator *cmp;
	unsigned config_id;
	unsigned buf_idx;
	unsigned src_idx;
	const unsigned long *bm;
	unsigned n;

	/* Sanity check */
	if (cmp_idx >= WREN_MAX_COMPARATORS) {
		dev_err(wren->dev_ctl, "async pulse: invalid comparator %u\n",
			cmp_idx);
		return;
	}

	cmp = &wren->comp_buf[cmp_idx];
	config_id = cmp->config_id;
	buf_idx = cmp->buf;
	src_idx = cmp->src_idx;

	dev_notice(wren->dev_ctl, "wren_irq_pulse cmp=%u, conf=%u, buf=%u\n",
		   cmp_idx, config_id, buf_idx);

	/* Remove the buffer from the comparator.  If set, the buffer will
	   be sent to the clients.  */
	cmp->buf = WREN_NO_BUFFER;

	/* Send a pulse message to all clients */

	read_lock(&wren->client_lock);

	bm = wren->client_configs[config_id].b;

	/* Push to all clients who subscribed to the config */
	n = find_first_bit(bm, WREN_MAX_CLIENTS);
	while (n < WREN_MAX_CLIENTS) {
		struct wren_client_msg *msg = wren_get_client_msg(wren, n);

		if (msg != NULL) {
			if (buf_idx != WREN_NO_BUFFER)
				atomic_inc(&wren->bufs[buf_idx].refcnt);
			atomic_set(&msg->buf_cmd,
				   CMD_ASYNC_PULSE | (buf_idx << 8));

			msg->config_id = config_id;
			msg->source_idx = src_idx;
			msg->ts = *ts;
			wake_up(&wren->clients[n]->wqh);
		}

		n = find_next_bit(bm, WREN_MAX_CLIENTS, n + 1);
	}

	read_unlock(&wren->client_lock);

	if (buf_idx != WREN_NO_BUFFER)
		wren_put_buffer(wren, buf_idx);
}

static unsigned wren_irq_packet(struct wren_dev *wren, unsigned h_off, unsigned len)
{
	union wren_capsule_hdr_un hdr;
	struct host_map *regs = wren->regs;

	/* Read header */
	hdr.u32 = ioread32(&regs->mem_async_host[h_off].data);

	if (hdr.hdr.len > len) {
		/* Comm error! */
		dev_err(wren->dev_ctl,
			"async bad len (pkt: %u, mem: %u)\n", hdr.hdr.len, len);

		/* Consume everything */
		return len;
	}

	switch (hdr.hdr.typ) {
	case CMD_ASYNC_CONTEXT: {
		unsigned buf_idx;
		if (hdr.hdr.len < sizeof(struct wren_capsule_ctxt_hdr) / 4) {
			dev_err(wren->dev_ctl, "async bad context len (%u)\n",
				hdr.hdr.len);
			break;
		}
		buf_idx = wren_irq_read_packet(wren, hdr, h_off);
		if (buf_idx == WREN_NO_BUFFER)
			break;
		wren_irq_context(wren, buf_idx, hdr.hdr.pad);
		break;
	}
	case CMD_ASYNC_EVENT: {
		unsigned buf_idx;
		if (hdr.hdr.len < sizeof(struct wren_capsule_event_hdr) / 4) {
			dev_err(wren->dev_ctl, "async bad event len (%u)\n",
				hdr.hdr.len);
			break;
		}
		buf_idx = wren_irq_read_packet(wren, hdr, h_off);
		if (buf_idx == WREN_NO_BUFFER)
			break;
		wren_irq_event(wren, buf_idx, hdr.hdr.pad);
		break;
	}
	case CMD_ASYNC_CONFIG: {
		/* A config has been applied */
		unsigned data;

		if (hdr.hdr.len != 2) {
			dev_err(wren->dev_ctl, "async bad config len (%u)\n",
				hdr.hdr.len);
			break;
		}
		h_off = (h_off + 1) & (MEM_ASYNC_WORDS - 1);
		data = ioread32(&regs->mem_async_host[h_off].data);

		wren_irq_config(wren, hdr.hdr.pad, data);
		break;
	}
	case CMD_ASYNC_PULSE: {
		/* A pulse was generated */
		unsigned comp_idx;
		struct wren_ts ts;

		if (hdr.hdr.len != 4) {
			dev_err(wren->dev_ctl, "async bad pulse len (%u)\n",
				hdr.hdr.len);
			break;
		}
		h_off = (h_off + 1) & (MEM_ASYNC_WORDS - 1);
		comp_idx = ioread32(&regs->mem_async_host[h_off].data);
		h_off = (h_off + 1) & (MEM_ASYNC_WORDS - 1);
		ts.sec = ioread32(&regs->mem_async_host[h_off].data);
		h_off = (h_off + 1) & (MEM_ASYNC_WORDS - 1);
		ts.nsec = ioread32(&regs->mem_async_host[h_off].data);

		wren_irq_pulse(wren, comp_idx, &ts);
		break;
	}
	default:
		dev_err(wren->dev_ctl,
			"async unknown typ: 0x%02x\n", hdr.hdr.typ);
		/* Not even sure the header length is correct, discard
		   everything */
		return len;
	}

	return hdr.hdr.len;
}

irqreturn_t wren_irq_thread(int irq, void *priv)
{
	struct wren_dev *wren = (struct wren_dev *)priv;
	struct host_map *regs = wren->regs;
	uint32_t h_off, b_off;
	int32_t  len;

	h_off = ioread32 (&regs->mb_async_host.host_offset);

	while (1) {
		/* Read offsets */
		b_off = ioread32 (&regs->mb_async_host.board_offset);
		if (b_off == h_off)
			break;

		/* Compute length in words */
		len = b_off - h_off;
		if (len < 0)
		  len += MEM_ASYNC_WORDS;

		do {
			unsigned l;

			l = wren_irq_packet(wren, h_off, len);

			h_off = (h_off + l) & (MEM_ASYNC_WORDS - 1);
			len -= l;
		} while (len > 0);
	}
	/* Update host offet */
	iowrite32 (h_off, &regs->mb_async_host.host_offset);
	return IRQ_HANDLED;
}
EXPORT_SYMBOL(wren_irq_thread);

irqreturn_t wren_irq_handler(struct wren_dev *wren)
{
	struct host_map *regs = wren->regs;
	irqreturn_t res = IRQ_HANDLED;

	while (1) {
		uint32_t isr = ioread32(&regs->intc.isr);

		if (isr == 0)
			break;

		dev_notice(wren->dev_ctl, "interrupt isr=0x%x\n", isr);

		/* Ack interrupts, before handling them, so that a new
		   interrupt won't be acknowledged */
		iowrite32(isr, &regs->intc.iack);

		if (isr & HOST_MAP_INTC_ISR_WR_SYNC) {
			uint32_t wr_state;

			wr_state = ioread32(&regs->wr_state);
			if (wr_state & HOST_MAP_WR_STATE_TIME_VALID) {
				dev_notice(wren->dev_ctl, "wr synced");
				wren->imr |= HOST_MAP_INTC_IMR_CLOCK;
				wren->wr_synced = 1;
				wren_sync_time(wren);
			}
			else {
				dev_warn(wren->dev_ctl, "wr un-synced");
				wren->imr &= ~HOST_MAP_INTC_IMR_CLOCK;
				wren->wr_synced = 0;
				/* FIXME: else flush timeout */
			}
			iowrite32(wren->imr, &regs->intc.imr);
		}

		if ((isr & HOST_MAP_INTC_ISR_CLOCK) && wren->wr_synced) {
			uint32_t tm_compact;
			unsigned msb;

			tm_compact = ioread32(&regs->intc.tm_compact);
			msb = tm_compact >> 31;
			if (msb == wren->tai_msb)
				dev_warn(wren->dev_ctl, "irq clock: bad msb");
			wren->tai_hi[msb ^ 1]++;
			wren->tai_msb = msb;
		}

		if (isr & HOST_MAP_INTC_ISR_TIMER) {
			struct wren_compact_time now;

			spin_lock(&wren->to_lock);
			if (wren_read_ctime(wren, &now) == 0)
				timeout_run(wren, &now);
			/* FIXME: else flush */
			spin_unlock(&wren->to_lock);
		}
		if (isr & HOST_MAP_INTC_ISR_MSG) {
			/* TODO: Atomic inc/dec of discard.  */
			if (wren->wqh_r_discard) {
				/* Simply discard the reply */
				iowrite32 (MAILBOX_OUT_REGS_MAP_CSR_READY,
					   &regs->mb_b2h_host.csr);
				wren->wqh_r_discard = 0;
			}
			else {
				/* Wake up the process waiting for the reply */
				wake_up(&wren->wqh_r);
			}
		}
		if (isr & HOST_MAP_INTC_ISR_ASYNC) {
			wake_up(&wren->wqh_a);
			res = IRQ_WAKE_THREAD;
		}
	}

	return res;
}
EXPORT_SYMBOL(wren_irq_handler);

/*
 * Char device stuff
 */

static const struct file_operations wren_ctl_fops = {
	.owner		= THIS_MODULE,
	.llseek		= no_llseek,
	.read           = wren_ctl_read,
	.open		= wren_ctl_open,
	.release	= wren_ctl_release,
	.unlocked_ioctl = wren_ctl_ioctl,
};

static const struct file_operations wren_usr_fops = {
	.owner		= THIS_MODULE,
	.llseek		= no_llseek,
	.read           = wren_usr_read,
	.open		= wren_usr_open,
	.poll           = wren_usr_poll,
	.release	= wren_usr_release,
	.unlocked_ioctl = wren_usr_ioctl,
};

static ssize_t version_show(struct device *dev,
			    struct device_attribute *attr,
			    char *buf)
{

	return sprintf(buf, "%u\n", WREN_CURRENT_VERSION);
}
DEVICE_ATTR(version, S_IRUGO, version_show, NULL);

static struct attribute *wren_attr_versions[] = {
	&dev_attr_version.attr,
	NULL,
};

static const struct attribute_group wren_attr_groups = {
	.attrs = wren_attr_versions,
};


void wren_unregister(struct wren_dev *wren)
{
	struct host_map *regs = wren->regs;

	/* Mask interrupts */
	iowrite32(0, &regs->intc.imr);

	/* Clear all pending interrupts. */
	iowrite32(0xffffff, &regs->intc.iack);

	debugfs_remove_recursive(wren->dfs_dir);

	/* Now we can release the ID for re-use */
	device_destroy(wren_class, wren->devid_ctl);
	device_destroy(wren_class, wren->devid_usr);

	cdev_del(&wren->cdev_ctl);
	cdev_del(&wren->cdev_usr);

	ida_simple_remove(&wren_map, wren->index);
}
EXPORT_SYMBOL(wren_unregister);

int wren_register(struct wren_dev *wren, struct device *parent)
{
	int err = 0;
	int index;
	int major = MAJOR(wren_devt);

	/* Allocate index (used as device minor) */
	index = ida_simple_get(&wren_map, 0, WREN_MAX_DEVS, GFP_KERNEL);
	if (index < 0) {
		err = index;
		goto no_slot;
	}

	wren->index = index;
	mutex_init(&wren->msg_mutex);
	init_waitqueue_head(&wren->wqh_r);
	wren->wqh_r_discard = 0;
	init_waitqueue_head(&wren->wqh_a);
	spin_lock_init(&wren->to_lock);
	INIT_LIST_HEAD(&wren->to_list);

	wren_init_data(wren);

	wren_init_hw(wren);

	/* Create character device for ctl. */
	wren->devid_ctl = MKDEV(major, 2 * index);
	cdev_init(&wren->cdev_ctl, &wren_ctl_fops);
	wren->cdev_ctl.owner = wren->owner;
	err = cdev_add(&wren->cdev_ctl, wren->devid_ctl, 1);
	if (err) {
		pr_err("wren: failed to add char device %d:%d\n",
		       MAJOR(wren->devid_ctl), MINOR(wren->devid_ctl));
		goto no_cdev_ctl;
	}

	/* Create a new device, register to sysfs */
	wren->dev_ctl = device_create(wren_class, parent, wren->devid_ctl,
				      wren, "wrenctl%u", wren->index);
	if (IS_ERR(wren->dev_ctl))
		goto no_device_ctl;

	dev_set_drvdata(wren->dev_ctl, wren);

	/* Create character device for usr. */
	wren->devid_usr = MKDEV(major, 2 * index + 1);
	cdev_init(&wren->cdev_usr, &wren_usr_fops);
	wren->cdev_usr.owner = wren->owner;
	err = cdev_add(&wren->cdev_usr, wren->devid_usr, 1);
	if (err) {
		pr_err("wren: failed to add char device %d:%d\n",
		       MAJOR(wren->devid_usr), MINOR(wren->devid_usr));
		goto no_cdev_usr;
	}

	/* Create a new device, register to sysfs */
	wren->dev_usr = device_create(wren_class, parent, wren->devid_usr,
				      wren, "wren%u", wren->index);
	if (IS_ERR(wren->dev_usr))
		goto no_device_usr;

	err = sysfs_create_group(&wren->dev_usr->kobj, &wren_attr_groups);
	if (err)
		goto no_sysfs;
	if (dfs_root) {
		wren->dfs_dir = debugfs_create_dir(dev_name(wren->dev_usr),
						   dfs_root);
		if (IS_ERR_OR_NULL(wren->dfs_dir)) {
			dev_err(wren->dev_ctl, "cannot create debugfs\n");
			wren->dfs_dir = NULL;
		}
	}


#if 0
	err = ptp_populate_sysfs(ptp);
	if (err)
		goto no_sysfs;
#endif

	return 0;

no_sysfs:
	sysfs_remove_group(&wren->dev_usr->kobj, &wren_attr_groups);
no_device_usr:
	cdev_del(&wren->cdev_usr);
no_cdev_usr:
	device_destroy(wren_class, wren->devid_ctl);
no_device_ctl:
	cdev_del(&wren->cdev_ctl);
no_cdev_ctl:
	ida_simple_remove(&wren_map, wren->index);
no_slot:
	return err;
}
EXPORT_SYMBOL(wren_register);

static int __init wren_module_init(void)
{
	int err;

	wren_class = class_create(THIS_MODULE, "wren");
	if (IS_ERR(wren_class)) {
		pr_err("wren: failed to allocate class\n");
		return PTR_ERR(wren_class);
	}

	err = alloc_chrdev_region(&wren_devt, 0, WREN_MAX_DEVS, "wren");
	if (err < 0) {
		pr_err("wren: failed to allocate char device region\n");
		goto no_region;
	}

	dfs_root = debugfs_create_dir("wren", NULL);
	if (IS_ERR_OR_NULL(dfs_root)) {
		pr_notice("wren: cannot create dfs\n");
		dfs_root = NULL;
	}

	pr_info("wren-core registered\n");
	return 0;

no_region:
	class_destroy(wren_class);
	return err;
}
module_init(wren_module_init);

static void __exit wren_module_exit(void)
{
	pr_info("destroying wren\n");
	debugfs_remove_recursive(dfs_root);
	unregister_chrdev_region(wren_devt, WREN_MAX_DEVS);
	class_destroy(wren_class);
	ida_destroy(&wren_map);
}
module_exit(wren_module_exit);

MODULE_AUTHOR("Tristan Gingold <tristan.gingold@cern.ch>");
MODULE_LICENSE("GPL v2");
MODULE_VERSION(VERSION);
MODULE_DESCRIPTION("Driver for WREN");
