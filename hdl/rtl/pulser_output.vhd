library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pulser_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity pulser_output is
  port (
    clk_500m_i : in std_logic;
    clk_125m_i : in std_logic;

    pad_o : out std_logic;

    out_i : in t_slv8
  );
end pulser_output;

architecture arch of pulser_output is
begin
  inst_OSERDESE3 : OSERDESE3
    generic map (
      DATA_WIDTH => 8,
      INIT => '0',
      IS_CLKDIV_INVERTED => '0',
      IS_CLK_INVERTED => '0',
      IS_RST_INVERTED => '0',
      SIM_DEVICE => "ULTRASCALE_PLUS"
      )
    port map (
      OQ => pad_o,
      T_OUT => open,
      CLK => clk_500m_i,
      CLKDIV => clk_125m_i,
      D => out_i,
      RST => '0',
      T => '0'
      );
end arch;
