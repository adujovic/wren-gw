--  Notes:
--  Biphase mark encoding
--  channel A is Bunch clock
--  channel B is data frames
--  Channel A is sent before channel B

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ttc_biphase is
  generic (
    g_chan_len : natural := 32
  );
  port (
    clk_i : in std_logic;
    rst_n_i : in std_logic;

    --  Output (DDR).  First (0) then (1)
    out_o : out std_logic_vector(1 downto 0);

    --  Data
    chan_a_i : in std_logic_vector(g_chan_len - 1 downto 0);
    chan_b_i : in std_logic_vector(g_chan_len - 1 downto 0);

    --  Immediate load of data
    load_imm_i : in std_logic;

    --  Data are ready and must be loaded when previous data have been sent. 
    rdy_i : in std_logic;
    --  Data have been loaded
    ack_o : out std_logic
  );
end ttc_biphase;

architecture arch of ttc_biphase is
  signal chan_a, chan_b : std_logic_vector(g_chan_len - 1 downto 0);
  signal prev, chan_sel : std_logic := '0';
  signal count : natural range 0 to g_chan_len;
begin
  process (clk_i)
    variable b : std_logic;
    variable sym : std_logic_vector(1 downto 0);
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        out_o <= "10";
        prev <= '0';
        count <= 0;
        ack_o <= '0';
        chan_sel <= '0';
        chan_a <= (others => '0');
        chan_b <= (others => '1');
      else
        --  channel a.
        if load_imm_i = '1' or (rdy_i = '1' and count = 0) then
          ack_o <= '1';
          if chan_sel = '0' then
            --  Can really be loaded immediately.
            b := chan_a_i (chan_a'left);
            chan_a <= chan_a_i (chan_a_i'left - 1 downto 0) & '0';
          else
            b := chan_b (chan_b'left);
            chan_a <= chan_a_i;
          end if;
          chan_b <= chan_b_i;
          count <= chan_b'length;
        else
          ack_o <= '0';
          if chan_sel = '0' then
            b := chan_a (chan_a'left);
            chan_a <= chan_a (chan_a'left - 1 downto 0) & '0';
          else
            b := chan_b (chan_b'left);
            chan_b <= chan_b (chan_b'left - 1 downto 0) & '1';
            if count /= 0 then
              count <= count - 1;
            end if;
          end if;
        end if;

        chan_sel <= not chan_sel;

        --  Encode the bit
        if b = '0' then
          sym := "11";
        else
          sym := "10";
        end if;

        --  Ensure phase change
        sym := sym xor (prev & prev);
        prev <= sym (0);

        --  Send.
        out_o <= sym;

      end if;
    end if;
  end process;
end arch;