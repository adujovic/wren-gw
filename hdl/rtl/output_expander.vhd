--------------------------------------------------------------------------------
-- CERN BE-CO-HT
--------------------------------------------------------------------------------
--
-- unit name:   output_expander
--
-- description: Mini-core for a chain of sn74lv595
--
--------------------------------------------------------------------------------
-- Copyright CERN 2024
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity output_expander is
  generic (
    --  Number of SN74LV595 in the chain 
    g_NBR_CHIPS : positive := 1;
    --  SCLK is clk_i / (2 * (g_CLKDIV + 1))
    g_CLKDIV    : natural := 1
  );
  port (
    clk_i : in std_logic;
    rst_n_i : in std_logic;

    --  Ports to sn74lv595
    exp_oe_n_o : out std_logic;
    exp_rclk_o : out std_logic;
    exp_sclk_o : out std_logic;

    --  Commands for the shift register.
    load_o : out std_logic;
    shift_o : out std_logic
  );
end;

architecture arch of output_expander is
  type t_state is (S_LOAD, S_LATCH, S_CLOCK, S_PULSE, S_PULSE_OE);
  signal state : t_state;
  signal clk_counter : unsigned(31 downto 0);
  signal clk_tick : std_logic;
  signal shift_counter : unsigned(31 downto 0);
begin
  process (clk_i)
  begin
    if rising_edge(clk_i) then
      clk_tick <= '0';
      if rst_n_i = '0' then
        clk_counter <= (others => '0');
      else
        if clk_counter = g_CLKDIV then
          clk_tick <= '1';
          clk_counter <= (others => '0');
        else
          clk_counter <= clk_counter + 1;
        end if;
      end if;
    end if;
  end process;

  process (clk_i)
  begin
    if rising_edge(clk_i) then
      load_o <= '0';
      shift_o <= '0';

      if rst_n_i = '0' then
        state <= S_LOAD;
        exp_oe_n_o <= '1';
        exp_sclk_o <= '0';
        exp_rclk_o <= '0';
      elsif clk_tick = '1' then
        case state is
          when S_LOAD =>
            load_o <= '1';
            shift_counter <= (others => '0');
            state <= S_LATCH;
          when S_LATCH =>
            exp_sclk_o <= '0';
            state <= S_CLOCK;
          when S_CLOCK =>
            exp_sclk_o <= '1';
            if shift_counter = 8 * g_NBR_CHIPS - 1 then
              state <= S_PULSE;
            else
              shift_counter <= shift_counter + 1;
              shift_o <= '1';
              state <= S_LATCH;
            end if;
          when S_PULSE =>
            exp_sclk_o <= '0';
            exp_rclk_o <= '1';
            state <= S_PULSE_OE;
          when S_PULSE_OE =>
            exp_oe_n_o <= '0';
            exp_rclk_o <= '0';
            state <= S_LOAD;
        end case;
      end if;
    end if;
  end process;

end arch;