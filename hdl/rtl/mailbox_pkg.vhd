library ieee;
use ieee.std_logic_1164.all;

package mailbox_pkg is
  --  mbw: mailbox writer
  --  mbr: mailbox reader
  --  The data are written by the writer and readen by the reader!

  --  Mailbox inputs
  type t_mailbox_in is record
    mbw_csr_ready   : std_logic;
    mbw_csr_wr      : std_logic;

    mbr_csr_ready   : std_logic;
    mbr_csr_wr      : std_logic;

    mbw_cmd         : std_logic_vector(31 downto 0);
    mbw_len         : std_logic_vector(31 downto 0);

    -- SRAM bus memo
    mbw_mem_addr    : std_logic_vector(11 downto 2);
    mbw_mem_data    : std_logic_vector(31 downto 0);
    mbw_mem_wr      : std_logic;

    mbr_mem_addr    : std_logic_vector(11 downto 2);
  end record;

  --  Mailbox outputs
  type t_mailbox_out is record
    mbr_csr_ready   : std_logic;
    mbw_csr_ready   : std_logic;

    mbr_cmd         : std_logic_vector(31 downto 0);
    mbr_len         : std_logic_vector(31 downto 0);

    mbr_mem_data    : std_logic_vector(31 downto 0);
  end record;
end mailbox_pkg;
