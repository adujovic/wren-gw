--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- White Rabbit Trigger Distribution
-- https://ohwr-gitlab.cern.ch/projects/wrtd
--------------------------------------------------------------------------------
--
-- unit name:   gmtwr_v0
--
-- description: Top entity for GMT over WR playground
--
--------------------------------------------------------------------------------
-- Copyright CERN 2014-2019
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.axi4_pkg.all;
use work.pulser_pkg.all;
use work.mailbox_pkg.all;
use work.host_map_pkg.all;
use work.wr_fabric_pkg.all;
use work.streamers_pkg.all;
use work.RFFrameTransceiver_pkg.all;
use work.wrcore_pkg.all;

entity wren_core is
  generic (
    g_map_version : std_logic_vector(31 downto 0) := x"3510_f003";
    g_host_ident  : std_logic_vector(31 downto 0) := x"57_52_45_4E"; --  WREN
    g_nbr_inputs : natural := 8;
    g_nbr_pulser_group : natural := 2;
    g_with_serdes : boolean
  );
  port (
    ---------------------------------------------------------------------------
    -- Clocks/resets
    ---------------------------------------------------------------------------
    clk_sys_62m5_i : in std_logic;
    clk_sys_125m_i : in std_logic;
    clk_sys_250m_i : in std_logic;
    clk_sys_500m_i : in std_logic;
    rst_sys_62m5_n_i : in std_logic;

    --  AXI4-Lite bus for board registers
    board_axi_o: out t_axi4_lite_slave_out_32;
    board_axi_i: in  t_axi4_lite_slave_in_32;

    --  WB-32 bus for host registers
    host_wb_o : out t_wishbone_slave_out;
    host_wb_i : in  t_wishbone_slave_in;

    --  Interrupt to host
    irq_o : out std_logic;

    --  Lemo
    pad_o : out std_logic_vector(31 downto 0);
    pad_oe_o : out std_logic_vector(31 downto 0);
    pad_term_o : out std_logic_vector(31 downto 0);
    pad_i : in std_logic_vector(7 downto 0);
  
    --  Timing interface from WR
    tm_link_up_i    : std_logic;
    tm_tai_i        : std_logic_vector(39 downto 0);
    tm_cycles_i     : std_logic_vector(27 downto 0);
    tm_time_valid_i : std_logic;
  
    --  pps for wr-nic (may not be used)
    pps_p_i     : in  std_logic;
    pps_valid_i : in std_logic;

    --  Master WB bus to wr-pc (from host)
    wrpc_master_o : out t_wishbone_master_out;
    wrpc_master_i : in  t_wishbone_master_in;
  
    --  WR fabric.
    eth_tx_o  : out t_wrf_source_out;
    eth_tx_i  : in  t_wrf_source_in;
    eth_rx_o  : out t_wrf_sink_out;
    eth_rx_i  : in  t_wrf_sink_in;

    --  For logging.  Use clk_sys_250m
    S_AXI_LOG_awid    : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_LOG_awaddr  : out STD_LOGIC_VECTOR ( 48 downto 0 );
    S_AXI_LOG_awlen   : out STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_LOG_awvalid : out STD_LOGIC;
    S_AXI_LOG_awready : in  STD_LOGIC;
    S_AXI_LOG_wdata   : out STD_LOGIC_VECTOR (127 downto 0 );
    S_AXI_LOG_wstrb   : out STD_LOGIC_VECTOR (15 downto 0 );
    S_AXI_LOG_wlast   : out STD_LOGIC;
    S_AXI_LOG_wvalid  : out STD_LOGIC;
    S_AXI_LOG_wready  : in  STD_LOGIC;
    S_AXI_LOG_bid     : in  STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_LOG_bresp   : in  STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_LOG_bvalid  : in  STD_LOGIC;
    S_AXI_LOG_bready  : out STD_LOGIC;

    ---------------------------------------------------------------------------
    -- LEDs
    ---------------------------------------------------------------------------
    led_o    : out std_logic_vector(5 downto 0);
    fp_led0_o : out std_logic;
    fp_led1_o : out std_logic;
    pp_led0_o : out std_logic;
    pp_led1_o : out std_logic;

    --  rf1    
    rf1_txp_o         : out std_logic;
    rf1_txn_o         : out std_logic;
    rf1_rxp_i         : in  std_logic;
    rf1_rxn_i         : in  std_logic;
    gth_clk_250m_i    : in  std_logic
  );
end entity wren_core;

architecture arch of wren_core is
  alias clk : std_logic is clk_sys_62m5_i;
  alias rst_n : std_logic is rst_sys_62m5_n_i;

  signal nic_master_out : t_wishbone_master_out;
  signal nic_master_in : t_wishbone_master_in;

  signal pulser_grp_out : t_wishbone_master_out_array(0 to 3);
  signal pulser_grp_in  : t_wishbone_master_in_array(0 to 3);

  signal outputs : t_slv8_array(g_nbr_pulser_group * 8 - 1 downto 0);
  signal inputs  : t_subpulse_array(7 downto 0);

  signal intc_out : t_intc_master_out;
  signal intc_in  : t_intc_master_in;

  signal ifr_msg, ifr_async, ifr_wr : std_logic;

  --  Mailboxes.  Direction is for the board.
  signal mb_h2b_in : t_mailbox_in;
  signal mb_h2b_out : t_mailbox_out;
  signal mb_b2h_in : t_mailbox_in;
  signal mb_b2h_out : t_mailbox_out;

  signal mb_async_board_offset, mb_async_host_offset : std_logic_vector(31 downto 0);
  signal mb_async_board_addr, mb_async_host_addr : std_logic_vector(12 downto 2);
  signal mb_async_board_data, mb_async_host_data : std_logic_vector(31 downto 0);
  signal mb_async_board_wr : std_logic;

  signal gth_rf1_cfg, gth_rf1_sta : std_logic_vector(31 downto 0);

  signal ftw_64 : std_logic_vector(63 downto 0);
  signal rf1_nco_out, rf1_nco_out2 : std_logic_vector(31 downto 0);
  signal rf1_load_en, rf1_load_wr, rf1_rst, rf1_rf_ftw_rst : std_logic;
  signal rf1_rf_ftw : std_logic_vector(47 downto 0);
  signal rf1_rf_rx_tai, rf1_rf_rx_cyc : std_logic_vector(31 downto 0);
  signal rf1_rf_rx_sta : std_logic;

  attribute mark_debug : string;
  attribute mark_debug of rf1_nco_out : signal is "true";

  signal rf1_streamer_master_out : t_wishbone_master_out;
  signal rf1_streamer_master_in : t_wishbone_master_in;

  signal nic_rx_out : t_wrf_sink_out;
  signal nic_rx_in  : t_wrf_sink_in;
  signal nic_fifo_full : std_logic;
  signal nic_fifo_max : std_logic_vector(15 downto 0);

  signal rf1_rx_out : t_wrf_sink_out;
  signal rf1_rx_in  : t_wrf_sink_in;
  signal rf1_fifo_full : std_logic;
  signal rf1_fifo_max : std_logic_vector(15 downto 0);
  signal rf1_h1 : std_logic_vector(31 downto 0);

  signal sync_62m5 : std_logic;
  signal tm_tai, tm_load_tai : std_logic_vector(31 downto 0);
  signal tm_cycles : std_logic_vector(25 downto 0);
  signal tm_load_steps : std_logic_vector(25 downto 5);
  signal tm_sync, pre_pps : std_logic;
  signal tm_us : std_logic_vector(19 downto 0);

  signal inputs_en : std_logic_vector(31 downto 0);
  signal inputs_rise_sr_in, inputs_rise_sr_out : std_logic_vector(31 downto 0);
  signal inputs_rise_sr_wr : std_logic;
  signal inputs_fall_sr : std_logic_vector(31 downto 0);
  signal inputs_ts_addr : std_logic_vector(8 downto 2);
  signal inputs_ts_data : std_logic_vector(31 downto 0);

  signal clocks : t_subpulse_array(7 downto 0);
  signal clock_1mhz, pulse_1mhz : t_subpulse;

  signal inp_log_chain : t_pulser_log_array(7 downto 0);
  signal inp_log_shift : std_logic;

  signal log_load : std_logic;
  signal log_shift_grp : std_logic_vector (g_nbr_pulser_group - 1 + 1 downto 0);
  signal log_value_grp : t_pulser_log_array (g_nbr_pulser_group - 1 + 1 downto 0);

  --attribute mark_debug of log_load : signal is "true";
  --attribute mark_debug of log_shift_grp0 : signal is "true";
  --attribute mark_debug of log_value_grp0 : signal is "true";

  signal pulsers_mask, pulsers_raw_125m, pulsers_raw, pulsers_int : std_logic_vector(31 downto 0);

  signal logs_addr_adr : std_logic_vector(7 downto 2);
  signal logs_addr_dat : std_logic_vector(31 downto 0);
  signal logs_addr_rd, logs_addr_wr, logs_addr_rack : std_logic;

  signal leds_colors_adr : std_logic_vector(7 downto 2);
  signal leds_colors_dato, leds_colors_dati : std_logic_vector(31 downto 0);
  signal leds_colors_rd, leds_colors_wr : std_logic;
  signal leds_colors_rack, leds_colors_wack : std_logic;
  signal leds_force : std_logic_vector(31 downto 0);
begin
  b_clocks: block
  begin
    inst_clockgen: entity work.timegen
      port map (
        clk_62m5_i => clk_sys_62m5_i,
        clk_125m_i => clk_sys_125m_i,
        rst_n_i => rst_sys_62m5_n_i,
        tm_tai_i => tm_tai_i,
        tm_cycles_i => tm_cycles_i (25 downto 0),
        tm_valid_i => tm_time_valid_i,
        sync_62m5_o => sync_62m5,
        pre_pps_o => pre_pps,
        tm_tai_o => tm_tai,
        tm_cycles_o => tm_cycles,
        tm_sync_o => tm_sync,
        tm_load_tai_o => tm_load_tai,
        tm_load_steps_o => tm_load_steps
        );

    inst_1hz: entity work.freqgen
      generic map (
        g_period => 1_000_000_000
        )
      port map (
        clk_125m_i => clk_sys_125m_i,
        pps_i => pre_pps,
        clk_o => clocks(0),
        pulse_o => open
        );

    inst_1khz: entity work.freqgen
      generic map (
        g_period => 1_000_000
        )
      port map (
        clk_125m_i => clk_sys_125m_i,
        pps_i => pre_pps,
        clk_o => clocks(1),
        pulse_o => open
        );

    inst_1mhz: entity work.freqgen
      generic map (
        g_period => 1_000
        )
      port map (
        clk_125m_i => clk_sys_125m_i,
        pps_i => pre_pps,
        clk_o => clock_1mhz,
        pulse_o => pulse_1mhz
        );

    inst_10mhz: entity work.freqgen
      generic map (
        g_period => 100
        )
      port map (
        clk_125m_i => clk_sys_125m_i,
        pps_i => pre_pps,
        clk_o => clocks(3),
        pulse_o => open
        );

    inst_40mhz: entity work.freqgen
      generic map (
        g_period => 25
        )
      port map (
        clk_125m_i => clk_sys_125m_i,
        pps_i => pre_pps,
        clk_o => clocks(4),
        pulse_o => open
        );

    process (clk_sys_125m_i)
      variable ns_cnt : natural range 0 to 125;
    begin
      if rising_edge(clk_sys_125m_i) then
        if pre_pps = '1' then
          tm_us <= (others => '0');
          ns_cnt := 0;
        elsif ns_cnt = 124 then
          tm_us <= std_logic_vector(unsigned(tm_us) + 1);
          ns_cnt := 0;
        else
          ns_cnt := ns_cnt + 1;
        end if;
      end if;
    end process;

    clocks (2) <= clock_1mhz;

    clocks (5) <= ('0', "000"); -- Frev1 ?
    clocks (6) <= ('0', "000"); -- Frev2 ?
    clocks (7) <= ('0', "000"); -- Fbunch1 ?
  end block;

  --  Inputs timestamping.
  b_inputs: block
    type t_ts_array is array(g_nbr_inputs - 1 downto 0) of std_logic_vector(31 downto 0);
    signal rise_sec, rise_ns, fall_sec, fall_ns : t_ts_array;
    signal prev_val: std_logic_vector(g_nbr_inputs - 1 downto 0);
  begin
    process (clk_sys_125m_i)
      variable idx : natural range 0 to 31;
    begin
      if rising_edge(clk_sys_125m_i) then
        if rst_sys_62m5_n_i = '0' then
          inputs_rise_sr_out <= (others => '0');
          inputs_fall_sr <= (others => '0');
        else
          for i in inputs'range loop
            if prev_val(i) = '0' and inputs (i).v = '1'
              and inputs_en (i) = '1' and inputs_rise_sr_out(i) = '0'
            then
              --  Save timestamp for rising edge.
              rise_sec (i) <= tm_tai;
              rise_ns (i) (2 downto 0) <= inputs (i).dly;
              rise_ns (i) (3) <= sync_62m5;
              rise_ns (i) (29 downto 4) <= tm_cycles;
              rise_ns (i) (31 downto 30) <= "00";
              inputs_rise_sr_out (i) <= '1';
            end if;
            if prev_val(i) = '1' and inputs (i).v = '0'
              and inputs_en (i) = '1' and inputs_fall_sr(i) = '0'
            then
              --  Save timestamp for falling edge.
              fall_sec (i) <= tm_tai;
              fall_ns (i) (2 downto 0) <= inputs (i).dly;
              fall_ns (i) (3) <= sync_62m5;
              fall_ns (i) (29 downto 4) <= tm_cycles;
              fall_ns (i) (31 downto 30) <= "00";
              inputs_fall_sr (i) <= '1';
            end if;

            prev_val (i) <= inputs (i).v;
          end loop;

          if inputs_rise_sr_wr = '1' then
            --  Clear status bits.
            inputs_rise_sr_out <= inputs_rise_sr_out and not inputs_rise_sr_in;
            inputs_fall_sr <= inputs_fall_sr and not inputs_rise_sr_in;
          end if;
        end if;

        idx := to_integer(unsigned(inputs_ts_addr(6 downto 4)));
        case inputs_ts_addr(3 downto 2) is
          when "00" =>
            inputs_ts_data <= rise_sec(idx);
          when "01" =>
            inputs_ts_data <= rise_ns(idx);
          when "10" =>
            inputs_ts_data <= fall_sec(idx);
          when "11" =>
            inputs_ts_data <= fall_ns(idx);
          when others =>
            inputs_ts_data <= (others => 'X');
        end case;
      end if;
    end process;
  end block;

  inst_host_map: entity work.host_map
    port map (
      rst_n_i => rst_sys_62m5_n_i,
      clk_i => clk_sys_62m5_i,
      wb_i => host_wb_i,
      wb_o => host_wb_o,
      ident_i => g_host_ident,
      version_i => g_map_version,
      wr_state_link_up_i => tm_link_up_i,
      wr_state_time_valid_i => tm_time_valid_i,
      tm_tai_lo_i => tm_tai_i (31 downto 0),
      tm_tai_hi_i (7 downto 0) => tm_tai_i(39 downto 32),
      tm_tai_hi_i (31 downto 8) => (others => '0'),
      tm_cycles_i (27 downto 0) => tm_cycles_i,
      tm_cycles_i (31 downto 28) => (others => '0'),
      intc_i => intc_in,
      intc_o => intc_out,

      mb_b2h_host_csr_ready_i => mb_b2h_out.mbr_csr_ready,
      mb_b2h_host_csr_ready_o => mb_b2h_in.mbr_csr_ready,
      mb_b2h_host_csr_wr_o => mb_b2h_in.mbr_csr_wr,
      mb_b2h_host_cmd_i => mb_b2h_out.mbr_cmd,
      mb_b2h_host_len_i => mb_b2h_out.mbr_len,

      mb_h2b_host_csr_ready_i => mb_h2b_out.mbw_csr_ready,
      mb_h2b_host_csr_ready_o => mb_h2b_in.mbw_csr_ready,
      mb_h2b_host_csr_wr_o => mb_h2b_in.mbw_csr_wr,
      mb_h2b_host_cmd_o => mb_h2b_in.mbw_cmd,
      mb_h2b_host_len_o => mb_h2b_in.mbw_len,

      mb_async_host_board_offset_i => mb_async_board_offset,
      mb_async_host_host_offset_o => mb_async_host_offset,

      wrpc_i => wrpc_master_i,
      wrpc_o => wrpc_master_o,

      mem_b2h_host_addr_o => mb_b2h_in.mbr_mem_addr,
      mem_b2h_host_data_i => mb_b2h_out.mbr_mem_data,
      mem_h2b_host_addr_o => mb_h2b_in.mbw_mem_addr,
      mem_h2b_host_data_o => mb_h2b_in.mbw_mem_data,
      mem_h2b_host_wr_o => mb_h2b_in.mbw_mem_wr,
      mem_async_host_addr_o => mb_async_host_addr,
      mem_async_host_data_i => mb_async_host_data
    );
  
  inst_board_map: entity work.board_map
    port map (
      aclk => clk,
      areset_n => rst_n,
      awvalid => board_axi_i.awvalid,
      awready => board_axi_o.awready,
      awaddr => board_axi_i.awaddr(16 downto 2),
      awprot => "000",
      wvalid => board_axi_i.wvalid,
      wready => board_axi_o.wready,
      wdata => board_axi_i.wdata,
      wstrb => board_axi_i.wstrb,
      bvalid => board_axi_o.bvalid,
      bready => board_axi_i.bready,
      bresp => board_axi_o.bresp,
      arvalid => board_axi_i.arvalid,
      arready => board_axi_o.arready,
      araddr => board_axi_i.araddr(16 downto 2),
      arprot => "000",
      rvalid => board_axi_o.rvalid,
      rready => board_axi_i.rready,
      rdata => board_axi_o.rdata,
      rresp => board_axi_o.rresp,

      version_i => g_map_version,
      ident_i => g_host_ident,
      wr_state_link_up_i => tm_link_up_i,
      wr_state_time_valid_i => tm_time_valid_i,
      tm_tai_lo_i => tm_tai_i (31 downto 0),
      tm_tai_hi_i (7 downto 0) => tm_tai_i (39 downto 32),
      tm_tai_hi_i (31 downto 8) => (others => '0'),
      tm_cycles_i (27 downto 0) => tm_cycles_i,
      tm_cycles_i (31 downto 28) => (others => '0'),

      ifr_msg_o => ifr_msg,
      ifr_async_o => ifr_async,
      ifr_wr_o => ifr_wr,

      lemo_term_o => pad_term_o,
      lemo_oe_o => pad_oe_o,
      pulsers_raw_i => pulsers_raw,
      pulsers_int_i => pulsers_int,
      pulsers_mask_o => pulsers_mask,
      nic_fifo_max_count_i => nic_fifo_max,
      nic_fifo_full_i => nic_fifo_full,

      inputs_en_o => inputs_en,
      inputs_rise_sr_i => inputs_rise_sr_out,
      inputs_rise_sr_o => inputs_rise_sr_in,
      inputs_rise_sr_wr_o => inputs_rise_sr_wr,
      inputs_fall_sr_i => inputs_fall_sr,

      inputs_ts_addr_o => inputs_ts_addr,
      inputs_ts_data_i => inputs_ts_data,

      logs_addr_adr_o => logs_addr_adr,
      logs_addr_dato_i => logs_addr_dat,
      logs_addr_rack_i => logs_addr_rack,
      logs_addr_rd_o => logs_addr_rd,
      logs_addr_wr_o => logs_addr_wr,
      logs_addr_wack_i => logs_addr_wr,
      logs_addr_dati_o => open,

      leds_colors_adr_o => leds_colors_adr,
      leds_colors_dati_o => leds_colors_dati,
      leds_colors_dato_i => leds_colors_dato,
      leds_colors_wr_o => leds_colors_wr,
      leds_colors_rd_o => leds_colors_rd,
      leds_colors_rack_i => leds_colors_rack,
      leds_colors_wack_i => leds_colors_wack,
      leds_force_o => leds_force,

      rf1_gth_sta_i => gth_rf1_sta,
      rf1_gth_cfg_o => gth_rf1_cfg,
      rf1_ftw_hi_o => ftw_64(63 downto 32),
      rf1_ftw_lo_o => ftw_64(31 downto 0),
      rf1_ftw_load_wr_o => rf1_load_wr,
      rf1_ftw_load_en_o => rf1_load_en,
      rf1_ftw_load_reset_o => rf1_rst,
      rf1_ftw_load_rfframe_o => open,
      rf1_h1_o => rf1_h1,
      rf1_rf_ftw_hi_i => rf1_rf_ftw (47 downto 32),
      rf1_rf_ftw_rst_i => rf1_rf_ftw_rst,
      rf1_rf_ftw_lo_i => rf1_rf_ftw (31 downto 0),
      rf1_rf_rx_tai_i => rf1_rf_rx_tai,
      rf1_rf_rx_cyc_i => rf1_rf_rx_cyc,
      rf1_rf_rx_sta_valid_i => rf1_rf_rx_sta,
      rf1_streamer_fifo_max_count_i => rf1_fifo_max,
      rf1_streamer_fifo_full_i => rf1_fifo_full,
      rf1_streamer_i => rf1_streamer_master_in,
      rf1_streamer_o => rf1_streamer_master_out,

      pulser_group_0_el_i => pulser_grp_in(0),
      pulser_group_0_el_o => pulser_grp_out(0),

      pulser_group_1_el_i => pulser_grp_in(1),
      pulser_group_1_el_o => pulser_grp_out(1),

      pulser_group_2_el_i => pulser_grp_in(2),
      pulser_group_2_el_o => pulser_grp_out(2),

      pulser_group_3_el_i => pulser_grp_in(3),
      pulser_group_3_el_o => pulser_grp_out(3),

      mb_b2h_board_csr_ready_i => mb_b2h_out.mbw_csr_ready,
      mb_b2h_board_csr_ready_o => mb_b2h_in.mbw_csr_ready,
      mb_b2h_board_csr_wr_o => mb_b2h_in.mbw_csr_wr,
      mb_b2h_board_cmd_o => mb_b2h_in.mbw_cmd,
      mb_b2h_board_len_o => mb_b2h_in.mbw_len,

      mb_h2b_board_csr_ready_i => mb_h2b_out.mbr_csr_ready,
      mb_h2b_board_csr_ready_o => mb_h2b_in.mbr_csr_ready,
      mb_h2b_board_csr_wr_o => mb_h2b_in.mbr_csr_wr,
      mb_h2b_board_cmd_i => mb_h2b_out.mbr_cmd,
      mb_h2b_board_len_i => mb_h2b_out.mbr_len,

      mb_async_board_host_offset_i => mb_async_host_offset,
      mb_async_board_board_offset_o => mb_async_board_offset,

      mem_b2h_board_addr_o => mb_b2h_in.mbw_mem_addr,
      mem_b2h_board_data_o => mb_b2h_in.mbw_mem_data,
      mem_b2h_board_wr_o   => mb_b2h_in.mbw_mem_wr,

      mem_h2b_board_addr_o => mb_h2b_in.mbr_mem_addr,
      mem_h2b_board_data_i => mb_h2b_out.mbr_mem_data,

      mem_async_board_addr_o => mb_async_board_addr,
      mem_async_board_data_o => mb_async_board_data,
      mem_async_board_wr_o   => mb_async_board_wr,

      wrnic_i => nic_master_in,
      wrnic_o => nic_master_out
    );

  pulsers_int <= pulsers_raw and pulsers_mask;

  process (clk_sys_125m_i)
  begin
    if rising_edge(clk_sys_125m_i) then
      --  Synchronize for the input.
      if sync_62m5 = '1' then
        pulsers_raw <= pulsers_raw_125m;
      else
        pulsers_raw <= pulsers_raw or pulsers_raw_125m;
      end if;
    end if;
  end process;

  b_intc: block
    signal int_clock, int_timer, int_wr_sync, int_msg, int_async : std_logic;
    signal intm_clock, intm_timer, intm_wr_sync, intm_msg, intm_async : std_logic;
    signal compact_clk_msb, compact_clk_msb_d : std_logic;
    signal wr_sync, wr_sync_d : std_logic;
  begin
    intm_clock <= int_clock and intc_out.imr_clock;
    intm_timer <= int_timer and intc_out.imr_timer;
    intm_wr_sync <= int_wr_sync and intc_out.imr_wr_sync;
    intm_msg <= int_msg and intc_out.imr_msg;
    intm_async <= int_async and intc_out.imr_async;

    irq_o <= intm_clock or intm_timer or intm_wr_sync or intm_msg or intm_async;

    intc_in <= (tm_compact_cycles => tm_cycles_i,
                tm_compact_sec => tm_tai_i(3 downto 0),
                isr_raw_clock => int_clock,
                isr_clock => intm_clock,
                isr_raw_timer => int_timer,
                isr_timer => intm_timer,
                isr_raw_wr_sync => int_wr_sync,
                isr_wr_sync => intm_wr_sync,
                isr_raw_msg => int_msg,
                isr_msg => intm_msg,
                isr_raw_async => int_async,
                isr_async => intm_async);

    compact_clk_msb <= intc_in.tm_compact_sec(intc_in.tm_compact_sec'left);

    --  Clock interrupt
    process (clk)
    begin
      if rising_edge(clk) then
        if rst_n = '0' then
          int_clock <= '0';
          compact_clk_msb_d <= '0';
        else
          if compact_clk_msb /= compact_clk_msb_d then
            int_clock <= '1';
          elsif intc_out.iack_clock = '1' and intc_out.iack_wr = '1' then
            int_clock <= '0';
          end if;
          compact_clk_msb_d <= compact_clk_msb;
        end if;
      end if;
    end process;

    --  Timer
    process (clk)
    begin
      if rising_edge(clk) then
        if rst_n = '0' then
          int_timer <= '0';
        elsif intc_out.tm_timer_cycles = intc_in.tm_compact_cycles
          and intc_out.tm_timer_sec = intc_in.tm_compact_sec
        then
          int_timer <= '1';
        elsif intc_out.iack_timer = '1' and intc_out.iack_wr = '1' then
          int_timer <= '0';
        end if;
      end if;
    end process;

    --  WR sync
    wr_sync <= tm_link_up_i and tm_time_valid_i;
    process (clk)
    begin
      if rising_edge(clk) then
        if rst_n = '0' then
          wr_sync_d <= '0';
          int_wr_sync <= '0';
        else
          if wr_sync /= wr_sync_d then
            int_wr_sync <= '1';
          elsif intc_out.iack_wr_sync = '1' and intc_out.iack_wr = '1' then
            int_wr_sync <= '0';
          end if;
          wr_sync_d <= wr_sync;
        end if;
      end if;
    end process;

    --  Software interrupt for msg
    process (clk)
    begin
      if rising_edge(clk) then
        if rst_n = '0' then
          int_msg <= '0';
        else
          if ifr_msg = '1' and ifr_wr = '1' then
            int_msg <= '1';
          elsif intc_out.iack_msg = '1' and intc_out.iack_wr = '1' then
            int_msg <= '0';
          end if;
        end if;
      end if;
    end process;

    --  Software interrupt for async
    process (clk)
    begin
      if rising_edge(clk) then
        if rst_n = '0' then
          int_async <= '0';
        else
          if ifr_async = '1' and ifr_wr = '1' then
            int_async <= '1';
          elsif intc_out.iack_async = '1' and intc_out.iack_wr = '1' then
            int_async <= '0';
          end if;
        end if;
      end if;
    end process;
  end block;

  inst_mb_h2b: entity work.mailbox
    port map (
      clk_i => clk,
      rst_n_i => rst_n,
      mb_i => mb_h2b_in,
      mb_o => mb_h2b_out
    );

  inst_mb_b2h: entity work.mailbox
    port map (
      clk_i => clk,
      rst_n_i => rst_n,
      mb_i => mb_b2h_in,
      mb_o => mb_b2h_out
    );

  p_mem_async: process (clk)
  is
    type t_mem is array(0 to 2048) of std_logic_vector(31 downto 0);
    variable mem : t_mem;
  begin
    if rising_edge(clk) then
      mb_async_host_data <= mem(to_integer(unsigned(mb_async_host_addr)));
      if mb_async_board_wr = '1' then
        mem(to_integer(unsigned(mb_async_board_addr))) := mb_async_board_data;
      end if;
    end if;
  end process;

  gen_pulser_group: for i in 0 to g_nbr_pulser_group - 1 generate
    inst_pulser_grp: entity work.pulser_group
      port map (
        rst_n_i => rst_n,
        clk_wb_i => clk,
        clk_pg_i => clk_sys_125m_i,
        clk_cmp_i => clk,
        sync_62m5_i => sync_62m5,
        wb_i => pulser_grp_out(i),
        wb_o => pulser_grp_in(i),
        tm_tai_i => tm_tai,
        tm_cycles_i => tm_cycles,
        tm_sync_i => tm_sync,
        tm_load_tai_i => tm_load_tai,
        tm_load_steps_i => tm_load_steps,
        inputs_i => inputs,
        clocks_i => clocks,
        comb_outputs_o => outputs(i*8+7 downto i*8),
        pulses_o => pulsers_raw_125m(i*8 + 7 downto i*8),
        log_load_i => log_load,
        log_shift_i => log_shift_grp(i),
        cur_log_o => log_value_grp(i)
        );
  end generate;
  gen_no_pulser_group: for i in g_nbr_pulser_group to 3 generate
    pulser_grp_in(i) <= c_DUMMY_WB_SLAVE_OUT;
  end generate;

  gen_pad_out: for i in outputs'range generate
    --  TODO: polarity, maybe output select (like ppm, 10Mhz, frev1, frev2, nco_rst1, no_rst2)
    gen_serdes: if g_with_serdes generate
      inst_pulser_output: entity work.pulser_output
        port map (
          clk_500m_i => clk_sys_500m_i,
          clk_125m_i => clk_sys_125m_i,
          pad_o => pad_o (i),
          out_i => outputs (i)
          );
    end generate;
    gen_no_serdes: if not g_with_serdes generate
      pad_o (i) <= outputs (i)(7);
    end generate;
  end generate;

  gen_pad_inp: for i in pad_i'range generate
    signal log, log_p, log_raw, log_d : std_logic;
  begin
    gen_serdes: if g_with_serdes generate
      inst_pulser_input: entity work.pulser_input
        port map (
          clk_500m_i => clk_sys_500m_i,
          clk_125m_i => clk_sys_125m_i,
          pad_i => pad_i(i),
          pulse_o => inputs (i)
          );
    end generate;
    gen_no_serdes: if not g_with_serdes generate
      inputs (i) <= (pad_i(i), "000");
    end generate;

    log_raw <= inputs (i).v;
    log_p <= log_raw and not log_d;

    p_log: process (clk_sys_125m_i)
    begin
      if rising_edge (clk_sys_125m_i) then
        if rst_n = '0' then
          log <= '0';
          --  As rising edges are detected, initialize to '1' so that there is no
          --  spure pikes at reset.
          log_d <= '1';
          inp_log_chain (i) <= (others => '0');
        else
          log_d <= log_raw;

          if log_load = '1' then
            inp_log_chain (i)(2) <= log; --  In the 'pulse' (2)  bit
          elsif inp_log_shift = '1' then
            if i /= inp_log_chain'left then
              inp_log_chain (i) <= inp_log_chain (i + 1);
            else
              inp_log_chain (i) <= (others => '0');
            end if;
          end if;

          if log_load = '1' then
            --  New acquisition
            log <= log_p;
          else
            --  Accumulate log.
            log <= log or log_p;
          end if;
        end if;
      end if;
    end process;
    log_value_grp (g_nbr_pulser_group) <= inp_log_chain (0);
    inp_log_shift <= log_shift_grp (g_nbr_pulser_group);
  end generate;

  b_log: block
    constant c_LOG_NBR_LOG_ENTRIES: natural := 6;
    constant c_NBR_LOG_ENTRIES: natural := 2**c_LOG_NBR_LOG_ENTRIES;
    constant c_LOG_RAM_ADDR: natural := 30; --  1GB of DRAM for logs
    --  DDR at 0GB (at also at 32GB), logs at +1GB
    constant c_LOG_RAM_BASE : std_logic_vector(47 downto 0) := x"0000_4000_0000";

    --  Number of address bits per entry (for words).
    constant c_LOG_ENTRY_ADDR: natural := c_LOG_RAM_ADDR - c_LOG_NBR_LOG_ENTRIES - 2;

    subtype t_index is std_logic_vector(c_LOG_ENTRY_ADDR - 1 downto 0);
    type t_index_ram is array (0 to c_NBR_LOG_ENTRIES - 1) of t_index;
    signal next_index : t_index;

    --  Index RAM to write new input data.
    signal indexes_inp : t_index_ram;

    --  Index RAM for the user (just a copy of indexes_up, but read by the user)
    --  Simulate a triple-port memory.
    signal indexes_usr : t_index_ram;

    signal cnt : unsigned(c_LOG_NBR_LOG_ENTRIES - 1 downto 0);
    signal in_rst : std_logic;
    signal done, done_d : std_logic;

    signal log_data : std_logic_vector(31 downto 0);
    signal log_wr : std_logic;

    signal updated : std_logic_vector(c_LOG_NBR_LOG_ENTRIES - 1 downto 0);

    signal fifo_wdata, fifo_rdata : std_logic_vector(63 downto 0);
    signal fifo_wen, fifo_full, fifo_ren, fifo_empty : std_logic;

    signal dma_wait_awready, dma_wait_wready : std_logic;
  begin

    inst_fifo: entity work.generic_async_fifo
      generic map (
        g_data_width => 32 + 32,
        g_size => 128,
        g_show_ahead => True,
        g_with_rd_empty => True,
        g_with_rd_full => False,
        g_with_rd_almost_empty => False,
        g_with_rd_almost_full => False,
        g_with_rd_count => False,
        g_with_wr_empty => False,
        g_with_wr_full => True,
        g_with_wr_almost_empty => False,
        g_with_wr_almost_full => False,
        g_with_wr_count => False,
        g_almost_empty_threshold => 4,
        g_almost_full_threshold => 4,
        g_memory_implementation_hint => open
      )
      port map (
        rst_n_i => rst_n,
        clk_wr_i => clk_sys_125m_i,
        d_i => fifo_wdata,
        we_i => fifo_wen,
        wr_empty_o => open,
        wr_full_o => fifo_full,
        wr_almost_empty_o => open,
        wr_almost_full_o => open,
        wr_count_o => open,
        clk_rd_i => clk_sys_250m_i,
        q_o => fifo_rdata,
        rd_i => fifo_ren,
        rd_empty_o => fifo_empty,
        rd_full_o => open,
        rd_almost_empty_o => open,
        rd_almost_full_o => open,
        rd_count_o => open
      );

    --  Write logs to the fifo
    process (clk_sys_125m_i)
      variable next_index_cnt : natural range 0 to 2**c_LOG_NBR_LOG_ENTRIES - 1;
      variable indexes_inp_val : t_index;
    begin
      if rising_edge(clk_sys_125m_i) then
        log_load <= '0';
        fifo_wen <= '0';

        if rst_sys_62m5_n_i = '0' then
          in_rst <= '1';
          cnt <= (others => '0');
          done <= '0';
          done_d <= '0';
          next_index_cnt := 0;
          updated <= (others => '0');
          logs_addr_rack <= '0';
          log_shift_grp <= (others => '0');
        else
          --  Log acquisition and write into cache.
          if done = '1' then
            --  Nothing to do...
            next_index_cnt := 0;
            if pulse_1mhz.v = '1' then
              --  unless restart
              done <= '0';
              done_d <= '1';
              cnt <= (others => '0');
              log_load <= '1';
            end if;
          elsif done_d = '1' then
            --  Need to wait one cycle to get the first data (ie to let pulser group handle the log_load).
            done_d <= '0';
            --  And start shifting (the first group)
            log_shift_grp (0) <= '1';
          else
            if log_wr = '1' then
              --  Fifo inputs
              fifo_wdata(63 downto 32) <= log_data;
              fifo_wdata(31 downto c_LOG_RAM_ADDR) <= (others => '0');
              fifo_wdata(c_LOG_RAM_ADDR - 1 downto c_LOG_ENTRY_ADDR + 2) <= std_logic_vector(cnt);
              fifo_wdata(c_LOG_ENTRY_ADDR + 1 downto 2) <= next_index;
              fifo_wen <= '1' and not fifo_full;

              --  Update index
              indexes_inp_val := std_logic_vector(unsigned(next_index) + 1);
              updated(to_integer(cnt)) <= '1';
            else
              --  Clear index on reset.
              indexes_inp_val := (others => '0');
            end if;

            if log_wr = '1' or in_rst = '1' then
              indexes_inp (to_integer(cnt)) <= indexes_inp_val;
              indexes_usr (to_integer(cnt)) <= indexes_inp_val;
            end if;

            if cnt = (cnt'range => '1') then
              done <= '1';
              log_shift_grp <= (others => '0');
              in_rst <= '0';
            else
              --  Continue shifting.
              if cnt (2 downto 0) = "111" then
                log_shift_grp <= log_shift_grp (log_shift_grp'left - 1 downto 0) & '0';
              end if;
              cnt <= cnt + 1;
              next_index_cnt := next_index_cnt + 1;
            end if;
          end if;
        end if;

        next_index <= indexes_inp(next_index_cnt);

        --  User read of indexes (sync on 62.5m)
        if sync_62m5 = '1' and logs_addr_rd = '1' then
          logs_addr_dat(31 downto c_LOG_RAM_ADDR) <= "01";
          logs_addr_dat(c_LOG_NBR_LOG_ENTRIES + c_LOG_ENTRY_ADDR+1 downto c_LOG_ENTRY_ADDR+2) <=
            logs_addr_adr(c_LOG_NBR_LOG_ENTRIES - 1 + 2 downto 2);
          logs_addr_dat(c_LOG_ENTRY_ADDR+1 downto 2) <= indexes_usr(to_integer(unsigned (logs_addr_adr)));
          logs_addr_dat(1 downto 0) <= "00";
          logs_addr_rack <= '1';
        elsif sync_62m5 = '1' then
          logs_addr_rack <= '0';
        end if;
      end if;
    end process;

    fifo_ren <= '1' when fifo_empty = '0'
      and (dma_wait_wready = '0' or S_AXI_LOG_wready = '1')
      and (dma_wait_awready = '0' or S_AXI_LOG_awready = '1')
      else '0';

    --  Write the fifo to memory.
    process (clk_sys_250m_i)
    begin
      if rising_edge(clk_sys_250m_i) then
        if rst_sys_62m5_n_i = '0' then
          S_AXI_LOG_awaddr <= (others => '0');
          S_AXI_LOG_awaddr (47 downto 0) <= c_LOG_RAM_BASE;
          S_AXI_LOG_awvalid <= '0';
          S_AXI_LOG_awid <= (others => '0');
          S_AXI_LOG_wvalid <= '0';

          dma_wait_wready <= '0';
          dma_wait_awready <= '0';
        else
          --  Cache to DDR
          if fifo_ren = '1' then
            S_AXI_LOG_awaddr (c_LOG_RAM_ADDR - 1 downto 0) <= fifo_rdata(c_LOG_RAM_ADDR - 1 downto 0);
            S_AXI_LOG_awaddr (3 downto 0) <= "0000";
            S_AXI_LOG_awvalid <= '1';
            S_AXI_LOG_awid <= (others => '0');
            S_AXI_LOG_awlen <= (others => '0');

            --  Use 128b data bus (with strobe).  Failed with a 32b data bus.
            S_AXI_LOG_wdata (31 downto 0) <= fifo_rdata(63 downto 32);
            S_AXI_LOG_wdata (63 downto 32) <= fifo_rdata(63 downto 32);
            S_AXI_LOG_wdata (95 downto 64) <= fifo_rdata(63 downto 32);
            S_AXI_LOG_wdata (127 downto 96) <= fifo_rdata(63 downto 32);

            S_AXI_LOG_wstrb <= (others => '0');
            case fifo_rdata (3 downto 2) is
              when "00" =>
                S_AXI_LOG_wstrb (3 downto 0) <= "1111";
              when "01" =>
                S_AXI_LOG_wstrb (7 downto 4) <= "1111";
              when "10" =>
                S_AXI_LOG_wstrb (11 downto 8) <= "1111";
              when others =>
                S_AXI_LOG_wstrb (15 downto 12) <= "1111";
            end case;
            S_AXI_LOG_wlast <= '1';
            S_AXI_LOG_wvalid <= '1';

            --  Need to wait until ready.
            dma_wait_wready <= '1';
            dma_wait_awready <= '1';
          else
            if S_AXI_LOG_wready = '1' then
              S_AXI_LOG_wvalid <= '0';
              dma_wait_wready <= '0';
            end if;
            if S_AXI_LOG_awready = '1' then
              S_AXI_LOG_awvalid <= '0';
              dma_wait_awready <= '0';
            end if;
          end if;
        end if;
      end if;
    end process;

    S_AXI_LOG_bready <= '1';

    log_data (31 downto 24) <= tm_tai (7 downto 0);
    log_data (23 downto 4) <= tm_us;
    log_data (3 downto 0) <= log_value_grp (to_integer (cnt(c_LOG_NBR_LOG_ENTRIES - 1 downto 3)));
    log_wr <= '1' when log_data (3 downto 0) /= b"0000" else '0';
  end block;

  my_inst: entity work.lemos_argb_leds
    generic map (
      g_clk_freq => 62_500_000
    )
    port map (
      clk_i => clk_sys_62m5_i,
      rst_n_i => rst_sys_62m5_n_i,
      leds_colors_adr_i => leds_colors_adr,
      leds_colors_dato_o => leds_colors_dato,
      leds_colors_dati_i => leds_colors_dati,
      leds_colors_rd_i => leds_colors_rd,
      leds_colors_wr_i => leds_colors_wr,
      leds_colors_rack_o => leds_colors_rack,
      leds_colors_wack_o => leds_colors_wack,
      dout_o (0) => fp_led0_o,
      dout_o (1) => fp_led1_o,
      dout_o (2) => pp_led0_o,
      dout_o (3) => pp_led1_o
    );

  inst_fifo_nic: entity work.wrf_fifo
    generic map (
      g_depth => 16
    )
    port map (
      clk_sys_i => clk_sys_62m5_i,
      rst_n_i => rst_sys_62m5_n_i,
      snk_i => eth_rx_i,
      snk_o => eth_rx_o,
      src_o => nic_rx_in,
      src_i => nic_rx_out,
      max_count_o => nic_fifo_max(3 downto 0),
      fifo_full_o => nic_fifo_full,
      clear_stat_i => '0'
    );

  inst_nic: entity work.xwr_nic
    generic map (
      g_interface_mode => CLASSIC,
      g_address_granularity => BYTE,
      g_src_cyc_on_stall => true,
      g_port_mask_bits => 32,
      g_rmon_events_pp => 1
      )
    port map (
      clk_sys_i => clk_sys_62m5_i,
      rst_n_i => rst_sys_62m5_n_i,
      pps_p_i => pps_p_i,
      pps_valid_i => pps_valid_i,
      snk_i => nic_rx_in,
      snk_o => nic_rx_out,
      src_i => eth_tx_i,
      src_o => eth_tx_o,
      rtu_dst_port_mask_o => open,
      rtu_prio_o => open,
      rtu_drop_o => open,
      rtu_rsp_valid_o => open,
      rtu_rsp_ack_i => '1',
      wb_i => nic_master_out,
      wb_o => nic_master_in,
      int_o => open,
      rmon_events_o => open
      );

  b_rf1: block
    component gtwizard_ultrascale_rf1
      port (
        gtwiz_userclk_tx_reset_in : in STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_userclk_tx_srcclk_out : out STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_userclk_tx_usrclk_out : out STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_userclk_tx_usrclk2_out : out STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_userclk_tx_active_out : out STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_userclk_rx_reset_in : in STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_userclk_rx_srcclk_out : out STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_userclk_rx_usrclk_out : out STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_userclk_rx_usrclk2_out : out STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_userclk_rx_active_out : out STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_reset_clk_freerun_in : in STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_reset_all_in : in STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_reset_tx_pll_and_datapath_in : in STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_reset_tx_datapath_in : in STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_reset_rx_pll_and_datapath_in : in STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_reset_rx_datapath_in : in STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_reset_rx_cdr_stable_out : out STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_reset_tx_done_out : out STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_reset_rx_done_out : out STD_LOGIC_VECTOR(0 downto 0);
        gtwiz_userdata_tx_in : in STD_LOGIC_VECTOR(31 downto 0);
        gtwiz_userdata_rx_out : out STD_LOGIC_VECTOR(31 downto 0);
        drpclk_in : in STD_LOGIC_VECTOR(0 downto 0);
        gthrxn_in : in STD_LOGIC_VECTOR(0 downto 0);
        gthrxp_in : in STD_LOGIC_VECTOR(0 downto 0);
        gtrefclk0_in : in STD_LOGIC_VECTOR(0 downto 0);
        gthtxn_out : out STD_LOGIC_VECTOR(0 downto 0);
        gthtxp_out : out STD_LOGIC_VECTOR(0 downto 0);
        gtpowergood_out : out STD_LOGIC_VECTOR(0 downto 0);
        rxpmaresetdone_out : out STD_LOGIC_VECTOR(0 downto 0);
        txpmaresetdone_out : out STD_LOGIC_VECTOR(0 downto 0)
      );
    end component;

    signal tx_usr_clk : std_logic;
    signal wrs_rx_first, wrs_rx_last, wrs_rx_valid, wrs_rx_dreq : std_logic;
    signal snmp_array_in  : t_generic_word_array(c_WR_STREAMERS_ARR_SIZE_IN-1 downto 0);
    signal snmp_array_out : t_generic_word_array(c_WR_STREAMERS_ARR_SIZE_OUT-1 downto 0);
    signal wrs_rx_data    : std_logic_vector(c_rx_streamer_params_RF.data_width-1 downto 0);

    signal wrs_rx_RFmFramePayloads  : t_RFmFramePayload;
    signal wrs_rx_Frame_valid    : std_logic;

    signal nco_rst, nco_load, nco_rst_250, nco_load_250, nco_load_250_d, nco_load_250_d2 : std_logic;
    signal nco_load_tfr : std_logic_vector(3 downto 0);
    signal nco_ftw, nco_ftw_250 : std_logic_vector(47 downto 0);
    signal nco_tfr_dis : unsigned (3 downto 0);
    signal nco_ftw_h1_250_uns : unsigned(63 downto 0);
    signal nco_ftw_h1_250 : std_logic_vector(47 downto 0);

    -- attribute mark_debug of nco_rst, nco_load : signal is "true";
  begin
    process (clk_sys_62m5_i)
    begin
      if rising_edge(clk_sys_62m5_i) then
        nco_load <= '0';
        if rf1_load_en = '1' and rf1_load_wr = '1' then
          --  User load
          nco_ftw <= ftw_64(47 downto 0);
          nco_load <= '1';
          nco_rst <= rf1_rst;
        elsif wrs_rx_frame_valid = '1' then
          --  From network
          nco_ftw <= wrs_rx_rfmframepayloads.FTW_H1_main;
          nco_rst <= wrs_rx_rfmframepayloads.control(0);
          nco_load <= '1';
        end if;
      end if;
    end process;

    process (tx_usr_clk)
    begin
      if rising_edge(tx_usr_clk) then
        --  TODO: reset
        nco_load_tfr <= nco_load_tfr(2 downto 0) & nco_load;
        nco_load_250 <= '0';
        if nco_tfr_dis /= 0 then
          nco_tfr_dis <= nco_tfr_dis - 1;
        elsif nco_load_tfr (1 downto 0) = "11" then
          nco_ftw_250 <= nco_ftw;
          nco_rst_250 <= nco_rst;
          nco_load_250 <= '1';
          nco_tfr_dis <= (others => '1');
        end if;

        --  Divide by 2, as our nco frequency is 250Mhz
        nco_ftw_h1_250 <= '0' & std_logic_vector (nco_ftw_h1_250_uns(47 downto 1));
        nco_load_250_d2 <= nco_load_250_d;

        --  Multiply by harmonic factor to get rf ftw.
        nco_ftw_h1_250_uns <= unsigned(nco_ftw_250) * unsigned(rf1_h1(15 downto 0));
        nco_load_250_d <= nco_load_250;
      end if;
    end process;


    inst_nco: entity work.nco_x32
      port map (
        nco_clk_i => tx_usr_clk,
        rst_n_i => rst_sys_62m5_n_i,
        ftw_load_i => nco_load_250_d2,
        ftw_i => nco_ftw_h1_250,
        nco_rst_i => nco_rst_250,
        nco_o => rf1_nco_out
        );

    process (tx_usr_clk)
    begin
      if rising_edge(tx_usr_clk) then
        rf1_nco_out2 <= rf1_nco_out;
      end if;
    end process;

    inst_rf1_gth: gtwizard_ultrascale_rf1
      port map (
        gtwiz_userclk_tx_reset_in (0) => gth_rf1_cfg(0),
        gtwiz_userclk_tx_srcclk_out => open,
        gtwiz_userclk_tx_usrclk_out (0) => tx_usr_clk,
        gtwiz_userclk_tx_usrclk2_out => open,
        gtwiz_userclk_tx_active_out (0) => gth_rf1_sta(0),
        gtwiz_userclk_rx_reset_in (0) => gth_rf1_cfg(1),
        gtwiz_userclk_rx_srcclk_out => open,
        gtwiz_userclk_rx_usrclk_out => open,
        gtwiz_userclk_rx_usrclk2_out => open,
        gtwiz_userclk_rx_active_out (0) => gth_rf1_sta(1),
        gtwiz_reset_clk_freerun_in (0) => clk_sys_62m5_i,
        gtwiz_reset_all_in (0) => gth_rf1_cfg(2),
        gtwiz_reset_tx_pll_and_datapath_in (0) => gth_rf1_cfg(3),
        gtwiz_reset_tx_datapath_in (0) => gth_rf1_cfg(4),
        gtwiz_reset_rx_pll_and_datapath_in (0) => gth_rf1_cfg(5),
        gtwiz_reset_rx_datapath_in (0) => gth_rf1_cfg(6),
        gtwiz_reset_rx_cdr_stable_out (0) => gth_rf1_sta(2),
        gtwiz_reset_tx_done_out (0) => gth_rf1_sta(3),
        gtwiz_reset_rx_done_out (0) => gth_rf1_sta(4),
        gtwiz_userdata_tx_in => rf1_nco_out2,
        gtwiz_userdata_rx_out  => open,
        drpclk_in (0) => clk_sys_62m5_i,
        gthrxn_in (0) => rf1_rxn_i,
        gthrxp_in (0) => rf1_rxp_i,
        gtrefclk0_in (0) => gth_clk_250m_i,
        gthtxn_out (0) => rf1_txn_o,
        gthtxp_out (0) => rf1_txp_o,
        gtpowergood_out (0) => gth_rf1_sta(5),
        rxpmaresetdone_out (0) => gth_rf1_sta(6),
        txpmaresetdone_out (0) => gth_rf1_sta(7)
        );

    inst_fifo_rf1: entity work.wrf_fifo
      generic map (
        g_depth => 16
        )
      port map (
        clk_sys_i => clk_sys_62m5_i,
        rst_n_i => rst_sys_62m5_n_i,
        snk_i => eth_rx_i,
        snk_o => open,
        src_o => rf1_rx_in,
        src_i => rf1_rx_out,
        max_count_o => rf1_fifo_max(3 downto 0),
        fifo_full_o => rf1_fifo_full,
        clear_stat_i => '0'
        );

  inst_streamer: entity work.xwr_streamers
  generic map (
    g_streamers_op_mode => RX_ONLY,
    g_clk_ref_rate => 62_500_000,
    g_tx_streamer_params => c_tx_streamer_params_RF,
    g_rx_streamer_params => c_rx_streamer_params_RF,
    g_stats_cnt_width => open,
    g_stats_acc_width => open,
    g_slave_mode => open,
    g_slave_granularity => open,
    g_simulation => 0,
    g_sim_cycle_counter_range => open,
    g_with_dbg_word => open
  )
  port map (
    clk_sys_i => clk_sys_62m5_i,
    clk_ref_i => clk_sys_62m5_i,
    rst_n_i => rst_sys_62m5_n_i,
    src_i => c_dummy_src_in,
    src_o => open,
    snk_i => eth_rx_i,
    snk_o => eth_rx_o,
    tx_data_i => open,
    tx_valid_i => '0',
    tx_dreq_o => open,
    tx_last_p1_i => '0',
    tx_flush_p1_i => '0',
    rx_first_p1_o => wrs_rx_first,
    rx_last_p1_o => wrs_rx_last,
    rx_data_o => wrs_rx_data,
    rx_valid_o => wrs_rx_valid,
    rx_dreq_i => wrs_rx_dreq,
    rx_late_o => open,
    rx_timeout_o => open,
    tm_time_valid_i => tm_time_valid_i,
    tm_tai_i => tm_tai_i,
    tm_cycles_i => tm_cycles_i,
    link_ok_i => tm_link_up_i,
    wb_slave_i => rf1_streamer_master_out,
    wb_slave_o => rf1_streamer_master_in,
    snmp_array_o => snmp_array_out,
    snmp_array_i => snmp_array_in,
    tx_streamer_cfg_i => c_tx_streamer_cfg_default,
    rx_streamer_cfg_i => c_rx_streamer_cfg_default
  );

  inst_rfframerx: entity work.rfframerxctrl
    generic map (
      g_rx_rfframetype => c_ID_RFmFrame
    )
    port map (
      clk_i => clk_sys_62m5_i,
      rst_n_i => rst_sys_62m5_n_i,
      rx_data_i => wrs_rx_data,
      rx_valid_i => wrs_rx_valid,
      rx_first_p1_i => wrs_rx_first,
      rx_last_p1_i => wrs_rx_last,
      rfframeheader_o => open,
      rfmframepayloads_o => wrs_rx_rfmframepayloads,
      rfsframepayloads_o => open,
      rxframe_valid_p1_o => wrs_rx_frame_valid,
      rxframe_type_o => open
    );

  process (clk_sys_62m5_i)
  begin
    if rising_edge(clk_sys_62m5_i) then
      rf1_rf_rx_sta <= '0';

      if wrs_rx_frame_valid = '1' then
        rf1_rf_rx_tai <= tm_tai_i(31 downto 0);
        rf1_rf_rx_cyc (27 downto 0) <= tm_cycles_i;
        rf1_rf_rx_cyc (31 downto 28) <= x"0";
        rf1_rf_rx_sta <= '1';
        rf1_rf_ftw <= wrs_rx_rfmframepayloads.FTW_H1_main;
        rf1_rf_ftw_rst <= wrs_rx_rfmframepayloads.control(0);
      end if;
    end if;
  end process;
end block;
end architecture arch;
