--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- White Rabbit Trigger Distribution
-- https://ohwr-gitlab.cern.ch/projects/wrtd
--------------------------------------------------------------------------------
--
-- unit name:   gmtwr_v0
--
-- description: Top entity for GMT over WR playground
--
--------------------------------------------------------------------------------
-- Copyright CERN 2014-2019
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity nco_x32 is
  port (
    --  Main (and nco) clock.
    nco_clk_i : std_logic;
    rst_n_i : std_logic;

    --  Load the ftw (on a rising edge)
    ftw_load_i : std_logic;
    ftw_i : std_logic_vector(47 downto 0);
    nco_rst_i : std_logic;

    nco_o : out std_logic_vector(31 downto 0)
  );
end nco_x32;

architecture arch of nco_x32 is
  signal load_d, ftw_load, nco_rst : std_logic;
  signal msb : std_logic_vector (31 downto 0);
  signal ftw, nco_init_2, nco_init_9, nco_init_16,
    nco_init_23, nco_init_30 : unsigned (47 downto 0);
begin
  inst_nco_2: entity work.nco_x7
    port map (
      nco_clk_i => nco_clk_i,
      rst_n_i => rst_n_i,
      ftw_load_i => ftw_load,
      ftw_i => ftw_i,
      nco_rst_i => nco_rst,
      nco_init_i => std_logic_vector (nco_init_2),
      msb_0_o => msb(2),
      msb_p1_o => msb(3),
      msb_p2_o => msb(4),
      msb_p4_o => msb(6),
      msb_m1_o => msb(1),
      msb_m2_o => msb(0),
      msb_m4_o => open
    );
  inst_nco_9: entity work.nco_x7
    port map (
      nco_clk_i => nco_clk_i,
      rst_n_i => rst_n_i,
      ftw_load_i => ftw_load,
      ftw_i => ftw_i,
      nco_rst_i => nco_rst,
      nco_init_i => std_logic_vector (nco_init_9),
      msb_0_o => msb(9),
      msb_p1_o => msb(10),
      msb_p2_o => msb(11),
      msb_p4_o => msb(13),
      msb_m1_o => msb(8),
      msb_m2_o => msb(7),
      msb_m4_o => msb(5)
    );
  inst_nco_16: entity work.nco_x7
    port map (
      nco_clk_i => nco_clk_i,
      rst_n_i => rst_n_i,
      ftw_load_i => ftw_load,
      ftw_i => ftw_i,
      nco_rst_i => nco_rst,
      nco_init_i => std_logic_vector (nco_init_16),
      msb_0_o => msb(16),
      msb_p1_o => msb(17),
      msb_p2_o => msb(18),
      msb_p4_o => msb(20),
      msb_m1_o => msb(15),
      msb_m2_o => msb(14),
      msb_m4_o => msb(12)
    );
  inst_nco_23: entity work.nco_x7
    port map (
      nco_clk_i => nco_clk_i,
      rst_n_i => rst_n_i,
      ftw_load_i => ftw_load,
      ftw_i => ftw_i,
      nco_rst_i => nco_rst,
      nco_init_i => std_logic_vector (nco_init_23),
      msb_0_o => msb(23),
      msb_p1_o => msb(24),
      msb_p2_o => msb(25),
      msb_p4_o => msb(27),
      msb_m1_o => msb(22),
      msb_m2_o => msb(21),
      msb_m4_o => msb(19)
    );
  inst_nco_30: entity work.nco_x7
    port map (
      nco_clk_i => nco_clk_i,
      rst_n_i => rst_n_i,
      ftw_load_i => ftw_load,
      ftw_i => ftw_i,
      nco_rst_i => nco_rst,
      nco_init_i => std_logic_vector (nco_init_30),
      msb_0_o => msb(30),
      msb_p1_o => msb(31),
      msb_p2_o => open,
      msb_p4_o => open,
      msb_m1_o => msb(29),
      msb_m2_o => msb(28),
      msb_m4_o => msb(26)
    );

  nco_o <= msb;

  nco_init_2  <= (ftw srl 4);  -- 2 = 32 / 16
  nco_init_9  <= (ftw srl 5) + (ftw srl 2); -- 9 = 32 / 32 + 32 / 4
  nco_init_16 <= (ftw srl 1); -- 16 = 32 / 2
  nco_init_23 <= (ftw srl 1) + (ftw srl 2) - (ftw srl 5); --  23 = 32 / 2 + 32 / 4 - 32 / 32
  nco_init_30 <= ftw - (ftw srl 4); -- 30 = 32 - 32 / 16

  process (nco_clk_i)
  begin
    if rising_edge (nco_clk_i) then
      load_d <= ftw_load_i;
      ftw_load <= '0';
      nco_rst <= '0';

      if rst_n_i = '0' then
        load_d <= '0';
      else
        if ftw_load_i = '1' and load_d = '0' then
          ftw_load <= '1';
          nco_rst <= nco_rst_i;
          ftw <= unsigned(ftw_i);
        end if;
      end if;
    end if;
  end process;
end arch;
