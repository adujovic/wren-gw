--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Project    : General Cores Collection library
--------------------------------------------------------------------------------
--
-- unit name:   gc_argb_led_drv
--
-- description: Driver for argb (or intelligent) led like ws2812b
--
--------------------------------------------------------------------------------
-- Copyright CERN 2024
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity lemos_argb_leds is
  generic (
    g_clk_freq : natural
  );
  port (
    clk_i   : in std_logic;
    rst_n_i : in std_logic;

    --  Access to the color memory
    leds_colors_adr_i    : in  std_logic_vector(7 downto 2);
    leds_colors_dato_o   : out std_logic_vector(31 downto 0);
    leds_colors_dati_i   : in  std_logic_vector(31 downto 0);
    leds_colors_rd_i     : in  std_logic;
    leds_colors_wr_i     : in  std_logic;
    leds_colors_rack_o   : out std_logic;
    leds_colors_wack_o   : out std_logic;

    --  Output to the first led.
    dout_o  : out std_logic_vector(3 downto 0)
  );
end lemos_argb_leds;

architecture arch of lemos_argb_leds is

  component ila_0 is
    port (
      clk    : in STD_LOGIC;
      probe0 : in STD_LOGIC_VECTOR(63 downto 0)
    );
  end component;

  --  The memory for forced colors.
  subtype t_word is std_logic_vector(31 downto 0);
  type t_memory is array(63 downto 0) of t_word;
  signal colors_memory: t_memory;

  signal cnt               : natural range 0 to 63;
  signal r, g, b           : std_logic_vector(7 downto 0);
  signal valid, ready, res : std_logic_vector(3 downto 0);

  signal read_ack : std_logic;
  signal mem_val : t_word;

  type t_state is (S_RESET, S_READ, S_WAIT_READY);
  signal state : t_state;

begin
  process(clk_i)
  begin
    if rising_edge(clk_i) then
      leds_colors_rack_o <= '0';
      leds_colors_wack_o <= '0';
      read_ack <= '0';

      if rst_n_i = '0' then
        null;
      else
        if leds_colors_wr_i = '1' then
          colors_memory(to_integer(unsigned(leds_colors_adr_i))) <= leds_colors_dati_i;
          leds_colors_wack_o <= '1';
        elsif leds_colors_rd_i = '1' then
          leds_colors_dato_o <= colors_memory(to_integer(unsigned(leds_colors_adr_i)));
          leds_colors_rack_o <= '1';
        elsif state = S_READ then
          mem_val <= colors_memory(cnt);
          read_ack <= '1';
        end if;
      end if;
    end if;
  end process;

  process(clk_i)
  begin
    if rising_edge(clk_i) then
      valid <= (others => '0');
      if rst_n_i = '0' then
        cnt <= 0;
        state <= S_RESET;
      else
        case state is
          when S_RESET =>
            --  Wait until ARGB reset
            if res = (res'range => '1') then
              state <= S_READ;
            end if;
          when S_READ =>
            --  Read the color
            if read_ack = '1' then
              state <= S_WAIT_READY;
            end if;
          when S_WAIT_READY =>
            if ready (cnt / 16) = '1' then
              r <= mem_val(23 downto 16);
              g <= mem_val(15 downto 8);
              b <= mem_val(7 downto 0);
              valid (cnt / 16) <= '1';

              if cnt = 63 then
                cnt <= 0;
                state <= S_RESET;
              else
                if cnt >= 48 then
                  --  Next led in the raw
                  cnt <= cnt - 48 + 1;
                else
                  --  Next raw
                  cnt <= cnt + 16;
                end if;
                state <= S_READ;
              end if;
            end if;
        end case;
      end if;
    end if;
  end process;

  g_drv: for i in 0 to 3 generate
    inst_drv: entity work.gc_argb_led_drv
      generic map (
        g_clk_freq => g_clk_freq
        )
      port map (
        clk_i => clk_i,
        rst_n_i => rst_n_i,
        g_i => g,
        r_i => r,
        b_i => b,
        valid_i => valid (i),
        dout_o => dout_o (i),
        ready_o => ready (i),
        res_o => res (i)
        );
  end generate;

  g_ila : if false generate
    signal is_reset, is_read, is_wait : std_logic;
    signal scnt : std_logic_vector(4 downto 0);
  begin
    is_reset <= '1' when state = S_RESET else '0';
    is_read <= '1' when state = S_READ else '0';
    is_wait <= '1' when state = S_WAIT_READY else '0';
    scnt <= std_logic_vector(to_unsigned(cnt, 5));

    inst_ila: ila_0
      port map (
        clk => clk_i,
        probe0(31 downto 0) => mem_val,
        probe0(32) => is_reset,
        probe0(33) => is_read,
        probe0(34) => is_wait,
        probe0(35) => read_ack,
        probe0(39 downto 36) => valid,
        probe0(43 downto 40) => ready,
        probe0(47 downto 44) => res,
        probe0(52 downto 48) => scnt,
        probe0 (63 downto 53) => (others => '0')
        );
  end generate;
end arch;