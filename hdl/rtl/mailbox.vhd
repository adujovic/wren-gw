library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mailbox_pkg.all;

entity mailbox is
  port (
    clk_i : in std_logic;
    rst_n_i : in std_logic;

    mb_i : in t_mailbox_in;
    mb_o : out t_mailbox_out
  );
end mailbox;

architecture arch of mailbox is
  signal ready : std_logic;
begin
  mb_o.mbr_cmd <= mb_i.mbw_cmd;
  mb_o.mbr_len <= mb_i.mbw_len;

  mb_o.mbr_csr_ready <= ready;
  mb_o.mbw_csr_ready <= ready;

  process (clk_i)
  begin
    if rising_edge (clk_i) then
      if rst_n_i = '0' then
        ready <= '0';
      elsif mb_i.mbw_csr_wr = '1' and mb_i.mbw_csr_ready = '1' then
        ready <= '1';
      elsif mb_i.mbr_csr_wr = '1' and mb_i.mbr_csr_ready = '1' then
        ready <= '0';
      end if;
    end if;
  end process;

  process (clk_i)
    type t_mem is array(0 to 1023) of std_logic_vector(31 downto 0);
    variable mem : t_mem;
  begin
    if rising_edge(clk_i) then
      mb_o.mbr_mem_data <= mem(to_integer(unsigned(mb_i.mbr_mem_addr)));
      if mb_i.mbw_mem_wr = '1' then
        mem(to_integer(unsigned(mb_i.mbw_mem_addr))) := mb_i.mbw_mem_data;
      end if;
    end if;
  end process;
end arch;
