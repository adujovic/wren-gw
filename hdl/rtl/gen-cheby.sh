#/bin/sh
OPT="--header commit"

cheby $OPT --gen-hdl pulser_group_map.vhd --gen-c ../../sw/hw-include/pulser_group_map.h --consts-style vhdl-ohwr --gen-consts pulser_group_map_consts.vhd -i pulser_group_map.cheby
cheby $OPT --gen-c ../../sw/hw-include/mailbox_in_regs_map.h -i mailbox_in_regs_map.cheby
cheby $OPT --gen-c ../../sw/hw-include/mailbox_out_regs_map.h -i mailbox_out_regs_map.cheby
cheby $OPT --gen-hdl board_map.vhd --gen-c ../../sw/hw-include/board_map.h -i board_map.cheby
cheby $OPT --gen-hdl host_map.vhd --gen-c ../../sw/hw-include/host_map.h -i host_map.cheby
cheby $OPT --gen-hdl pci_map.vhd --gen-c ../../sw/hw-include/pci_map.h -i pci_map.cheby
cheby $OPT --gen-hdl vme_map.vhd --gen-c ../../sw/hw-include/vme_map.h -i vme_map.cheby
