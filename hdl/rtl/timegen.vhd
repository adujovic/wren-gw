library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pulser_pkg.all;

entity timegen is
  port (
    clk_62m5_i    : in  std_logic;
    clk_125m_i   : in  std_logic;
    rst_n_i     : in  std_logic;

    --  tm_* uses 62.5Mhz (16ns)
    tm_tai_i    : in  std_logic_vector(39 downto 0);
    tm_cycles_i : in  std_logic_vector(25 downto 0);
    tm_valid_i  : in  std_logic;

    --  Synchro between 125m and 62m5.
    --  If the sync signal is sampled as 1, it means the rising
    --  edge of the 125m occured at the rising edge of 62m5.
    --           |--|  |--|  |--|  |--|  |--|  |--|  |--|
    -- 125Mhz: --|  |--|  |--|  |--|  |--|  |--|  |--|  |--
    --
    --           |----|    |----|    |----|    |----|    |----|  
    -- 62Mhz:  --|    |----|    |----|    |----|    |----|    |--
    --
    --         --|    |----|    |----|    |----|    |----|    |----
    -- sync:     |----|    |----|    |----|    |----|    |----|
    sync_62m5_o : out std_logic;

    pre_pps_o : out std_logic;

    --  TAI + Cycles, but with an advance of 32ns (2x 62.5Mhz cycles)
    --  Used to compensate internal delay.
    tm_tai_o    : out std_logic_vector(31 downto 0);
    tm_cycles_o : out std_logic_vector(25 downto 0);
    tm_sync_o   : out std_logic;

    --  Pulsers must be loaded one step (512ns) in advance;
    --  provide that time reference (512ns = 32*16ns)
    tm_load_tai_o    : out std_logic_vector(31 downto 0);
    tm_load_steps_o : out std_logic_vector(25 downto 5)
  );
end timegen;

architecture arch of timegen is
  constant C_CYC_FREQ : natural := 62_500_000;
  signal tog_62m5, tog_125m_d : std_logic;
  signal tm_cycles, tm_load_cycles : unsigned(25 downto 0);
  signal pps_m512, pps : boolean;
begin
  process (clk_62m5_i)
  begin
    if rising_edge(clk_62m5_i) then
      if rst_n_i = '0' then
        tog_62m5 <= '0';
      else
        tog_62m5 <= not tog_62m5;
      end if;
    end if;
  end process;

  --  Generate sync_62m5
  process (clk_125m_i)
  begin
    if rising_edge(clk_125m_i) then
      if rst_n_i = '0' then
          sync_62m5_o <= '0';
          tog_125m_d <= '0';
      else
        --  If they are different, clk_62m5 will rise on the next clk_125m rising edge.
        if tog_125m_d = tog_62m5 then
          sync_62m5_o <= '1';
        else
          sync_62m5_o <= '0';
        end if;
        tog_125m_d <= tog_62m5;
      end if;
    end if;
  end process;

  --  2 cycles earlier, and 512ns (32*16ns) earlier.
  pps_m512 <= unsigned (tm_cycles_i) = to_unsigned(C_CYC_FREQ - 2 - 32, 26);
  pps <= unsigned (tm_cycles_i) = to_unsigned(C_CYC_FREQ - 2, 26);

  process (clk_62m5_i)
  begin
    if rising_edge(clk_62m5_i) then
      if rst_n_i = '0' or tm_valid_i = '0' then
        tm_sync_o <= '0';
        pre_pps_o <= '0';
      else
        if pps_m512 then
          tm_sync_o <= '1';
          tm_cycles <= to_unsigned(C_CYC_FREQ - 32, 26);
          tm_tai_o <= std_logic_vector (unsigned (tm_tai_i (31 downto 0)));

          tm_load_cycles <= (others => '0');
          tm_load_tai_o <= std_logic_vector (unsigned (tm_tai_i (31 downto 0)) + 1);
        elsif pps then
          tm_cycles <= (others => '0');
          tm_tai_o <= std_logic_vector (unsigned (tm_tai_i (31 downto 0)) + 1);
          tm_load_cycles <= tm_load_cycles + 1;
        else
          tm_cycles <= tm_cycles + 1;
          tm_load_cycles <= tm_load_cycles + 1;
        end if;

        if unsigned (tm_cycles_i) = to_unsigned(C_CYC_FREQ - 2, 26) then
          pre_pps_o <= '1';
        else
          pre_pps_o <= '0';
        end if;
      end if;
    end if;
  end process;
  
  tm_cycles_o <= std_logic_vector(tm_cycles);
  tm_load_steps_o <= std_logic_vector(tm_load_cycles (25 downto 5));

end arch;
