library ieee;
use ieee.std_logic_1164.all;

entity comparator is
  port (
    clk_i : in std_logic;
    rst_n_i : in std_logic;

    --  WR time input
    tm_tai_i             : in  std_logic_vector(39 downto 0);
    tm_cycles_i          : in  std_logic_vector(27 downto 0);

    --  Timestamp to be compared
    ts_sec_i : in  std_logic_vector(31 downto 0);
    ts_cyc_i : in  std_logic_vector(27 downto 0);

    en_i     : in std_logic;
    trig_o   : out std_logic
  );
end comparator;


architecture arch of comparator is
begin
  process (clk_i) is
  begin
    if rising_edge(clk_i) then
      trig_o <= '0';
      if rst_n_i = '1'
        and en_i = '1'
        and ts_sec_i = tm_tai_i(31 downto 0)
        and ts_cyc_i = tm_cycles_i
      then
        trig_o <= '1';
      end if;
    end if;
  end process;
end arch;