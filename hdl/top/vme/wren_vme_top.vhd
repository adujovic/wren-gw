--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- White Rabbit Trigger Distribution
-- https://ohwr-gitlab.cern.ch/projects/wrtd
--------------------------------------------------------------------------------
--
-- unit name:   wren_vme_top
--
-- description: Top entity for GMT over WR playground
--
--------------------------------------------------------------------------------
-- Copyright CERN 2014-2019
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.vme64x_pkg.all;
use work.axi4_pkg.all;
use work.wr_xilinx_pkg.all;
use work.pulser_pkg.all;
use work.mailbox_pkg.all;
use work.wr_fabric_pkg.all;
use work.wrcore_pkg.all;
use work.endpoint_pkg.all;
use work.streamers_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity wren_vme_top is
  generic (
    --g_WRPC_INITF    : string  :=  "../../../../../dependencies/files/wrc-pxie.bram";
    g_WRPC_INITF    : string  :=  "";
    g_map_version   : std_logic_vector(31 downto 0) := x"3510_f003";
    g_hwbld_date    : std_logic_vector(31 downto 0) := x"0000_0000";
    -- Simulation-mode enable parameter. Set by default (synthesis) to 0, and
    -- changed to non-zero in the instantiation of the top level DUT in the
    -- testbench. Its purpose is to reduce some internal counters/timeouts
    -- to speed up simulations.
    g_SIMULATION     : integer := 0);
  port (
    ---------------------------------------------------------------------------
    -- Clocks/resets
    ---------------------------------------------------------------------------
    ps_por_i               : in std_logic;

    fpga_pl_clksys_p_i     : in  std_logic;
    fpga_pl_clksys_n_i     : in  std_logic;

    wr_clk_helper_125m_p_i : in  std_logic;
    wr_clk_helper_125m_n_i : in  std_logic;
    wr_clk_main_125m_p_i   : in  std_logic;
    wr_clk_main_125m_n_i   : in  std_logic;
    wr_clk_sfp_125m_p_i    : in  std_logic;
    wr_clk_sfp_125m_n_i    : in  std_logic;

    --  Unused
    wr_clk_aux_p_i         : in  std_logic;
    wr_clk_aux_n_i         : in  std_logic;

    -- clk_helper_25m_i       : in  std_logic;  --  The oscillator before the pll

    --  VME
    vme_as_n_i      : in    std_logic;
    vme_as_n_o      : out   std_logic;
    vme_as_dir_o    : out   std_logic;
    vme_write_n_b   : inout std_logic;
    vme_write_dir_o : out   std_logic;
    vme_ds_n_i      : in    std_logic_vector(1 downto 0);
    vme_ds_n_o      : out   std_logic_vector(1 downto 0);
    vme_ds_oe_o     : out   std_logic;
    vme_data_b      : inout std_logic_vector(31 downto 0);
    vme_data_dir_o  : out   std_logic;
    vme_data_oe_n_o : out   std_logic;
    vme_addr_b      : inout std_logic_vector(31 downto 1);
    vme_lword_n_b   : inout std_logic;
    vme_am_b        : inout std_logic_vector(5 downto 0);
    vme_addr_dir_o  : out   std_logic;
    vme_addr_oe_n_o : out   std_logic;
    vme_dtack_n_i   : in    std_logic;
    vme_dtack_n_o   : out   std_logic;
    vme_dtack_oe_o  : out   std_logic;
    vme_retry_n_i   : in    std_logic;
    vme_retry_n_o   : out   std_logic;
    vme_retry_oe_o  : out   std_logic;
    vme_berr_n_i    : in    std_logic;
    vme_berr_n_o    : out   std_logic;
    vme_berr_oe_o   : out   std_logic;
    vme_iack_n_i    : in    std_logic;
    vme_iack_n_o    : in    std_logic;
    vme_iack_oe_n_o : out   std_logic;
    vme_iackin_n_i  : in    std_logic;
    vme_iackout_n_o : out   std_logic;
    vme_irq_n_o     : out   std_logic_vector(7 downto 1);
    vme_irq_n_i     : in    std_logic_vector(7 downto 1);
    vme_bg3in_n_i   : in    std_logic;
    vme_bg3out_n_o  : out   std_logic;
    vme_br3_n_o     : out   std_logic;
    vme_sysclk_b    : inout std_logic;
    vme_sysreset_n_b : inout std_logic;

    --  IO expanders
    ioexp_vme_sclk_o : out std_logic;
    ioexp_vme_rclk_o : out std_logic;
    ioexp_vme_d_o    : out std_logic;
    ioexp_hwbyte1_d_o : out std_logic;
    ioexp_hwbyte2_d_o : out std_logic;
    ioexp_hwbyte1_rclk_o : out std_logic;
    ioexp_hwbyte2_rclk_o : out std_logic;

    ioexp_fpio_rclk_o : out std_logic;
    ioexp_fpio_sclk_o : out std_logic;
    ioexp_fpio_d_o    : out std_logic;

    ioexp_gpio_rclk_o : out std_logic;
    ioexp_gpio_sclk_o : out std_logic;
    ioexp_gpio_d_o    : out std_logic;
    ioexp_gpio_q_i    : in  std_logic;

    ioexp_all_rst_n_o : out std_logic;
    ioexp_all_oe_n_o : out std_logic;

    fp_led0_o : out std_logic;
    fp_led1_o : out std_logic;
    pp_led0_o : out std_logic;
    pp_led1_o : out std_logic;
    sfp_led_o : out std_logic;

    ---------------------------------------------------------------------------
    -- SPI interface to DACs
    ---------------------------------------------------------------------------
    plldac_sclk_o   : out std_logic;
    plldac_din_o    : out std_logic;
    plldac_sync_n_o : out std_logic;

    wr_abscal_o : out std_logic;
    wr_pps_o    : out std_logic;

    --  SPI interface to PLL
    spi_helper_cs_n_o : out std_logic;
    spi_main_cs_n_o   : out std_logic;
    spi_mosi_o        : out std_logic;
    spi_miso_i        : in  std_logic;
    spi_sck_o         : out std_logic;

    ---------------------------------------------------------------------------
    -- SFP I/Os for transceiver
    ---------------------------------------------------------------------------
    sfp0_txp_o         : out std_logic;
    sfp0_txn_o         : out std_logic;
    sfp0_rxp_i         : in  std_logic;
    sfp0_rxn_i         : in  std_logic;
    sfp0_sda_b         : inout std_logic;
    sfp0_scl_b         : inout std_logic;

    userio_b   : inout std_logic_vector(31 downto 0);
    fp_io_b    : inout std_logic_vector(31 downto 0);

    p0_turnclk1_o : out std_logic;
    p0_turnclk2_o : out std_logic;
    p0_bunchclk1_o : out std_logic;
    p0_bunchclk2_o : out std_logic;
    p0_ttc_d0_o   : out std_logic;
    p0_ttc_d1_o   : out std_logic;
    p0_rs485_d0_b : inout std_logic;
    p0_rs485_d1_b : inout std_logic;
    
    ---------------------------------------------------------------------------
    -- EEPROM I2C interface for storing configuration and accessing unique ID
    ---------------------------------------------------------------------------
    eeprom_sda_b  : inout std_logic;
    eeprom_scl_b  : inout std_logic;
    ---------------------------------------------------------------------------
    -- UART
    ---------------------------------------------------------------------------
    uart_rxd_i    : in  std_logic;
    uart_txd_o    : out std_logic;

    --  sfp1    
    sfp1_txp_o  : out std_logic;
    sfp1_txn_o  : out std_logic;
    sfp1_rxp_i  : in  std_logic;
    sfp1_rxn_i  : in  std_logic;

    sfp1_sda_b  : inout std_logic;
    sfp1_scl_b  : inout std_logic
);
end entity wren_vme_top;

architecture arch of wren_vme_top is
  -- clock and reset
  signal clk_sys_62m5     : std_logic;
  signal rst_sys_62m5_n   : std_logic;
  signal clk_sys_125m     : std_logic;
  signal rst_sys_125m_n   : std_logic;
  signal clk_sys_500m     : std_logic;

  signal clk_250m : std_logic;
  signal rst_clk_250m_n : std_logic;

  signal clk_dmtd : std_logic;

  signal gth_clk_250m : std_logic;
  signal clk_gth_250m_bufgt : std_logic;

  alias clk : std_logic is clk_sys_62m5;
  alias rst_n : std_logic is rst_sys_62m5_n;

  attribute keep                   : string;

  --  WR fabric.
  signal eth_tx_out : t_wrf_source_out;
  signal eth_tx_in  : t_wrf_source_in;
  signal eth_rx_out : t_wrf_sink_out;
  signal eth_rx_in  : t_wrf_sink_in;

  --  mpsoc to pci map
  signal m_axi_out: t_axi4_lite_master_out_32;
  signal m_axi_in : t_axi4_lite_master_in_32;

  signal board_axi_out: t_axi4_lite_master_out_32;
  signal board_axi_in : t_axi4_lite_master_in_32;
  
  signal vme_wb_out : t_wishbone_master_out;
  signal vme_wb_in  : t_wishbone_master_in;
  signal vme_dati_swp, vme_dato_swp : std_logic_vector(31 downto 0);

  signal vme_irq_level  : std_logic_vector(2 downto 0);
  signal vme_irq_vector : std_logic_vector(7 downto 0);
  
  signal host_wb_out : t_wishbone_master_out;
  signal host_wb_in  : t_wishbone_master_in;

  signal wr_master_out : t_wishbone_master_out;
  signal wr_master_in  : t_wishbone_master_in;

  -- WRPC TM interface and aux clocks
  signal tm_link_up         : std_logic;
  signal tm_tai             : std_logic_vector(39 downto 0);
  signal tm_cycles          : std_logic_vector(27 downto 0);
  signal tm_time_valid      : std_logic;

  -- MT TM interface
--  signal tm : t_mt_timing_if;

  signal M_AXI_araddr  : STD_LOGIC_VECTOR (39 downto 32);
  signal M_AXI_awaddr  : STD_LOGIC_VECTOR (39 downto 32);

  signal s_axi_addr : STD_LOGIC_VECTOR ( 48 downto 0 );
  signal s_axi_awvalid : STD_LOGIC;
  signal s_axi_awready :  STD_LOGIC;
  signal s_axi_wdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_wvalid : STD_LOGIC;
  signal s_axi_wready :  STD_LOGIC;
  signal s_axi_bresp :  STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_bvalid :  STD_LOGIC;
  signal s_axi_bready : STD_LOGIC;
  signal s_axi_arvalid : std_logic;
  signal s_axi_arready : std_logic;
  signal s_axi_rready : std_logic;
  signal s_axi_rvalid : std_logic;
  signal s_axi_rrsp : std_logic_vector(1 downto 0);
  signal s_axi_rdata : STD_LOGIC_VECTOR ( 31 downto 0 );

  signal S_AXI_LOG_awid : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_LOG_awaddr : STD_LOGIC_VECTOR ( 48 downto 0 );
  signal S_AXI_LOG_awlen : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal S_AXI_LOG_awvalid : STD_LOGIC;
  signal S_AXI_LOG_awready : STD_LOGIC;
  signal S_AXI_LOG_wdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal S_AXI_LOG_wstrb : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal S_AXI_LOG_wlast : STD_LOGIC;
  signal S_AXI_LOG_wvalid : STD_LOGIC;
  signal S_AXI_LOG_wready : STD_LOGIC;
  signal S_AXI_LOG_bid : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_LOG_bresp : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_LOG_bvalid : STD_LOGIC;
  signal S_AXI_LOG_bready : STD_LOGIC;

  signal pps_p, pps_valid : std_logic;

  signal irq : std_logic;

  signal clk_pl, rst_pl_n, rst_pl : std_logic;

  signal clk_mon : std_logic;

  signal lemo_out, lemo_oe, lemo_term : std_logic_vector(31 downto 0);
  signal bst_out : std_logic;

  signal sfp0_led_link, sfp0_led_act, pps_led : std_logic;

  --  io-expander VME
  signal ioexp_vme_oe_n, ioexp_vme_rclk : std_logic;
  signal ioexp_vme_load, ioexp_vme_shift : std_logic;
  signal rs485_data1_re_n, rs485_data1_de : std_logic;
  signal rs485_data0_re_n, rs485_data0_de : std_logic;
  signal p0_oe_n : std_logic;
  signal vme_sysclk_dir : std_logic;
  signal vme_sysreset_dir : std_logic;
  signal p0_hwbyte_h2, p0_hwbyte_l2 : std_logic_vector(7 downto 0);
  signal p0_hwbyte_h1, p0_hwbyte_l1 : std_logic_vector(7 downto 0);
  signal userio_dir, userio_oe_n : std_logic_vector(3 downto 0);
  signal p0_hwbyte_h2_oe_n, p0_hwbyte_l2_oe_n : std_logic;
  signal p0_hwbyte_h1_oe_n, p0_hwbyte_l1_oe_n : std_logic;

  -- io-expander FP
  signal ioexp_fpio_oe_n : std_logic;
  signal ioexp_fpio_load, ioexp_fpio_shift : std_logic;
  signal fp_term_en : std_logic_vector(31 downto 0);
  signal fp_en_n      : std_logic_vector(31 downto 0);

  --  io-expander gpio
  signal ioexp_gpio_oe_n : std_logic;
  signal sfp0_los, sfp1_los : std_logic;  -- rssi
  signal switch0_n, switch1_n : std_logic;
  signal vme_noga_n, vme_noga_usr : std_logic_vector(4 downto 0);
  signal vme_usega_n : std_logic;
  signal vme_ga_int : std_logic_vector(4 downto 0);
  signal vme_gap_int : std_logic;
  signal vme_ga : std_logic_vector(5 downto 0);
  signal fp_id : std_logic_vector(2 downto 0);
  signal pp_id : std_logic_vector(2 downto 0);
  signal sfp0_tx_disable, sfp1_tx_disable : std_logic;
  signal sfp0_det, sfp1_det : std_logic;
  signal pp_oe_n : std_logic;
  signal ioexp_gpio_unused22, ioexp_gpio_unused23 : std_logic;

  signal vme_rst_n, gpio_inp_valid : std_logic;

  --  From 'view instantiation template'
  component mpsoc is
    port (
      M_AXI_awaddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
      M_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_awvalid : out STD_LOGIC;
      M_AXI_awready : in STD_LOGIC;
      M_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_wvalid : out STD_LOGIC;
      M_AXI_wready : in STD_LOGIC;
      M_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_bvalid : in STD_LOGIC;
      M_AXI_bready : out STD_LOGIC;
      M_AXI_araddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
      M_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_arvalid : out STD_LOGIC;
      M_AXI_arready : in STD_LOGIC;
      M_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_rvalid : in STD_LOGIC;
      M_AXI_rready : out STD_LOGIC;
      GPIO_tri_i : in STD_LOGIC_VECTOR ( 94 downto 0 );
      GPIO_tri_o : out STD_LOGIC_VECTOR ( 94 downto 0 );
      GPIO_tri_t : out STD_LOGIC_VECTOR ( 94 downto 0 );
      S_AXI_aruser : in STD_LOGIC;
      S_AXI_awuser : in STD_LOGIC;
      S_AXI_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_awaddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_awlock : in STD_LOGIC;
      S_AXI_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_awvalid : in STD_LOGIC;
      S_AXI_awready : out STD_LOGIC;
      S_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_wlast : in STD_LOGIC;
      S_AXI_wvalid : in STD_LOGIC;
      S_AXI_wready : out STD_LOGIC;
      S_AXI_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_bvalid : out STD_LOGIC;
      S_AXI_bready : in STD_LOGIC;
      S_AXI_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_araddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_arlock : in STD_LOGIC;
      S_AXI_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_arvalid : in STD_LOGIC;
      S_AXI_arready : out STD_LOGIC;
      S_AXI_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_rlast : out STD_LOGIC;
      S_AXI_rvalid : out STD_LOGIC;
      S_AXI_rready : in STD_LOGIC;
      S_AXI_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      clk_i : in STD_LOGIC;
      aresetn_i : in STD_LOGIC;
      pl_resetn_o : out STD_LOGIC;
      pl_ps_irq0_i : in STD_LOGIC_VECTOR ( 0 to 0 );
      pl_clk_o : out STD_LOGIC;
      saxi_clk : in STD_LOGIC;
      spi0_sclk_i : in STD_LOGIC;
      spi0_sclk_o : out STD_LOGIC;
      spi0_sclk_t : out STD_LOGIC;
      spi0_m_i : in STD_LOGIC;
      spi0_m_o : out STD_LOGIC;
      spi0_mo_t : out STD_LOGIC;
      spi0_s_i : in STD_LOGIC;
      spi0_s_o : out STD_LOGIC;
      spi0_so_t : out STD_LOGIC;
      spi0_ss_i_n : in STD_LOGIC;
      spi0_ss_o_n : out STD_LOGIC;
      spi0_ss_n_t : out STD_LOGIC;
      spi0_ss1_o_n : out STD_LOGIC;
      S_AXI_LOG_aruser : in STD_LOGIC;
      S_AXI_LOG_awuser : in STD_LOGIC;
      S_AXI_LOG_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_awaddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_LOG_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_LOG_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_awlock : in STD_LOGIC;
      S_AXI_LOG_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_LOG_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_awvalid : in STD_LOGIC;
      S_AXI_LOG_awready : out STD_LOGIC;
      S_AXI_LOG_wdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
      S_AXI_LOG_wstrb : in STD_LOGIC_VECTOR ( 15 downto 0 );
      S_AXI_LOG_wlast : in STD_LOGIC;
      S_AXI_LOG_wvalid : in STD_LOGIC;
      S_AXI_LOG_wready : out STD_LOGIC;
      S_AXI_LOG_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_bvalid : out STD_LOGIC;
      S_AXI_LOG_bready : in STD_LOGIC;
      S_AXI_LOG_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_araddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_LOG_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_LOG_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_arlock : in STD_LOGIC;
      S_AXI_LOG_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_LOG_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_arvalid : in STD_LOGIC;
      S_AXI_LOG_arready : out STD_LOGIC;
      S_AXI_LOG_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_rdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
      S_AXI_LOG_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_rlast : out STD_LOGIC;
      S_AXI_LOG_rvalid : out STD_LOGIC;
      S_AXI_LOG_rready : in STD_LOGIC;
      S_AXI_LOG_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_LOG_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      saxi_log_clk : in STD_LOGIC
    );
    end component mpsoc;
    component ila_0 is
      port (
        clk    : in STD_LOGIC;
        probe0 : in STD_LOGIC_VECTOR(63 downto 0)
      );
    end component;
begin
  inst_mpsoc: mpsoc
    port map (
      --  Master bus (from PS) to give CPU access to the wren memmap.
      M_AXI_araddr(31 downto 0)  => m_axi_out.araddr,
      M_AXI_araddr(39 downto 32) => M_AXI_araddr(39 downto 32),
      M_AXI_arprot  => open,
      M_AXI_arready => m_axi_in.arready,
      M_AXI_arvalid => m_axi_out.arvalid,
      M_AXI_awaddr(31 downto 0) => m_axi_out.awaddr,
      M_AXI_awaddr(39 downto 32) => M_AXI_awaddr(39 downto 32),
      M_AXI_awprot  => open,
      M_AXI_awready => m_axi_in.awready,
      M_AXI_awvalid => m_axi_out.awvalid,
      M_AXI_bready  => m_axi_out.bready,
      M_AXI_bresp   => m_axi_in.bresp,
      M_AXI_bvalid  => m_axi_in.bvalid,
      M_AXI_rdata   => m_axi_in.rdata,
      M_AXI_rready  => m_axi_out.rready,
      M_AXI_rresp   => m_axi_in.rresp,
      M_AXI_rvalid  => m_axi_in.rvalid,
      M_AXI_wdata   => m_axi_out.wdata,
      M_AXI_wready  => m_axi_in.wready,
      M_AXI_wstrb   => m_axi_out.wstrb,
      M_AXI_wvalid  => m_axi_out.wvalid,

      --  Windowed access to the PS from the host.
      S_AXI_awuser => '0',
      S_AXI_awid => (others => '0'),
      S_AXI_awaddr => s_axi_addr,
      S_AXI_awlen => x"00", --  No burst, 1 beat
      S_AXI_awsize => "010", -- 4B
      S_AXI_awburst => "01", -- INCR
      S_AXI_awlock => '0',   -- Normal
      S_AXI_awcache => "0001",
      S_AXI_awprot => "001",
      S_AXI_awvalid => s_axi_awvalid,
      S_AXI_awready => s_axi_awready,
      S_AXI_awqos => (others => '0'),
      S_AXI_wdata => s_axi_wdata,
      S_AXI_wstrb => "1111",
      S_AXI_wlast => '1',
      S_AXI_wvalid => s_axi_wvalid,
      S_AXI_wready => s_axi_wready,
      S_AXI_bid => open,
      S_AXI_bresp => s_axi_bresp,
      S_AXI_bvalid => s_axi_bvalid,
      S_AXI_bready => s_axi_bready,
      S_AXI_aruser => '0',
      S_AXI_arid => (others => '0'),
      S_AXI_araddr => s_axi_addr,
      S_AXI_arlen => x"00",  -- No burst, 1 beat
      S_AXI_arsize => "010", -- 4B
      S_AXI_arburst => "01", -- INCR
      S_AXI_arlock => '0',  --  Normal
      S_AXI_arcache => "0001",
      S_AXI_arprot => "001",
      S_AXI_arvalid => s_axi_arvalid,
      S_AXI_arready => s_axi_arready,
      S_AXI_rid => open,
      S_AXI_rdata => s_axi_rdata,
      S_AXI_rresp => s_axi_rrsp,
      S_AXI_rlast => open,
      S_AXI_rvalid => s_axi_rvalid,
      S_AXI_rready => s_axi_rready,
      S_AXI_arqos => (others => '0'),

      saxi_clk => clk,

      saxi_log_clk => clk_250m,
  
      --  Direct access to DDR for logging.
      S_AXI_LOG_awid => S_AXI_LOG_awid,
      S_AXI_LOG_awaddr => S_AXI_LOG_awaddr,
      S_AXI_LOG_awlen => S_AXI_LOG_awlen,
      S_AXI_LOG_awsize => b"100",  -- 100: 16 bytes, 011: 8 bytes
      S_AXI_LOG_awburst => b"01",  -- 00: FIXED, (01: INCR)
      S_AXI_LOG_awvalid => S_AXI_LOG_awvalid,
      S_AXI_LOG_awready => S_AXI_LOG_awready,
      S_AXI_LOG_wdata => S_AXI_LOG_wdata,
      S_AXI_LOG_wstrb => S_AXI_LOG_wstrb,
      S_AXI_LOG_wlast => S_AXI_LOG_wlast,
      S_AXI_LOG_wvalid => S_AXI_LOG_wvalid,
      S_AXI_LOG_wready => S_AXI_LOG_wready,
      S_AXI_LOG_bid => S_AXI_LOG_bid,
      S_AXI_LOG_bresp => S_AXI_LOG_bresp,
      S_AXI_LOG_bvalid => S_AXI_LOG_bvalid,
      S_AXI_LOG_bready => S_AXI_LOG_bready,
      s_AXI_LOG_awuser => '0',
      s_AXI_LOG_awqos => (others => '0'),
      s_AXI_LOG_awcache => "0001",  -- Set AWCACHE[1] to upsizing ?
      s_AXI_LOG_awlock => '0',
      s_AXI_LOG_awprot => "001",
      s_AXI_LOG_araddr => (others => 'X'),
      s_AXI_LOG_aruser => 'X',
      s_AXI_LOG_arid => (others => 'X'),
      s_AXI_LOG_arlen => (others => 'X'),
      s_AXI_LOG_arsize => (others => 'X'),
      s_AXI_LOG_arburst => (others => 'X'),
      s_AXI_LOG_arcache => (others => 'X'),
      s_AXI_LOG_arlock => 'X',
      s_AXI_LOG_arprot => (others => 'X'),
      s_AXI_LOG_arvalid => '0',
      s_AXI_LOG_arready => open,
      s_AXI_LOG_arqos => (others => 'X'),
      s_AXI_LOG_rready => '0',
      s_AXI_LOG_rid => open,
      s_AXI_LOG_rvalid => open,
      s_AXI_LOG_rlast => open,
      s_AXI_LOG_rresp => open,
      s_AXI_LOG_rdata => open,

      pl_ps_irq0_i  => "0",
      GPIO_tri_i => (others => '0'),
      GPIO_tri_o => open,
      GPIO_tri_t => open,

      spi0_m_i => spi_miso_i,
      spi0_m_o => spi_mosi_o,
      spi0_mo_t => open,
      spi0_s_i => '1', -- spi_miso_i,
      spi0_s_o => open,
      spi0_so_t => open,
      spi0_sclk_i => '0',
      spi0_sclk_o => spi_sck_o,
      spi0_sclk_t => open,
      spi0_ss_i_n => '1',
      spi0_ss_n_t => open,
      spi0_ss_o_n => spi_main_cs_n_o,
      spi0_ss1_o_n => spi_helper_cs_n_o,

      aresetn_i     => rst_n,
      clk_i         => clk,
      pl_clk_o      => clk_pl,
      pl_resetn_o   => rst_pl_n);

    rst_pl <= not rst_pl_n;

  b_vme: block
    --  DIR: 1: out, 0: in
    signal vme_write_n_in  : std_logic;
    signal vme_data_in     : std_logic_vector(31 downto 0);
    signal vme_data_out    : std_logic_vector(31 downto 0);
    signal vme_data_dir    : std_logic;
    signal vme_data_oe_n   : std_logic;
    signal vme_addr_in     : std_logic_vector(31 downto 1);
    signal vme_addr_out    : std_logic_vector(31 downto 1);
    signal vme_lword_n_in  : std_logic;
    signal vme_lword_n_out : std_logic;
    signal vme_am_in       : std_logic_vector(5 downto 0);
    signal vme_addr_dir    : std_logic;
    signal vme_addr_oe_n   : std_logic;
    signal vme_retry_n_out : std_logic;
    signal vme_retry_oe    : std_logic;
    signal vme_berr_n_out  : std_logic;
    signal vme_iack_n_in   : std_logic;

    signal vme_irq_n_out : std_logic_vector(7 downto 1);
    signal vme_nogap : std_logic;
--    signal vme_ga : std_logic_vector(5 downto 0);

    signal base_addr_adr : std_logic_vector(4 downto 2);
    signal base_addr_idx : natural range 0 to 7;
    signal base_addr_rack, base_addr_wack : std_logic;
    signal base_addr_dati, base_addr_dato : std_logic_vector(31 downto 0);
    signal base_addr_rd, base_addr_wr : std_logic;

    type t_window_bar is array(7 downto 0) of std_logic_vector(31 downto 0);
    signal windows_bar: t_window_bar;

    signal windows_wr, windows_rd : std_logic;
    signal windows_rack, windows_wack : std_logic;
    signal windows_dati, windows_dato : std_logic_vector(31 downto 0);
    signal windows_adr : std_logic_vector(14 downto 2);
    signal windows_idx : natural range 0 to 7;

    type t_windows_state is (S_IDLE, S_READ, S_WRITE);
    signal windows_state : t_windows_state;
  begin
    vme_as_dir_o <= '0'; --  Always an input
    vme_as_n_o <= '0';   --  Not used

    vme_write_dir_o <= '0'; --  Always an input
    vme_write_n_in <= vme_write_n_b;

    vme_ds_oe_o <= '0'; --  Always an input
    vme_ds_n_o <= "00"; --  Not used

    vme_addr_oe_n_o <= vme_addr_oe_n;
    vme_addr_dir_o <= vme_addr_dir;
    vme_addr_in <= vme_addr_b;
    vme_addr_b <= vme_addr_out when vme_addr_oe_n = '0' and vme_addr_dir = '1' else (others => 'Z');
    vme_am_in <= vme_am_b;
    vme_lword_n_in <= vme_lword_n_b;
    vme_lword_n_b <= vme_lword_n_out when vme_addr_oe_n = '0' and vme_addr_dir = '1' else 'Z';

    --  dtack_n_i is not used.

    vme_retry_oe_o <= vme_retry_oe;
    vme_retry_n_o <= vme_retry_n_out;

    vme_berr_n_o <= vme_berr_n_out;
    vme_berr_oe_o <= not vme_berr_n_out;

    --  TODO: fix schema.
    vme_iack_n_in <= vme_iack_n_o;
    vme_iack_oe_n_o <= '1';
    -- vme_iack_n_i <= 'Z';

    vme_data_in <= vme_data_b;
    vme_data_b <= vme_data_out when vme_data_oe_n = '0' and vme_data_dir = '1' else (others => 'Z');
    vme_data_oe_n_o <= vme_data_oe_n;
    vme_data_dir_o <= vme_data_dir;

    vme_bg3out_n_o <= vme_bg3in_n_i;
    vme_br3_n_o <= '1';

    vme_noga_usr <= vme_noga_n;
    vme_nogap <= not (vme_noga_usr(0) xor vme_noga_usr(1) xor vme_noga_usr(2)
      xor vme_noga_usr(3) xor vme_noga_usr(4));
    --  Use vme_ga_n_i if not unconnected (there are pull-up).
    --  Despite the name, the vme64x core want negated logic.
    vme_ga <= vme_gap_int & vme_ga_int when vme_ga_int /= (4 downto 0 => '1')
      else vme_nogap & vme_noga_usr;

    inst_vme: entity work.xvme64x_core
      generic map (
        g_clock_period => 16,
        g_decode_am => False,
        g_enable_cr_csr => True,
        g_user_csr_ext => True,
        g_vme32 => True,
        g_vme_2e => False,
        g_async_dtack => False,
        g_wb_granularity => BYTE,
        g_wb_mode => CLASSIC,
        g_manufacturer_id => c_CERN_ID,
        g_board_id => x"0000_01dc",
        g_revision_id => x"0000_0000",
        g_program_id => x"00",
        g_ascii_ptr => open,
        g_beg_user_cr => open,
        g_end_user_cr => open,
        g_beg_cram => open,
        g_end_cram => open,
        g_beg_user_csr => open,
        g_end_user_csr => open,
        g_beg_sn => open,
        g_end_sn => open,
        g_decoder => c_vme64x_decoders_default
        )
      port map (
        clk_i => clk,
        rst_n_i => vme_rst_n,
        rst_n_o => open, -- rst_n_o,
        vme_i.as_n => vme_as_n_i,
        vme_i.rst_n    => '1',
        vme_i.write_n  => vme_write_n_in,
        vme_i.am       => vme_am_in,
        vme_i.ds_n     => vme_ds_n_i,
        vme_i.ga       => vme_ga,
        vme_i.lword_n  => vme_lword_n_in,
        vme_i.data     => vme_data_in,
        vme_i.addr     => vme_addr_in,
        vme_i.iack_n   => vme_iack_n_in,
        vme_i.iackin_n => vme_iackin_n_i,
        vme_o.iackout_n => vme_iackout_n_o,
        vme_o.dtack_n   => vme_dtack_n_o,
        vme_o.dtack_oe  => vme_dtack_oe_o,
        vme_o.lword_n   => vme_lword_n_out,
        vme_o.data      => vme_data_out,
        vme_o.data_dir  => vme_data_dir,
        vme_o.data_oe_n => vme_data_oe_n,
        vme_o.addr      => vme_addr_out,
        vme_o.addr_dir  => vme_addr_dir,
        vme_o.addr_oe_n => vme_addr_oe_n,
        vme_o.retry_n   => vme_retry_n_out,
        vme_o.retry_oe  => vme_retry_oe,
        vme_o.berr_n    => vme_berr_n_out,
        vme_o.irq_n     => vme_irq_n_out,

        wb_i => vme_wb_in,
        wb_o => vme_wb_out,
        int_i => irq,
        irq_ack_o => open,
        irq_level_i  => vme_irq_level,
        irq_vector_i => vme_irq_vector,
        user_csr_addr_o => open,
        user_csr_data_i => open,
        user_csr_data_o => open,
        user_csr_we_o => open,
        user_cr_addr_o => open,
        user_cr_data_i => open
        );
  
    vme_irq_n_o <= vme_irq_n_out;

    --  Stay little endian (even on VME).
    vme_dato_swp <= vme_wb_out.dat(7 downto 0) & vme_wb_out.dat(15 downto 8) & vme_wb_out.dat(23 downto 16) & vme_wb_out.dat(31 downto 24);
    vme_wb_in.dat <= vme_dati_swp(7 downto 0) & vme_dati_swp(15 downto 8) & vme_dati_swp(23 downto 16) & vme_dati_swp(31 downto 24);

    inst_vme_map: entity work.vme_map
      port map (
        rst_n_i => rst_n,
        clk_i => clk,

        wb_i.cyc => vme_wb_out.cyc,
        wb_i.stb => vme_wb_out.stb,
        wb_i.adr => vme_wb_out.adr,
        wb_i.sel => vme_wb_out.sel,
        wb_i.we => vme_wb_out.we,
        wb_i.dat => vme_dato_swp,
        wb_o.ack => vme_wb_in.ack,
        wb_o.err => vme_wb_in.err,
        wb_o.rty => vme_wb_in.rty,
        wb_o.stall => vme_wb_in.stall,
        wb_o.dat => vme_dati_swp,

        host_i => host_wb_in,
        host_o => host_wb_out,
        vme_irq_level_o => vme_irq_level,
        vme_irq_vector_o => vme_irq_vector,
        vme_ga_ga_i (4 downto 0) => vme_ga_int,
        vme_ga_ga_i (5) => vme_gap_int,
        vme_ga_noga_i (4 downto 0) => vme_noga_n,
        vme_ga_noga_i (5) => '0',
        vme_ga_vme_ga_i => vme_ga,
        vme_ga_cst_i => x"5a",

        vme_base_addr_adr_o => base_addr_adr,
        vme_base_addr_rd_o  => base_addr_rd,
        vme_base_addr_wr_o  => base_addr_wr,
        vme_base_addr_rack_i => base_addr_rack,
        vme_base_addr_wack_i => base_addr_wack,
        vme_base_addr_dati_o => base_addr_dati,
        vme_base_addr_dato_i => base_addr_dato,

        vme_windows_adr_o => windows_adr,
        vme_windows_wr_o => windows_wr,
        vme_windows_rd_o => windows_rd,
        vme_windows_dato_i => windows_dato,
        vme_windows_dati_o => windows_dati,
        vme_windows_rack_i => windows_rack,
        vme_windows_wack_i => windows_wack
      );

    board_axi_out <= m_axi_out;
    m_axi_in <= board_axi_in;

    --  Windows bar
    base_addr_idx <= to_integer(unsigned(base_addr_adr));
    p_wbar: process (clk)
    begin
      if rising_edge (clk) then
        base_addr_rack <= '0';
        base_addr_wack <= '0';

        if base_addr_rd = '1' then
          base_addr_dato <= windows_bar(base_addr_idx);
          base_addr_rack <= '1';
        end if;
        if base_addr_wr = '1' then
          windows_bar(base_addr_idx) <= base_addr_dati;
          base_addr_wack <= '1';
        end if;
      end if;
    end process;

    --  Windows access
    windows_idx <= to_integer(unsigned(windows_adr (14 downto 12)));
    p_windows: process (clk)
    begin
      if rising_edge(clk) then
        windows_rack <= '0';
        windows_wack <= '0';

        if rst_n = '0' then
          windows_state <= S_IDLE;
          s_axi_awvalid <= '0';
          s_axi_wvalid <= '0';
          s_axi_bready <= '0';
          s_axi_arvalid <= '0';
          s_axi_rready <= '0';
        else
          case windows_state is
            when S_IDLE =>
              s_axi_addr (48 downto 32) <= (others => '0');
              s_axi_addr (31 downto 0) <= windows_bar(windows_idx);
              s_axi_addr (11 downto 2) <= windows_adr(11 downto 2);
              s_axi_addr (1 downto 0) <= "00";

              if windows_rd = '1' then
                s_axi_arvalid <= '1';
                s_axi_rready <= '1';
                windows_state <= S_READ;
              end if;
              if windows_wr = '1' then
                s_axi_awvalid <= '1';
                s_axi_wvalid <= '1';
                s_axi_wdata <= windows_dati;
                s_axi_bready <= '1';
                windows_state <= S_WRITE;
              end if;
            when S_READ =>
              if s_axi_arready = '1' then
                s_axi_arvalid <= '0';
              end if;
              if s_axi_rvalid = '1' then
                s_axi_rready <= '0';
--                if s_axi_rrsp = "00" then
                  -- Okday
                  windows_dato <= s_axi_rdata;
--                else
--                  windows_dato <= (others => '1');
--                end if;
              end if;
              if (s_axi_arvalid = '0' or s_axi_arready = '1')
                and (s_axi_rvalid = '1' or s_axi_rready = '0')
              then
                  windows_rack <= '1';
                  windows_state <= S_IDLE;
              end if;
            when S_WRITE =>
              if s_axi_awready = '1' then
                s_axi_awvalid <= '0';
              end if;
              if s_axi_wready = '1' then
                s_axi_wvalid <= '0';
              end if;
              if s_axi_bvalid = '1' then
                s_axi_bready <= '0';
              end if;
              if (s_axi_bvalid = '1' or s_axi_bready = '0')
                and (s_axi_wready = '1' or s_axi_wvalid = '0')
                and (s_axi_awready = '1' or s_axi_awvalid = '0')
              then
                windows_wack <= '1';
                windows_state <= S_IDLE;
              end if;
          end case;
        end if;
      end if;
    end process;
  end block;
  
  --  p19 IC 48, 1, 66
  inst_ioexp_sh_vme: entity work.output_shifter
    generic map (
      g_WIDTH => 24
      )
    port map (
      clk_i => clk,
      load_i => ioexp_vme_load,
      shift_i => ioexp_vme_shift,
      data_i (0) => rs485_data1_re_n,
      data_i (1) => rs485_data1_de,
      data_i (2) => rs485_data0_re_n,
      data_i (3) => rs485_data0_de,
      data_i (4) => p0_oe_n,
      data_i (5) => '0',
      data_i (6) => vme_sysclk_dir,
      data_i (7) => vme_sysreset_dir,
      data_i (11 downto 8) => userio_dir,
      data_i (15 downto 12) => "0000",
      data_i (19 downto 16) => userio_oe_n,
      data_i (20) => p0_hwbyte_l1_oe_n,
      data_i (21) => p0_hwbyte_h1_oe_n,
      data_i (22) => p0_hwbyte_l2_oe_n,
      data_i (23) => p0_hwbyte_h2_oe_n,
      msb_o => ioexp_vme_d_o
      );

  -- p19 IC 63, 65
  inst_ioexp_sh_hwbyte1: entity work.output_shifter
    generic map (
      g_WIDTH => 24
      )
    port map (
      clk_i => clk,
      load_i => ioexp_vme_load,
      shift_i => ioexp_vme_shift,
      data_i(7 downto 0) => p0_hwbyte_h1,
      data_i(15 downto 8) => p0_hwbyte_l1,
      data_i(23 downto 16) => x"00",
      msb_o => ioexp_hwbyte1_d_o
      );

  -- p19 IC 49, 64
  inst_ioexp_sh_hwbyte2: entity work.output_shifter
    generic map (
      g_WIDTH => 24
      )
    port map (
      clk_i => clk,
      load_i => ioexp_vme_load,
      shift_i => ioexp_vme_shift,
      data_i(7 downto 0) => p0_hwbyte_h2,
      data_i(15 downto 8) => p0_hwbyte_l2,
      data_i(23 downto 16) => x"00",
      msb_o => ioexp_hwbyte2_d_o
      );

  inst_ioexp_vme: entity work.output_expander
    generic map (
      g_nbr_chips => 7,
      g_clkdiv => 7
      )
    port map (
      clk_i => clk,
      rst_n_i => rst_n,
      load_o => ioexp_vme_load,
      shift_o => ioexp_vme_shift,
      exp_oe_n_o => ioexp_vme_oe_n,
      exp_rclk_o => ioexp_vme_rclk,
      exp_sclk_o => ioexp_vme_sclk_o
      );

  ioexp_hwbyte1_rclk_o <= ioexp_vme_rclk;
  ioexp_hwbyte2_rclk_o <= ioexp_vme_rclk;
  ioexp_vme_rclk_o <= ioexp_vme_rclk;

  ioexp_all_rst_n_o <= '1';
  ioexp_all_oe_n_o <= ioexp_vme_oe_n or ioexp_fpio_oe_n or ioexp_gpio_oe_n;


  --  TODO
  rs485_data1_re_n <= '1';
  rs485_data1_de <= '0';
  rs485_data0_re_n <= '1';
  rs485_data0_de <= '0';
  p0_oe_n <= '1';
  vme_sysclk_dir <= '0';
  vme_sysreset_dir <= '0';
  p0_hwbyte_h2 <= (others => '0');
  p0_hwbyte_l2 <= (others => '0');
  p0_hwbyte_h1 <= (others => '0');
  p0_hwbyte_l1 <= (others => '0');
  userio_dir <= (others => '0');
  userio_oe_n <= (others => '1');
  p0_hwbyte_h2_oe_n <= '1';
  p0_hwbyte_l2_oe_n <= '1';
  p0_hwbyte_h1_oe_n <= '1';
  p0_hwbyte_l1_oe_n <= '1';

  -- p28
  -- p29 IC 6, 7
  inst_ioexp_fp_sh: entity work.output_shifter
    generic map (
      g_WIDTH => 64
      )
    port map (
      clk_i => clk,
      load_i => ioexp_fpio_load,
      shift_i => ioexp_fpio_shift,
      data_i (1 downto 0)   => fp_term_en(1 downto 0),
      data_i (3 downto 2)   => fp_en_n(1 downto 0),
      data_i (5 downto 4)   => fp_term_en(3 downto 2),
      data_i (7 downto 6)   => fp_en_n(3 downto 2),
      data_i (9 downto 8)   => fp_term_en(5 downto 4),
      data_i (11 downto 10) => fp_en_n(5 downto 4),
      data_i (13 downto 12) => fp_term_en(7 downto 6),
      data_i (15 downto 14) => fp_en_n(7 downto 6),
      data_i (17 downto 16) => fp_term_en(9 downto 8),
      data_i (19 downto 18) => fp_en_n(9 downto 8),
      data_i (21 downto 20) => fp_term_en(11 downto 10),
      data_i (23 downto 22) => fp_en_n(11 downto 10),
      data_i (25 downto 24) => fp_term_en(13 downto 12),
      data_i (27 downto 26) => fp_en_n(13 downto 12),
      data_i (29 downto 28) => fp_term_en(15 downto 14),
      data_i (31 downto 30) => fp_en_n(15 downto 14),
      data_i (33 downto 32) => fp_term_en(17 downto 16),
      data_i (35 downto 34) => fp_en_n(17 downto 16),
      data_i (37 downto 36) => fp_term_en(19 downto 18),
      data_i (39 downto 38) => fp_en_n(19 downto 18),
      data_i (41 downto 40) => fp_term_en(21 downto 20),
      data_i (43 downto 42) => fp_en_n(21 downto 20),
      data_i (45 downto 44) => fp_term_en(23 downto 22),
      data_i (47 downto 46) => fp_en_n(23 downto 22),
      data_i (49 downto 48) => fp_term_en(25 downto 24),
      data_i (51 downto 50) => fp_en_n(25 downto 24),
      data_i (53 downto 52) => fp_term_en(27 downto 26),
      data_i (55 downto 54) => fp_en_n(27 downto 26),
      data_i (57 downto 56) => fp_term_en(29 downto 28),
      data_i (59 downto 58) => fp_en_n(29 downto 28),
      data_i (61 downto 60) => fp_term_en(31 downto 30),
      data_i (63 downto 62) => fp_en_n(31 downto 30),
      msb_o => ioexp_fpio_d_o
      );

  fp_en_n <= not lemo_oe;
  fp_term_en <= lemo_term;

  inst_ioexp_fp: entity work.output_expander
    generic map (
      g_nbr_chips => 8,
      g_clkdiv => 7
      )
    port map (
      clk_i => clk,
      rst_n_i => rst_n,
      shift_o => ioexp_fpio_shift,
      load_o => ioexp_fpio_load,
      exp_oe_n_o => ioexp_fpio_oe_n,
      exp_rclk_o => ioexp_fpio_rclk_o,
      exp_sclk_o => ioexp_fpio_sclk_o
      );

  --  FIXME: modabs are connected as output!
  sfp0_det <= '0';
  sfp1_det <= '0';

  -- p1, p51 slow_gpio
  inst_ioexp_gpio: entity work.io_expander
    generic map (
      g_nbr_chips_out => 1,
      g_nbr_chips_in => 3,
      g_clkdiv => 7
      )
    port map (
      clk_i => clk,
      rst_n_i => rst_n,
      outputs_i (0) => sfp0_tx_disable,
      outputs_i (1) => sfp0_det,
      outputs_i (2) => sfp1_tx_disable,
      outputs_i (3) => sfp1_det,
      outputs_i (4) => pp_oe_n,
      outputs_i (5) => '0',
      outputs_i (6) => '0',
      outputs_i (7) => '0',
      inputs_o (7) => sfp0_los,
      inputs_o (6) => sfp1_los,
      inputs_o (5) => switch0_n,
      inputs_o (4) => switch1_n,
      inputs_o (3) => vme_noga_n(0),
      inputs_o (2) => vme_noga_n(1),
      inputs_o (1) => vme_noga_n(2),
      inputs_o (0) => vme_noga_n(3),
      inputs_o (16) => vme_noga_n(4),
      inputs_o (17) => vme_usega_n,
      inputs_o (18) => vme_ga_int(0),
      inputs_o (19) => vme_ga_int(1),
      inputs_o (20) => vme_ga_int(2),
      inputs_o (21) => vme_ga_int(3),
      inputs_o (22) => vme_ga_int(4),
      inputs_o (23) => vme_gap_int,
      inputs_o (8) => fp_id(0),
      inputs_o (9) => fp_id(1),
      inputs_o (10) => fp_id(2),
      inputs_o (11) => pp_id(0),
      inputs_o (12) => pp_id(1),
      inputs_o (13) => pp_id(2),
      inputs_o (14) => ioexp_gpio_unused22,
      inputs_o (15) => ioexp_gpio_unused23,

      inp_valid_o => gpio_inp_valid,
      exp_oe_n_o => ioexp_gpio_oe_n,
      exp_rclk_o => ioexp_gpio_rclk_o,
      exp_sclk_o => ioexp_gpio_sclk_o,
      exp_d_o => ioexp_gpio_d_o,
      exp_q_i => ioexp_gpio_q_i
      );

  --  The vme core reads vme_ga once just after reset, so we must wait
  --  for the io expander.
  vme_rst_n <= rst_n and gpio_inp_valid;

  inst_wren: entity work.wren_core
    generic map (
      g_map_version => g_map_version,
      g_host_ident => open,
      g_with_serdes => false
      )
    port map (
      clk_sys_62m5_i => clk_sys_62m5,
      clk_sys_125m_i => clk_sys_125m,
      clk_sys_250m_i => clk_250m,
      clk_sys_500m_i => clk_sys_500m,
      rst_sys_62m5_n_i => rst_sys_62m5_n,
      board_axi_o => board_axi_in,
      board_axi_i => board_axi_out,
      host_wb_o => host_wb_in,
      host_wb_i => host_wb_out,
      tm_link_up_i => tm_link_up,
      tm_tai_i => tm_tai,
      tm_cycles_i => tm_cycles,
      tm_time_valid_i => tm_time_valid,
      wrpc_master_o => wr_master_out,
      wrpc_master_i => wr_master_in,
      irq_o => irq,
      eth_tx_o => eth_tx_out,
      eth_tx_i => eth_tx_in,
      eth_rx_o => eth_rx_out,
      eth_rx_i => eth_rx_in,

      S_AXI_LOG_awid => S_AXI_LOG_awid,
      S_AXI_LOG_awaddr => S_AXI_LOG_awaddr,
      S_AXI_LOG_awlen => S_AXI_LOG_awlen,
      S_AXI_LOG_awvalid => S_AXI_LOG_awvalid,
      S_AXI_LOG_awready => S_AXI_LOG_awready,
      S_AXI_LOG_wdata => S_AXI_LOG_wdata,
      S_AXI_LOG_wstrb => S_AXI_LOG_wstrb,
      S_AXI_LOG_wlast => S_AXI_LOG_wlast,
      S_AXI_LOG_wvalid => S_AXI_LOG_wvalid,
      S_AXI_LOG_wready => S_AXI_LOG_wready,
      S_AXI_LOG_bid => S_AXI_LOG_bid,
      S_AXI_LOG_bresp => S_AXI_LOG_bresp,
      S_AXI_LOG_bvalid => S_AXI_LOG_bvalid,
      S_AXI_LOG_bready => S_AXI_LOG_bready,

      led_o => open, -- led_o(5 downto 0),
      fp_led0_o => fp_led0_o,
      fp_led1_o => fp_led1_o,
      pp_led0_o => pp_led0_o,
      pp_led1_o => pp_led1_o,
      pad_o => lemo_out,
      pad_oe_o => lemo_oe,
      pad_term_o => lemo_term,
      pad_i => fp_io_b (7 downto 0),
      pps_p_i => pps_p,
      pps_valid_i => pps_valid,

      rf1_rxn_i => sfp1_rxn_i,
      rf1_rxp_i => sfp1_rxp_i,
      rf1_txn_o => sfp1_txn_o,
      rf1_txp_o => sfp1_txp_o,
      gth_clk_250m_i => gth_clk_250m
      );
  
  --  Clocking.
  blk_clock: block
    signal fpga_pl_clksys : std_logic;
    signal clk_gth_250m_int: std_logic;
    signal clk_fb, clk_fb_int : std_logic;
    signal clk_sys_125m_int, clk_sys_500m_int, clk_250m_int : std_logic;
    signal clk_main_125m_int, clk_dmtd_int : std_logic;
    signal clk_sys_62m5_int : std_logic;
    signal clk_locked : std_logic;
    signal rstlogic_arst : std_logic;

  begin
    inst_ibufds_gt : IBUFDS_GTE4
      generic map (
        REFCLK_EN_TX_PATH  => '0',
        REFCLK_HROW_CK_SEL => "00",
        REFCLK_ICNTL_RX    => "00")
      port map (
        O     => gth_clk_250m,
        ODIV2 => clk_gth_250m_int,
        CEB   => '0',
        I     => wr_clk_sfp_125m_p_i,
        IB    => wr_clk_sfp_125m_n_i);

    inst_buf_gt : BUFG_GT
      port map (
        O => clk_gth_250m_bufgt,
        CE => '1',
        CEMASK => '0',
        CLR => '0',
        CLRMASK => '0',
        DIV => "000",
        I => clk_gth_250m_int);

    --  Not used
    inst_bufds: IBUFDS
      port map (
        O => fpga_pl_clksys,
        I => fpga_pl_clksys_p_i,
        IB => fpga_pl_clksys_n_i
        );

    --  Not used.
    inst_ibufgds_pllmain : IBUFDS
      generic map (
        DQS_BIAS  => "FALSE")
      port map (
        O  => clk_main_125m_int,
        I  => wr_clk_main_125m_p_i,
        IB => wr_clk_main_125m_n_i);

    
    inst_ibufgds_dmtd : IBUFDS
      generic map (
        DQS_BIAS     => "FALSE")
      port map (
        O  => clk_dmtd_int,
        I  => wr_clk_helper_125m_p_i,
        IB => wr_clk_helper_125m_n_i);

    inst_bufg_clk_dmtd: BUFGCE_DIV
      generic map (
        BUFGCE_DIVIDE => 1)
      port map (
        O   => clk_dmtd,
        CE  => '1',
        CLR => '0',
        I   => clk_dmtd_int);
  
    --  PLL Fvco should be between 800Mhz and 1600Mhz
    --  (cf DS925 p 67)
    --  For an input of 250Mhz -> *4  [previously 125Mhz -> *8]
    inst_MMCME4_BASE : MMCME4_BASE
      generic map (
        BANDWIDTH => "OPTIMIZED",-- Jitter programming
        CLKFBOUT_MULT_F => 4.0,  -- Multiply value for all CLKOUT
        CLKFBOUT_PHASE => 0.0,  -- Phase offset in degrees of CLKFB
        CLKIN1_PERIOD => 4.0,   -- Input clock period in ns to ps resolution (i.e., 33.333 is 30 MHz).
        CLKOUT0_DIVIDE_F => 16.0, -- 62.5Mhz
        CLKOUT0_DUTY_CYCLE => 0.5,
        CLKOUT0_PHASE => 0.0,
        CLKOUT1_DIVIDE => 4,      --  250m
        CLKOUT1_DUTY_CYCLE => 0.5,
        CLKOUT1_PHASE => 0.0,
        CLKOUT2_DIVIDE => 8,      -- 125m
        CLKOUT2_DUTY_CYCLE => 0.5,
        CLKOUT2_PHASE => 0.0,
        CLKOUT3_DIVIDE => 2,      -- 500m
        CLKOUT3_DUTY_CYCLE => 0.5,
        CLKOUT3_PHASE => 0.0,
        DIVCLK_DIVIDE => 1,
        IS_CLKFBIN_INVERTED => '0',
        IS_CLKIN1_INVERTED => '0',
        IS_PWRDWN_INVERTED => '0',
        IS_RST_INVERTED => '0',
        REF_JITTER1 => 0.0,
        STARTUP_WAIT => "FALSE"
        )
      port map (
        CLKFBOUT => clk_fb_int,
        CLKFBOUTB => open,
        CLKOUT0 => clk_sys_62m5_int,
        CLKOUT0B => open,
        CLKOUT1 => clk_250m_int,
        CLKOUT1B => open,
        CLKOUT2 => clk_sys_125m_int,
        CLKOUT2B => open,
        CLKOUT3 => clk_sys_500m_int,
        CLKOUT3B => open,
        CLKOUT4 => open,
        CLKOUT5 => open,
        CLKOUT6 => open,
        LOCKED => clk_locked,
        CLKFBIN => clk_fb,
        CLKIN1 => clk_gth_250m_bufgt,
        PWRDWN => '0',
        RST => rst_pl
        );

    inst_bufg_fb: BUFG
      port map (
        I => clk_fb_int,
        O => clk_fb
      );

    inst_bufg_62m5: BUFG
      port map (
        I => clk_sys_62m5_int,
        O => clk_sys_62m5
        );

    inst_bufg_125m: BUFG
      port map (
        I => clk_sys_125m_int,
        O => clk_sys_125m
        );

    --  Not used
    inst_bufg_clk250m : BUFG
      port map (
        I => clk_250m_int,     -- 1-bit input: Buffer
        O => clk_250m          -- 1-bit output: Buffer
        );

    inst_bufg_clk500m : BUFG
      port map (
        I => clk_sys_500m_int,     -- 1-bit input: Buffer
        O => clk_sys_500m      -- 1-bit output: Buffer
        );

    rstlogic_arst <= (not rst_pl_n) or (not clk_locked);

    inst_rstlogic_reset : entity work.gc_reset_multi_aasd
      generic map (
        g_CLOCKS  => 3,   -- 62.5MHz, 125MHz
        g_RST_LEN => 16)  -- 16 clock cycles
      port map (
        arst_i  => rstlogic_arst,
        clks_i (0) => clk_sys_125m,
        clks_i (1) => clk_250m,
        clks_i (2) => clk_sys_62m5,
        rst_n_o (0) => rst_sys_125m_n,
        rst_n_o (1) => rst_clk_250m_n,
        rst_n_o (2) => rst_sys_62m5_n
        );
  end block;
  
  b_wr: block
    constant g_diag_id             : integer := 0;
    constant g_diag_ver            : integer := 0;
    constant g_diag_ro_size        : integer := 0;
    constant g_diag_rw_size        : integer := 0;
    constant c_streamers_diag_id  : integer := 1;  -- id reserved for streamers
    constant c_streamers_diag_ver : integer := 2;  -- version that will be probably increased
--    constant c_diag_id            : integer := f_pick_diag_val(g_fabric_iface,  c_streamers_diag_id,         g_diag_id);
--    constant c_diag_ver           : integer := f_pick_diag_val(g_fabric_iface,  c_streamers_diag_ver,        g_diag_id);
    constant c_diag_ro_size       : integer := 0; --f_pick_diag_size(g_fabric_iface, c_WR_STREAMERS_ARR_SIZE_OUT, g_diag_ro_size);
    constant c_diag_rw_size       : integer := 0; --f_pick_diag_size(g_fabric_iface, c_WR_STREAMERS_ARR_SIZE_IN,  g_diag_rw_size);

    signal phy16_out :t_phy_16bits_from_wrc;
    signal phy16_in :t_phy_16bits_to_wrc;

    signal sfp0_scl_out, sfp0_scl_in : std_logic;
    signal sfp0_sda_out, sfp0_sda_in : std_logic;

    signal eeprom_scl_out, eeprom_scl_in : std_logic;
    signal eeprom_sda_out, eeprom_sda_in : std_logic;

    signal wb_aux_out : t_wishbone_master_out;
    signal wb_aux_in  : t_wishbone_master_in;

    signal dac_mpll_val, dac_hpll_val : std_logic_vector(15 downto 0);
    signal dac_mpll_wr, dac_hpll_wr : std_logic;

    signal rst_sys_62m5 : std_logic;

    signal plldac_sclk, plldac_din, plldac_sync_n : std_logic;

    -- WR SNMP
    signal aux_diag_in  : t_generic_word_array(c_diag_ro_size-1 downto 0);
    signal aux_diag_out : t_generic_word_array(c_diag_rw_size-1 downto 0);
  begin
    rst_sys_62m5 <= not rst_sys_62m5_n;

    cmp_gth: entity work.wr_gthe4_phy_family7_xilinx_ip_pxie
      generic map (
        g_simulation         => g_simulation,
        g_use_gclk_as_refclk => false)
      port map (
        clk_gth_i      => gth_clk_250m,
        clk_freerun_i  => clk_sys_62m5,
        por_i          => rst_sys_62m5,
        tx_out_clk_o   => phy16_in.ref_clk,
        tx_locked_o    => open,
        tx_data_i      => phy16_out.tx_data,
        tx_k_i         => phy16_out.tx_k,
        tx_disparity_o => phy16_in.tx_disparity,
        tx_enc_err_o   => phy16_in.tx_enc_err,
        rx_rbclk_o     => phy16_in.rx_clk,
        rx_data_o      => phy16_in.rx_data,
        rx_k_o         => phy16_in.rx_k,
        rx_enc_err_o   => phy16_in.rx_enc_err,
        rx_bitslide_o  => phy16_in.rx_bitslide,
        rst_i          => phy16_out.rst,
        loopen_i       => "000",
        debug_i        => x"0000",
        debug_o        => open,
        pad_txn_o      => sfp0_txn_o,
        pad_txp_o      => sfp0_txp_o,
        pad_rxn_i      => sfp0_rxn_i,
        pad_rxp_i      => sfp0_rxp_i,
        rdy_o          => phy16_in.rdy,
        
        clk_mon_o => clk_mon);

    phy16_in.sfp_tx_fault <= '0'; --  not connected
    phy16_in.sfp_los      <= sfp0_los;
    sfp0_tx_disable       <= phy16_out.sfp_tx_disable;

    inst_WR_CORE : entity work.xwr_core
    generic map(
        g_simulation                     => g_simulation,
        g_with_external_clock_input      => false,
        --
        g_board_name                     => "VMEN",
        --g_ram_address_space_size_kb    => 256,
        g_phys_uart                      => true,  --  Is false OK ?
        g_virtual_uart                   => true,
        g_vuart_fifo_size                => 1024,
        g_aux_clks                       => 0,
--        g_softpll_reverse_dmtds          => true,
        g_ep_rxbuf_size                  => 1024,
        g_tx_runt_padding                => true,
        g_records_for_phy                => true,
        g_pcs_16bit                      => true,
        g_dpram_initf                    => g_wrpc_initf,
        g_dpram_size                     => (131072+65536)/4,
        g_interface_mode                 => PIPELINED,
        g_address_granularity            => BYTE,
        g_aux_sdb                        => c_wrc_periph3_sdb,
        g_softpll_enable_debugger        => true,
--        g_softpll_use_sampled_ref_clocks => true,
        g_diag_id                        => 0,
        g_diag_ver                       => 0,
        g_diag_ro_size                   => 0,
        g_diag_rw_size                   => 0,
        g_hwbld_date                     => g_hwbld_date)
    port map(
        clk_sys_i              => clk_sys_62m5,
        clk_dmtd_i             => clk_dmtd,
        clk_ref_i              => clk_sys_62m5,
        --  No external 10Mhz
        clk_ext_i              => open,
        clk_ext_mul_i          => open,
        clk_ext_stopped_i      => open,
        clk_ext_mul_locked_i   => open,
        clk_ext_rst_o          => open,
        clk_aux_i              => open,
        clk_dmtd_over_i        => open,
        pps_ext_i              => open,
        rst_n_i                => rst_sys_62m5_n,

        dac_hpll_load_p1_o     => dac_hpll_wr,
        dac_hpll_data_o        => dac_hpll_val,
        dac_dpll_load_p1_o     => dac_mpll_wr,
        dac_dpll_data_o        => dac_mpll_val,

        phy16_i                => phy16_in,
        phy16_o                => phy16_out,
        phy_mdio_master_o      => open,
        phy_mdio_master_i      => open,
        phy_ref_clk_i          => open,
        led_act_o              => sfp0_led_act,
        led_link_o             => sfp0_led_link,
        scl_o                  => eeprom_scl_out,
        scl_i                  => eeprom_scl_in,
        sda_o                  => eeprom_sda_out,
        sda_i                  => eeprom_sda_in,
        sfp_scl_o              => sfp0_scl_out,
        sfp_scl_i              => sfp0_scl_in,
        sfp_sda_o              => sfp0_sda_out,
        sfp_sda_i              => sfp0_sda_in,
        sfp_det_i              => sfp0_det,
        spi_sclk_o             => open,
        spi_ncs_o              => open,
        spi_mosi_o             => open,
        spi_miso_i             => open,
        uart_rxd_i             => uart_rxd_i,
        uart_txd_o             => uart_txd_o,
        owr_pwren_o            => open,
        owr_en_o               => open,
        owr_i                  => open,

        slave_i                => wr_master_out,
        slave_o                => wr_master_in,
        aux_master_o           => wb_aux_out,
        aux_master_i           => wb_aux_in,

        wrf_src_i => eth_rx_out,
        wrf_src_o => eth_rx_in,
        wrf_snk_i => eth_tx_out,
        wrf_snk_o => eth_tx_in,
  
        timestamps_o           => open,
        timestamps_ack_i       => '1',
        fc_tx_pause_req_i      => '0',
        fc_tx_pause_delay_i    => x"0000",
        fc_tx_pause_ready_o    => open,

        tm_link_up_o => tm_link_up,
        tm_time_valid_o => tm_time_valid,
        tm_tai_o => tm_tai,
        tm_cycles_o => tm_cycles,
  
        tm_dac_value_o         => open,
        tm_dac_wr_o            => open,
        tm_clk_aux_lock_en_i   => (others => '1'),
        tm_clk_aux_locked_o    => open,

        abscal_rxts_o          => wr_abscal_o,

        pps_p_o                => pps_p,
        pps_valid_o            => pps_valid,
        pps_led_o              => pps_led,
        pps_csync_o            => open,
        rst_aux_n_o            => open,
        aux_diag_i             => aux_diag_in,
        aux_diag_o             => aux_diag_out,

        link_ok_o              => open);

        sfp0_scl_b <= '0' when sfp0_scl_out = '0' else 'Z';
        sfp0_sda_b <= '0' when sfp0_sda_out = '0' else 'Z';
        sfp0_scl_in <= sfp0_scl_b;
        sfp0_sda_in <= sfp0_sda_b;
      
        eeprom_scl_b <= '0' when eeprom_scl_out = '0' else 'Z';
        eeprom_sda_b <= '0' when eeprom_sda_out = '0' else 'Z';
        eeprom_scl_in <= eeprom_scl_b;
        eeprom_sda_in <= eeprom_sda_b;
      
    inst_serial_dac: entity work.serial_dac856x
      generic map (
        g_sclk_div => 7
        )
      port map (
        clk_i => clk_sys_62m5,
        rst_n_i => rst_sys_62m5_n,
        value_a_i => dac_mpll_val,
        wr_a_i => dac_mpll_wr,
        value_b_i => dac_hpll_val,
        wr_b_i => dac_hpll_wr,
        sclk_o => plldac_sclk,
        d_o => plldac_din,
        sync_n_o => plldac_sync_n
        );

    g_ila: if false generate
      inst_ila : ila_0
        port map (
          clk => clk_sys_62m5,
          probe0(0) => plldac_sclk,
          probe0(1) => plldac_din,
          probe0(2) => plldac_sync_n,
          probe0(3) => dac_mpll_wr,
          probe0(4) => dac_hpll_wr,
          probe0(15 downto 5) => dac_mpll_val(15 downto 5),
          probe0(31 downto 16) => dac_hpll_val,
          probe0(63 downto 32) => (others => '0')
          );
    end generate g_ila;
      
    plldac_sclk_o <= plldac_sclk;
    plldac_din_o <= plldac_din;
    plldac_sync_n_o <= plldac_sync_n;
  end block;

  wr_pps_o <= pps_p;

  b_bst: block
    signal bst_out_ddr : std_logic_vector(1 downto 0);
    signal ttc_data, ttc_addr : std_logic_vector(7 downto 0);
    signal frev, load : std_logic;
    signal count : unsigned(10 downto 0);
    signal clk_fb, clk_80m_int, clk_locked, clk_locked_n, clk_80m, rst_80m_n : std_logic;
  begin
    --  Input: clk_250, fvco=1Ghz
    inst_MMCME4_BASE : MMCME4_BASE
      generic map (
        BANDWIDTH => "OPTIMIZED",-- Jitter programming
        CLKFBOUT_MULT_F => 4.0,  -- Multiply value for all CLKOUT
        CLKFBOUT_PHASE => 0.0,  -- Phase offset in degrees of CLKFB
        CLKIN1_PERIOD => 4.0,   -- Input clock period in ns to ps resolution (i.e., 33.333 is 30 MHz).
        CLKOUT0_DIVIDE_F => 12.5, -- 80Mhz
        CLKOUT0_DUTY_CYCLE => 0.5,
        CLKOUT0_PHASE => 0.0,
        DIVCLK_DIVIDE => 1,
        IS_CLKFBIN_INVERTED => '0',
        IS_CLKIN1_INVERTED => '0',
        IS_PWRDWN_INVERTED => '0',
        IS_RST_INVERTED => '0',
        REF_JITTER1 => 0.0,
        STARTUP_WAIT => "FALSE"
        )
      port map (
        CLKFBOUT => clk_fb,
        CLKFBOUTB => open,
        CLKOUT0 => clk_80m_int,
        CLKOUT0B => open,
        LOCKED => clk_locked,
        CLKFBIN => clk_fb,
        CLKIN1 => clk_gth_250m_bufgt,
        PWRDWN => '0',
        RST => rst_pl
        );

    inst_bufg_80m: BUFG
      port map (
        I => clk_80m_int,
        O => clk_80m
        );

    clk_locked_n <= not clk_locked;

    inst_rstlogic_reset : entity work.gc_reset_multi_aasd
      generic map (
        g_CLOCKS  => 1,   -- 62.5MHz, 125MHz
        g_RST_LEN => 16)  -- 16 clock cycles
      port map (
        arst_i  => clk_locked_n,
        clks_i (0) => clk_80m,
        rst_n_o (0) => rst_80m_n
        );

    inst_biphase: entity work.ttc_frametx
      port map (
        clk_i => clk_80m,
        rst_n_i => rst_80m_n,
        out_o => bst_out_ddr,
        data_i => ttc_data,
        subaddr_i => ttc_addr,
        frev_i => frev,
        load_imm_i => load,
        rdy_i => '0',
        ack_o => open
      );
    
    process (clk_80m)
    begin
      if rising_edge(clk_80m) then
        load <= '0';
        frev <= '0';
        if rst_80m_n = '0' then
          count <= (others => '0');
          ttc_data <= x"ff";
          ttc_addr <= x"00";
        else
          if count = 923 * 2 then
            load <= '1';
            frev <= '1';
            count <= (others => '0');
            ttc_addr <= std_logic_vector(unsigned(ttc_addr) + 1);
            ttc_data <= not ttc_addr;
          else
            count <= count + 1;
          end if;
        end if;
      end if;
    end process;

    g_oddr: if false generate
    inst_ODDR : ODDRE1
      generic map (
        IS_C_INVERTED => '0', -- Optional inversion for C
        SIM_DEVICE => "ULTRASCALE_PLUS",
        SRVAL => '0'
        )
      port map (
        Q => bst_out,
        C => clk_80m,
        D1 => bst_out_ddr(1),
        D2 => bst_out_ddr(0),
        SR => '0'
        );
  end generate;
  end block;

  --  ==========================================
  --    SFP   0     2     4    6
  --          1     3     5    7
  g_fpio: for i in fp_io_b'range generate
    --  FIXME: avoid temporary driving conflict in case of change of OE.
    fp_io_b(i) <= lemo_out(i) when lemo_oe(i) = '1' else 'Z';

    userio_b(i) <= lemo_out(i) when userio_oe_n(i / 8) = '0' and userio_dir (i / 8) = '1' else 'Z';
  end generate;

  g_lemo_ila: if false generate
    inst_ila: ila_0
      port map (
        clk => clk_sys_125m,
        probe0(7 downto 0) => lemo_out(7 downto 0),
        probe0(15 downto 8) => lemo_oe(7 downto 0),
        probe0(63 downto 16) => (others => '0')
        );
  end generate g_lemo_ila;

  b_leds: block
    constant clk_freq : natural := 62_500_000;
  begin
    inst_sfp: entity work.sfp_argb_leds
      generic map (
        g_clk_freq => clk_freq
      )
      port map (
        clk_i => clk,
        rst_n_i => rst_n,
        sfp_act_i => sfp0_led_act,
        sfp_link_i => sfp0_led_link,
        pps_led_i => pps_led,
        pps_valid_i => pps_valid,
        dout_o => sfp_led_o
      );
  end block;
end architecture arch;
