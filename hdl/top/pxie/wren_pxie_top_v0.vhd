--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- White Rabbit Trigger Distribution
-- https://ohwr-gitlab.cern.ch/projects/wrtd
--------------------------------------------------------------------------------
--
-- unit name:   wren_pxie_top
--
-- description: Top entity for GMT over WR playground
--
--------------------------------------------------------------------------------
-- Copyright CERN 2014-2019
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.axi4_pkg.all;
use work.wr_xilinx_pkg.all;
use work.pulser_pkg.all;
use work.mailbox_pkg.all;
use work.wr_fabric_pkg.all;
use work.wrcore_pkg.all;
use work.endpoint_pkg.all;
use work.streamers_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity wren_pxie_top is
  generic (
    g_WRPC_INITF    : string  :=  "../../../../../dependencies/files/wrc-pxie.bram";
    --  g_WRPC_INITF    : string  :=  "";
    g_map_version   : std_logic_vector(31 downto 0) := x"3510_f003";
    g_hwbld_date    : std_logic_vector(31 downto 0) := x"0000_0000";
    -- Simulation-mode enable parameter. Set by default (synthesis) to 0, and
    -- changed to non-zero in the instantiation of the top level DUT in the
    -- testbench. Its purpose is to reduce some internal counters/timeouts
    -- to speed up simulations.
    g_SIMULATION     : integer := 0);
  port (
    ---------------------------------------------------------------------------
    -- Clocks/resets
    ---------------------------------------------------------------------------
    ps_por_i               : in std_logic;

    fpga_pl_clksys_p_i     : in  std_logic;
    fpga_pl_clksys_n_i     : in  std_logic;

    wr_clk_helper_125m_p_i : in  std_logic;
    wr_clk_helper_125m_n_i : in  std_logic;
    wr_clk_main_125m_p_i   : in  std_logic;
    wr_clk_main_125m_n_i   : in  std_logic;
    wr_clk_sfp_125m_p_i    : in  std_logic;
    wr_clk_sfp_125m_n_i    : in  std_logic;

    -- clk_helper_25m_i       : in  std_logic;  --  The oscillator before the pll

    ---------------------------------------------------------------------------
    -- SPI interface to DACs
    ---------------------------------------------------------------------------
    plldac_sclk_o   : out std_logic;
    plldac_din_o    : out std_logic;
    plldac_sync_n_o : out std_logic;
    plldac_ldac_n_o : out std_logic;
    plldac_clr_n_o  : out std_logic;

    ---------------------------------------------------------------------------
    -- SFP I/Os for transceiver
    ---------------------------------------------------------------------------
    sfp_txp_o         : out std_logic;
    sfp_txn_o         : out std_logic;
    sfp_rxp_i         : in  std_logic;
    sfp_rxn_i         : in  std_logic;
    sfp_det_i         : in  std_logic;
    sfp_sda_b         : inout std_logic;
    sfp_scl_b         : inout std_logic;
    sfp_tx_disable_o  : out std_logic;
    sfp_los_i         : in  std_logic;

    --  SFP leds
    sfp_led_act_o         : out std_logic;
    sfp_led_link_o        : out std_logic;

    trigio_out_o : out std_logic_vector(7 downto 0);
    trigio_oe_n_o : out std_logic_vector(7 downto 0);
    trigio_term_o : out std_logic_vector(7 downto 0);
    trigio_in_i : in std_logic_vector(7 downto 0);

    -- iqdac
    dac_data_p_o : out std_logic_vector(15 downto 0);
    dac_data_n_o : out std_logic_vector(15 downto 0);
    dac_dci_p_o  : out std_logic;
    dac_dci_n_o  : out std_logic;
    dac_dco_p_o  : in std_logic;
    dac_dco_n_o  : in std_logic;
    dac_reset_o  : out std_logic;

    dac_sclk_o  : out std_logic;
    dac_cs_n_o  : out std_logic;
    dac_sdo_i   : in  std_logic;
    dac_sdi_o   : out std_logic;

    ---------------------------------------------------------------------------
    -- EEPROM I2C interface for storing configuration and accessing unique ID
    ---------------------------------------------------------------------------
    eeprom_sda_b  : inout std_logic;
    eeprom_scl_b  : inout std_logic;
    ---------------------------------------------------------------------------
    -- UART
    ---------------------------------------------------------------------------
    uart_rxd_i    : in  std_logic;
    uart_txd_o    : out std_logic;

    ---------------------------------------------------------------------------
    -- LEDs
    ---------------------------------------------------------------------------
    led_o    : out std_logic_vector(15 downto 0);
    pps_p_o    : out std_logic;

    --  WR PLL control
    wr_clk_sync_n_o : out std_logic;
    wr_clk_rst_n_o : out std_logic;
    i2c_scl_b : inout std_logic;
    i2c_sda_b : inout std_logic;
    
    --  rf1    
    rf1_txp_o         : out std_logic;
    rf1_txn_o         : out std_logic;
    rf1_rxp_i         : in  std_logic;
    rf1_rxn_i         : in  std_logic
);
end entity wren_pxie_top;

architecture arch of wren_pxie_top is
  -- clock and reset
  signal clk_sys_62m5     : std_logic;
  signal rst_sys_62m5_n   : std_logic;
  signal clk_sys_125m     : std_logic;
  signal rst_sys_125m_n   : std_logic;
  signal clk_sys_500m     : std_logic;

  signal clk_250m : std_logic;
  signal rst_clk_250m_n : std_logic;

  signal clk_dmtd : std_logic;
  signal clk_helper_62m5 : std_logic;

  signal gth_clk_250m : std_logic;
  signal clk_gth_250m_bufgt : std_logic;

  alias clk : std_logic is clk_sys_62m5;
  alias rst_n : std_logic is rst_sys_62m5_n;

  attribute keep                   : string;

  --  WR fabric.
  signal eth_tx_out : t_wrf_source_out;
  signal eth_tx_in  : t_wrf_source_in;
  signal eth_rx_out : t_wrf_sink_out;
  signal eth_rx_in  : t_wrf_sink_in;

  --  mpsoc to pci map
  signal m_axi_out: t_axi4_lite_master_out_32;
  signal m_axi_in : t_axi4_lite_master_in_32;

  signal board_axi_out: t_axi4_lite_master_out_32;
  signal board_axi_in : t_axi4_lite_master_in_32;
  
  signal host_wb_out : t_wishbone_master_out;
  signal host_wb_in  : t_wishbone_master_in;

  signal wr_master_out : t_wishbone_master_out;
  signal wr_master_in  : t_wishbone_master_in;

  -- WRPC TM interface and aux clocks
  signal tm_link_up         : std_logic;
  signal tm_tai             : std_logic_vector(39 downto 0);
  signal tm_cycles          : std_logic_vector(27 downto 0);
  signal tm_time_valid      : std_logic;

  -- MT TM interface
--  signal tm : t_mt_timing_if;

  signal M_AXI_araddr  : STD_LOGIC_VECTOR (39 downto 32);
  signal M_AXI_awaddr  : STD_LOGIC_VECTOR (39 downto 32);

  signal s_axi_awuser : STD_LOGIC;
  signal s_axi_awid : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal s_axi_awaddr : STD_LOGIC_VECTOR ( 48 downto 0 );
  signal s_axi_awlen : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal s_axi_awsize : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_awburst : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_awlock : STD_LOGIC;
  signal s_axi_awcache : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axi_awprot : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_awvalid : STD_LOGIC;
  signal s_axi_awready :  STD_LOGIC;
  signal s_axi_wdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_wstrb : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axi_wlast : STD_LOGIC;
  signal s_axi_wvalid : STD_LOGIC;
  signal s_axi_wready :  STD_LOGIC;
  signal s_axi_bid :  STD_LOGIC_VECTOR ( 5 downto 0 );
  signal s_axi_bresp :  STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_bvalid :  STD_LOGIC;
  signal s_axi_bready : STD_LOGIC;

  signal S_AXI_LOG_awid : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_LOG_awaddr : STD_LOGIC_VECTOR ( 48 downto 0 );
  signal S_AXI_LOG_awlen : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal S_AXI_LOG_awvalid : STD_LOGIC;
  signal S_AXI_LOG_awready : STD_LOGIC;
  signal S_AXI_LOG_wdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal S_AXI_LOG_wstrb : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal S_AXI_LOG_wlast : STD_LOGIC;
  signal S_AXI_LOG_wvalid : STD_LOGIC;
  signal S_AXI_LOG_wready : STD_LOGIC;
  signal S_AXI_LOG_bid : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_LOG_bresp : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_LOG_bvalid : STD_LOGIC;
  signal S_AXI_LOG_bready : STD_LOGIC;

  signal iic0_sda_in, iic0_sda_out, iic0_sda_tri : std_logic;
  signal iic0_scl_in, iic0_scl_out, iic0_scl_tri : std_logic;

  signal pps_p, pps_valid : std_logic;

  signal irq : std_logic;

  signal clk_pl, rst_pl_n, rst_pl : std_logic;

  signal clk_mon : std_logic;

  signal lemo_out, lemo_oe, lemo_term : std_logic_vector(31 downto 0);
  signal bst_out : std_logic;

  signal spi_sck, spi_ss0, spi_ss1, spi_mosi, spi_miso : std_logic;

  component mpsoc is
    port (
      M_AXI_awaddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
      M_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_awvalid : out STD_LOGIC;
      M_AXI_awready : in STD_LOGIC;
      M_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_wvalid : out STD_LOGIC;
      M_AXI_wready : in STD_LOGIC;
      M_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_bvalid : in STD_LOGIC;
      M_AXI_bready : out STD_LOGIC;
      M_AXI_araddr : out STD_LOGIC_VECTOR ( 39 downto 0 );
      M_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_arvalid : out STD_LOGIC;
      M_AXI_arready : in STD_LOGIC;
      M_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_rvalid : in STD_LOGIC;
      M_AXI_rready : out STD_LOGIC;
      GPIO_tri_i : in STD_LOGIC_VECTOR ( 94 downto 0 );
      GPIO_tri_o : out STD_LOGIC_VECTOR ( 94 downto 0 );
      GPIO_tri_t : out STD_LOGIC_VECTOR ( 94 downto 0 );
      IIC_0_scl_i : in STD_LOGIC;
      IIC_0_scl_o : out STD_LOGIC;
      IIC_0_scl_t : out STD_LOGIC;
      IIC_0_sda_i : in STD_LOGIC;
      IIC_0_sda_o : out STD_LOGIC;
      IIC_0_sda_t : out STD_LOGIC;
      S_AXI_aruser : in STD_LOGIC;
      S_AXI_awuser : in STD_LOGIC;
      S_AXI_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_awaddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_awlock : in STD_LOGIC;
      S_AXI_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_awvalid : in STD_LOGIC;
      S_AXI_awready : out STD_LOGIC;
      S_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_wlast : in STD_LOGIC;
      S_AXI_wvalid : in STD_LOGIC;
      S_AXI_wready : out STD_LOGIC;
      S_AXI_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_bvalid : out STD_LOGIC;
      S_AXI_bready : in STD_LOGIC;
      S_AXI_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_araddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_arlock : in STD_LOGIC;
      S_AXI_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_arvalid : in STD_LOGIC;
      S_AXI_arready : out STD_LOGIC;
      S_AXI_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_rlast : out STD_LOGIC;
      S_AXI_rvalid : out STD_LOGIC;
      S_AXI_rready : in STD_LOGIC;
      S_AXI_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      SPI0_sck_i : in STD_LOGIC;
      SPI0_sck_o : out STD_LOGIC;
      SPI0_sck_t : out STD_LOGIC;
      SPI0_io1_i : in STD_LOGIC;
      SPI0_io0_o : out STD_LOGIC;
      SPI0_io0_t : out STD_LOGIC;
      SPI0_io0_i : in STD_LOGIC;
      SPI0_io1_o : out STD_LOGIC;
      SPI0_io1_t : out STD_LOGIC;
      SPI0_ss_i : in STD_LOGIC;
      SPI0_ss_o : out STD_LOGIC;
      SPI0_ss1_o : out STD_LOGIC;
      SPI0_ss_t : out STD_LOGIC;
      clk_i : in STD_LOGIC;
      aresetn_i : in STD_LOGIC;
      pl_resetn_o : out STD_LOGIC;
      pl_ps_irq0_i : in STD_LOGIC_VECTOR ( 0 to 0 );
      pl_clk_o : out STD_LOGIC;
      saxi_clk : in STD_LOGIC;
      S_AXI_LOG_aruser : in STD_LOGIC;
      S_AXI_LOG_awuser : in STD_LOGIC;
      S_AXI_LOG_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_awaddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_LOG_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_LOG_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_awlock : in STD_LOGIC;
      S_AXI_LOG_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_LOG_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_awvalid : in STD_LOGIC;
      S_AXI_LOG_awready : out STD_LOGIC;
      S_AXI_LOG_wdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
      S_AXI_LOG_wstrb : in STD_LOGIC_VECTOR ( 15 downto 0 );
      S_AXI_LOG_wlast : in STD_LOGIC;
      S_AXI_LOG_wvalid : in STD_LOGIC;
      S_AXI_LOG_wready : out STD_LOGIC;
      S_AXI_LOG_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_bvalid : out STD_LOGIC;
      S_AXI_LOG_bready : in STD_LOGIC;
      S_AXI_LOG_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_araddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_LOG_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_LOG_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_arlock : in STD_LOGIC;
      S_AXI_LOG_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_LOG_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_arvalid : in STD_LOGIC;
      S_AXI_LOG_arready : out STD_LOGIC;
      S_AXI_LOG_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_rdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
      S_AXI_LOG_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_rlast : out STD_LOGIC;
      S_AXI_LOG_rvalid : out STD_LOGIC;
      S_AXI_LOG_rready : in STD_LOGIC;
      S_AXI_LOG_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_LOG_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      saxi_log_clk : in STD_LOGIC
    );
    end component mpsoc;

    component ila_0 is
      port (
        clk    : in STD_LOGIC;
        probe0 : in STD_LOGIC_VECTOR(31 downto 0)
      );
    end component;
begin
  inst_mpsoc: mpsoc
    port map (
      M_AXI_araddr(31 downto 0)  => m_axi_out.araddr,
      M_AXI_araddr(39 downto 32) => M_AXI_araddr(39 downto 32),
      M_AXI_arprot  => open,
      M_AXI_arready => m_axi_in.arready,
      M_AXI_arvalid => m_axi_out.arvalid,
      M_AXI_awaddr(31 downto 0) => m_axi_out.awaddr,
      M_AXI_awaddr(39 downto 32) => M_AXI_awaddr(39 downto 32),
      M_AXI_awprot  => open,
      M_AXI_awready => m_axi_in.awready,
      M_AXI_awvalid => m_axi_out.awvalid,
      M_AXI_bready  => m_axi_out.bready,
      M_AXI_bresp   => m_axi_in.bresp,
      M_AXI_bvalid  => m_axi_in.bvalid,
      M_AXI_rdata   => m_axi_in.rdata,
      M_AXI_rready  => m_axi_out.rready,
      M_AXI_rresp   => m_axi_in.rresp,
      M_AXI_rvalid  => m_axi_in.rvalid,
      M_AXI_wdata   => m_axi_out.wdata,
      M_AXI_wready  => m_axi_in.wready,
      M_AXI_wstrb   => m_axi_out.wstrb,
      M_AXI_wvalid  => m_axi_out.wvalid,

      S_AXI_awuser => s_axi_awuser,
      S_AXI_awid => s_axi_awid,
      S_AXI_awaddr => s_axi_awaddr,
      S_AXI_awlen => s_axi_awlen,
      S_AXI_awsize => s_axi_awsize,
      S_AXI_awburst => s_axi_awburst,
      S_AXI_awlock => s_axi_awlock,
      S_AXI_awcache => s_axi_awcache,
      S_AXI_awprot => s_axi_awprot,
      S_AXI_awvalid => s_axi_awvalid,
      S_AXI_awready => s_axi_awready,
      S_AXI_awqos => (others => '0'),
      S_AXI_wdata => s_axi_wdata,
      S_AXI_wstrb => s_axi_wstrb,
      S_AXI_wlast => s_axi_wlast,
      S_AXI_wvalid => s_axi_wvalid,
      S_AXI_wready => s_axi_wready,
      S_AXI_bid => s_axi_bid,
      S_AXI_bresp => s_axi_bresp,
      S_AXI_bvalid => s_axi_bvalid,
      S_AXI_bready => s_axi_bready,
      S_AXI_aruser => 'X',
      S_AXI_arid => (others => 'X'),
      S_AXI_araddr => (others => 'X'),
      S_AXI_arlen => (others => 'X'),
      S_AXI_arsize => (others => 'X'),
      S_AXI_arburst => (others => 'X'),
      S_AXI_arlock => 'X',
      S_AXI_arcache => (others => 'X'),
      S_AXI_arprot => (others => 'X'),
      S_AXI_arvalid => '0',
      S_AXI_arready => open,
      S_AXI_rid => open,
      S_AXI_rdata => open,
      S_AXI_rresp => open,
      S_AXI_rlast => open,
      S_AXI_rvalid => open,
      S_AXI_rready => '0',
      S_AXI_arqos => (others => 'X'),

      saxi_clk => clk,

      saxi_log_clk => clk_250m,
  
      S_AXI_LOG_awid => S_AXI_LOG_awid,
      S_AXI_LOG_awaddr => S_AXI_LOG_awaddr,
      S_AXI_LOG_awlen => S_AXI_LOG_awlen,
      S_AXI_LOG_awsize => b"100",  -- 100: 16 bytes, 011: 8 bytes
      S_AXI_LOG_awburst => b"01",  -- 00: FIXED, (01: INCR)
      S_AXI_LOG_awvalid => S_AXI_LOG_awvalid,
      S_AXI_LOG_awready => S_AXI_LOG_awready,
      S_AXI_LOG_wdata => S_AXI_LOG_wdata,
      S_AXI_LOG_wstrb => S_AXI_LOG_wstrb,
      S_AXI_LOG_wlast => S_AXI_LOG_wlast,
      S_AXI_LOG_wvalid => S_AXI_LOG_wvalid,
      S_AXI_LOG_wready => S_AXI_LOG_wready,
      S_AXI_LOG_bid => S_AXI_LOG_bid,
      S_AXI_LOG_bresp => S_AXI_LOG_bresp,
      S_AXI_LOG_bvalid => S_AXI_LOG_bvalid,
      S_AXI_LOG_bready => S_AXI_LOG_bready,
      s_AXI_LOG_awuser => '0',
      s_AXI_LOG_awqos => (others => '0'),
      s_AXI_LOG_awcache => "0001",  -- Set AWCACHE[1] to upsizing ?
      s_AXI_LOG_awlock => '0',
      s_AXI_LOG_awprot => "001",
      s_AXI_LOG_araddr => (others => 'X'),
      s_AXI_LOG_aruser => 'X',
      s_AXI_LOG_arid => (others => 'X'),
      s_AXI_LOG_arlen => (others => 'X'),
      s_AXI_LOG_arsize => (others => 'X'),
      s_AXI_LOG_arburst => (others => 'X'),
      s_AXI_LOG_arcache => (others => 'X'),
      s_AXI_LOG_arlock => 'X',
      s_AXI_LOG_arprot => (others => 'X'),
      s_AXI_LOG_arvalid => '0',
      s_AXI_LOG_arready => open,
      s_AXI_LOG_arqos => (others => 'X'),
      s_AXI_LOG_rready => '0',
      s_AXI_LOG_rid => open,
      s_AXI_LOG_rvalid => open,
      s_AXI_LOG_rlast => open,
      s_AXI_LOG_rresp => open,
      s_AXI_LOG_rdata => open,

      spi0_sck_i => '0',
      spi0_sck_t => open,
      spi0_ss_i => '0',
      spi0_ss_t => open,
      spi0_io1_i => '0',
      spi0_io1_t => open,
      spi0_io1_o => open,
      spi0_io0_i => spi_miso,
      spi0_io0_o => spi_mosi,
      spi0_io0_t => open,
      spi0_ss_o => spi_ss0,
      spi0_ss1_o => spi_ss1,
      spi0_sck_o => spi_sck,

      pl_ps_irq0_i  => "0",
      GPIO_tri_i => (others => '0'),
      GPIO_tri_o => open,
      GPIO_tri_t => open,
      IIC_0_scl_i => iic0_scl_in,
      IIC_0_scl_o => iic0_scl_out,
      IIC_0_scl_t => iic0_scl_tri,
      IIC_0_sda_i => iic0_sda_in,
      IIC_0_sda_o => iic0_sda_out,
      IIC_0_sda_t => iic0_sda_tri,
      aresetn_i     => rst_n,
      clk_i         => clk,
      pl_clk_o      => clk_pl,
      pl_resetn_o   => rst_pl_n);

    rst_pl <= not rst_pl_n;

    inst_int_gen: entity work.mpsoc_int_gen
      port map (
        clk_i => clk,
        rst_n_i => rst_n,
        irq_i => irq,
        s_axi_awaddr => s_axi_awaddr,
        s_axi_awburst => s_axi_awburst,
        s_axi_awcache => s_axi_awcache,
        s_axi_awid => s_axi_awid,
        s_axi_awlen => s_axi_awlen,
        s_axi_awlock => s_axi_awlock,
        s_axi_awprot => s_axi_awprot,
        s_axi_awready => s_axi_awready,
        s_axi_awsize => s_axi_awsize,
        s_axi_awuser => s_axi_awuser,
        s_axi_awvalid => s_axi_awvalid,
        s_axi_wdata => s_axi_wdata,
        s_axi_wlast => s_axi_wlast,
        s_axi_wready => s_axi_wready,
        s_axi_wstrb => s_axi_wstrb,
        s_axi_wvalid => s_axi_wvalid,
        s_axi_bid => s_axi_bid,
        s_axi_bready => s_axi_bready,
        s_axi_bresp => s_axi_bresp,
        s_axi_bvalid => s_axi_bvalid
      );
    
    inst_pci_map: entity work.pci_map
      port map (
        aclk => clk,
        areset_n => rst_n,
        awvalid => m_axi_out.awvalid,
        awready => m_axi_in.awready,
        awaddr => m_axi_out.awaddr(20 downto 2),
        awprot => "000",
        wvalid => m_axi_out.wvalid,
        wready => m_axi_in.wready,
        wdata => m_axi_out.wdata,
        wstrb => m_axi_out.wstrb,
        bvalid => m_axi_in.bvalid,
        bready => m_axi_out.bready,
        bresp => m_axi_in.bresp,
        arvalid => m_axi_out.arvalid,
        arready => m_axi_in.arready,
        araddr => m_axi_out.araddr(20 downto 2),
        arprot => "000",
        rvalid => m_axi_in.rvalid,
        rready => m_axi_out.rready,
        rdata => m_axi_in.rdata,
        rresp => m_axi_in.rresp,
  
        host_i => host_wb_in,
        host_o => host_wb_out,
  
        board_awvalid_o => board_axi_out.awvalid,
        board_awready_i => board_axi_in.awready,
        board_awaddr_o => board_axi_out.awaddr (16 downto 2),
        board_awprot_o => open,
        board_wvalid_o => board_axi_out.wvalid,
        board_wready_i => board_axi_in.wready,
        board_wdata_o => board_axi_out.wdata,
        board_wstrb_o => board_axi_out.wstrb,
        board_bvalid_i => board_axi_in.bvalid,
        board_bready_o => board_axi_out.bready,
        board_bresp_i => board_axi_in.bresp,
        board_arvalid_o => board_axi_out.arvalid,
        board_arready_i => board_axi_in.arready,
        board_araddr_o => board_axi_out.araddr (16 downto 2),
        board_arprot_o => open,
        board_rvalid_i => board_axi_in.rvalid,
        board_rready_o => board_axi_out.rready,
        board_rdata_i => board_axi_in.rdata,
        board_rresp_i => board_axi_in.rresp
      );
  
    inst_wren: entity work.wren_core
        generic map (
          g_map_version => g_map_version,
          g_host_ident => open,
          g_with_serdes => false
        )
        port map (
          clk_sys_62m5_i => clk_sys_62m5,
          clk_sys_125m_i => clk_sys_125m,
          clk_sys_500m_i => clk_sys_500m,
          clk_sys_250m_i => clk_250m,
          rst_sys_62m5_n_i => rst_sys_62m5_n,
          board_axi_o => board_axi_in,
          board_axi_i => board_axi_out,
          host_wb_o => host_wb_in,
          host_wb_i => host_wb_out,
          tm_link_up_i => tm_link_up,
          tm_tai_i => tm_tai,
          tm_cycles_i => tm_cycles,
          tm_time_valid_i => tm_time_valid,
          wrpc_master_o => wr_master_out,
          wrpc_master_i => wr_master_in,
          irq_o => irq,
          eth_tx_o => eth_tx_out,
          eth_tx_i => eth_tx_in,
          eth_rx_o => eth_rx_out,
          eth_rx_i => eth_rx_in,

          S_AXI_LOG_awid => S_AXI_LOG_awid,
          S_AXI_LOG_awaddr => S_AXI_LOG_awaddr,
          S_AXI_LOG_awlen => S_AXI_LOG_awlen,
          S_AXI_LOG_awvalid => S_AXI_LOG_awvalid,
          S_AXI_LOG_awready => S_AXI_LOG_awready,
          S_AXI_LOG_wdata => S_AXI_LOG_wdata,
          S_AXI_LOG_wstrb => S_AXI_LOG_wstrb,
          S_AXI_LOG_wlast => S_AXI_LOG_wlast,
          S_AXI_LOG_wvalid => S_AXI_LOG_wvalid,
          S_AXI_LOG_wready => S_AXI_LOG_wready,
          S_AXI_LOG_bid => S_AXI_LOG_bid,
          S_AXI_LOG_bresp => S_AXI_LOG_bresp,
          S_AXI_LOG_bvalid => S_AXI_LOG_bvalid,
          S_AXI_LOG_bready => S_AXI_LOG_bready,

          led_o => open, -- led_o(5 downto 0),
          pad_o => lemo_out,
          pad_oe_o => lemo_oe,
          pad_term_o => lemo_term,
          pad_i => trigio_in_i,
          pps_p_i => pps_p,
          pps_valid_i => pps_valid,

          rf1_rxn_i => rf1_rxn_i,
          rf1_rxp_i => rf1_rxp_i,
          rf1_txn_o => rf1_txn_o,
          rf1_txp_o => rf1_txp_o,
          gth_clk_250m_i => gth_clk_250m
        );
  
  --  Clocking.
  blk_clock: block
    signal fpga_pl_clksys : std_logic;
    signal clk_gth_250m_int: std_logic;
    signal clk_fb, clk_fb_int : std_logic;
    signal clk_sys_125m_int, clk_sys_500m_int, clk_250m_int : std_logic;
    signal clk_main_125m_int, clk_dmtd_int : std_logic;
    signal clk_sys_62m5_int : std_logic;
    signal clk_locked : std_logic;
    signal rstlogic_arst : std_logic;
  begin
    inst_ibufds_gt : IBUFDS_GTE4
      generic map (
        REFCLK_EN_TX_PATH  => '0',
        REFCLK_HROW_CK_SEL => "00",
        REFCLK_ICNTL_RX    => "00")
      port map (
        O     => gth_clk_250m,
        ODIV2 => clk_gth_250m_int,
        CEB   => '0',
        I     => wr_clk_sfp_125m_p_i,
        IB    => wr_clk_sfp_125m_n_i);

    inst_buf_gt : BUFG_GT
      port map (
        O => clk_gth_250m_bufgt,
        CE => '1',
        CEMASK => '0',
        CLR => '0',
        CLRMASK => '0',
        DIV => "000",
        I => clk_gth_250m_int);

    --  Not used
    inst_bufds: IBUFDS
      port map (
        O => fpga_pl_clksys,
        I => fpga_pl_clksys_p_i,
        IB => fpga_pl_clksys_n_i
        );

    --  Not used.
    inst_ibufgds_pllmain : IBUFDS
      generic map (
        DQS_BIAS  => "FALSE")
      port map (
        O  => clk_main_125m_int,
        I  => wr_clk_main_125m_p_i,
        IB => wr_clk_main_125m_n_i);

    
    inst_ibufgds_dmtd : IBUFDS
      generic map (
        DQS_BIAS     => "FALSE")
      port map (
        O  => clk_dmtd_int,
        I  => wr_clk_helper_125m_p_i,
        IB => wr_clk_helper_125m_n_i);

    inst_bufg_clk_dmtd: BUFGCE_DIV
      generic map (
        BUFGCE_DIVIDE => 1)
      port map (
        O   => clk_dmtd,
        CE  => '1',
        CLR => '0',
        I   => clk_dmtd_int);
  
    --  PLL Fvco should be between 800Mhz and 1600Mhz
    --  (cf DS925 p 67)
    --  For an input of 250Mhz -> *4  [previously 125Mhz -> *8]
    inst_MMCME4_BASE : MMCME4_BASE
      generic map (
        BANDWIDTH => "OPTIMIZED",-- Jitter programming
        CLKFBOUT_MULT_F => 4.0,  -- Multiply value for all CLKOUT
        CLKFBOUT_PHASE => 0.0,  -- Phase offset in degrees of CLKFB
        CLKIN1_PERIOD => 4.0,   -- Input clock period in ns to ps resolution (i.e., 33.333 is 30 MHz).
        CLKOUT0_DIVIDE_F => 16.0, -- 62.5Mhz
        CLKOUT0_DUTY_CYCLE => 0.5,
        CLKOUT0_PHASE => 0.0,
        CLKOUT1_DIVIDE => 4,      --  250m
        CLKOUT1_DUTY_CYCLE => 0.5,
        CLKOUT1_PHASE => 0.0,
        CLKOUT2_DIVIDE => 8,      -- 125m
        CLKOUT2_DUTY_CYCLE => 0.5,
        CLKOUT2_PHASE => 0.0,
        CLKOUT3_DIVIDE => 2,      -- 500m
        CLKOUT3_DUTY_CYCLE => 0.5,
        CLKOUT3_PHASE => 0.0,
        DIVCLK_DIVIDE => 1,
        IS_CLKFBIN_INVERTED => '0',
        IS_CLKIN1_INVERTED => '0',
        IS_PWRDWN_INVERTED => '0',
        IS_RST_INVERTED => '0',
        REF_JITTER1 => 0.0,
        STARTUP_WAIT => "FALSE"
        )
      port map (
        CLKFBOUT => clk_fb_int,
        CLKFBOUTB => open,
        CLKOUT0 => clk_sys_62m5_int,
        CLKOUT0B => open,
        CLKOUT1 => clk_250m_int,
        CLKOUT1B => open,
        CLKOUT2 => clk_sys_125m_int,
        CLKOUT2B => open,
        CLKOUT3 => clk_sys_500m_int,
        CLKOUT3B => open,
        CLKOUT4 => open,
        CLKOUT5 => open,
        CLKOUT6 => open,
        LOCKED => clk_locked,
        CLKFBIN => clk_fb,
        CLKIN1 => clk_gth_250m_bufgt,
        PWRDWN => '0',
        RST => rst_pl
        );

    inst_bufg_fb: BUFG
      port map (
        I => clk_fb_int,
        O => clk_fb
      );

    inst_bufg_62m5: BUFG
      port map (
        I => clk_sys_62m5_int,
        O => clk_sys_62m5
        );

    inst_bufg_125m: BUFG
      port map (
        I => clk_sys_125m_int,
        O => clk_sys_125m
        );

    --  Not used
    inst_bufg_clk250m : BUFG
      port map (
        I => clk_250m_int,     -- 1-bit input: Buffer
        O => clk_250m          -- 1-bit output: Buffer
        );

    inst_bufg_clk500m : BUFG
      port map (
        I => clk_sys_500m_int,     -- 1-bit input: Buffer
        O => clk_sys_500m      -- 1-bit output: Buffer
        );

    rstlogic_arst <= (not rst_pl_n) or (not clk_locked);

    inst_rstlogic_reset : entity work.gc_reset_multi_aasd
      generic map (
        g_CLOCKS  => 3,   -- 62.5MHz, 125MHz
        g_RST_LEN => 16)  -- 16 clock cycles
      port map (
        arst_i  => rstlogic_arst,
        clks_i (0) => clk_sys_125m,
        clks_i (1) => clk_250m,
        clks_i (2) => clk_sys_62m5,
        rst_n_o (0) => rst_sys_125m_n,
        rst_n_o (1) => rst_clk_250m_n,
        rst_n_o (2) => rst_sys_62m5_n
        );

    --  ==========================================
    --    SFP   L2     L6     L10    L14
    --          L0     L4     L8     L12
    led_o(0) <= '0';
    led_o(1) <= rst_pl_n; -- clk_locked;

    led_o(2) <= '0';
    led_o(3) <= rst_pl; -- rst_sys_62m5_n;

    process(clk_pl)
      variable cnt : unsigned(22 downto 0); -- natural range 0 to 2**23-1;
    begin
      led_o(4) <= '0';
      if rising_edge(clk_pl) then
--        if rst_pl_n = '0' then
--          cnt := 0;
--          led_o(5) <= '0';
--        else
          cnt := cnt + 1;
          if cnt = 2_500_000 then
            led_o(5) <= '1';
          elsif cnt = 5_000_000 then
            led_o(5) <= '0';
            cnt := (others => '0');
          end if;
--        end if;
      end if;
    end process;

    process(clk)
      variable cnt : natural;
    begin
      led_o(6) <= '0';
      if rising_edge(clk) then
        if rst_n = '0' then
          cnt := 0;
          led_o(7) <= '0';
        else
          cnt := cnt + 1;
          if cnt = 5_000_000 then
            led_o(7) <= '1';
          elsif cnt = 10_000_000 then
            led_o(7) <= '0';
            cnt := 0;
          end if;
        end if;
      end if;
    end process;

    process(clk_mon)
      variable cnt : unsigned(25 downto 0);
    begin
      led_o(8) <= '0';
      led_o(10) <= '0';

      led_o(12) <= '0'; -- Yellow
      led_o(13) <= '1';

      led_o(14) <= '1'; -- Green
      led_o(15) <= '0';
      if rising_edge(clk_mon) then
          led_o(9) <= cnt(24);
          led_o(11) <= cnt(25);
          cnt := cnt + 1;
      end if;
    end process;
  end block;
  
  b_wr: block
    constant g_diag_id             : integer := 0;
    constant g_diag_ver            : integer := 0;
    constant g_diag_ro_size        : integer := 0;
    constant g_diag_rw_size        : integer := 0;
    constant c_streamers_diag_id  : integer := 1;  -- id reserved for streamers
    constant c_streamers_diag_ver : integer := 2;  -- version that will be probably increased
--    constant c_diag_id            : integer := f_pick_diag_val(g_fabric_iface,  c_streamers_diag_id,         g_diag_id);
--    constant c_diag_ver           : integer := f_pick_diag_val(g_fabric_iface,  c_streamers_diag_ver,        g_diag_id);
    constant c_diag_ro_size       : integer := 0; --f_pick_diag_size(g_fabric_iface, c_WR_STREAMERS_ARR_SIZE_OUT, g_diag_ro_size);
    constant c_diag_rw_size       : integer := 0; --f_pick_diag_size(g_fabric_iface, c_WR_STREAMERS_ARR_SIZE_IN,  g_diag_rw_size);

    signal phy16_out :t_phy_16bits_from_wrc;
    signal phy16_in :t_phy_16bits_to_wrc;

    signal sfp_scl_out, sfp_scl_in : std_logic;
    signal sfp_sda_out, sfp_sda_in : std_logic;

    signal eeprom_scl_out, eeprom_scl_in : std_logic;
    signal eeprom_sda_out, eeprom_sda_in : std_logic;

    signal wb_aux_out : t_wishbone_master_out;
    signal wb_aux_in  : t_wishbone_master_in;

    signal dac_mpll_val, dac_hpll_val : std_logic_vector(15 downto 0);
    signal dac_mpll_wr, dac_hpll_wr : std_logic;

    signal rst_sys_62m5 : std_logic;

    signal plldac_sclk, plldac_din, plldac_sync_n : std_logic;

    -- WR SNMP
    signal aux_diag_in  : t_generic_word_array(c_diag_ro_size-1 downto 0);
    signal aux_diag_out : t_generic_word_array(c_diag_rw_size-1 downto 0);
  begin
    rst_sys_62m5 <= not rst_sys_62m5_n;

    cmp_gth: entity work.wr_gthe4_phy_family7_xilinx_ip_pxie
      generic map (
        g_simulation         => g_simulation,
        g_use_gclk_as_refclk => false)
      port map (
        clk_gth_i      => gth_clk_250m,
        clk_freerun_i  => clk_sys_62m5,
        por_i => rst_sys_62m5,
        tx_out_clk_o   => phy16_in.ref_clk,
        tx_locked_o    => open,
        tx_data_i      => phy16_out.tx_data,
        tx_k_i         => phy16_out.tx_k,
        tx_disparity_o => phy16_in.tx_disparity,
        tx_enc_err_o   => phy16_in.tx_enc_err,
        rx_rbclk_o     => phy16_in.rx_clk,
        rx_data_o      => phy16_in.rx_data,
        rx_k_o         => phy16_in.rx_k,
        rx_enc_err_o   => phy16_in.rx_enc_err,
        rx_bitslide_o  => phy16_in.rx_bitslide,
        rst_i          => phy16_out.rst,
        loopen_i       => "000",
        debug_i        => x"0000",
        debug_o        => open,
        pad_txn_o      => sfp_txn_o,
        pad_txp_o      => sfp_txp_o,
        pad_rxn_i      => sfp_rxn_i,
        pad_rxp_i      => sfp_rxp_i,
        rdy_o          => phy16_in.rdy,
        
        clk_mon_o => clk_mon);

    phy16_in.sfp_tx_fault <= '0'; --  not connected
    phy16_in.sfp_los      <= sfp_los_i;
    sfp_tx_disable_o     <= phy16_out.sfp_tx_disable;

    inst_WR_CORE : entity work.xwr_core
    generic map(
        g_simulation                     => g_simulation,
        g_with_external_clock_input      => false,
        --
        g_board_name                     => "PXEN",
        --g_ram_address_space_size_kb    => 256,
        g_phys_uart                      => true,  --  Is false OK ?
        g_virtual_uart                   => true,
        g_vuart_fifo_size                => 1024,
        g_aux_clks                       => 0,
--        g_softpll_reverse_dmtds          => true,
        g_ep_rxbuf_size                  => 1024,
        g_tx_runt_padding                => true,
        g_records_for_phy                => true,
        g_pcs_16bit                      => true,
        g_dpram_initf                    => g_wrpc_initf,
        g_dpram_size                     => (131072+65536)/4,
        g_interface_mode                 => PIPELINED,
        g_address_granularity            => BYTE,
        g_aux_sdb                        => c_wrc_periph3_sdb,
        g_softpll_enable_debugger        => true,
--        g_softpll_use_sampled_ref_clocks => true,
        g_diag_id                        => 0,
        g_diag_ver                       => 0,
        g_diag_ro_size                   => 0,
        g_diag_rw_size                   => 0,
        g_hwbld_date                     => g_hwbld_date)
    port map(
        clk_sys_i              => clk_sys_62m5,
        clk_dmtd_i             => clk_dmtd,
        clk_ref_i              => clk_sys_62m5,
        --  No external 10Mhz
        clk_ext_i              => open,
        clk_ext_mul_i          => open,
        clk_ext_stopped_i      => open,
        clk_ext_mul_locked_i   => open,
        clk_ext_rst_o          => open,
        clk_aux_i              => open,
        clk_dmtd_over_i        => open,
        pps_ext_i              => open,
        rst_n_i                => rst_sys_62m5_n,

        dac_hpll_load_p1_o     => dac_hpll_wr,
        dac_hpll_data_o        => dac_hpll_val,
        dac_dpll_load_p1_o     => dac_mpll_wr,
        dac_dpll_data_o        => dac_mpll_val,

        phy16_i                => phy16_in,
        phy16_o                => phy16_out,
        phy_mdio_master_o      => open,
        phy_mdio_master_i      => open,
        phy_ref_clk_i          => open,
        led_act_o              => sfp_led_act_o,
        led_link_o             => sfp_led_link_o,
        scl_o                  => eeprom_scl_out,
        scl_i                  => eeprom_scl_in,
        sda_o                  => eeprom_sda_out,
        sda_i                  => eeprom_sda_in,
        sfp_scl_o              => sfp_scl_out,
        sfp_scl_i              => sfp_scl_in,
        sfp_sda_o              => sfp_sda_out,
        sfp_sda_i              => sfp_sda_in,
        sfp_det_i              => sfp_det_i,
        spi_sclk_o             => open,
        spi_ncs_o              => open,
        spi_mosi_o             => open,
        spi_miso_i             => open,
        uart_rxd_i             => uart_rxd_i,
        uart_txd_o             => uart_txd_o,
        owr_pwren_o            => open,
        owr_en_o               => open,
        owr_i                  => open,

        slave_i                => wr_master_out,
        slave_o                => wr_master_in,
        aux_master_o           => wb_aux_out,
        aux_master_i           => wb_aux_in,

        wrf_src_i => eth_rx_out,
        wrf_src_o => eth_rx_in,
        wrf_snk_i => eth_tx_out,
        wrf_snk_o => eth_tx_in,
  
        timestamps_o           => open,
        timestamps_ack_i       => '1',
        fc_tx_pause_req_i      => '0',
        fc_tx_pause_delay_i    => x"0000",
        fc_tx_pause_ready_o    => open,

        tm_link_up_o => tm_link_up,
        tm_time_valid_o => tm_time_valid,
        tm_tai_o => tm_tai,
        tm_cycles_o => tm_cycles,
  
        tm_dac_value_o         => open,
        tm_dac_wr_o            => open,
        tm_clk_aux_lock_en_i   => (others => '1'),
        tm_clk_aux_locked_o    => open,

        abscal_rxts_o          => open,

        pps_p_o                => pps_p,
        pps_valid_o            => pps_valid,
        pps_led_o              => open,
        pps_csync_o            => open,
        rst_aux_n_o            => open,
        aux_diag_i             => aux_diag_in,
        aux_diag_o             => aux_diag_out,

        link_ok_o              => open);

        sfp_scl_b <= '0' when sfp_scl_out = '0' else 'Z';
        sfp_sda_b <= '0' when sfp_sda_out = '0' else 'Z';
        sfp_scl_in <= sfp_scl_b;
        sfp_sda_in <= sfp_sda_b;
      
        eeprom_scl_b <= '0' when eeprom_scl_out = '0' else 'Z';
        eeprom_sda_b <= '0' when eeprom_sda_out = '0' else 'Z';
        eeprom_scl_in <= eeprom_scl_b;
        eeprom_sda_in <= eeprom_sda_b;
      
    inst_serial_dac: entity work.serial_dac856x
      generic map (
        g_sclk_div => 7
        )
      port map (
        clk_i => clk_sys_62m5,
        rst_n_i => rst_sys_62m5_n,
        value_a_i => dac_mpll_val,
        wr_a_i => dac_mpll_wr,
        value_b_i => dac_hpll_val,
        wr_b_i => dac_hpll_wr,
        sclk_o => plldac_sclk,
        d_o => plldac_din,
        sync_n_o => plldac_sync_n
        );

    g_ila: if false generate
      inst_ila : ila_0
        port map (
          clk => clk_sys_62m5,
          probe0(0) => plldac_sclk,
          probe0(1) => plldac_din,
          probe0(2) => plldac_sync_n,
          probe0(3) => dac_mpll_wr,
          probe0(4) => dac_hpll_wr,
          probe0(15 downto 5) => dac_mpll_val(15 downto 5),
          probe0(31 downto 16) => dac_hpll_val
          );
    end generate g_ila;
      
    plldac_sclk_o <= plldac_sclk;
    plldac_din_o <= plldac_din;
    plldac_sync_n_o <= plldac_sync_n;

    plldac_clr_n_o <= '1';
    --  Synchronous ldac, ldac remains low.
    plldac_ldac_n_o <= '0';

    -- WR PLL I2C
    wr_clk_sync_n_o <= '1';
    wr_clk_rst_n_o <= '1';
  end block;

  b_i2c: block
  begin
    inst_I2C0_scl_iobuf: component IOBUF
      port map (
        I => iic0_scl_out,
        IO => i2c_scl_b,
        O => iic0_scl_in,
        T => iic0_scl_tri
        );
    inst_I2C0_sda_iobuf: component IOBUF
      port map (
        I => iic0_sda_out,
        IO => i2c_sda_b,
        O => iic0_sda_in,
        T => iic0_sda_tri
        );
  end block;

  pps_p_o <= pps_p;

  --  DAC
  b_dac: block
    signal dac_idata, dac_qdata : unsigned (15 downto 0);
  begin
    process (clk_250m)
    begin
      if rising_edge(clk_250m) then
        if rst_clk_250m_n = '0' then
          dac_idata <= x"4000";
          dac_qdata <= x"0000";
        else
          dac_idata <= dac_idata + 1;
          dac_qdata <= dac_qdata + 1;
        end if;
      end if;
    end process;
    
    inst_iqdac: entity work.iqdac_io
      generic map (
        dpins => 16)
      port map (
        clk_i => clk_250m,
        reset_io_i => '0',
        iqdac_idata_i => std_logic_vector(dac_idata),
        iqdac_qdata_i => std_logic_vector(dac_qdata),
        iqdac_data_p_o => dac_data_p_o,
        iqdac_data_n_o => dac_data_n_o,
        iqdac_dc_p_o   => dac_dci_p_o,
        iqdac_dc_n_o   => dac_dci_n_o);

    --  TODO: DAC control (SPI)
    dac_reset_o <= '0';
    dac_sclk_o  <= '0';
    dac_cs_n_o  <= '1';
    dac_sdi_o   <= '0';
  end block;

  b_bst: block
    signal bst_out_ddr : std_logic_vector(1 downto 0);
    signal ttc_data, ttc_addr : std_logic_vector(7 downto 0);
    signal frev, load : std_logic;
    signal count : unsigned(10 downto 0);
    signal clk_fb, clk_80m_int, clk_locked, clk_locked_n, clk_80m, rst_80m_n : std_logic;
  begin
    --  Input: clk_250, fvco=1Ghz
    inst_MMCME4_BASE : MMCME4_BASE
      generic map (
        BANDWIDTH => "OPTIMIZED",-- Jitter programming
        CLKFBOUT_MULT_F => 4.0,  -- Multiply value for all CLKOUT
        CLKFBOUT_PHASE => 0.0,  -- Phase offset in degrees of CLKFB
        CLKIN1_PERIOD => 4.0,   -- Input clock period in ns to ps resolution (i.e., 33.333 is 30 MHz).
        CLKOUT0_DIVIDE_F => 12.5, -- 80Mhz
        CLKOUT0_DUTY_CYCLE => 0.5,
        CLKOUT0_PHASE => 0.0,
        DIVCLK_DIVIDE => 1,
        IS_CLKFBIN_INVERTED => '0',
        IS_CLKIN1_INVERTED => '0',
        IS_PWRDWN_INVERTED => '0',
        IS_RST_INVERTED => '0',
        REF_JITTER1 => 0.0,
        STARTUP_WAIT => "FALSE"
        )
      port map (
        CLKFBOUT => clk_fb,
        CLKFBOUTB => open,
        CLKOUT0 => clk_80m_int,
        CLKOUT0B => open,
        LOCKED => clk_locked,
        CLKFBIN => clk_fb,
        CLKIN1 => clk_gth_250m_bufgt,
        PWRDWN => '0',
        RST => rst_pl
        );

    inst_bufg_80m: BUFG
      port map (
        I => clk_80m_int,
        O => clk_80m
        );

    clk_locked_n <= not clk_locked;

    inst_rstlogic_reset : entity work.gc_reset_multi_aasd
      generic map (
        g_CLOCKS  => 1,   -- 62.5MHz, 125MHz
        g_RST_LEN => 16)  -- 16 clock cycles
      port map (
        arst_i  => clk_locked_n,
        clks_i (0) => clk_80m,
        rst_n_o (0) => rst_80m_n
        );

    inst_biphase: entity work.ttc_frametx
      port map (
        clk_i => clk_80m,
        rst_n_i => rst_80m_n,
        out_o => bst_out_ddr,
        data_i => ttc_data,
        subaddr_i => ttc_addr,
        frev_i => frev,
        load_imm_i => load,
        rdy_i => '0',
        ack_o => open
      );
    
    process (clk_80m)
    begin
      if rising_edge(clk_80m) then
        load <= '0';
        frev <= '0';
        if rst_80m_n = '0' then
          count <= (others => '0');
          ttc_data <= x"ff";
          ttc_addr <= x"00";
        else
          if count = 923 * 2 then
            load <= '1';
            frev <= '1';
            count <= (others => '0');
            ttc_addr <= std_logic_vector(unsigned(ttc_addr) + 1);
            ttc_data <= not ttc_addr;
          else
            count <= count + 1;
          end if;
        end if;
      end if;
    end process;

    inst_ODDR : ODDRE1
      generic map (
        IS_C_INVERTED => '0', -- Optional inversion for C
        SIM_DEVICE => "ULTRASCALE_PLUS",
        SRVAL => '0'
        )
      port map (
        Q => bst_out,
        C => clk_80m,
        D1 => bst_out_ddr(1),
        D2 => bst_out_ddr(0),
        SR => '0'
        );
  end block;

  spi_miso <= '0';

  --  ==========================================
  --    SFP   0     2     4    6
  --          1     3     5    7
  g_trigio_dbg: if false generate
    trigio_out_o (0) <= spi_sck;
    trigio_out_o (1) <= spi_ss0;
    trigio_out_o (2) <= spi_mosi;
    trigio_out_o (3) <= spi_ss1;
    trigio_out_o (4) <= clk_helper_62m5;
    trigio_out_o (5) <= '0';
    trigio_out_o (6) <= clk_dmtd;
    trigio_out_o (7) <= '0';
    trigio_oe_n_o <= (others => '0');
    trigio_term_o <= (others => '0');
  end generate;

  g_trigio: if true generate
    trigio_out_o <= lemo_out(7 downto 0);
    trigio_oe_n_o <= not lemo_oe (7 downto 0); --  Invert so that default is input, 1 is output
    trigio_term_o <= lemo_term(7 downto 0);
  end generate;

  g_trigio_bst: if false generate
    trigio_out_o (7) <= bst_out;
    trigio_oe_n_o (7) <= '0';
    trigio_out_o (6 downto 0) <= lemo_out(6 downto 0);
    trigio_oe_n_o (6 downto 0) <= not lemo_oe (6 downto 0); --  Invert so that default is input, 1 is output
    trigio_term_o <= lemo_term(7 downto 0);
  end generate;

end architecture arch;
