library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.wishbone_pkg.all;
use work.pulser_pkg.all;
use work.pulser_group_map_consts_pkg.all;

entity tb_timing is
end tb_timing;

architecture arch of tb_timing is
  signal rst_n, clk_1g, clk_500m, clk_125m, clk_62m5 : std_logic := '0';
  signal wb_in : t_wishbone_slave_in;
  signal wb_out : t_wishbone_slave_out;
  signal inputs : t_subpulse_array(7 downto 0);
  signal comb_outputs : t_slv8_array(7 downto 0);
  signal outputs : std_logic_vector(7 downto 0);

  --  t0 is used for synchro
  constant tm_t0 : std_logic_vector(31 downto 0) := x"0000_0124"; --   t0 mod 4 = 0
  constant tm_t1 : std_logic_vector(31 downto 0) := tm_t0 or x"0000_0001";

  signal tm_tai_raw : std_logic_vector(39 downto 0) := x"00" & tm_t0;
  signal tm_cycles_raw : std_logic_vector(27 downto 0) := std_logic_vector(to_unsigned(62_499_960, 28));

  signal sync_62m5 : std_logic;
  signal tm_tai, tm_load_tai : std_logic_vector(31 downto 0);
  signal tm_cycles : std_logic_vector(25 downto 0);
  signal tm_load_steps : std_logic_vector(25 downto 5);
  signal tm_sync, pre_pps : std_logic;

  signal log_load, log_shift : std_logic;

  signal clock_1mhz : t_subpulse;
  signal clocks : t_subpulse_array(7 downto 0);
begin
  rst_n <= '0', '1' after 16 ns;

  --  Clocking
  p_clk: process
  begin
    clk_62m5 <= not clk_62m5;
    for i in 1 to 2 loop
      clk_125m <= not clk_125m;
      for j in 1 to 4 loop
        clk_500m <= not clk_500m;
        for k in 1 to 2 loop
          clk_1g <= not clk_1g;
          wait for 500 ps;
        end loop;
       end loop;
    end loop;
  end process;

  p_tai: process (clk_62m5)
  begin
    if rising_edge(clk_62m5) then
      if unsigned (tm_cycles_raw) = 62_499_999 then
        tm_cycles_raw <= (others => '0');
        tm_tai_raw <= std_logic_vector(unsigned(tm_tai_raw) + 1);
      else
        tm_cycles_raw <= std_logic_vector(unsigned(tm_cycles_raw) + 1);
      end if;
    end if;
  end process;

  p_out: process
    variable pre_outputs : t_slv8_array(7 downto 0);
  begin
    wait until rising_edge(clk_125m);
      --report "gen output";
      pre_outputs := comb_outputs;
      for i in 0 to 7 loop
        for k in 0 to 7 loop
          outputs (k) <= pre_outputs (k)(0);
          pre_outputs (k) := '0' & pre_outputs (k)(7 downto 1);
        end loop;
        if i /= 7 then
          wait until rising_edge(clk_1g);
        end if;
      end loop;
  end process;

  p_measure: process(clk_1g)
    variable cur, prev : std_logic;
    variable t_h : time := 0 ns;
  begin
    if rising_edge(clk_1g) then
      cur := outputs (4);
      if cur = '1' and prev = '0' then
        --  Rising edge.
        report "period=" & time'image(now - t_h);
        t_h := now;
      elsif cur = '0' and prev = '1' then
        --  falling edge
        report "high=" & time'image(now - t_h);
      end if;
      prev := cur;
    end if;
  end process;

  inst_clockgen: entity work.timegen
    port map (
      clk_62m5_i => clk_62m5,
      clk_125m_i => clk_125m,
      rst_n_i => rst_n,
      tm_tai_i => tm_tai_raw,
      tm_cycles_i => tm_cycles_raw (25 downto 0),
      tm_valid_i => '1',
      sync_62m5_o => sync_62m5,
      pre_pps_o => pre_pps,
      tm_tai_o => tm_tai,
      tm_cycles_o => tm_cycles,
      tm_sync_o => tm_sync,
      tm_load_tai_o => tm_load_tai,
      tm_load_steps_o => tm_load_steps
      );

  inst_1mhz: entity work.freqgen
      generic map (
        g_period => 1_000
        )
      port map (
        clk_125m_i => clk_125m,
        pps_i => pre_pps,
        clk_o => clocks (2),
        pulse_o => open
        );

  inst_timing: entity work.pulser_group
    generic map (
      C_NBR_PULSER => comb_outputs'length
    )
    port map (
      rst_n_i => rst_n,
      clk_pg_i => clk_125m,
      clk_cmp_i => clk_62m5,
      clk_wb_i => clk_62m5,
      sync_62m5_i => sync_62m5,
      wb_i => wb_in,
      wb_o => wb_out,
      tm_tai_i => tm_tai,
      tm_cycles_i => tm_cycles,
      tm_sync_i => tm_sync,
      tm_load_tai_i => tm_load_tai,
      tm_load_steps_i => tm_load_steps,
      inputs_i => inputs,
      clocks_i => clocks,
      comb_outputs_o => comb_outputs,
      log_load_i => log_load,
      log_shift_i => log_shift
    );

  clocks (1 downto 0) <= (others => ('0', "000"));
  clocks (7 downto 3) <= (others => ('0', "000"));

  inputs <= (others => ('0', "000"));

  p_log: process
  begin
    log_load <= '0';
    log_shift <= '0';

    loop
      wait until rising_edge(clk_125m);
      exit when pre_pps = '1';
    end loop;

    loop
      log_load <= '1';
      log_shift <= '1';

      wait until rising_edge(clk_125m);
      log_load <= '0';

      for i in 1 to 8 loop
        wait until rising_edge(clk_125m);
      end loop;

      log_shift <= '0';

      for i in 1 to 8 loop
        wait until rising_edge(clk_125m);
      end loop;
    end loop;

    wait;
  end process;
  
  p_test: process
    procedure write_wb(addr : natural; val : std_logic_vector(31 downto 0)) is
    begin
      wb_in.adr <= std_logic_vector(to_unsigned(addr, 32));
      wb_in.dat <= val;
      wb_in.we <= '1';
      wb_in.cyc <= '1';
      wb_in.stb <= '1';
      wb_in.sel <= "1111";
      loop
        wait until rising_edge (clk_62m5);
        exit when wb_out.ack = '1';
      end loop;
      wb_in.cyc <= '0';
      wb_in.stb <= '0';
    end write_wb;

    procedure read_wb(addr : natural; val : out std_logic_vector(31 downto 0)) is
    begin
      wb_in.adr <= std_logic_vector(to_unsigned(addr, 32));
      wb_in.dat <= (others => 'X');
      wb_in.we <= '0';
      wb_in.cyc <= '1';
      wb_in.stb <= '1';
      wb_in.sel <= "1111";
      loop
        wait until rising_edge (clk_62m5);
        exit when wb_out.ack = '1';
      end loop;
      val := wb_out.dat;
      wb_in.cyc <= '0';
      wb_in.stb <= '0';
    end read_wb;

    variable v : std_logic_vector(31 downto 0);

    constant input_0  : std_logic_vector(7 downto 0) := b"000_01000";
    constant input_1  : std_logic_vector(7 downto 0) :=
      std_logic_vector(to_unsigned(comb_outputs'length + inputs'length + 1, 8));
    constant no_clock : std_logic_vector(7 downto 0) := b"000_11111";
    constant no_start : std_logic_vector(7 downto 0) := b"000_11111";
    constant no_stop  : std_logic_vector(7 downto 0) := b"000_11111";
    constant clk_1m   : std_logic_vector(7 downto 0) := b"000_10010";

    constant c_COMPARATOR_17_ADDR : natural := c_PULSER_GROUP_MAP_COMPARATORS_ADDR + 17*32;
    constant c_COMPARATOR_3_ADDR : natural := c_PULSER_GROUP_MAP_COMPARATORS_ADDR + 3*32;
  begin
    wb_in.cyc <= '0';
    wb_in.stb <= '0';
    wait until rising_edge(clk_62m5) and rst_n = '1';

    report "config comb-output";

    --  Configure comb-output 4 (pulser 4)
    write_wb (c_PULSER_GROUP_MAP_OUT_CFG_ADDR + 4 * c_PULSER_GROUP_MAP_OUT_CFG_SIZE,
              x"00_00_00_10");
    --  and 5 (pulser 0)
    write_wb (c_PULSER_GROUP_MAP_OUT_CFG_ADDR + 5 * c_PULSER_GROUP_MAP_OUT_CFG_SIZE,
              x"00_00_00_01");

    report "config comparator";

    --  Configure comparator 17.
    write_wb (c_COMPARATOR_17_ADDR + c_PULSER_GROUP_MAP_COMPARATORS_TIME_SEC_ADDR,
              tm_t1);
    write_wb (c_COMPARATOR_17_ADDR + c_PULSER_GROUP_MAP_COMPARATORS_TIME_NSEC_ADDR,
              x"000002_12"); -- ns (21 + 9 bits)
    -- Start, Stop, clock, Pulser = 4
    v := input_0 & no_stop & no_clock & x"04";
    v(c_PULSER_GROUP_MAP_COMPARATORS_CONF1_REPEAT_OFFSET) := '1';
    write_wb (c_COMPARATOR_17_ADDR + c_PULSER_GROUP_MAP_COMPARATORS_CONF1_ADDR,
              v);
    write_wb (c_COMPARATOR_17_ADDR + c_PULSER_GROUP_MAP_COMPARATORS_HIGH_ADDR,
              x"0000_0008");
    write_wb (c_COMPARATOR_17_ADDR + c_PULSER_GROUP_MAP_COMPARATORS_PERIOD_ADDR,
              x"0000_007d");
    write_wb (c_COMPARATOR_17_ADDR + c_PULSER_GROUP_MAP_COMPARATORS_NPULSES_ADDR,
              x"0000_000a");
    write_wb (c_COMPARATOR_17_ADDR + c_PULSER_GROUP_MAP_COMPARATORS_IDELAY_ADDR,
              x"0000_0000");

    --  Configure comparator 3
    write_wb (c_COMPARATOR_3_ADDR + c_PULSER_GROUP_MAP_COMPARATORS_TIME_SEC_ADDR, tm_t1);
    write_wb (c_COMPARATOR_3_ADDR + c_PULSER_GROUP_MAP_COMPARATORS_TIME_NSEC_ADDR, x"000002_10");
    -- Start = 1, Stop = 0, Enable = 1, Pulser = 0
    write_wb (c_COMPARATOR_3_ADDR + c_PULSER_GROUP_MAP_COMPARATORS_CONF1_ADDR,
              no_start & no_stop & clk_1m & x"00");
    write_wb (c_COMPARATOR_3_ADDR + c_PULSER_GROUP_MAP_COMPARATORS_HIGH_ADDR, x"0000_0014");
    write_wb (c_COMPARATOR_3_ADDR + c_PULSER_GROUP_MAP_COMPARATORS_PERIOD_ADDR, x"0000_0001");
    write_wb (c_COMPARATOR_3_ADDR + c_PULSER_GROUP_MAP_COMPARATORS_NPULSES_ADDR, x"0000_0005");
    write_wb (c_COMPARATOR_3_ADDR + c_PULSER_GROUP_MAP_COMPARATORS_IDELAY_ADDR, x"0000_0000");

    --  Enable comparator 17 & 3
    write_wb (c_PULSER_GROUP_MAP_COMP_STATUS_ADDR, x"0000_0008");

    report "setup done";
    --  TODO: expect pulses.
    assert comb_outputs(4) = x"00";
    wait until comb_outputs(4) /= x"00";
    report "pulse 4 detected";
    assert tm_tai = x"00" & tm_t1 report "Incorrect tai sec for pulse 4";
    assert tm_cycles = std_logic_vector(to_unsigned(1 * 125 + 16#12# / 2, 26)) report "incorrect tai cyc for pulse 4";
    for i in 1 to 4 loop
      wait until rising_edge(clk_62m5);
    end loop;
    read_wb (c_PULSER_GROUP_MAP_PULSES_EVNT_STS_ADDR, v);
    assert v(c_PULSER_GROUP_MAP_PULSES_EVNT_STS_NEMPTY_OFFSET) = '1';

    report "end of tests";
    wait;
  end process;
end arch;
