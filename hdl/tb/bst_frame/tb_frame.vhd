library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

entity tb_frame is
end tb_frame;

architecture arch of tb_frame is
  signal clk_80m, clk_160m, rst_80m_n : std_logic := '0';
  signal out_ddr : std_logic_vector(1 downto 0);
  signal output, bst_out : std_logic;
  signal sym : std_logic;
  signal chan_a, chan_b, alt : std_logic := '1';
begin
  --  Clocking
  p_clk: process
  begin
    clk_80m <= not clk_80m;
    for i in 1 to 2 loop
      clk_160m <= not clk_160m;
      wait for 3_125 ps;
    end loop;
  end process;

  rst_80m_n <= '0', '1' after 25 ns;

  --  Output
  process
  begin
    wait until rising_edge(clk_80m);
    output <= out_ddr (0);
    wait until falling_edge(clk_80m);
    output <= out_ddr (1);
  end process;

  my_inst: entity work.ttc_demo
    port map (
      clk_80m_i => clk_80m,
      rst_80m_n_i => rst_80m_n,
      bst_out_ddr_o => out_ddr
    );
 
  sym <= out_ddr(0) xor out_ddr(1);

  process (clk_80m) is
  begin
    if rising_edge(clk_80m) then
      if rst_80m_n = '0' then
        alt <= '0';
      else
        if alt = '0' then
          chan_b <= sym;
        else
          chan_a <= sym;
        end if;
        alt <= not alt;
      end if;
    end if;
  end process;

  b_decode: block
    signal qx_d, qx_e, qx_valid : std_logic;
    signal qx_a, qx_b, qx_e2, qx_sync2, qx_valid2 : std_logic;
    signal qx_data3 : std_logic_vector(31 downto 0);
    signal qx_valid3, qx_err3 : std_logic;
    signal qx_valid4, qx_err4 : std_logic;
    signal qx_addr4, qx_data4 : std_logic_vector(7 downto 0);
    begin
    inst_biphase_decoder: entity work.BiPhaseMark_Decoder
      port map (
        ClkxC => clk_160m,
        ResetxRNA => rst_80m_n,
        DxD => output,
        QxD => qx_d,
        QxE => qx_e,
        ValidQxS => qx_valid);

    inst_demux: entity work.ttcrx_ab_demux
      port map (
        clkxc => clk_160m,
        resetxrna => rst_80m_n,
        dxd => qx_d,
        dxe => qx_e,
        validdxs => qx_valid,
        achanqxd => qx_a,
        bchanqxd => qx_b,
        qxe => qx_e2,
        syncqxs => qx_sync2,
        validqxs => qx_valid2
        );

    inst_decoder: entity work.ttcrx_hamming_decoder
      port map (
        clkxc => clk_160m,
        resetxrna => rst_80m_n,
        dxd => qx_b,
        dxe => qx_e2,
        validdxs => qx_valid2,
        syncdxs => qx_sync2,
        qxd => qx_data3,
        validqxs => qx_valid3,
        errorqxs => qx_err3
        );

        inst_frame: entity work.ttcrx_frame_decoder
          port map (
            clkxc => clk_160m,
            resetxrna => rst_80m_n,
            dxe => qx_e2,
            dxd => qx_data3,
            validdxd => qx_valid3,
            errordxs => qx_err3,
            subaddrqxd => qx_addr4,
            cmddataqxd => qx_data4,
            validqxs => qx_valid4,
            errorqxs => qx_err4
          );
  end block;
  
  inst_ODDR : ODDRE1
    generic map (
      IS_C_INVERTED => '0', -- Optional inversion for C
      SIM_DEVICE => "ULTRASCALE_PLUS",
      SRVAL => '0'
      )
    port map (
      Q => bst_out,
      C => clk_80m,
      D1 => out_ddr(1),
      D2 => out_ddr(0),
      SR => '0'
      );
end arch;
