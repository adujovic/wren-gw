##################
# Clocks
##################
create_clock -period 8.000 -name fpga_pl_clksys_125m -waveform {0.000 4.000} [get_ports fpga_pl_clksys_p_i]

create_clock -period 8.000 -name wr_clk_helper_125m -waveform {0.000 4.000} [get_ports wr_clk_helper_125m_p_i]
create_clock -period 40.000 -name clk_helper_25m -waveform {0.000 20.000} [get_ports clk_helper_25m_i]
create_clock -period 8.000 -name wr_clk_main_125m -waveform {0.000 4.000} [get_ports wr_clk_main_125m_p_i]
create_clock -period 4.000 -name wr_clk_sfp_125m [get_ports wr_clk_sfp_125m_p_i]

create_generated_clock -name clk_sys_62m5 [get_pins blk_clock.inst_MMCME4_BASE/CLKOUT0]

create_generated_clock -name gth_rxclk [get_pins {b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_channel_container[1].gen_enabled_channel.gthe4_channel_wrapper_inst/channel_inst/gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST/RXOUTCLK}]

create_generated_clock -name gth_txclk [get_pins {b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_channel_container[1].gen_enabled_channel.gthe4_channel_wrapper_inst/channel_inst/gthe4_channel_gen.gen_gthe4_channel_inst[0].GTHE4_CHANNEL_PRIM_INST/TXOUTCLK}]

# All the clocks are asynch
set_clock_groups -asynchronous -group {wr_clk_main_125m fpga_pl_clksys_125m wr_clk_sfp_125m} -group wr_clk_helper_125m -group clk_helper_25m -group gth_rxclk -group gth_txclk -group clk_sys_62m5

# FIXME
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets blk_clock.inst_ibuf_helper25m/O]

##################
# I/O constraints
##################

# clock (125Mhz ?)
set_property PACKAGE_PIN D4 [get_ports fpga_pl_clksys_p_i]
set_property IOSTANDARD LVDS [get_ports fpga_pl_clksys_p_i]

# reset
#set_property PACKAGE_PIN P16 [get_ports ps_por_i]
#set_property IOSTANDARD LVCMOS33 [get_ports ps_por_i]

#uart (WR)
set_property IOSTANDARD LVCMOS33 [get_ports uart_rxd_i]
set_property IOSTANDARD LVCMOS33 [get_ports uart_txd_o]
set_property PACKAGE_PIN L14 [get_ports uart_rxd_i]
set_property PACKAGE_PIN K14 [get_ports uart_txd_o]

# PLL I2C (from pl)
set_property IOSTANDARD LVCMOS33 [get_ports i2c_sda_b]
set_property IOSTANDARD LVCMOS33 [get_ports i2c_scl_b]
set_property SLEW FAST [get_ports i2c_sda_b]
set_property SLEW FAST [get_ports i2c_scl_b]
set_property DRIVE 8 [get_ports i2c_sda_b]
set_property DRIVE 8 [get_ports i2c_scl_b]
set_property PACKAGE_PIN AA12 [get_ports i2c_scl_b]
set_property PACKAGE_PIN W11 [get_ports i2c_sda_b]
set_property PULLUP true [get_ports i2c_scl_b]
set_property PULLUP true [get_ports i2c_sda_b]

# PLL control
set_property PACKAGE_PIN B9 [get_ports wr_clk_sync_n_o]
set_property PACKAGE_PIN C9 [get_ports wr_clk_rst_n_o]
set_property IOSTANDARD LVCMOS18 [get_ports wr_clk_sync_n_o]
set_property IOSTANDARD LVCMOS18 [get_ports wr_clk_rst_n_o]

# wr eeprom
set_property IOSTANDARD LVCMOS18 [get_ports eeprom_sda_b]
set_property IOSTANDARD LVCMOS18 [get_ports eeprom_scl_b]
set_property PACKAGE_PIN D9 [get_ports eeprom_scl_b]
set_property PACKAGE_PIN E9 [get_ports eeprom_sda_b]

# UserLEDs
set_property IOSTANDARD LVCMOS33 [get_ports {led_o[*]}]
set_property SLEW SLOW [get_ports {led_o[*]}]
set_property PACKAGE_PIN E13 [get_ports {led_o[0]}]
set_property PACKAGE_PIN A14 [get_ports {led_o[1]}]
set_property PACKAGE_PIN G13 [get_ports {led_o[2]}]
set_property PACKAGE_PIN B13 [get_ports {led_o[3]}]
set_property PACKAGE_PIN H14 [get_ports {led_o[4]}]
set_property PACKAGE_PIN A13 [get_ports {led_o[5]}]
set_property PACKAGE_PIN F13 [get_ports {led_o[6]}]
set_property PACKAGE_PIN C14 [get_ports {led_o[7]}]
set_property PACKAGE_PIN G14 [get_ports {led_o[8]}]
set_property PACKAGE_PIN E14 [get_ports {led_o[9]}]
set_property PACKAGE_PIN J14 [get_ports {led_o[10]}]
set_property PACKAGE_PIN D14 [get_ports {led_o[11]}]
set_property PACKAGE_PIN D15 [get_ports {led_o[12]}]
set_property PACKAGE_PIN F15 [get_ports {led_o[13]}]
set_property PACKAGE_PIN G15 [get_ports {led_o[14]}]
set_property PACKAGE_PIN E15 [get_ports {led_o[15]}]


# SFP GT
set_property PACKAGE_PIN P2 [get_ports sfp_rxp_i]
set_property PACKAGE_PIN N4 [get_ports sfp_txp_o]

# SFP i2c
set_property IOSTANDARD LVCMOS33 [get_ports sfp_sda_b]
set_property PACKAGE_PIN B15 [get_ports sfp_sda_b]
set_property PACKAGE_PIN A15 [get_ports sfp_scl_b]
set_property IOSTANDARD LVCMOS33 [get_ports sfp_scl_b]

# SFP misc
set_property PACKAGE_PIN L13 [get_ports sfp_los_i];  # RSSI
set_property IOSTANDARD LVCMOS33 [get_ports sfp_los_i]
set_property PACKAGE_PIN C13 [get_ports sfp_tx_disable_o]; # TXDIS
set_property IOSTANDARD LVCMOS33 [get_ports sfp_tx_disable_o]
set_property PACKAGE_PIN H13 [get_ports sfp_det_i];  # MODABS ?
set_property IOSTANDARD LVCMOS33 [get_ports sfp_det_i]

set_property PACKAGE_PIN W12 [get_ports sfp_led_act_o]
set_property IOSTANDARD LVCMOS33 [get_ports sfp_led_act_o]
set_property PACKAGE_PIN Y13 [get_ports sfp_led_link_o]
set_property IOSTANDARD LVCMOS33 [get_ports sfp_led_link_o]

# SFP clock
set_property PACKAGE_PIN V6 [get_ports wr_clk_sfp_125m_p_i]

# WR clocks
set_property PACKAGE_PIN E5 [get_ports wr_clk_helper_125m_p_i]; # and D5
set_property IOSTANDARD LVDS [get_ports wr_clk_helper_125m_p_i]

set_property PACKAGE_PIN D7 [get_ports wr_clk_main_125m_p_i]; # and D6
set_property IOSTANDARD LVDS [get_ports wr_clk_main_125m_p_i]

set_property PACKAGE_PIN AB15 [get_ports clk_helper_25m_i]
set_property IOSTANDARD LVCMOS33 [get_ports clk_helper_25m_i]

# WR dac
set_property PACKAGE_PIN AB10 [get_ports plldac_sclk_o]
set_property PACKAGE_PIN AB9 [get_ports plldac_din_o]
set_property PACKAGE_PIN Y12 [get_ports plldac_sync_n_o]
set_property PACKAGE_PIN AE15 [get_ports plldac_ldac_n_o]
set_property PACKAGE_PIN AB14 [get_ports plldac_clr_n_o]
set_property IOSTANDARD LVCMOS33 [get_ports plldac_sclk_o]
set_property IOSTANDARD LVCMOS33 [get_ports plldac_din_o]
set_property IOSTANDARD LVCMOS33 [get_ports plldac_sync_n_o]
set_property IOSTANDARD LVCMOS33 [get_ports plldac_ldac_n_o]
set_property IOSTANDARD LVCMOS33 [get_ports plldac_clr_n_o]

# pps
set_property PACKAGE_PIN Y9 [get_ports pps_p_o]
set_property IOSTANDARD LVCMOS33 [get_ports pps_p_o]

# DAC
set_property PACKAGE_PIN L1 [get_ports {dac_data_p_o[15]}]
set_property PACKAGE_PIN K1 [get_ports {dac_data_n_o[15]}]
set_property PACKAGE_PIN W8 [get_ports {dac_data_p_o[14]}]
set_property PACKAGE_PIN Y8 [get_ports {dac_data_n_o[14]}]
set_property PACKAGE_PIN U9 [get_ports {dac_data_p_o[13]}]
set_property PACKAGE_PIN V9 [get_ports {dac_data_n_o[13]}]
set_property PACKAGE_PIN U8 [get_ports {dac_data_p_o[12]}]
set_property PACKAGE_PIN V8 [get_ports {dac_data_n_o[12]}]
set_property PACKAGE_PIN R8 [get_ports {dac_data_p_o[11]}]
set_property PACKAGE_PIN T8 [get_ports {dac_data_n_o[11]}]
set_property PACKAGE_PIN J1 [get_ports {dac_data_p_o[10]}]
set_property PACKAGE_PIN H1 [get_ports {dac_data_n_o[10]}]
set_property PACKAGE_PIN R7 [get_ports {dac_data_p_o[9]}]
set_property PACKAGE_PIN T7 [get_ports {dac_data_n_o[9]}]
set_property PACKAGE_PIN R6 [get_ports {dac_data_p_o[8]}]
set_property PACKAGE_PIN T6 [get_ports {dac_data_n_o[8]}]
set_property PACKAGE_PIN P7 [get_ports {dac_data_p_o[7]}]
set_property PACKAGE_PIN P6 [get_ports {dac_data_n_o[7]}]
set_property PACKAGE_PIN M6 [get_ports {dac_data_p_o[6]}]
set_property PACKAGE_PIN L5 [get_ports {dac_data_n_o[6]}]
set_property PACKAGE_PIN J5 [get_ports {dac_data_p_o[5]}]
set_property PACKAGE_PIN J4 [get_ports {dac_data_n_o[5]}]
set_property PACKAGE_PIN H4 [get_ports {dac_data_p_o[4]}]
set_property PACKAGE_PIN H3 [get_ports {dac_data_n_o[4]}]
set_property PACKAGE_PIN N7 [get_ports {dac_data_p_o[3]}]
set_property PACKAGE_PIN N6 [get_ports {dac_data_n_o[3]}]
set_property PACKAGE_PIN K2 [get_ports {dac_data_p_o[2]}]
set_property PACKAGE_PIN J2 [get_ports {dac_data_n_o[2]}]
set_property PACKAGE_PIN N9 [get_ports {dac_data_p_o[1]}]
set_property PACKAGE_PIN N8 [get_ports {dac_data_n_o[1]}]
set_property PACKAGE_PIN M8 [get_ports {dac_data_p_o[0]}]
set_property PACKAGE_PIN L8 [get_ports {dac_data_n_o[0]}]
set_property PACKAGE_PIN L7 [get_ports dac_dci_p_o]
set_property PACKAGE_PIN L6 [get_ports dac_dci_n_o]
set_property IOSTANDARD LVDS [get_ports {dac_data_n_o[*]}]
set_property IOSTANDARD LVDS [get_ports dac_dci_n_o]

set_property PACKAGE_PIN H6 [get_ports dac_sclk_o]
set_property IOSTANDARD LVCMOS18 [get_ports dac_sclk_o]
set_property PACKAGE_PIN H7 [get_ports dac_reset_o]
set_property IOSTANDARD LVCMOS18 [get_ports dac_reset_o]
set_property PACKAGE_PIN J7 [get_ports dac_sdi_o]
set_property IOSTANDARD LVCMOS18 [get_ports dac_sdi_o]
set_property PACKAGE_PIN K7 [get_ports dac_sdo_i]
set_property IOSTANDARD LVCMOS18 [get_ports dac_sdo_i]
set_property PACKAGE_PIN H8 [get_ports dac_cs_n_o]
set_property IOSTANDARD LVCMOS18 [get_ports dac_cs_n_o]

# Trigio_out
set_property IOSTANDARD LVCMOS33 [get_ports {trigio_out_o[*]}]
set_property SLEW FAST [get_ports {trigio_out_o[*]}]
set_property PACKAGE_PIN G11 [get_ports {trigio_out_o[0]}]
set_property PACKAGE_PIN E10 [get_ports {trigio_out_o[1]}]
set_property PACKAGE_PIN F11 [get_ports {trigio_out_o[2]}]
set_property PACKAGE_PIN F10 [get_ports {trigio_out_o[3]}]
set_property PACKAGE_PIN D11 [get_ports {trigio_out_o[4]}]
set_property PACKAGE_PIN D10 [get_ports {trigio_out_o[5]}]
set_property PACKAGE_PIN F12 [get_ports {trigio_out_o[6]}]
set_property PACKAGE_PIN E12 [get_ports {trigio_out_o[7]}]

# Trigio_out_en
set_property IOSTANDARD LVCMOS33 [get_ports {trigio_oe_n_o[*]}]
set_property SLEW SLOW [get_ports {trigio_oe_n_o[*]}]
set_property PACKAGE_PIN C11 [get_ports {trigio_oe_n_o[0]}]
set_property PACKAGE_PIN B10 [get_ports {trigio_oe_n_o[1]}]
set_property PACKAGE_PIN C12 [get_ports {trigio_oe_n_o[2]}]
set_property PACKAGE_PIN A10 [get_ports {trigio_oe_n_o[3]}]
set_property PACKAGE_PIN B11 [get_ports {trigio_oe_n_o[4]}]
set_property PACKAGE_PIN D12 [get_ports {trigio_oe_n_o[5]}]
set_property PACKAGE_PIN A12 [get_ports {trigio_oe_n_o[6]}]
set_property PACKAGE_PIN A11 [get_ports {trigio_oe_n_o[7]}]

# Trigio_term
set_property IOSTANDARD LVCMOS33 [get_ports {trigio_term_o[*]}]
set_property SLEW SLOW [get_ports {trigio_term_o[*]}]
set_property PACKAGE_PIN J11 [get_ports {trigio_term_o[0]}]
set_property PACKAGE_PIN G10 [get_ports {trigio_term_o[1]}]
set_property PACKAGE_PIN H11 [get_ports {trigio_term_o[2]}]
set_property PACKAGE_PIN J10 [get_ports {trigio_term_o[3]}]
set_property PACKAGE_PIN H12 [get_ports {trigio_term_o[4]}]
set_property PACKAGE_PIN J12 [get_ports {trigio_term_o[5]}]
set_property PACKAGE_PIN K12 [get_ports {trigio_term_o[6]}]
set_property PACKAGE_PIN K13 [get_ports {trigio_term_o[7]}]

# Trigio_in
set_property IOSTANDARD LVCMOS18 [get_ports {trigio_in_i[*]}]
set_property PACKAGE_PIN G1 [get_ports {trigio_in_i[0]}]
set_property PACKAGE_PIN G3 [get_ports {trigio_in_i[1]}]
set_property PACKAGE_PIN F2 [get_ports {trigio_in_i[2]}]
set_property PACKAGE_PIN F1 [get_ports {trigio_in_i[3]}]
set_property PACKAGE_PIN E2 [get_ports {trigio_in_i[4]}]
set_property PACKAGE_PIN E1 [get_ports {trigio_in_i[5]}]
set_property PACKAGE_PIN F3 [get_ports {trigio_in_i[6]}]
set_property PACKAGE_PIN D1 [get_ports {trigio_in_i[7]}]

# config options
set_property BITSTREAM.CONFIG.OVERTEMPSHUTDOWN ENABLE [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
# set_property BITSTREAM.GENERAL.JTAG_SYSMON DISABLE [current_design]
set_property BITSTREAM.CONFIG.UNUSEDPIN PULLNONE [current_design]


