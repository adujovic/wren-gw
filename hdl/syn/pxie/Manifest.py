action = "synthesis"

syn_device = "xczu4cg"
syn_grade = "-1-e"
syn_package = "-sfvc784"
syn_top = "wren_pxie_top"
syn_project = "wren-pxie"
syn_tool = "vivado"

target = "xilinx"

# For WR:
# do not use a predefined board
board = 'none'
# do not use a predefined platform
wrcore_platform=False

files = [ 'wren-pxie.xdc', 'gencores_constraints.xdc',
          'ila_0.xci']

modules = {
    'local': ['../../top/pxie'],
    "git" : [
        "https://ohwr.org/project/general-cores.git",
        "https://ohwr.org/project/wr-cores.git",
        "https://ohwr.org/project/urv-core.git",
    ],
    'system': ['xilinx', 'vhdl']
}
