##################
# Clocks
##################
create_clock -period 8.000 -name wr_clk_helper_125m -waveform {0.000  4.000} [get_ports {wr_clk_helper_125m_p_i}]
create_clock -period 8.000 -name wr_clk_main_125m   -waveform {0.000  4.000} [get_ports {wr_clk_main_125m_p_i}]
create_clock -period 8.000 -name wr_clk_sfp_125m    -waveform {0.000  4.000} [get_ports {wr_clk_sfp_125m_p_i}]
create_clock -period 16.000 -name gth_txclk         -waveform {0.000 8.000} [get_nets "cmp_xwrc_board_pxie_fmc/cmp_xwrc_platform/gen_phy_zynqus.cmp_gth/U_gtwizard_gthe4/gtwiz_userclk_tx_usrclk2_out[0]"]
create_clock -period 16.000 -name gth_rxclk         -waveform {0.000 8.000} [get_nets "cmp_xwrc_board_pxie_fmc/cmp_xwrc_platform/gen_phy_zynqus.cmp_gth/U_gtwizard_gthe4/gtwiz_userclk_rx_usrclk2_out[0]"]
       

create_generated_clock -name clk_dmtd -source [get_ports {wr_clk_helper_125m_p_i}] -divide_by 2 [get_pins cmp_xwrc_board_pxie_fmc/cmp_xwrc_platform/gen_default_plls.gen_zynqus_default_plls.cmp_clk_dmtd_buf_o/O]

set_clock_groups -asynchronous -group {wr_clk_main_125m wr_clk_sfp_125m} -group {wr_clk_helper_125m clk_dmtd} -group {gth_txclk} -group {gth_rxclk}


##################
# I/O constraints
##################

# clocks
# set_property PACKAGE_PIN AN17 [get_ports pl_sys_clk_p_i]
# create_clock -period 10.000 -name pl_sys_clk [get_ports pl_sys_clk_p_i]

# reset
set_property PACKAGE_PIN K12 [get_ports ps_por_i]
set_property IOSTANDARD LVCMOS33 [get_ports ps_por_i]

#uart (WR)
set_property IOSTANDARD LVCMOS33 [get_ports uart_rxd_i]
set_property IOSTANDARD LVCMOS33 [get_ports uart_txd_o]
set_property PACKAGE_PIN C14 [get_ports uart_rxd_i]
set_property PACKAGE_PIN C12 [get_ports uart_txd_o]
set_property OFFCHIP_TERM NONE [get_ports uart_txd_o]

# I2C (from pl)
set_property IOSTANDARD LVCMOS12 [get_ports i2c_sda_b]
set_property IOSTANDARD LVCMOS12 [get_ports i2c_scl_b]
set_property SLEW FAST [get_ports i2c_sda_b]
set_property SLEW FAST [get_ports i2c_scl_b]
set_property DRIVE 8 [get_ports i2c_sda_b]
set_property DRIVE 8 [get_ports i2c_scl_b]
set_property PACKAGE_PIN A33 [get_ports i2c_scl_b]
set_property PACKAGE_PIN B33 [get_ports i2c_sda_b]

# wr eeprom
set_property IOSTANDARD LVCMOS18 [get_ports eeprom_sda_b]
set_property IOSTANDARD LVCMOS18 [get_ports eeprom_scl_b]
set_property PACKAGE_PIN AW10 [get_ports eeprom_scl_b]
set_property PACKAGE_PIN AW11 [get_ports eeprom_sda_b]
set_property OFFCHIP_TERM NONE [get_ports eeprom_scl_b]
set_property OFFCHIP_TERM NONE [get_ports eeprom_sda_b]

# LEDs
set_property IOSTANDARD LVCMOS18 [get_ports {led_o[*]}]
set_property SLEW SLOW [get_ports {led_o[*]}]
set_property PACKAGE_PIN AV9 [get_ports {led_o[0]}]
set_property PACKAGE_PIN AW9 [get_ports {led_o[1]}]
set_property PACKAGE_PIN AU9 [get_ports {led_o[2]}]
set_property PACKAGE_PIN AU8 [get_ports {led_o[3]}]
set_property PACKAGE_PIN AP6 [get_ports {led_o[4]}]
set_property PACKAGE_PIN AU1 [get_ports {led_o[5]}]


# SFP GT
set_property PACKAGE_PIN AM2 [get_ports sfp_rxp_i]
set_property PACKAGE_PIN AM6 [get_ports sfp_txp_o]

# SFP i2c
set_property IOSTANDARD LVCMOS18 [get_ports sfp_sda_b]
set_property PACKAGE_PIN AT21 [get_ports sfp_sda_b]
set_property PACKAGE_PIN AU21 [get_ports sfp_scl_b]
set_property IOSTANDARD LVCMOS18 [get_ports sfp_scl_b]

# SFP misc
set_property PACKAGE_PIN K10 [get_ports sfp_los_i]
set_property IOSTANDARD LVCMOS33 [get_ports sfp_los_i]
set_property PACKAGE_PIN D12 [get_ports sfp_tx_disable_o]
set_property IOSTANDARD LVCMOS33 [get_ports sfp_tx_disable_o]
set_property PACKAGE_PIN G10 [get_ports sfp_det_i]
set_property IOSTANDARD LVCMOS33 [get_ports sfp_det_i]

# SFP clock
set_property PACKAGE_PIN AH10 [get_ports wr_clk_sfp_125m_p_i]

# WR clocks
set_property PACKAGE_PIN AP20 [get_ports wr_clk_helper_125m_p_i]
set_property IOSTANDARD LVDS [get_ports wr_clk_helper_125m_p_i]

set_property PACKAGE_PIN AN19 [get_ports wr_clk_main_125m_p_i]
set_property IOSTANDARD LVDS [get_ports wr_clk_main_125m_p_i]

# WR dac
set_property PACKAGE_PIN J14 [get_ports plldac_sclk_o]
set_property PACKAGE_PIN K13 [get_ports plldac_din_o]
set_property PACKAGE_PIN K14 [get_ports pll25dac_cs_n_o]
set_property PACKAGE_PIN L12 [get_ports pll20dac_cs_n_o]
set_property IOSTANDARD LVCMOS33 [get_ports pll20dac_cs_n_o]
set_property IOSTANDARD LVCMOS33 [get_ports pll25dac_cs_n_o]
set_property IOSTANDARD LVCMOS33 [get_ports plldac_din_o]
set_property IOSTANDARD LVCMOS33 [get_ports plldac_sclk_o]

# pps
set_property PACKAGE_PIN A11 [get_ports pps_p_o]
set_property IOSTANDARD LVCMOS33 [get_ports pps_p_o]
set_property OFFCHIP_TERM NONE [get_ports pps_p_o]

# config options
set_property BITSTREAM.CONFIG.OVERTEMPSHUTDOWN ENABLE [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
# set_property BITSTREAM.GENERAL.JTAG_SYSMON DISABLE [current_design]
set_property BITSTREAM.CONFIG.UNUSEDPIN PULLNONE [current_design]

#revert back to original instance
current_instance -quiet
