action = "synthesis"

syn_device = "xczu7cg"
syn_grade = "-1-e"
syn_package = "-ffvf1517"
syn_top = "spexi7u_top"
syn_project = "spexi7u"
syn_tool = "vivado"

target = "xilinx"

# For WR
board = 'pxie-fmc'

files = [ 'spexi7u.xdc' ]

modules = {
    'local': ['../../top/spexi7u'],
    "git" : [
        "https://ohwr.org/project/general-cores.git",
        "https://ohwr.org/project/wr-cores.git",
        "https://ohwr.org/project/urv-core.git",
    ],
    'system': ['xilinx', 'vhdl']
}
