action = "synthesis"

syn_device = "xczu4cg"
syn_grade = "-1-e"
syn_package = "-sfvc784"
syn_top = "wren_vme_top"
syn_project = "wren-vme"
syn_tool = "vivado"

target = "xilinx"

# For WR:
# do not use a predefined board
board = 'none'
# do not use a predefined platform
wrcore_platform=False

files = [
    'wren-vme.xdc', "wren-vme-io.xdc",
    # 'gencores_constraints.xdc',
    # 'ila_0.xci',
]

modules = {
    'local': ['../../top/vme'],
    "git" : [
        "https://ohwr.org/project/general-cores.git",
        "https://ohwr.org/project/wr-cores.git",
        "https://ohwr.org/project/urv-core.git",
        "https://ohwr.org/project/vme64x-core.git",
    ],
    'system': ['xilinx', 'vhdl']
}
